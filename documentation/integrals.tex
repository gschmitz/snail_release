

\chapter{Integral evaluation}

\section{Cartesian Gaussians}

 \begin{itemize}

   \item Integral evaluation usually works on the basis of primitive Cartesian Gaussians centered at $\pmb{A}$:
         \begin{equation}
           G_{ijk}(\pmb{r},a,\pmb{A}) = x_A^i y_A^j z_A^k \exp\left(-a r_A^2 \right), \quad
         \end{equation}

    \begin{itemize}
     \item The total angular momentum is $l=i+j=k \ge 0$
    \end{itemize}

   \item Cartesian Gaussian factorize in the Cartesian directions:
     \begin{equation}
       G_{ijk}(\pmb{r},a,\pmb{A}) = G_{i}(a,x_A) G_{i}(a,y_A) G_{i}(a,z_A)
     \end{equation}

     \begin{itemize}
       \item This is extremely helpful for disentangling parts in the integral evaluation
       \item \textbf{Note:} This does not hold for solid-harmonic Gaussians nor Slater-type orbitals
     \end{itemize}

    \item Cartesian Gaussians satisfy a simple recurrence relation:
      \begin{equation}
        x_A G_{i}(a,x_A) = G_{i+1}(a,x_A)
      \end{equation}

    \item Differentiation yields a linear combination of two Cartesian Gaussians:

      \begin{equation}
        \frac{\partial G_{i}(a,x_A)}{\partial A_x} = - \frac{\partial G_{i}(a,x_A)}{\partial x}
         = 2 a G_{i+1}(a,x_A) - i G_{i-1}(a,x_A)
      \end{equation}


 \end{itemize}


\section{What is a shell}

  \begin{itemize}

    \item In molecular integral evaluation we constitute a shell as all basis functions with same origin and exponent.

    \item Usually also the same angular momentum, but there are counter examples form the early days like sp-shells.

    \item If we also require the same angular momentum $l$ we have:
         \begin{align}
            (l+1) \cdot (l+2) / 2  & \quad\text{basis functions (Cartesian Gaussians)} \\
            2 \cdot l + 1          & \quad\text{basis functions (solid harmonics)}
         \end{align}

    \item Although, we will use Cartesian Gaussian due to their nice properties we want at an early stage transform to solid harmonics:

          \begin{equation}
            \tilde{G}_{lm}(\pmb{r}) = N_{lm}^S \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/2-v_m] + v_m}
                                     C_{tuv}^{lm} G_{2t+|m|-2(u+v),2(u+v),l-2t-|m|} (\pmb{r})
          \end{equation}

  \end{itemize}

  Why is the concept of a shell important?

  \begin{itemize}

    \item \textbf{Efficiency:} Basis function in a shell share common intermediates.

    \item \textbf{Example:} (pp|pp) shell $3 \cdot3 \cdot3 \cdot 3 \cdot 3 = 81$ integrals with common intermediates.

    \item Early integral codes based on Ref. \citenum{Huzinaga} calculated one integral at a time.

    \item Alml\"{o}f realized that there is gain when calculating integrals in batches (shells).

    \item Design your code that it exploits the shell structure!

    \item \textbf{Fun fact:} Today calculating the integrals in shell batches can be challenging for a GPU implementation
                             since memory transfer is then a bottle-neck. Some GPU codes perform shell splitting.

  \end{itemize}

  %{\footnotesize \textcolor{darkgreen}{[1] H. Taketa et al., \textit{J. Phys. Soc. Japan} \textbf{21}, 2313--2324 (1966)}}\\
  %{\footnotesize \textcolor{darkgreen}{[2] J. Alml\"{o}f, \textit{report 72–09. Technical report. (University of Stockholm Institute of Physics)}, (1972)}}\\


\section{Gaussian product rule}

  \begin{itemize}
    \item Well known, but worthy to re-state is the \textbf{Gaussian product rule}

    \item The product of two Gaussians is \textbf{again a Gaussian}, which center is on the line
          connecting the original Gaussians.
          \begin{equation}
            \exp{\left(-a x_A^2\right)} \exp{\left(-b x_b^2\right)} = \exp{\left(-\mu X_{AB}^2\right)} \exp{\left(-p x_p^2\right)}
          \end{equation}

  
       \begin{minipage}{.25\linewidth}
        \begin{center}
         \begin{tikzpicture}[scale=0.5,transform shape]
           \begin{axis}[every axis plot post/.append style={
             mark=none,domain=-2:3,samples=50,smooth},
               % All plots: from -2:2, 50 samples, smooth, no marks
             axis x line*=bottom, % no box around the plot, only x and y axis
             axis y line*=left, % the * suppresses the arrow tips
             enlargelimits=upper] % extend the axes a bit to the right and top
             \addplot {gauss(0,0.75)};
             \addplot {gauss(1,0.5)};
             \addplot {gaussproduct(0,0.75,1,0.5)};
           \end{axis}
           \end{tikzpicture}
        \end{center}
       \end{minipage}%
       \begin{minipage}{.75\linewidth}
          \begin{align}
            p      & = a + b  \\
            P_x    & = \frac{aA_x + bB_x}{p} \\
            X_{AB} & = A_x - B_x \\
            \mu    & = \frac{ab}{a+b}
          \end{align}
       \end{minipage}

     \item This simple relation helps us a lot:
        \begin{itemize}
          \item two-center integrals are reduced to one-center integrals.
          \item four-center integrals are reduced to two-center integrals.
        \end{itemize}

  \end{itemize}


\section{The overlap distribution}

  \begin{itemize}
    \item We define the \textbf{Gaussian overlap distribution}
          \begin{equation}
            \Omega_{ab} (x) = G_A(r,A) G_B(r,B)
          \end{equation}
          which factorizes as
          \begin{equation}
            \Omega_{ab}(r) = G_i(x,a,A_x)G_j(x,b,B_x) ... = \Omega_{ij}(x,a,b,A_x,B_x) ...
          \end{equation}
     \item Using the Gaussian product rule the two-center integral is reduced to a \textbf{one-center} integral
           \begin{equation}
             \int \Omega_{ij}(x) dx = K_{AB}^x \int x_A^i x_B^j \exp(-p x_P^2) dx, \quad K_{AB}^x = \exp(-\mu X_{AB}^2)
           \end{equation}

     \item Properties for differentiation and increasing angular momenta follow that of Cartesian Gaussians...

     \item In the following we try to express the integrals in terms of the overlap distribution.
  \end{itemize}


\section{Hermite Gaussians}

 \begin{itemize}
   \item There are also Hermite Gaussians:
   \begin{equation}
     \Lambda_{tuv}(\pmb{r},p,\pmb{P}) = \left(\frac{\partial}{\partial P_x}\right)^t
                                        \left(\frac{\partial}{\partial P_y}\right)^u
                                        \left(\frac{\partial}{\partial P_z}\right)^v
                                        \exp{\left( -p \pmb{r}_P^2 \right)}
   \end{equation}
   \begin{itemize}
     \item Like Cartesian Gaussian the factorize in the Cartesian direction:
   \end{itemize}

   \item They have nice integration properties
   \begin{equation}
      \int_{-\infty}^{\infty} \Lambda_{t}(x) dx = \delta_{t0} \sqrt{\frac{\pi}{p}}
   \end{equation}

   \item We can expand a product of Cartesian Gaussians in Hermite Gaussians.
   \begin{equation}
      G_i(x_A) G_j(x_B) = \sum_{t=0}^{i+j} E^{ij}_t \Lambda_t\left( x_P \right)
   \end{equation}

   \item And there exist recurrence relations to obtain expansion coefficients $E^{ij}_t$
   \begin{equation}
    E_t^{i+1,j} = \frac{1}{2p} E_{t-1}^{ij} + X_{PA} E_t^{ij} + \left(t + 1\right) E^{ij}_{t+1}
   \end{equation}
   \begin{equation}
    E_t^{i,j+1} = \frac{1}{2p} E_{t-1}^{ij} + X_{PB} E_t^{ij} + \left(t + 1\right) E^{ij}_{t+1}
   \end{equation}


 \end{itemize}


\section{Simple one electron integrals}

 \begin{itemize}

   \item Let's talk about overlap integrals
     \begin{equation}
       S_{ab} = \braket{G_a|G_b}
     \end{equation}
     with
     \begin{equation}
       G_{a} = G_{ikm}(\pmb{r},a,\pmb{A}) = G_i(x_A) G_k(y_A) G_m(z_A)
     \end{equation}
     \begin{equation}
       G_{b} = G_{jln}(\pmb{r},b,\pmb{B}) = G_j(x_B) G_l(y_B) G_n(z_B)
     \end{equation}

   \item Exploiting that we can expand the overlap distribution in Hermite Gaussians we get along one
         Cartesian direction:
     \begin{equation}
       S_{ij} = \int \Omega_{ij}(x) dx = \sum_{t=0}^{i+j} E^{ij}_t \int \Lambda_t\left( x_P \right)
              = \sum_{t=0}^{i+j} E^{ij}_t  \delta_{t0} \sqrt{\frac{\pi}{p}} = E^{ij}_0 \sqrt{\frac{\pi}{p}}
     \end{equation}

   \item Thus the total overlap integral is given as
     \begin{equation}
       S_{ab} = E^{ij}_0 E^{kl}_0 E^{mn}_0 \left(\frac{\pi}{p} \right)^{3/2}
     \end{equation}
 \end{itemize}


\section{Coulomb integrals}
  \begin{itemize}
    \item Let's now directly target \textbf{two-electron} repulsion integrals:
    \begin{equation}
      \braket{G_a(r_{1A})G_b(r_{1B})|r_{12}^{-1}|G_c(r_{2C})G_b(d_{2D})}
    \end{equation}

    \item Coulomb integrals are \textbf{not} separable in the Cartesian directions.

    \item But we will see how do reduce them to one-dimensional integrals on a finite interval.

    \item Let's first consider the one-electron spherical Coulomb integral:\\
       \begin{minipage}{.25\linewidth}
        \begin{center}
           \definecolor{ududff}{rgb}{0.30196078431372547,0.30196078431372547,1.}
           \definecolor{zzttqq}{rgb}{0.6,0.2,0.}
           \definecolor{cqcqcq}{rgb}{0.7529411764705882,0.7529411764705882,0.7529411764705882}
           \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5,transform shape]
           \clip(-1.9725211392664432,-3.5669593606994012) rectangle (5.991415750290285,2.608848302711567);
           \fill[line width=.5pt,color=zzttqq,fill=zzttqq,fill opacity=0.10000000149011612] (-1.18,-1.92) -- (1.52,0.6) -- (3.24,-1.7) -- cycle;
           \draw [line width=.5pt,fill=blue, opacity=0.2] (0.7,0.58) circle (1.6368261972488098cm);
           \draw [line width=.5pt,color=zzttqq] (1.52,0.6)-- (3.24,-1.7);
           \draw (0.3415284475104174, 0.2797724199166763)  node[anchor=north west] {$r$};
           \draw (2.6706043303053093,-0.12593757257017565) node[anchor=north west] {$r_C = |\mathbf{r}-\mathbf{r}_c|$};
           \draw (3.2566298750085405, 1.6471653575575476)  node[anchor=north west] {$\rho_p(\mathbf{r})$};
           \draw [->,line width=.5pt] (-1.18,-1.92) -- (1.52,0.6);
           \draw [->,line width=.5pt] (-1.18,-1.92) -- (3.24,-1.7);
           %\draw [->,line width=.5pt] (2.882,1.336) -- (1.034,1.424);  % Arrow to charge
           \draw [->, bend left = 20] (2.882,1.336) -- (1.034,1.424);
           \draw (-1.5367585547435278,-2.169513831022467) node[anchor=north west] {(0,0,0)};
           \draw (0.9275539922136483 ,-1.868987910661836) node[anchor=north west] {$\mathbf{C}$};
           \begin{scriptsize}
           \draw [fill=ududff] (2.882,1.336) circle (0.5pt);
           \end{scriptsize}
           \end{tikzpicture}
        \end{center}
       \end{minipage}%
       \begin{minipage}{.75\linewidth}
         \begin{equation}
            V_p = \int \frac{\exp(-p r_p^2)}{r_C} d\pmb{r}
         \end{equation}
       \end{minipage}
  \end{itemize}

  \begin{itemize}
     \item We don't like $r_C^{-1}$ in the equation. Using the Laplace transform
           \begin{equation}
             \frac{1}{r_C} = \frac{1}{\sqrt{\pi}} \int_{-\infty}^{\infty} \exp{ \left( -r_C^2 t^2 \right)} dt
           \end{equation}
     \item We get a four dimensional integral (so actually worse)
           \begin{equation}
             V_p = \frac{1}{\sqrt{\pi}} \int \exp(-p r_p^2) \textcolor{red}{ \int_{-\infty}^{\infty} \exp \left( -r_C^2 t^2 \right) dt}
           \end{equation}

     \item Using \textbf{again} the Gaussian product theorem we obtain
           \begin{equation}
             V_p = \frac{1}{\sqrt{\pi}} \int_{-\infty}^{\infty} \textcolor{red}{\int \exp\left( -(p+t^2)r_S^2 \right) d\pmb{r}}
                                        \exp \left( -\frac{pt^2}{p+t^2} R_{CP}^2 \right) dt
           \end{equation}

     \item Integration over $\pmb{r}$ and exploiting symmetry yields
           \begin{equation}
             V_p = \frac{2}{\sqrt{\pi}} \int_{0}^{\infty} \textcolor{red}{ \left( \frac{\pi}{p+t^2}\right)^2  } \exp \left( -\frac{pt^2}{p+t^2} R_{CP}^2 \right) dt
           \end{equation}


     \item Introducing some substitutions
           \begin{equation*}
             u^2 = \frac{t^2}{p+t^2} \rightarrow 1+pt^{-2} = u^{-2} \rightarrow p t^{-3} dt = u^{-3} du \rightarrow dt = p^{-1} \left( \frac{t^2}{u^2}  \right)^{3/2} du
           \end{equation*}

     \item We get a finite integration range
           \begin{equation}
             V_p = \frac{2}{\sqrt{\pi}} \int_{0}^{1} \exp \left( -p R_{CP}^2 u^2 \right) du = \frac{2\pi}{p} F_0 \left( p R_{CP}^2 \right)
           \end{equation}

     \item and introduced the \textbf{Boys function}
           \begin{equation}
             F_n(x) = \int_0^1 \exp \left( -x t^2\right) t^{2n} dt
           \end{equation}

  \end{itemize}


\section{The Boys function}

  %\includegraphics[width=1.10\textwidth]{images/BoysFunc.pdf}
  %\includegraphics[width=0.40\textwidth]{images/BoysTab.eps}

  \begin{itemize}

    \item \makebox[\linewidth][l]{The integrand is positive and therefore:}
          \begin{equation*}
            F_n(x) = \int_0^1 \exp \left( -x t^2\right) t^{2n} dt > 0
          \end{equation*}

    \item There are asymptotic formulas for large and small x:
          \begin{equation*}
             F_n(x) = \frac{1}{2n+1} + \sum^{\infty}_{k=1} \frac{(-x)^k}{k!(2n+2k+1)} \quad\textcolor{red}{(\text{small}\, x)}
          \end{equation*}
          \begin{equation*}
            F_n(x) \approx \int_0^\infty \exp \left( -x t^2\right) t^{2n} dt = \frac{(2n-1)!!}{2^{n+1}} \sqrt{\frac{\pi}{x^{2n+1}}} \quad\textcolor{red}{(\text{large}\, x)}
          \end{equation*}

     \item \makebox[\linewidth][l]{Up- and downwards recurrence relations. But only downward is numerical stable:}
         \begin{equation*}
            F_{n-1} = \frac{2x F_n(x)+ \exp(-x)}{2n-1}
          \end{equation*}

     \item An efficient implementation of the Boys function is crucial since it is called many many many times.

     \item The Boys function can be calculated efficiently by pre-tabulating $F_m(x_k)$ for a series of grid points $x_k$

     \item The Boys function can be computed with machine precision from a six-term Taylor expansion around $x_k$
           \begin{equation}
             \begin{split}
             F_n(x) = F_n(x_k+\Delta x) & = F_n(x_k) - F_{n+1}(x_k) \Delta x + \frac{1}{2} F_{n+2}(x_k)(\Delta x)^2 \\
                                       & - \frac{1}{6} F_{n+3}(x_k) (\Delta x)^3 + \frac{1}{24} F_{n+4}(x_k) (\Delta x)^4 \\
                                       & - \frac{1}{120} F_{n+5}(x_k) (\Delta x)^5 + \frac{1}{720} F_{n+6}(x_k) (\Delta x)^6
             \end{split}
           \end{equation}

  \end{itemize}


\section{Cartesian Coulomb integrals}

\begin{itemize}
  \item For two-center one-electron Coulomb integrals we have
        \begin{equation}
          V_{ab} = \braket{G_a|r_C^{-1}|G_b} = \int \frac{\Omega_{ab}(\pmb{r})}{r_C} d\pmb{r}
        \end{equation}

  \item We expand the overlap distribution in Hermite Gaussians and use our results
        \begin{equation}
          V_{ab} = \sum_{tuv} E_{tuv}^{ab} \textcolor{blue}{\int \frac{\Lambda_{tuv}(\pmb{r}_p)}{r_C} d\pmb{r}} \quad,
                                           \int \frac{\exp\left(-pr_p^2 \right)}{r_C} d\pmb{r}  = \frac{2\pi}{p} F_0(pR_{PC}^2)
        \end{equation}

  \item Changing the order of integration and differentiation yields:
        \begin{equation}
          \textcolor{blue}{
          \begin{split}
           \int \frac{\Lambda_{tuv}(\pmb{r}_p)}{r_C} d\pmb{r} & = \frac{\partial^{t+u+v}}{\partial P_x^t \partial P_y^u \partial P_z^v }
                                                                  \int \frac{\exp\left(-pr_p^2 \right)}{r_C} d\pmb{r} \\
            & = \frac{2\pi}{p} \frac{\partial^{t+u+v} F_0(pR_{PC}^2)}{\partial P_x^t \partial P_y^u \partial P_z^v }
              = \frac{2\pi}{p} R_{tuv}(p,\pmb{R}_{PC})
          \end{split}
          }
        \end{equation}


\end{itemize}


\section{Hermite Coulomb integrals}

\begin{itemize}

  \item We already introduced the Hermite Coulomb integrals:
        \begin{equation}
          R_{tuv} (p,\pmb{R}_{PC}) = \frac{\partial^{t+u+v}F_0(pR^2_{PC})}{\partial P_x^t \partial P_y^u \partial P_z^v}
        \end{equation}

  \item To evaluate them efficiently we define auxiliary Hermite Coulomb integrals
        \begin{equation}
          R^{\textcolor{blue}{n}}_{tuv} (p,\pmb{R}_{PC}) =  \textcolor{blue}{(-p)^n}  \frac{\partial^{t+u+v}F_0(pR^2_{PC})}{\partial P_x^t \partial P_y^u \partial P_z^v}
        \end{equation}


  \item We can set up recurrence relations for these integrals:
        \begin{equation}
          R_{t+1,u,v}^n = t R_{t-1,u,v}^{n+1} + X_{PC} R_{t,u,v}^{n+1}
        \end{equation}
        \begin{equation}
          R_{t,u+1,v}^n = u R_{t,u-1,v}^{n+1} + Y_{PC} R_{t,u,v}^{n+1}
        \end{equation}
        \begin{equation}
          R_{t,u,v+1}^n = v R_{t,u,v-1}^{n+1} + Z_{PC} R_{t,u,v}^{n+1}
        \end{equation}

  \item Calculate first $R^n_{000} = (-2p)^n F_n$ and use recurrence relations for the remaining.

\end{itemize}


\section{Two electron integrals}

 \begin{itemize}

   \item We took a closer look at the one-electron integrals. Along the same lines we can also derive expressions for
         two-electron integrals.


 \begin{equation}
   \begin{split}
   \left( G_a(1)G_b(1)| G_c(2) G_d(2) \right) & = \int \int \frac{\Omega_{ab}(1)\Omega_{cd}(2)}{r_{12}} d \pmb{r}_1 d\pmb{r}_2 \\
                                              & = \sum_{tuv} E^{ab}_{tuv} \sum_{\tau\nu\phi} E^{cd}_{\tau\nu\phi} \int \int \frac{\Lambda_{tuv}(1)\Lambda_{\tau\nu\phi}(2)}{r_{12}} d \pmb{r}_1 d\pmb{r}_2 \\
                                              & = \frac{2\pi^{5/2}}{pq\sqrt{p+q}}
                                                  \sum_{tuv} E^{ab}_{tuv} \sum_{\tau\nu\phi}  E^{cd}_{\tau\nu\phi}
                                                  (-1)^{\tau+\nu+\phi} \\
                                              & \quad\quad R_{t+\tau,u+\nu,v+\phi}\left(\alpha,\pmb{R}_{PQ} \right)
   \end{split}
 \end{equation}

    \item All we need are the $E$ coefficients and the $R$ integrals.\cite{MCMURCHIE1978218}

  \end{itemize}

  %{\footnotesize \textcolor{darkgreen}{[1] L. E. McMurchie and E. R. Davidson, \textit{   J. Comput. Phys.} \textbf{26}, 218--231 (1978)}}\\

\section{Pseudocode two-electron Coulomb integrals}

\begin{algorithm}[H]
 \begin{algorithmic}[1]
 \FORALL[\hfill $\mathcal O(p^2l^3)$]{ $ab$ shell pairs and set $p$, $\pmb{P}$, $E_{tuv}^{ab}$}
   \FORALL{ $cd$ shell pairs and set $q$, $\pmb{Q}$, $E_{\tau\nu\phi}^{cd}$}
     \STATE Set $\alpha$, $\pmb{R}_{PQ}$ for all primitive products between shells $ab$ and $cd$ \COMMENT{\hfill $\mathcal O(p^4)$}
     \STATE Calculate $F_n(\alpha,\pmb{R}_{PQ})$                                                 \COMMENT{\hfill $\mathcal O(p^4l)$}
     \STATE Calculate $R_{t+\tau,u+\nu,v+\phi}(\alpha,\pmb{R}_{PQ})$                             \COMMENT{\hfill $\mathcal O(p^4l^4)$}
     \STATE Transform $[tuv|cd] = \sum_{\tau\nu\phi} (-1)^{\tau+\nu+\phi}
                                 E^{cd}_{\tau\nu\phi}
                                 R_{t+\tau,u+\nu,v+\phi}\left(\alpha,\pmb{R}_{PQ} \right)$
                                                                          \COMMENT{\hfill \textcolor{red}{$\mathcal O(p^4l^8)$}}
     \STATE Contract primitives on Ket side $\rightarrow$ $[tuv|cd)$      \COMMENT{\hfill $\mathcal O(p^4l^5c)$}
     \STATE Transform $[ab|cd) = \sum_{tuv} E^{ab}_{tuv} [tuv|cd)$        \COMMENT{\hfill $\mathcal O(p^2l^7c^2)$}
     \STATE Contract all primitives on Bra side  $\rightarrow$ $(ab|cd)$  \COMMENT{\hfill $\mathcal O(p^2l^4c^3)$}
   \ENDFOR
 \ENDFOR
 \end{algorithmic}
 \caption{MMD integral evaluation.}
 \label{alg:mmd}

\end{algorithm}

%  {\footnotesize \textcolor{darkgreen}{[1] L. E. McMurchie and E. R. Davidson, \textit{   J. Comput. Phys.} \textbf{26}, 218--231 (1978)}}\\
%  {\footnotesize \textcolor{darkgreen}{[2] S. Reine et al., \textit{WIREs Comput Mol Sci} \textbf{2}, 290--303 (2012)}}




