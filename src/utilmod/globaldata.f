module globaldata

 !------------------------------------------------------------------+
 ! Contains global variables:
 ! ---------------------------
 !
 ! Try to keep the ammount of global variables as small as possible
 !
 !------------------------------------------------------------------+

 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  character(len=255) :: homedir

  public :: homedir


end module
