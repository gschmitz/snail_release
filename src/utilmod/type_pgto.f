module type_pgto

 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'


  type pgto_info

    real(kdp) :: origin(3) = (/ zero, zero, zero /)
    integer   :: la = 0
    integer   :: ma = 0
    integer   :: na = 0
 
    real(kdp) :: expo = zero
    real(kdp) :: norm = one 
    real(kdp) :: coef = zero

  end type pgto_info 

  public :: pgto_info,pgto_movecenter, pgto_normalize, pgto_overlap,&
            pgto_kinetic, pgto_nuclear, pgto_coulomb, pgto_multipole,&
            pgto_shift_origin

!------------------------------------------------------------------------------!
  contains
!------------------------------------------------------------------------------!

  subroutine pgto_movecenter(pgto,dx,dy,dz)
    implicit none

    include 'fortrankinds.h'

    real(kdp),       intent(in) :: dx,dy,dz
    type(pgto_info), intent(inout) :: pgto

    pgto%origin(1) = pgto%origin(1) + dx
    pgto%origin(2) = pgto%origin(2) + dy
    pgto%origin(3) = pgto%origin(3) + dz
  end subroutine


  subroutine pgto_normalize(pgto)
    implicit none

    include 'fortrankinds.h'
    include 'constants.h'

    type(pgto_info), intent(inout) :: pgto
 
    real(kdp) :: Sij

    Sij = pgto_overlap(pgto,pgto)

    pgto%norm = one / dsqrt(Sij)

  end subroutine

  function pgto_overlap(pgto,other) result(dret)
    implicit none

    include 'fortrankinds.h'

    ! constants:
    logical, parameter :: luseOS = .true.

    ! return value:
    real(kdp) :: dret

    type(pgto_info), intent(in) :: pgto,other

    real(kdp) :: overlap, os_overlap

    if (luseOS) then
      dret = os_overlap(pgto%expo,pgto%la,pgto%ma,pgto%na,pgto%origin, &
                        other%expo,other%la,other%ma,other%na,other%origin)
    else
      dret = overlap(pgto%expo,pgto%la,pgto%ma,pgto%na,pgto%origin, &
                     other%expo,other%la,other%ma,other%na,other%origin)    
    end if

    dret = dret * pgto%norm * other%norm

    return
  end function

  function pgto_kinetic(pgto,other) result(dret)
    implicit none

    include 'fortrankinds.h'

    ! constants:
    logical, parameter :: luseOS = .false.

    ! return value:
    real(kdp) :: dret
 
    ! input
    type(pgto_info), intent(in) :: pgto,other

    ! functions
    real(kdp) :: kinetic, os_kinetic

    if (luseOS) then
      dret = os_kinetic(pgto%expo,pgto%la,pgto%ma,pgto%na,pgto%origin,&
                        other%expo,other%la,other%ma,other%na,other%origin)
    else
      dret = kinetic(pgto%expo,pgto%la,pgto%ma,pgto%na,pgto%origin,&
                     other%expo,other%la,other%ma,other%na,other%origin) 
    end if

    dret = dret * pgto%norm * other%norm
    return
  end function

  function pgto_nuclear(pgto,other,xyz_nuc) result(dret)
    implicit none

    include 'fortrankinds.h'
    
    ! constants:
    logical, parameter :: luseOS = .true.

    ! return value
    real(kdp) :: dret

    ! input
    type(pgto_info), intent(in) :: pgto,other
    real(kdp),  intent(in) :: xyz_nuc(3)

    ! functions
    real(kdp) :: nuclear, os_nuclear

    if (luseOS) then
      dret =  os_nuclear(pgto%origin,pgto%norm,pgto%la,pgto%ma,pgto%na,pgto%expo, &
                         other%origin,other%norm,other%la,other%ma,other%na,other%expo,xyz_nuc)
    else
      dret =  nuclear(pgto%origin,pgto%norm,pgto%la,pgto%ma,pgto%na,pgto%expo, &
                      other%origin,other%norm,other%la,other%ma,other%na,other%expo,xyz_nuc) 
    end if

    return
  end function

  function pgto_coulomb(gA,gB,gC,gD) result(dret)
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dret

  
    type(pgto_info), intent(in) :: gA,gB,gC,gD

    real(kdp) :: coulomb_repulsion


    dret = coulomb_repulsion(gA%origin,gA%norm,gA%la,gA%ma,gA%na,gA%expo,&
                             gB%origin,gB%norm,gB%la,gB%ma,gB%na,gB%expo,&
                             gC%origin,gC%norm,gC%la,gC%ma,gC%na,gC%expo,&
                             gD%origin,gD%norm,gD%la,gD%ma,gD%na,gD%expo) 

    return
  end function

  function pgto_multipole(pgto,other,origin,lx,mx,nx) result(dret)
    implicit none

    include 'fortrankinds.h'

    ! return value:
    real(kdp) :: dret

    ! input
    type(pgto_info), intent(in) :: pgto,other
    real(kdp),       intent(in) :: origin(3)
    integer,         intent(in) :: lx,mx,nx

    ! local
    real(kdp) :: overlap, os_multipole

    dret = os_multipole(pgto%expo,pgto%la,pgto%ma,pgto%na,pgto%origin, &
                        other%expo,other%la,other%ma,other%na,&
                        other%origin,lx,mx,nx)

    dret = dret * pgto%norm * other%norm

    return
  end function

  subroutine pgto_shift_origin(pgto,shift)
    implicit none

    include 'fortrankinds.h'

    ! input/output
    type(pgto_info), intent(inout) :: pgto
    real(kdp),       intent(in)    :: shift(3)

    ! local
    integer :: iprim

    pgto%origin = pgto%origin + shift

  end subroutine


end module

 
