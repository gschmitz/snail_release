module type_atom


 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  type atom_info

    real(kdp) :: xyz(3) = (/zero,zero,zero/)
    real(kdp) :: charge = zero
    real(kdp) :: mass   = zero
    integer   :: atono  = 0
    integer   :: atoid  = 0
    
    character(3) :: elem = 'xxx'
  end type

  public :: atom_info,at_SetPosition,at_GetAtNum,at_GetAtMass,at_shift_origin

!------------------------------------------------------------------------------!
  contains
!------------------------------------------------------------------------------!

  subroutine at_SetPosition(atom,x,y,z)

    implicit none

    include 'fortrankinds.h'

    type(atom_info), intent(inout) :: atom
    real(kdp), intent(in) :: x,y,z

    atom%xyz(1) = x
    atom%xyz(2) = y
    atom%xyz(3) = z

  end subroutine

  function at_GetAtNum(sname) result(atno)

    implicit none

    ! return value
    integer :: atno

    ! input
    character(3), intent(in) :: sname
 
    ! init
    atno = -99 ! error code

    select case (sname(1:2))
      case ('h ','H ') ; atno = 1 
      case ('he','He') ; atno = 2
      case ('li','Li') ; atno = 3
      case ('be','Be') ; atno = 4
      case ('b ','B ') ; atno = 5
      case ('c ','C ') ; atno = 6
      case ('n ','N ') ; atno = 7
      case ('o ','O ') ; atno = 8
      case ('f ','F ') ; atno = 9
      case ('ne','Ne') ; atno = 10
      case ('na','Na') ; atno = 11
      case ('mg','Mg') ; atno = 12
      case ('al','Al') ; atno = 13
      case ('si','Si') ; atno = 14
      case ('p ','P ') ; atno = 15
      case ('s ','S ') ; atno = 16
      case ('cl','Cl') ; atno = 17
      case ('ar','Ar') ; atno = 18
      case ('k ','K ') ; atno = 19
      case ('ca','Ca') ; atno = 20
      case ('sc','Sc') ; atno = 21     
      case ('ti','Ti') ; atno = 22 
      case ('v' ,'V' ) ; atno = 23 
      case ('cr','Cr') ; atno = 24 
      case ('mn','Mn') ; atno = 25
      case ('fe','Fe') ; atno = 26 
      case ('co','Co') ; atno = 27 
      case ('ni','Ni') ; atno = 28 
      case ('cu','Cu') ; atno = 29 
      case ('zn','Zn') ; atno = 30
      case ('ga','Ga') ; atno = 31 
      case ('ge','Ge') ; atno = 32 
      case ('as','As') ; atno = 33 
      case ('se','Se') ; atno = 34 
      case ('br','Br') ; atno = 35
      case ('kr','Kr') ; atno = 36 
      case ('rb','Rb') ; atno = 37 
      case ('sr','Sr') ; atno = 38 
      case ('y' ,'Y' ) ; atno = 39 
      case ('zr','Zr') ; atno = 40
      case ('nb','Nb') ; atno = 41 
      case ('mo','Mo') ; atno = 42 
      case ('tc','Tc') ; atno = 43 
      case ('ru','Ru') ; atno = 44 
      case ('rh','Rh') ; atno = 45
      case ('pd','Pd') ; atno = 46 
      case ('ag','Ag') ; atno = 47 
      case ('cd','Cd') ; atno = 48 
      case ('in','In') ; atno = 49 
      case ('sn','Sn') ; atno = 50
      case ('sb','Sb') ; atno = 51 
      case ('te','Te') ; atno = 52 
      case ('i ','I ') ; atno = 53 
      case ('xe','Xe') ; atno = 54 
      case ('cs','Cs') ; atno = 55
      case ('ba','Ba') ; atno = 56 
      case ('la','La') ; atno = 57 
      case ('ce','Ce') ; atno = 58 
      case ('pr','Pr') ; atno = 59 
      case ('nd','Nd') ; atno = 60
      case ('pm','Pm') ; atno = 61 
      case ('sm','Sm') ; atno = 62 
      case ('eu','Eu') ; atno = 63 
      case ('gd','Gd') ; atno = 64 
      case ('tb','Tb') ; atno = 65
      case ('dy','Dy') ; atno = 66 
      case ('ho','Ho') ; atno = 67 
      case ('er','Er') ; atno = 68 
      case ('tm','Tm') ; atno = 69 
      case ('yb','Yb') ; atno = 70
      case ('lu','Lu') ; atno = 71 
      case ('hf','Hf') ; atno = 72 
      case ('ta','Ta') ; atno = 73 
      case ('w ','W ') ; atno = 74 
      case ('re','Re') ; atno = 75
      case ('os','Os') ; atno = 76 
      case ('ir','Ir') ; atno = 77 
      case ('pt','Pt') ; atno = 78 
      case ('au','Au') ; atno = 79 
      case ('hg','Hg') ; atno = 80
      case ('tl','Tl') ; atno = 81 
      case ('pb','Pb') ; atno = 82 
      case ('bi','Bi') ; atno = 83 
      case ('po','Po') ; atno = 84 
      case ('at','At') ; atno = 85
      case ('rn','Rn') ; atno = 86 
      case ('fr','Fr') ; atno = 87 
      case ('ra','Ra') ; atno = 88 
      case ('ac','Ac') ; atno = 89 
      case ('th','Th') ; atno = 90
      case ('pa','Pa') ; atno = 91  
      case ('u ','U ') ; atno = 92 
      case ('np','Np') ; atno = 93
      case ('pu','Pu') ; atno = 94 
      case ('am','Am') ; atno = 95  
      case ('cm','Cm') ; atno = 96 
      case ('bk','Bk') ; atno = 97 
      case ('cf','Cf') ; atno = 98 
      case ('es','Es') ; atno = 99 
      case ('fm','Fm') ; atno = 100 
      case ('md','Md') ; atno = 101
      case ('no','No') ; atno = 102
      case ('lr','Lr') ; atno = 103
      
      case default 
        write(6,*) 'Name ', sname
        call quit("Unsupported atom","at_GetAtNum")
    end select
 
  end function

  function at_GetAtMass(sname) result(atmass)

    implicit none

    ! return value
    real(kdp) :: atmass

    ! input
    character(3), intent(in) :: sname
 
    ! init
    atmass = -99.0 ! error code

    select case (sname(1:2))
      case ('h ','H ') ; atmass = 1.00797   
      case ('he','He') ; atmass = 4.0026 
      case ('li','Li') ; atmass =  6.939 
      case ('be','Be') ; atmass = 9.0122 
      case ('b ','B ') ; atmass = 10.811 
      case ('c ','C ') ; atmass = 12.01115
      case ('n ','N ') ; atmass = 14.0067
      case ('o ','O ') ; atmass = 15.9994
      case ('f ','F ') ; atmass = 18.9984
      case ('ne','Ne') ; atmass = 20.183 
      case ('na','Na') ; atmass = 22.9898
      case ('mg','Mg') ; atmass = 24.312 
      case ('al','Al') ; atmass = 26.9815
      case ('si','Si') ; atmass = 28.086 
      case ('p ','P ') ; atmass = 30.9738
      case ('s ','S ') ; atmass = 2.064  
      case ('cl','Cl') ; atmass = 35.453 
      case ('ar','Ar') ; atmass = 39.948 
      case ('k ','K ') ; atmass = 39.102 
      case ('ca','Ca') ; atmass = 40.08  
      case ('sc','Sc') ; atmass = 44.956     
      case ('ti','Ti') ; atmass = 47.90  
      case ('v' ,'V' ) ; atmass = 50.942 
      case ('cr','Cr') ; atmass = 51.996 
      case ('mn','Mn') ; atmass = 54.9380
      case ('fe','Fe') ; atmass = 55.85  
      case ('co','Co') ; atmass = 58.9332
      case ('ni','Ni') ; atmass = 58.71  
      case ('cu','Cu') ; atmass = 63.54  
      case ('zn','Zn') ; atmass = 65.37  
      case ('ga','Ga') ; atmass = 69.72  
      case ('ge','Ge') ; atmass = 72.59  
      case ('as','As') ; atmass = 74.9216
      case ('se','Se') ; atmass = 78.96  
      case ('br','Br') ; atmass = 79.909 
      case ('kr','Kr') ; atmass = 83.80  
      case ('rb','Rb') ; atmass = 85.47  
      case ('sr','Sr') ; atmass = 87.62  
      case ('y' ,'Y' ) ; atmass = 88.905 
      case ('zr','Zr') ; atmass = 91.22  
      case ('nb','Nb') ; atmass = 92.906 
      case ('mo','Mo') ; atmass = 95.94  
      case ('tc','Tc') ; atmass = 99.0   
      case ('ru','Ru') ; atmass = 101.07 
      case ('rh','Rh') ; atmass = 102.905
      case ('pd','Pd') ; atmass = 106.4  
      case ('ag','Ag') ; atmass = 107.87 
      case ('cd','Cd') ; atmass = 112.40 
      case ('in','In') ; atmass = 114.82 
      case ('sn','Sn') ; atmass = 118.69 
      case ('sb','Sb') ; atmass = 121.75 
      case ('te','Te') ; atmass = 127.60 
      case ('i ','I ') ; atmass = 126.904
      case ('xe','Xe') ; atmass = 131.30 
      case ('cs','Cs') ; atmass = 132.905
      case ('ba','Ba') ; atmass = 137.33 
      case ('la','La') ; atmass = 138.91 
      case ('ce','Ce') ; atmass = 140.115
      case ('pr','Pr') ; atmass = 140.908
      case ('nd','Nd') ; atmass = 144.24 
      case ('pm','Pm') ; atmass = 146.92 
      case ('sm','Sm') ; atmass = 150.36 
      case ('eu','Eu') ; atmass = 151.965
      case ('gd','Gd') ; atmass = 157.25 
      case ('tb','Tb') ; atmass = 158.925
      case ('dy','Dy') ; atmass = 162.50 
      case ('ho','Ho') ; atmass = 164.93 
      case ('er','Er') ; atmass = 167.26 
      case ('tm','Tm') ; atmass = 168.93 
      case ('yb','Yb') ; atmass = 173.04 
      case ('lu','Lu') ; atmass = 174.97 
      case ('hf','Hf') ; atmass = 178.49 
      case ('ta','Ta') ; atmass = 180.95 
      case ('w ','W ') ; atmass = 183.85 
      case ('re','Re') ; atmass = 186.21 
      case ('os','Os') ; atmass = 190.2  
      case ('ir','Ir') ; atmass = 192.22 
      case ('pt','Pt') ; atmass = 195.08 
      case ('au','Au') ; atmass = 196.97 
      case ('hg','Hg') ; atmass = 200.59 
      case ('tl','Tl') ; atmass = 204.38 
      case ('pb','Pb') ; atmass = 207.2 
      case ('bi','Bi') ; atmass = 208.980
      case ('po','Po') ; atmass = 208.98 
      case ('at','At') ; atmass = 209.99 
      case ('rn','Rn') ; atmass = 222.02 
      case ('fr','Fr') ; atmass = 223.02 
      case ('ra','Ra') ; atmass = 226.03 
      case ('ac','Ac') ; atmass = 227.03 
      case ('th','Th') ; atmass = 232.04 
      case ('pa','Pa') ; atmass = 231.04  
      case ('u ','U ') ; atmass = 238.03 
      case ('np','Np') ; atmass = 37.0482
      case ('pu','Pu') ; atmass = 244    
      case ('am','Am') ; atmass = 243     
      case ('cm','Cm') ; atmass = 247    
      case ('bk','Bk') ; atmass = 247    
      case ('cf','Cf') ; atmass = 251    
      case ('es','Es') ; atmass = 252    
      case ('fm','Fm') ; atmass = 257     
      case ('md','Md') ; atmass = 258    
      case ('no','No') ; atmass = 259    
      case ('lr','Lr') ; atmass = 262
      
      case default 
        write(6,*) 'Name ', sname
        call quit("Unsupported atom","at_GetAtMass")
    end select
 
  end function

  subroutine at_shift_origin(atom,shift)
    implicit none

    include 'fortrankinds.h'

    ! input/output
    type(atom_info), intent(inout) :: atom
    real(kdp),       intent(in)    :: shift(3)
    
    atom%xyz = atom%xyz + shift

  end subroutine

end module
