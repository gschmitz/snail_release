module type_int4idx
!------------------------------------------------------------------------------+
! Purpose:
! ---------
!
! Module with object definitions to handle 4 index integrals
!
!  + Use of compressed/uncompressed storage
!
!------------------------------------------------------------------------------+

  implicit none

   private
 
  include 'fortrankinds.h'
  include 'numbers.h'

  type int4idx

    logical :: lcompressed = .false.

    real(kdp), pointer :: dintfull(:,:,:,:) => null()
    real(kdp), pointer :: dintcomp(:) => null()

    integer :: nbf = 0
    integer :: nsize = 0

  end type

  public :: int4idx, allocate_int4idx, deallocate_int4idx, int4idx_getind, &
            int4idx_getvalue, int4idx_setvalue
!------------------------------------------------------------------------------!
  contains
!------------------------------------------------------------------------------!

  subroutine allocate_int4idx(twoe,lcompress,nbf)

    implicit none

    include 'fortrankinds.h'

    type(int4idx), intent(inout) :: twoe
    logical,       intent(in)    :: lcompress
    integer,       intent(in)    :: nbf
   
    integer :: ndimtwoe 

    twoe%nbf = nbf
    twoe%lcompressed = lcompress

    if (lcompress) then
      ndimtwoe = int4idx_getind(nbf, nbf, nbf, nbf) 
      allocate(twoe%dintcomp(ndimtwoe))    
      twoe%nsize = ndimtwoe
      twoe%dintcomp = 0.0d0
    else
      allocate(twoe%dintfull(nbf,nbf,nbf,nbf))
      twoe%nsize = nbf*nbf*nbf*nbf
      twoe%dintfull = 0.0d0
    end if
 
  end subroutine

  subroutine deallocate_int4idx(twoe)
    implicit none

    type(int4idx), intent(inout) :: twoe

    if (twoe%lcompressed) then
      deallocate(twoe%dintcomp)
      twoe%dintcomp => null()
    else
      deallocate(twoe%dintfull)
      twoe%dintfull => null()
    end if

    twoe%nbf = 0
    twoe%lcompressed = .false.
    twoe%nsize = 0
  end subroutine

  function int4idx_getind(i0,j0,k0,l0) result (ind) 
   implicit none

   integer :: ind 

   integer, intent(in) :: i0,j0,k0,l0

   integer :: ij,kl,i,j,k,l

   i = i0-1; j = j0-1; k = k0-1; l = l0-1

   if (i .lt. j) then
     call swap(i,j)
   end if
   
   if (k .lt. l) then
     call swap(k,l)
   end if

   ij =  i * (i + 1) / 2 + j 
   kl =  k * (k + 1) / 2 + l 

   if (ij .lt. kl) then
     call swap(ij,kl)
   end if

   ind = ij * (ij + 1) / 2 + kl + 1 

   return
 end function

 subroutine int4idx_setvalue(twoe,i0,j0,k0,l0,dval,use_8foldsym)

   implicit none

   include 'fortrankinds.h'

   ! input
   type(int4idx), intent(inout) :: twoe
   real(kdp),     intent(in)    :: dval
   integer,       intent(in)    :: i0,j0,k0,l0
   
   logical, optional, intent(in) :: use_8foldsym

   ! local
   integer :: ind
   logical :: use_sym = .false.

   if (present(use_8foldsym)) use_sym = use_8foldsym

   if (twoe%lcompressed) then
      ind = int4idx_getind(i0,j0,k0,l0)
      twoe%dintcomp(ind) = dval
   else
      twoe%dintfull(i0,j0,k0,l0) = dval

      if (use_sym) then
         twoe%dintfull(j0,i0,k0,l0) = dval 
         twoe%dintfull(i0,j0,l0,k0) = dval 
         twoe%dintfull(j0,i0,l0,k0) = dval 
   
         twoe%dintfull(k0,l0,i0,j0) = dval
         twoe%dintfull(l0,k0,i0,j0) = dval
         twoe%dintfull(k0,l0,j0,i0) = dval
         twoe%dintfull(l0,k0,j0,i0) = dval
      end if
   end if

 end subroutine

 function int4idx_getvalue(twoe,i0,j0,k0,l0) result(dval)

   implicit none

   include 'fortrankinds.h'

   ! output
   real(kdp) :: dval
  
   ! input
   type(int4idx), intent(inout) :: twoe
   integer,       intent(in)    :: i0,j0,k0,l0
   
   ! local
   integer :: ind

   if (twoe%lcompressed) then
      ind  = int4idx_getind(i0,j0,k0,l0)
      dval = twoe%dintcomp(ind) 
   else
      dval = twoe%dintfull(i0,j0,k0,l0) 
   end if

   return
 end function

end module
