module sysmod

   use, intrinsic  :: iso_c_binding

   implicit none

   private

   interface !to function: int gethostname(char *name, size_t namelen);
      integer(c_int) function gethostname(name, namelen) bind(c)
         use, intrinsic  :: iso_c_binding, only: c_char, c_int, c_size_t
         integer(c_size_t), value, intent(in) :: namelen
         character(len=1,kind=c_char), dimension(namelen),  intent(inout) ::  name
      end function gethostname
   end interface

   public :: hostname

!--------------------------------------------------------------------------------- 
 contains
!--------------------------------------------------------------------------------- 

   ! convert c_string to f_string
   pure function c2fstring(c_string) result(f_string)

      use, intrinsic :: iso_c_binding, only: c_char, c_null_char

      ! input
      character(kind=c_char,len=1), intent(in) :: c_string(:)

      ! local
      character(len=:), allocatable :: f_string
      integer :: idx, nlen

      idx = 1
      do
        if (c_string(idx) == c_null_char) exit
        idx = idx + 1
      end do
      nlen = idx - 1  ! exclude c_null_char

      allocate(character(len=nlen) :: f_string)
      f_string = transfer(c_string(1:nlen), f_string)

   end function c2fstring
 
   function hostname() result(hname)

     ! constants
     integer,parameter :: HOST_NAME_MAX = 255

     ! local
     integer(c_int)    :: stat
     integer(c_size_t) :: lenstr

     character(kind=c_char,len=1),dimension(HOST_NAME_MAX) :: cstr_hostname
     character(len=:),allocatable :: hname

     lenstr = HOST_NAME_MAX
     stat   = gethostname(cstr_hostname, lenstr)
     hname  = c2fstring(cstr_hostname)

   end function 

end module
