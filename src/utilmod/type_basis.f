module type_basis

  use type_shell

  implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'


  type basis_info

    integer :: ncbf = 0   ! number of cartesian basis functions
    integer :: nsbf = 0   ! number of solid harmonic basis functions
    integer :: nbf  = 0   ! number of basis functions (either equals ncbf or nsbf)
    integer :: nshell = 0 ! number of shells in basis set

    integer :: mxprim = 0
    integer :: mxcart = 0
  
    integer :: lmax = 0

    integer :: natoms

    type(shell_info), pointer :: shells(:)  ! info on shells
   
    logical :: have_shells = .false.
  end type

  public :: basis_info,allocate_basis,deallocate_basis,print_BasisInfo, basis_shift_origin, copy_basis

!-----------------------------------------------------------------
  contains
!-----------------------------------------------------------------

  subroutine allocate_basis(basis,lmax,ncbf,nsbf,nshell,natoms,mxprim,mxcart)

    use type_shell

    implicit none
 
    include 'fortrankinds.h'

     integer, intent(in)    :: ncbf,nsbf,nshell,natoms
     integer, intent(in)    :: mxprim,mxcart,lmax

     type(basis_info), intent(inout) :: basis

     allocate(basis%shells(nshell))

     basis%ncbf = ncbf
     basis%nsbf = nsbf
     basis%nbf  = ncbf ! For the moment we only support cartesian basis functions
     basis%nshell = nshell
     basis%have_shells = .true.

     basis%natoms = natoms

     basis%mxcart = mxcart
     basis%mxprim = mxprim

     basis%lmax = lmax

  end subroutine


  subroutine deallocate_basis(basis)
    implicit none

    type(basis_info), intent(inout)   :: basis

    integer :: ishl,nshl

    if (basis%have_shells) then
      nshl = basis%nshell

      do ishl = 1, nshl
        call deallocate_shell(basis%shells(ishl))
      end do

      deallocate(basis%shells)
      basis%shells => null()
      basis%have_shells = .false.
      basis%ncbf = 0
      basis%nsbf = 0
      basis%nbf  = 0
      basis%mxcart = 0
      basis%mxprim = 0
      basis%lmax = 0
    end if

  end subroutine

  subroutine copy_basis(basis,other)

    use type_shell

    implicit none
 
    include 'fortrankinds.h'

     type(basis_info), intent(inout) :: basis
     type(basis_info), intent(in)    :: other 

     ! local
     integer :: ishl

     if (basis%have_shells) deallocate(basis%shells)

     allocate(basis%shells(other%nshell))

     basis%ncbf = other%ncbf
     basis%nsbf = other%nsbf
     basis%nbf  = other%ncbf ! For the moment we only support cartesian basis functions
     basis%nshell = other%nshell
     basis%have_shells = .true.

     basis%natoms = other%natoms

     basis%mxcart = other%mxcart
     basis%mxprim = other%mxprim

     basis%lmax = other%lmax

     do ishl = 1, basis%nshell
       call copy_shell(other%shells(ishl),basis%shells(ishl))
     end do

  end subroutine

  subroutine print_BasisInfo(basis)
    implicit none

    ! input
    type(basis_info), intent(in)   :: basis

    ! local
    integer           :: iat, ishl, nbf, ncart, nprim, nprim_all
    integer           :: iang, nfun, ndig, iten, idig, ind, irun 
    integer, target   :: nfunc(0:basis%lmax),nfunp(0:basis%lmax)
    integer, pointer  :: no_of_func(:)
    character(len=3)  :: satom
    character(len=10) :: basisname
    character(len=20) :: descr, gtotype

    gtotype = "spdfghijklmnopqrstuwvxyz"

    if (basis%have_shells) then

      write(6,'(5x,a)'),'atom    basis       contr.   prim.'
      write(6,'(5x,a)'), repeat('-',66)

      nprim_all = 0
      nbf = 0

      do iat = 1, basis%natoms
        ! Loop over all shells (for this atom)
        ncart    = 0
        nprim    = 0
        nfunc(:) = 0
        nfunp(:) = 0
        do ishl = 1, basis%nshell
          if (basis%shells(ishl)%iatom == iat) then
            satom     = basis%shells(ishl)%atom
            basisname = basis%shells(ishl)%basisname
            ncart = ncart + basis%shells(ishl)%ncart
            nprim = nprim + basis%shells(ishl)%nprim * basis%shells(ishl)%ncart

            ! count s,p,d,... functions
            nfunc(basis%shells(ishl)%angm) = nfunc(basis%shells(ishl)%angm) + 1
            nfunp(basis%shells(ishl)%angm) = nfunp(basis%shells(ishl)%angm) + basis%shells(ishl)%nprim 
          end if
        end do
 
        descr = ''
        ind = 1
        descr(ind:ind) = '['
        do irun = 1, 2
          if (irun == 1) no_of_func => nfunc
          if (irun == 2) no_of_func => nfunp

          do iang = 0, basis%lmax

            if (no_of_func(iang) > 0) then
              nfun = no_of_func(iang)
              ndig = log10(real(nfun,kdp)) + 1
              iten = 10**ndig
              do idig = 1, ndig
                iten = iten/10
                ind = ind + 1
                descr(ind:ind) = char(ichar("0") + int(nfun/iten)) 
                nfun = mod(nfun,iten)
              end do
              ind = ind + 1
              descr(ind:ind) = gtotype(iang+1:iang+1) 
            end if
          end do
          if (irun == 1) then
            ind = ind + 1
            descr(ind:ind) = "|" 
          end if
        end do
        ind = ind + 1
        descr(ind:ind) = ']'

        write(6,'(5x,a3,3x,a,2x,i5,4x,i5,6x,a)'), satom, basisname, ncart, nprim, descr(1:ind)
        nbf       = nbf + ncart
        nprim_all = nprim_all + nprim
      end do
 

      write(6,'(5x,a)'), repeat('-',66)
      write(6,'(5x,a,13x,i5,4x,i5)'), 'total', nbf, nprim_all

    end if

  end subroutine

  subroutine basis_shift_origin(basis,iatom,shift)
    implicit none

    include 'fortrankinds.h'

    ! input/output
    type(basis_info), intent(inout) :: basis
    integer,          intent(in)    :: iatom
    real(kdp),        intent(in)    :: shift(3)

    ! local
    integer :: ishl

    do ishl = 1, basis%nshell
      if (iatom == basis%shells(ishl)%iatom) then
        call shell_shift_origin(basis%shells(ishl),shift)
      end if
    end do 


  end subroutine


end module



