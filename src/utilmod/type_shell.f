module type_shell

  use type_cgto
  use type_atom

  implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'


  type shell_info
    real(kdp) :: origin(3) = (/ zero, zero, zero /)

    integer :: ncart = 0
    integer :: nharm = 0
    integer :: angm = 0
    integer :: nprim = 0  

    integer :: ibfstart  = -1
    integer :: ibfend    = -1
    integer :: icbfstart = -1
    integer :: icbfend   = -1
    integer :: isbfstart = -1
    integer :: isbfend   = -1

    integer :: iatom = -1

    character(len=3) :: atom = "XXX"
 
    type(cgto_info), pointer :: gtos(:)
    logical :: have_gtos = .false.

    type(atom_info)   :: atominfo
    character(len=10) :: basisname
  end type

  public :: shell_info,allocate_shell,deallocate_shell,copy_shell,shell_getind,&
            shell_getcartcomp,shell_swap,sort_shells,&
            shell_EvaluateDensityOnGrid,shell_EvaluateMoOnGrid,shell_EvaluateMEPOnGrid,&
            shell_shift_origin

!-----------------------------------------------------------------
  contains
!-----------------------------------------------------------------

  subroutine allocate_shell(shell,angm,iat,atom,atominfo,basisname,exps,coefs,origin,nbf,nsbf,nprim)

    use type_cgto
    use type_atom

    implicit none
 
    include 'fortrankinds.h'

     integer, intent(in)    :: nprim

     integer, intent(inout) :: nbf,nsbf

     type(shell_info), intent(inout) :: shell

     integer,           intent(in) :: angm
     integer,           intent(in) :: iat
     character(len=3),  intent(in) :: atom
     character(len=10), intent(in) :: basisname
     real(kdp),         intent(in) :: exps(nprim),coefs(nprim),&
                                      origin(3)

     type(atom_info),   intent(in) :: atominfo

     integer :: m_ncart
     integer :: m_nharm
     integer :: nx,ny,nz
     integer :: iprim,i,j,icart

     ! calculate number of needed cartesian gaussians
     m_ncart = (angm + 1) * (angm + 2) / 2
     ! calculate number of solid harmonics (needed later)
     m_nharm = 2*angm + 1

     shell%ncart = m_ncart
     shell%nharm = m_nharm
     shell%angm  = angm

     shell%nprim = nprim

     allocate(shell%gtos(m_ncart))

     shell%icbfstart = nbf + 1         ! index of first basis function in this shell (cartesian gaussians)
     shell%icbfend   = nbf + m_ncart   ! index of last basis function in this shell (cartesian gaussians)

     shell%isbfstart = nsbf + 1        ! index of first basis function in this shell (solid harmonics)
     shell%isbfend   = nsbf + m_nharm  ! index of last basis function in this shell (solid harmonics)

     shell%ibfstart = shell%icbfstart  ! index of first basis function in this shell (either cartesian or solid harmonic)
     shell%ibfend   = shell%icbfend    ! index of last basis function in this shell (either cartesian or solid harmonic)

     shell%iatom    = iat
     shell%atom     = atom
     shell%atominfo = atominfo

     shell%basisname = basisname

     shell%origin = origin

     !-------------------------------------------------------------------------------------------+
     ! Add cartesian basis function to shell the order is like
     !
     !   x^2, xy, xz, y^2, yz, z^2,...
     !   --------------------------  
     !    1   2   3    4    5   6
     !
     ! In order to get the corresponding index of lets say xz simple use shell_getind(1,0,1)
     !
     !-------------------------------------------------------------------------------------------+

     icart = 0
     do i = 0, angm  
       nx = angm - i
       do j = 0, i    
         ny = i - j
         nz = j

         ! Add CGTO
         icart = icart + 1
         call allocate_cgto(shell%gtos(icart),nprim)  
         shell%gtos(icart)%origin = origin
         shell%gtos(icart)%la = nx; shell%gtos(icart)%ma = ny; shell%gtos(icart)%na = nz 

         nbf = nbf + 1

         do iprim = 1, nprim
           call cgto_SetPrimitive(shell%gtos(icart),iprim,exps(iprim),coefs(iprim))
         end do

         ! normalize
         call cgto_normalize(shell%gtos(icart))

         shell%have_gtos = .true.
       end do
     end do

  end subroutine


  subroutine deallocate_shell(shell)

    implicit none

    include 'numbers.h'

    type(shell_info), intent(inout)   :: shell

    integer :: icart,ncart

    if (shell%have_gtos) then
      ncart = shell%ncart

      do icart = 1, ncart 
        call deallocate_cgto(shell%gtos(icart))
      end do

      deallocate(shell%gtos)
      shell%gtos => null()
      shell%have_gtos = .false.
      shell%nprim = 0
    end if

    shell%angm  = 0
    shell%ncart = 0
    shell%nharm = 0  
    shell%ibfstart  = -1
    shell%ibfend    = -1
    shell%isbfstart = -1
    shell%isbfend   = -1
    shell%iatom     = -1
    shell%origin    = (/ zero, zero, zero /)
    shell%nprim     = 0
  end subroutine

  subroutine copy_shell(shell,other)
    implicit none

    ! input/output
    type(shell_info), intent(inout) :: shell
    type(shell_info), intent(inout) :: other 

    ! local
    integer :: icart

    ! destroy old object
    call deallocate_shell(other)

    ! copy information
    other%origin    = shell%origin 
    other%ncart     = shell%ncart 
    other%angm      = shell%angm  
    other%nharm     = shell%nharm    
    other%nprim     = shell%nprim
    other%ibfstart  = shell%ibfstart  
    other%ibfend    = shell%ibfend    
    other%isbfstart = shell%isbfstart 
    other%isbfend   = shell%isbfend   
    other%iatom     = shell%iatom     
    other%have_gtos = shell%have_gtos
    other%atom      = shell%atom
    other%basisname = shell%basisname
    other%atominfo  = shell%atominfo
 
   allocate(other%gtos(shell%ncart))
 
   do icart = 1, other%ncart
     call copy_cgto(shell%gtos(icart),other%gtos(icart))
   end do 

  end subroutine

  subroutine shell_swap(shell,other)
   implicit none

   ! input/output
   type(shell_info), intent(inout) :: shell
   type(shell_info), intent(inout) :: other

   ! local
   type(shell_info) :: temp

   call copy_shell(shell,temp)
   call copy_shell(other,shell)
   call copy_shell(temp,other)

  end subroutine

  function shell_getind(nx,ny,nz) result(ind)
   implicit none

   integer :: ind

   integer, intent(in) :: nx,ny,nz
   integer :: ii,jj

   ii = ny + nz
   jj = nz

   !  return index in one dimensional array
   ind = ii * (ii + 1) / 2 + jj + 1
   return
 end function

 subroutine shell_getcartcomp(cartcomp,angm)


   implicit none
 
   include 'fortrankinds.h'

    integer, intent(in)  :: angm

    integer, intent(out) :: cartcomp(3,(angm+1)*(angm+2)/2)


    ! local
    integer :: ncart
    integer :: nx,ny,nz
    integer :: i,j,icart

    ! calculate number of needed cartesian gaussians
    ncart = (angm + 1) * (angm + 2) / 2

    icart = 0
    do i = 0, angm  
      nx = angm - i
      do j = 0, i    
        ny = i - j
        nz = j

        icart = icart + 1

        cartcomp(1,icart) = nx
        cartcomp(2,icart) = ny
        cartcomp(3,icart) = nz
      end do
    end do

 end subroutine

 subroutine sort_shells(shells,nshell)

   implicit none

   ! dimension
   integer, intent(in) :: nshell

   ! input/output
   type(shell_info), intent(inout) :: shells(nshell)

   ! local
   integer :: istart,ibottom,istr

   !
   ! Sort via heap sort
   !
   istr = nshell / 2
   do istart = istr, 1, -1
     call sort_shiftdown(istart,nshell,shells,nshell)
   end do

   do ibottom = nshell, 2, -1
     call shell_swap(shells(1),shells(ibottom))
     call sort_shiftdown(1,ibottom,shells,nshell)
   end do

 end subroutine

 subroutine sort_shiftdown(istart,ibottom,shells,nshell)

   implicit none

   ! dimensions
   integer, intent(in) :: nshell
 
   ! input/output
   type(shell_info), intent(inout) :: shells(nshell)

   ! input
   integer, intent(in) :: istart,ibottom

   ! local
   integer :: ichild, iroot

   iroot = istart
   do while(iroot*2 < ibottom)
     ichild = iroot*2
 
     if(ichild + 1 < ibottom) then
       if (shells(ichild)%angm  < shells(ichild+1)%angm) ichild = ichild + 1
     end if
 
     if(shells(iroot)%angm < shells(ichild)%angm) then
       call shell_swap(shells(ichild),shells(iroot))
       iroot = ichild
     else
       return
     end if  
   end do      

 end subroutine

 subroutine shell_EvaluateDensityOnGrid(shells,orbs,icubfil,xyz,at,nshell,nbf,natoms,nocc,nspin)

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   ! grid spacing for cube file
   real(kdp), parameter :: step = 0.4d0

   ! dimensions
   integer, intent(in) :: nspin
   integer, intent(in) :: nshell, natoms, nocc(nspin), nbf
 
   ! input
   type(shell_info), intent(in) :: shells(nshell)
   real(kdp),        intent(in) :: orbs(nbf,nbf),xyz(3,natoms)
   integer,          intent(in) :: icubfil,at(natoms)

   ! local
   integer   :: ishl,jshl,iprim,jprim,nzstp,nystp,nxstp
   integer   :: ibf,jbf,kbf,icart,jcart,iat,langa,langb
   integer   :: ix,iy,iz,jx,jy,jz
   integer   :: i,j,k,cnt

   real(kdp) :: px,py,pz,mx,my,mz,zinc,yinc,xinc
   real(kdp) :: dx1,dx2,dy1,dy2,dz1,dz2,dxx1,dxx2,dyy1,dyy2,dzz1,dzz2,r1xy,r2xy
   real(kdp) :: dtemp,dfac,f1,f2,gridp(3),ar1,ar2,r1,r2

   real(kdp), allocatable :: pointval(:,:,:),dens(:,:)
   integer,   allocatable :: cartcompi(:,:),cartcompj(:,:)
  
   ! Current limitations
   if (nspin /= 1) call quit("Can evaluate density only for RHF case!","type_shell")
 
   ! get Grid boundaries
   px = maxval(xyz(1,1:natoms))+3.0
   py = maxval(xyz(2,1:natoms))+3.0
   pz = maxval(xyz(3,1:natoms))+3.0
   mx = minval(xyz(1,1:natoms))-3.0
   my = minval(xyz(2,1:natoms))-3.0
   mz = minval(xyz(3,1:natoms))-3.0

   ! calculate step sizes
   nxstp = floor((abs(px)+abs(mx))/step)
   xinc  = (abs(px)+abs(mx))/nxstp
   nystp = floor((abs(py)+abs(my))/step)
   yinc  = (abs(py)+abs(my))/nystp
   nzstp = floor((abs(pz)+abs(mz))/step)
   zinc  = (abs(pz)+abs(mz))/nzstp
 
   !
   ! Calculate density
   !
   allocate(dens(nbf,nbf))

   do jbf = 1, nbf
     do ibf = 1, nbf
       dens(ibf,jbf) = zero
       do kbf = 1, nocc(1)
         dens(ibf,jbf) = dens(ibf,jbf) + two * orbs(ibf,kbf) * orbs(jbf,kbf) 
       end do
     end do
   end do

   !
   ! Writing Header
   !
   call cube_WriteHeader(0,1,icubfil,xyz,xinc,yinc,zinc,     &
                         at,nxstp,nystp,nzstp,mx,my,mz,natoms)

   allocate(pointval(0:nzstp,0:nystp,0:nxstp))

   pointval = zero

   do ishl = 1, nshell
     langa = shells(ishl)%angm

     do jshl = 1, nshell
       langb = shells(jshl)%angm

       allocate(cartcompi(3,shells(ishl)%ncart),&
                cartcompj(3,shells(jshl)%ncart) )

       call shell_getcartcomp(cartcompi,langa)
       call shell_getcartcomp(cartcompj,langb)

       do icart = 1, shells(ishl)%ncart
         ix = cartcompi(1,icart) 
         iy = cartcompi(2,icart) 
         iz = cartcompi(3,icart) 
         ibf = shells(ishl)%ibfstart + icart - 1
 
         do jcart = 1, shells(jshl)%ncart
           jx = cartcompj(1,jcart) 
           jy = cartcompj(2,jcart) 
           jz = cartcompj(3,jcart) 
           jbf = shells(jshl)%ibfstart + jcart - 1

           do iprim = 1, shells(ishl)%nprim
             dtemp = dens(jbf,ibf) * shells(ishl)%gtos(icart)%coefs(iprim) * shells(ishl)%gtos(icart)%norms(iprim)

             do jprim = 1, shells(jshl)%nprim
               dfac = dtemp * shells(jshl)%gtos(jcart)%coefs(jprim) * shells(jshl)%gtos(jcart)%norms(jprim)

               !
               ! Loop over the grid
               !
               do i = 0, nxstp 
                 gridp(1) = mx + xinc*i
                 dx1  = shells(ishl)%origin(1) - gridp(1)
                 dx2  = shells(jshl)%origin(1) - gridp(1)

                 dxx1 = dx1*dx1
                 dxx2 = dx2*dx2

                 do j = 0, nystp
                   gridp(2) = my + yinc*j
                   dy1  = shells(ishl)%origin(2) - gridp(2)
                   dy2  = shells(jshl)%origin(2) - gridp(2)

                   dyy1 = dy1*dy1
                   dyy2 = dy2*dy2
                   r1xy = dxx1 + dyy1
                   r2xy = dxx2 + dyy2
 
                   do k = 0, nzstp
                     gridp(3) = mz + zinc*k
                     dz1  = shells(ishl)%origin(3) - gridp(3)
                     dz2  = shells(jshl)%origin(3) - gridp(3)

                     dzz1 = dz1*dz1
                     dzz2 = dz2*dz2

                     r1 = r1xy + dzz1
                     r2 = r2xy + dzz2

                     ar1 = shells(ishl)%gtos(1)%exps(iprim) * r1
                     ar2 = shells(jshl)%gtos(1)%exps(jprim) * r2

                     ! Evaluate primitive basis function on center A 
                     f1 = dx1**ix * dy1**iy * dz1**iz * dexp(-ar1)

                     ! Evaluate primitive basis function on center B 
                     f2 = dx2**jx * dy2**jy * dz2**jz * dexp(-ar2)

                     pointval(k,j,i) = pointval(k,j,i) + dfac * f1 * f2
 
                   end do
                 end do
               end do

             end do ! jprim
           end do ! iprim

         end do ! jcart
       end do ! icart

       deallocate(cartcompi,cartcompj)
     end do ! jshl
   end do ! ishl

   !
   ! Write cube file
   ! 
   call cube_WriteBody(pointval,icubfil,nxstp,nystp,nzstp)

 end subroutine

 subroutine shell_EvaluateMoOnGrid(shells,orbs,imo,icubfil,xyz,at,nshell,nbf,natoms,nocc,nspin)

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   ! grid spacing for cube file
   real(kdp), parameter :: step = 0.4d0

   ! dimensions
   integer, intent(in) :: nspin
   integer, intent(in) :: nshell, natoms, nocc(nspin), nbf
 
   ! input
   type(shell_info), intent(in) :: shells(nshell)
   real(kdp),        intent(in) :: orbs(nbf,nbf),xyz(3,natoms)
   integer,          intent(in) :: icubfil,imo,at(natoms)

   ! local
   integer   :: ishl,iprim,nzstp,nystp,nxstp
   integer   :: ibf,icart,iat,langa
   integer   :: ix,iy,iz,jx,jy,jz
   integer   :: i,j,k,cnt

   real(kdp) :: px,py,pz,mx,my,mz,zinc,yinc,xinc
   real(kdp) :: dx1,dy1,dz1,dxx1,dyy1,dzz1,r1xy
   real(kdp) :: dfac,f1,gridp(3),ar1,r1

   real(kdp), allocatable :: pointval(:,:,:)
   integer,   allocatable :: cartcompi(:,:)
  
   ! Current limitations
   if (nspin /= 1) call quit("Can evaluate MO only for RHF case!","type_shell")
 
   ! get Grid boundaries
   px = maxval(xyz(1,1:natoms))+3.0
   py = maxval(xyz(2,1:natoms))+3.0
   pz = maxval(xyz(3,1:natoms))+3.0
   mx = minval(xyz(1,1:natoms))-3.0
   my = minval(xyz(2,1:natoms))-3.0
   mz = minval(xyz(3,1:natoms))-3.0

   ! calculate step sizes
   nxstp = floor((abs(px)+abs(mx))/step)
   xinc  = (abs(px)+abs(mx))/nxstp
   nystp = floor((abs(py)+abs(my))/step)
   yinc  = (abs(py)+abs(my))/nystp
   nzstp = floor((abs(pz)+abs(mz))/step)
   zinc  = (abs(pz)+abs(mz))/nzstp
 
   !
   ! Writing Header
   !
   call cube_WriteHeader(1,imo,icubfil,xyz,xinc,yinc,zinc,   &
                         at,nxstp,nystp,nzstp,mx,my,mz,natoms)

   allocate(pointval(0:nzstp,0:nystp,0:nxstp))

   pointval = zero

   do ishl = 1, nshell
     langa = shells(ishl)%angm

     allocate(cartcompi(3,shells(ishl)%ncart))

     call shell_getcartcomp(cartcompi,langa)

     do icart = 1, shells(ishl)%ncart
       ix = cartcompi(1,icart) 
       iy = cartcompi(2,icart) 
       iz = cartcompi(3,icart) 
       ibf = shells(ishl)%ibfstart + icart - 1
 
       do iprim = 1, shells(ishl)%nprim
         dfac = orbs(ibf,imo) * shells(ishl)%gtos(icart)%coefs(iprim) * shells(ishl)%gtos(icart)%norms(iprim)

         !
         ! Loop over the grid
         !
         do i = 0, nxstp 
           gridp(1) = mx + xinc*i
           dx1  = shells(ishl)%origin(1) - gridp(1)

           dxx1 = dx1*dx1

           do j = 0, nystp
             gridp(2) = my + yinc*j
             dy1  = shells(ishl)%origin(2) - gridp(2)

             dyy1 = dy1*dy1
             r1xy = dxx1 + dyy1
 
             do k = 0, nzstp
               gridp(3) = mz + zinc*k
               dz1  = shells(ishl)%origin(3) - gridp(3)

               dzz1 = dz1*dz1

               r1 = r1xy + dzz1

               ar1 = shells(ishl)%gtos(1)%exps(iprim) * r1

               ! Evaluate primitive basis function on center 
               f1 = dx1**ix * dy1**iy * dz1**iz * dexp(-ar1)

               pointval(k,j,i) = pointval(k,j,i) + dfac * f1 
 
             end do
           end do
         end do

       end do ! iprim

     end do ! icart

     deallocate(cartcompi)
   end do ! ishl

   !
   ! Write cube file
   ! 
   call cube_WriteBody(pointval,icubfil,nxstp,nystp,nzstp)

 end subroutine

 subroutine shell_EvaluateMEPOnGrid(shells,charge,orbs,icubfil,xyz,at,nshell,nbf,natoms,nocc,nspin)

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   ! grid spacing for cube file
   real(kdp), parameter :: step = 0.4d0

   ! dimensions
   integer, intent(in) :: nspin
   integer, intent(in) :: nshell, natoms, nocc(nspin), nbf
 
   ! input
   type(shell_info), intent(in) :: shells(nshell)
   real(kdp),        intent(in) :: orbs(nbf,nbf),xyz(3,natoms),charge(natoms)
   integer,          intent(in) :: icubfil,at(natoms)

   ! local
   integer   :: ishl,jshl,iprim,jprim,nzstp,nystp,nxstp
   integer   :: ibf,jbf,kbf,icart,jcart,iat,ncarta,ncartb,langa,langb
   integer   :: ibfst,ibfnd,jbfst,jbfnd,nprimi,nprimj
   integer   :: i,j,k

   real(kdp) :: px,py,pz,mx,my,mz,zinc,yinc,xinc
   real(kdp) :: dtemp,dfac,gridp(3),dxyz(3),xyzp(3),r1
   real(kdp) :: alpha,beta,zeta,cna,cnb

   real(kdp), allocatable :: pointval(:,:,:),dens(:,:),vnai(:,:)
 
   ! functions
   real(kdp) :: product_center_1D
 
   ! Current limitations
   if (nspin /= 1) call quit("Can evaluate electrostatic pontential only for RHF case!","type_shell")
 
   ! get Grid boundaries
   px = maxval(xyz(1,1:natoms))+3.0
   py = maxval(xyz(2,1:natoms))+3.0
   pz = maxval(xyz(3,1:natoms))+3.0
   mx = minval(xyz(1,1:natoms))-3.0
   my = minval(xyz(2,1:natoms))-3.0
   mz = minval(xyz(3,1:natoms))-3.0

   ! calculate step sizes
   nxstp = floor((abs(px)+abs(mx))/step)
   xinc  = (abs(px)+abs(mx))/nxstp
   nystp = floor((abs(py)+abs(my))/step)
   yinc  = (abs(py)+abs(my))/nystp
   nzstp = floor((abs(pz)+abs(mz))/step)
   zinc  = (abs(pz)+abs(mz))/nzstp
 
   !
   ! Calculate density
   !
   allocate(dens(nbf,nbf))

   do jbf = 1, nbf
     do ibf = 1, nbf
       dens(ibf,jbf) = zero
       do kbf = 1, nocc(1)
         dens(ibf,jbf) = dens(ibf,jbf) + two * orbs(ibf,kbf) * orbs(jbf,kbf) 
       end do
     end do
   end do

   !
   ! Writing Header
   !
   call cube_WriteHeader(0,1,icubfil,xyz,xinc,yinc,zinc,     &
                         at,nxstp,nystp,nzstp,mx,my,mz,natoms)

   allocate(pointval(0:nzstp,0:nystp,0:nxstp))


   ! 
   ! Init point values
   ! 
   do iat = 1, natoms

     do i = 0, nxstp 
       gridp(1) = mx + xinc*i
       dxyz(1)  = xyz(1,iat) - gridp(1) 

       do j = 0, nystp
         gridp(2) = my + yinc*j
         dxyz(2)  = xyz(2,iat) - gridp(2) 

         do k = 0, nzstp
           gridp(3) = mz + zinc*k
           dxyz(3)  = xyz(3,iat) - gridp(3) 

           r1 = dsqrt(dxyz(1)**2 + dxyz(2)**2 + dxyz(3)**2)

           pointval(k,j,i) = charge(iat) / r1
  
         end do
       end do
     end do

   end do ! iat

   do ishl = 1, nshell
     ncarta = shells(ishl)%ncart
     langa  = shells(ishl)%angm
     nprimi = shells(ishl)%nprim     
     ibfst  = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend

     do jshl = 1, nshell
       ncartb = shells(jshl)%ncart
       langb  = shells(jshl)%angm
       nprimj = shells(jshl)%nprim
       jbfst  = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend

       allocate(vnai(ncarta,ncartb)) 

       do iat = 1, natoms

         do jprim = 1, nprimj
           beta = shells(jshl)%gtos(1)%prim(jprim)%expo 

           do iprim = 1, nprimi
             alpha = shells(ishl)%gtos(1)%prim(iprim)%expo

             zeta = alpha + beta

             xyzp(1) = product_center_1D(alpha,shells(ishl)%origin(1),beta,shells(jshl)%origin(1))
             xyzp(2) = product_center_1D(alpha,shells(ishl)%origin(2),beta,shells(jshl)%origin(2))
             xyzp(3) = product_center_1D(alpha,shells(ishl)%origin(3),beta,shells(jshl)%origin(3))

             !
             ! Loop over the grid
             !
             do i = 0, nxstp 
               gridp(1) = mx + xinc*i
               dxyz(1)  = xyz(1,iat) - gridp(1) 

               do j = 0, nystp
                 gridp(2) = my + yinc*j
                 dxyz(2)  = xyz(2,iat) - gridp(2) 

                 do k = 0, nzstp
                   gridp(3) = mz + zinc*k
                   dxyz(3)  = xyz(3,iat) - gridp(3) 

                   call os_nuclear_shell(vnai,shells(ishl)%origin,shells(jshl)%origin, &
                                         dxyz,xyzp,langa,langb,alpha,beta,zeta) 

                   do jcart = 1, ncartb
                     jbf = jbfst + jcart - 1
                     cnb = shells(jshl)%gtos(jcart)%prim(jprim)%coef &
                         * shells(jshl)%gtos(jcart)%prim(jprim)%norm

                     do icart = 1, ncarta
                       ibf = ibfst + icart - 1
                       cna = shells(ishl)%gtos(icart)%prim(iprim)%coef &
                           * shells(ishl)%gtos(icart)%prim(iprim)%norm
                     
                       pointval(k,j,i) = pointval(k,j,i) &
                                       - vnai(icart,jcart) * cna * cnb * charge(iat) * dens(ibf,jbf)
                     end do
                   end do             
 
                 end do
               end do
             end do

           end do ! iprim
         end do ! jprim

       end do ! iat

       deallocate(vnai)

     end do ! jshl
   end do ! ishl

   !
   ! Write cube file
   ! 
   call cube_WriteBody(pointval,icubfil,nxstp,nystp,nzstp)

 end subroutine

 subroutine cube_WriteHeader(imode,imo,icubfil,xyz,xinc,yinc,zinc,&
                             at,nxstp,nystp,nzstp,mx,my,mz,natoms)

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   ! dimensions
   integer, intent(in) :: natoms

   ! input
   integer,   intent(in) :: imode,imo,icubfil
   integer,   intent(in) :: at(natoms),nxstp,nystp,nzstp
   real(kdp), intent(in) :: xyz(3,natoms),xinc,yinc,zinc,mx,my,mz

   ! local
   integer :: iat
  
   if (imode == 0) then
     write(icubfil,*)'Density'
   else if (imode == 1) then
     write(icubfil,*)'MO',imo
   end if
   write(icubfil,*)'Snail program'

   write(icubfil,'(i5,3f16.6)') natoms,mx,my,mz
   write(icubfil,'(i5,3f16.6)') nxstp+1,xinc,0.0,0.0
   write(icubfil,'(i5,3f16.6)') nystp+1,0.0,yinc,0.0
   write(icubfil,'(i5,3f16.6)') nzstp+1,0.0,0.0,zinc
   do iat = 1, natoms
      write(icubfil,'(i5,4f16.6)') at(iat), 0.0, xyz(1,iat), xyz(2,iat), xyz(3,iat)
   enddo

 end subroutine
 
 subroutine cube_WriteBody(pointval,icubfil,nxstp,nystp,nzstp)

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   ! dimensions
   integer, intent(in) :: nxstp,nystp,nzstp

   ! input
   real(kdp), intent(in) :: pointval(0:nzstp,0:nystp,0:nxstp)
   integer,   intent(in) :: icubfil

   ! local
   integer :: i,j,k,cnt

   cnt = 1
   do i = 0, nxstp
     do j = 0, nystp
       do k = 0, nzstp
         if (cnt.lt.6) then
           write(icubfil,'(E14.8,1X)',advance='no') pointval(k,j,i)
           cnt = cnt + 1
         else
           write(icubfil,'(E14.8)') pointval(k,j,i)
           cnt = 1
         endif
       enddo
     enddo
   enddo

 end subroutine

  subroutine shell_shift_origin(shell,shift)
    implicit none
 
    include 'fortrankinds.h'

    ! input/output
    type(shell_info), intent(inout) :: shell
    real(kdp), intent(in)           :: shift(3)

    ! local
    integer :: icart

    shell%origin = shell%origin + shift 

    call at_shift_origin(shell%atominfo,shift)

    do icart = 1, shell%ncart
      call cgto_shift_origin(shell%gtos(icart),shift)
    end do 

  end subroutine

end module
