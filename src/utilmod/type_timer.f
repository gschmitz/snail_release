module type_timer

  implicit none

  include "fortrankinds.h"

  private

  integer, parameter :: mxlen=20

  type timing
    character(len=mxlen) :: idname                   ! id name 
    integer :: lenidname = -1                        ! length of id name 

    real(kdp) :: cpust  = 0.0d0                      ! Start of CPU timing                 
    real(kdp) :: cputot = 0.0d0                      ! Total CPU timing

    real(kdp) :: wallst  = 0.0d0                     ! Start of Wall timing                 
    real(kdp) :: walltot = 0.0d0                     ! Total Wall timing

    type(timing), pointer :: handle => null()        ! Handle for linked list 
  end type timing 

  type timer_info

    integer :: numid = 0                             ! Total number of active measurements

    type(timing), pointer :: head => null()          ! Head of linked list
    type(timing), pointer :: tail => null()          ! Tail of linked list
  end type

  public :: timer_info, timer_exec

 !---------------------------------------------------------------------------------------+
  contains
 !---------------------------------------------------------------------------------------+

 !----------------------------------------------------------------------------------------
 ! Main driver for profiling
 !
 !    command (cmd)          purpose
 !  --------------------------------------------------------------------------------
 !     init           ->     init and store start time for measurement "identifer"
 !     measure        ->     measure timing for measurement "identifer"
 !     get            ->     return results for measurement "identifer"
 !     report         ->     print timings for measurement "identifer"
 !     report_all     ->     print timings for all measurements
 !
 !----------------------------------------------------------------------------------------
 subroutine timer_exec(timer,cmd,identifier,cpu,wall)

   implicit none

   include 'fortrankinds.h'

   ! input output
   type(timer_info),    intent(inout) :: timer
   real(kdp), optional, intent(inout) :: cpu,wall 

   ! input
   character(len=*), intent(in)    :: cmd,identifier


   ! local
   character(len=mxlen)  :: idname
 
   real(kdp)             :: cpux,wallx 
   logical               :: lidexists
   integer               :: nlen,lenidname,ierr
   type(timing), pointer :: ptr,cptr


   nlen = len_trim(identifier) 

   ! sanity check(s)
   if (nlen == 0) call quit("identifier can't be empty","timer_exec")

   idname = identifier(1:min(mxlen,nlen))
   lenidname = len_trim(idname)
   
   ! Check different possible commands
   !==================================

   if (index(cmd,'init') == 1) then

     ! Check if we have already this identifier
     lidexists = .false.
     ptr => timer%head

     do 
       if (.not. associated(ptr)) exit

       if (ptr%lenidname == lenidname) then
         if (index(ptr%idname,idname) == 1) then
           lidexists = .true.

           ! initialize measurment of CPU and wall times and exit
           call cpu_wall_time(ptr%cpust,ptr%wallst)
           if (present(cpu))  cpu  = ptr%cpust
           if (present(wall)) wall = ptr%wallst    
           exit
         end if 
       end if

       ptr => ptr%handle
     end do

     ! If id does not exist, allocate and initialize it
     if (.not. lidexists) then
       if(.not. associated(timer%head)) then

         allocate(timer%head,stat=ierr)
         if(ierr /= 0) call quit('Problem in memory allocation','timer_exec')
         timer%tail => timer%head
         nullify(timer%tail%handle) 

       else
       
         allocate(timer%tail%handle,stat=ierr)
         if(ierr /= 0) call quit('Problem in memory allocation', 'timer_exec')
         timer%tail => timer%tail%handle
         nullify(timer%tail%handle) 

       end if

       ! init values
       timer%numid = timer%numid + 1
       call cpu_wall_time(timer%tail%cpust, timer%tail%wallst)
       timer%tail%idname    = idname
       timer%tail%lenidname = lenidname
       timer%tail%cputot    = 0.0d0
     end if 

   elseif (index(cmd,'reset') == 1) then

        ! Reset (deallocate) the profiling list 

        ptr => timer%head

        do

          if(.not. associated(ptr)) exit
          
          cptr => ptr%handle 
          
          deallocate(ptr,stat=ierr)
          if(ierr /= 0) call quit('Problem in memory deallocation', 'timer_exec')
          
          ptr => cptr
          
        end do

        timer%numid = 0

   elseif (index(cmd,'measure') == 1) then

     ptr => timer%head
     do 

       if(.not. associated(ptr)) exit

       if(ptr%lenidname == lenidname) then

         if(index(ptr%idname,idname) == 1) then
           call cpu_wall_time(cpux,wallx)
           cpux  = cpux  - ptr%cpust
           wallx = wallx - ptr%wallst

           ptr%cputot  = ptr%cputot  + cpux
           ptr%walltot = ptr%walltot + wallx

           if (present(cpu))  cpu = ptr%cputot
           if (present(wall)) wall = ptr%walltot
         end if

       end if          

       ptr => ptr%handle

     end do

   elseif (index(cmd,'get') == 1) then

     ptr => timer%head
     do 

       if(.not. associated(ptr)) exit

       if(ptr%lenidname == lenidname) then

         if(index(ptr%idname,idname) == 1) then
           if (present(cpu))  cpu  = ptr%cputot
           if (present(wall)) wall = ptr%walltot
         end if

       end if          

       ptr => ptr%handle

     end do


   elseif (index(cmd,'report_all') == 1) then
     write(6,'(/5x,a)') 'Module        CPU-time   WALL-time '
     write(6,'(5x,a)')  '                (sec)      (sec)   ' 
     write(6,'(5x,a)')  '-----------------------------------' 

     ptr => timer%head
     do 

       if(.not. associated(ptr)) exit

       write(6,'(5x,a10,a3,2(f10.2))') ptr%idname, ':', ptr%cputot, ptr%walltot

       ptr => ptr%handle

     end do

   elseif (index(cmd,'report') == 1) then
     ! TODO
 
   end if
 end subroutine


end module
