
subroutine ovlp_3int(gout,&
                     aexps,acoefs,anorms,xyza,la,ma,na, &
                     bexps,bcoefs,bnorms,xyzb,lb,mb,nb, &
                     cexps,ccoefs,cnorms,xyzc,lc,mc,nc, &
                     cnab,cnc,&
                     nprima,nprimb,nprimc,&
                     ncarta,ncartb,ncartc,&
                     nprimij,nprimkl,nprimijk)

  use type_shell

  implicit none


  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  ! dimensions
  integer, intent(in) :: nprima,nprimb,nprimc
  integer, intent(in) :: ncarta,ncartb,ncartc
  integer, intent(in) :: nprimij,nprimkl,nprimijk
 
  ! output:
  real(kdp), intent(out) :: gout(ncarta,ncartb,ncartc)

  ! input:
  real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3)
  real(kdp), intent(in) :: anorms(nprima,ncarta),bnorms(nprimb,ncartb),&
                           cnorms(nprimc,ncartc)
  real(kdp), intent(in) :: aexps(nprima,ncarta),bexps(nprimb,ncartb),&
                           cexps(nprimc,ncartc)
  real(kdp), intent(in) :: acoefs(nprima,ncarta),bcoefs(nprimb,ncartb),&
                           ccoefs(nprimc,ncartc)
  integer,   intent(in) :: la(ncarta),ma(ncarta),na(ncarta),&
                           lb(ncartb),mb(ncartb),nb(ncartb),&
                           lc(ncartc),mc(ncartc),nc(ncartc)
  real(kdp), intent(in) :: cnab(nprimij,ncarta*ncartb),&
                           cnc(nprimc,ncartc)


  ! local
  integer,   allocatable :: cartcompi(:,:),cartcompj(:,:),cartcompk(:,:)
  real(kdp), allocatable :: sints(:,:,:)

  real(kdp) :: Pxyz(3),Qxyz(3)
  real(kdp) :: QAx,QBx,QCx,xab2,xpc2
  real(kdp) :: QAy,QBy,QCy,yab2,ypc2
  real(kdp) :: QAz,QBz,QCz,zab2,zpc2
  integer   :: langa, langb, langc
  integer   :: size_a, size_b, size_c
  integer   :: ix,iy,iz,jx,jy,jz,kx,ky,kz
  integer   :: icart,jcart,kcart,ijcart
  integer   :: iprim,jprim,kprim,ijprim
  integer   :: lambdaa,lambdab,lambdac
  integer   :: Nan,Nam,Nal,Nbn,Nbm,Nbl,Ncn,Ncm,Ncl
  integer   :: ii,jj,kk,ll,mm,nn,idxa,idxb,idxc

  real(kdp) :: mu, nu, zeta, rab2, rpc2, qexp, o2q

  gout = zero

  ! Total angular momentum in shell
  langa = la(1) + ma(1) + na(1)  
  langb = lb(1) + mb(1) + nb(1) 
  langc = lc(1) + mc(1) + nc(1) 

  ! Get size for working array 
  size_a = (langa+1)*(langa+1)*langa+1
  size_c = (langc+1)*(langc+1)*langc+1
  size_b = (langb+1)*(langb+1)*langb+1

  ! allocate memory for recursion formulas
  allocate(sints(0:size_a,0:size_c,0:size_b))

  Nan = 1
  Nam = langa+1
  Nal = Nam*Nam

  Nbn = 1
  Nbm = langb+1
  Nbl = Nbm*Nbm

  Ncn = 1
  Ncm = langc+1
  Ncl = Ncm*Ncm

  allocate(cartcompi(3,ncarta),&
           cartcompj(3,ncartb),&
           cartcompk(3,ncartc) )

  call shell_getcartcomp(cartcompi,langa)
  call shell_getcartcomp(cartcompj,langb)
  call shell_getcartcomp(cartcompk,langc)

  rab2 =   (xyza(1) - xyzb(1)) * (xyza(1) - xyzb(1)) &
         + (xyza(2) - xyzb(2)) * (xyza(2) - xyzb(2)) &
         + (xyza(3) - xyzb(3)) * (xyza(2) - xyzb(3)) 

  do kprim = 1, nprimc
    do iprim = 1, nprima
      do jprim = 1, nprimb

        ijprim = (jprim-1) * nprima + iprim

        zeta = aexps(iprim,1) + bexps(jprim,1)
        mu   = aexps(iprim,1) * bexps(jprim,1) / zeta 

        qexp = zeta + cexps(kprim,1)
        nu   = zeta * cexps(kprim,1) / qexp 

        ! one over 2q
        o2q = 0.5d0 / qexp

        ! Calculate center P 
        call product_center(Pxyz,aexps(iprim,1),xyza,bexps(jprim,1),xyzb) 

        rpc2 =   (Pxyz(1) - xyzc(1)) * (Pxyz(1) - xyzc(1)) &
               + (Pxyz(2) - xyzc(2)) * (Pxyz(2) - xyzc(2)) &
               + (Pxyz(3) - xyzc(3)) * (Pxyz(3) - xyzc(3)) 
   
        ! Calculate center Q
        call product_center(Qxyz,zeta,Pxyz,cexps(kprim,1),xyzc)

        QAx = Qxyz(1) - xyza(1); QBx = Qxyz(1) - xyzb(1); QCx = Qxyz(1) - xyzc(1) 
        QAy = Qxyz(2) - xyza(2); QBy = Qxyz(2) - xyzb(2); QCy = Qxyz(2) - xyzc(2)
        QAz = Qxyz(3) - xyza(3); QBz = Qxyz(3) - xyzb(3); QCz = Qxyz(3) - xyzc(3)

        sints = zero 

        ! Compute S_000
        sints(0,0,0) = (pi/qexp)*dsqrt(pi/qexp) * dexp(-mu*rab2 - nu*rpc2)

        !
        ! Increase angular momenta a in (a,0,0)
        !
        do lambdaa = 1, langa
          ! Loop over angular momentum functions belonging to this shell
          do ii = 0, lambdaa
            ix  = lambdaa - ii
            do jj = 0, ii
              iy = ii - jj
              iz = jj

              ! Index in integrals table
              idxa = ix*Nal + iy*Nam + iz*Nan

              ! Obara Saika relations
              if (ix > 0) then

                sints(idxa,0,0) = QAx * sints(idxa-Nal,0,0)
                if(ix > 1) sints(idxa,0,0) = sints(idxa,0,0) + o2q*(ix-1)*sints(idxa-2*Nal,0,0)
               
              else if(iy > 0) then

                sints(idxa,0,0) = QAy * sints(idxa-Nam,0,0)
                if(iy > 1) sints(idxa,0,0) = sints(idxa,0,0) + o2q*(iy-1)*sints(idxa-2*Nam,0,0)
              
              else if(iz > 0) then

                sints(idxa,0,0) = QAz * sints(idxa-Nan,0,0)
                if(iz > 1) sints(idxa,0,0) = sints(idxa,0,0) + o2q*(iz-1)*sints(idxa-2*Nan,0,0)

              end if
            end do ! jj
          end do ! ii
        end do ! lambdaa

        !
        ! Increase angular momenta b in (a,0,b)
        !
        do lambdaa = 0, langa

          ! Loop over angular momentum of second shell
          do kk = 0, lambdaa
            ix = lambdaa - kk

            do ll = 0, kk
              iy = kk - ll 
              iz = ll

              ! LHS index is
              idxa = ix*Nal + iy*Nam + iz*Nan

              ! Loop over total angular momentum of RHS
              do lambdab = 1, langb

                ! Loop over the functions belonging to the shell
                do ii = 0, lambdab
                  jx = lambdab - ii
                  do jj = 0, ii
                    jy = ii - jj
                    jz = jj

                    ! RHS index is
                    idxb = jx*Nbl + jy*Nbm + jz*Nbn

                    ! Obara Saika relations
                    if(jx > 0) then
                      sints(idxa,0,idxb) = QBx*sints(idxa,0,idxb-Nbl)
                      if(jx > 1) sints(idxa,0,idxb) = sints(idxa,0,idxb) + o2q*(jx-1)*sints(idxa,0,idxb-2*Nbl)
                      if(ix > 0) sints(idxa,0,idxb) = sints(idxa,0,idxb) + o2q*ix*sints(idxa-Nal,0,idxb-Nbl)
                          
                    else if(jy > 0) then
                      sints(idxa,0,idxb) = QBy*sints(idxa,0,idxb-Nbm)
                      if(jy > 1) sints(idxa,0,idxb) = sints(idxa,0,idxb) + o2q*(jy-1)*sints(idxa,0,idxb-2*Nbm)
                      if(iy > 0) sints(idxa,0,idxb) = sints(idxa,0,idxb) + o2q*iy*sints(idxa-Nam,0,idxb-Nbm)
                    
                    else if (jz > 0) then
                      sints(idxa,0,idxb) = QBz*sints(idxa,0,idxb-Nbn)
                      if(jz > 1) sints(idxa,0,idxb) = sints(idxa,0,idxb) + o2q*(jz-1)*sints(idxa,0,idxb-2*Nbn)
                      if(iz > 0) sints(idxa,0,idxb) = sints(idxa,0,idxb) + o2q*iz*sints(idxa-Nan,0,idxb-Nbn)
                    else
                      call quit("Problem in Obara Saika scheme!",'ovlp_3ints')
                    end if
                  end do ! jj
                end do ! ii
              end do ! lambdab
            end do ! ll
          end do  ! kk
        end do ! lambdaa

        !
        ! Increase angular momenta b in (a,c,b)
        !
        do lambdaa = 0, langa

          ! Loop over angular momentum of second shell
          do kk = 0, lambdaa
            ix = lambdaa - kk

            do ll = 0, kk
              iy = kk - ll
              iz = ll

              ! LHS index is
              idxa = ix*Nal + iy*Nam + iz*Nan

              ! Loop over total angular momentum of RHS
              do lambdab = 0, langb

                ! Loop over the functions belonging to the shell
                do ii = 0, lambdab
                  jx = lambdab - ii
                  do jj = 0, ii
                    jy = ii - jj
                    jz = jj

                    ! RHS index is
                    idxb = jx*Nbl + jy*Nbm + jz*Nbn

                    ! Loop over total angular momentum of middle
                    do lambdac = 1, langc
                      ! Loop over the functions belonging to the shell
                      do mm = 0, lambdac
                        kx = lambdac - mm
                        do nn = 0, mm
                          ky = mm - nn
                          kz = nn

                          ! RHS index is
                          idxc = kx*Ncl + ky*Ncm + kz*Ncn

                          ! Obara Saika relations
                          if(kx > 0) then

                            sints(idxa,idxc,idxb) = QCx*sints(idxa,idxc-Ncl,idxb)
                            if(kx > 1) sints(idxa,idxc,idxb) = sints(idxa,idxc,idxb) + o2q*(kx-1)*sints(idxa,idxc-2*Ncl,idxb)
                            if(ix > 0) sints(idxa,idxc,idxb) = sints(idxa,idxc,idxb) + o2q*ix*sints(idxa-Nal,idxc-Ncl,idxb)
                            if(jx > 0) sints(idxa,idxc,idxb) = sints(idxa,idxc,idxb) + o2q*jx*sints(idxa,idxc-Ncl,idxb-Nbl)
                               
                          else if(ky > 0) then

                            sints(idxa,idxc,idxb) = QCy*sints(idxa,idxc-Ncm,idxb)
                            if(ky > 1) sints(idxa,idxc,idxb) = sints(idxa,idxc,idxb) + o2q*(ky-1)*sints(idxa,idxc-2*Ncm,idxb)
                            if(iy > 0) sints(idxa,idxc,idxb) = sints(idxa,idxc,idxb) + o2q*iy*sints(idxa-Nam,idxc-Ncm,idxb)
                            if(jy > 0) sints(idxa,idxc,idxb) = sints(idxa,idxc,idxb) + o2q*jy*sints(idxa,idxc-Ncm,idxb-Nbm)
                          
                          else if(kz > 0) then

                            sints(idxa,idxc,idxb) = QCz*sints(idxa,idxc-Ncn,idxb)
                            if(kz > 1) sints(idxa,idxc,idxb) = sints(idxa,idxc,idxb) + o2q*(kz-1)*sints(idxa,idxc-2*Ncn,idxb)
                            if(iz > 0) sints(idxa,idxc,idxb) = sints(idxa,idxc,idxb) + o2q*iz*sints(idxa-Nan,idxc-Ncn,idxb)
                            if(jz > 0) sints(idxa,idxc,idxb) = sints(idxa,idxc,idxb) + o2q*jz*sints(idxa,idxc-Ncn,idxb-Nbn)
                          else
                            call quit("Error in Obara Saika scheme!","ovlp_3ints")
                          end if
                        end do ! nn
                      end do ! mm
                    end do ! lambdac
                  end do ! jj
                end do ! ii
              end do ! lambdab
            end do ! ll
          end do ! kk 
        end do ! lambdaa

        ! assemble in gout
        do icart = 1, ncarta
          ix = cartcompi(1,icart)
          iy = cartcompi(2,icart)
          iz = cartcompi(3,icart)
       
          ! LHS index in worker array
          idxa = ix*Nal + iy*Nam + iz
       
          do jcart = 1, ncartb
            jx = cartcompj(1,jcart)
            jy = cartcompj(2,jcart)
            jz = cartcompj(3,jcart)

            ijcart = (jcart-1) * ncarta + icart
       
            ! RHS index in worker array
            idxb = jx*Nbl + jy*Nbm + jz
       
            do kcart = 1, ncartc
              kx = cartcompk(1,kcart)
              ky = cartcompk(2,kcart)
              kz = cartcompk(3,kcart)
       
              ! Middle index in worker array
              idxc = kx*Ncl + ky*Ncm + kz*Ncn
       
              gout(icart,jcart,kcart) = gout(icart,jcart,kcart) &
                                      + cnab(ijprim,ijcart) * cnc(kprim,kcart) * sints(idxa,idxc,idxb)

            end do
          end do
        end do
 

      end do ! jprim
    end do ! iprim
  end do ! kprim

end subroutine


