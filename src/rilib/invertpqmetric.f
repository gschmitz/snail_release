subroutine InvertPQMetric(vpqmat,useOvlp,useEig,cbas,nxbf,nxshell)
!------------------------------------------------------------------------------------+
! Purpose:
!
! Calculates (P|Q)^{-1/2} or S_(PQ)^{-1) V_{PQ} S_{PQ}^{-1} )^{1/2} depending on
! if Coulomb or overlap metric is used. Coulomb metric is known to be more accurate.
!
! For (P|Q)^{-1/2} two options excist
!
!  1.) Build square root of (P|Q) and invert:
!      1.1 Do eigen decomposition (P|Q) = V D V^T
!      1.2 Build the square root of D and invert it
!      1.3 Sandwich D^{-1/2} between the eigen vectors
!
!  2.) Calculate Cholesky decomposition of (P|Q) = L L^T and then invert L
!      It follows that (L^T)^{-1} L^{-1} = (P|Q)^{-1}
!
! Route 1.) is taken for useEig = true. Note that route 2.) is more efficient.
!
! Gunnar Schmitz, moved out of rimp2.f August 2019
!------------------------------------------------------------------------------------+


  use type_basis

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,           intent(in)    :: nxbf,nxshell

  ! input/output
  real(kdp),         intent(inout) :: vpqmat(nxbf,nxbf)

  ! input
  type(basis_info),  intent(in)    :: cbas
  logical,           intent(in)    :: useOvlp, useEig


  ! local  
  real(kdp), allocatable :: temp1(:,:),temp2(:,:),work(:),eigval(:)
  real(kdp), allocatable :: spqmat(:,:),cpqmat(:,:)

  integer   :: ixbf,jxbf,info,lwork
  real(kdp) :: dlen

  if (useOvlp) then
    !
    ! Calculate ( S_(PQ)^{-1) V_{PQ} S_{PQ}^{-1} )^{1/2} 
    !

    ! 1.) Get S_{PQ}
    allocate(spqmat(nxbf,nxbf))
    call getovlp(spqmat,cbas%shells,nxbf,nxshell)

    ! 2.) Invert overlap matrix
    call invsymmat(spqmat,.false.,nxbf)

    ! 3. a) C_PQ = V_PQ
    allocate(cpqmat(nxbf,nxbf)) 

    call dcopy(nxbf*nxbf,vpqmat,1,cpqmat,1)

    ! 4. Calculate Cholesky decomposition of C_PQ 
    call dpotrf('L', nxbf, cpqmat, nxbf, info )

    if (info /= 0) call quit("Cholesky decomposition failed!","invertpqmatrix")

    ! Zero out not referenced values in vpqmat...
    do jxbf = 1, nxbf
      do ixbf = 1, jxbf - 1
        cpqmat(ixbf,jxbf) = zero
      end do
    end do
   
    ! 3. b) ~V_PQ = C_PQ S_PQ^(-1)
    call dgemm( 't', 'n', nxbf, nxbf, nxbf, one &
              , cpqmat, nxbf, spqmat, nxbf      &
              , zero, vpqmat, nxbf )

    deallocate(spqmat,cpqmat)
 
  else
    !
    ! Calculate (P|Q)^{-1/2) 
    !
    if (useEig) then
      !------------------------------------------------------------------------+
      ! Build square root of (P|Q) and invert:  
      !   1) Do eigen decomposition (P|Q) = V D V^T
      !   2) Build the square root of D and invert it
      !   3) Sandwich D^{-1/2} between the eigen vectors
      !------------------------------------------------------------------------+
      allocate(eigval(nxbf),temp1(nxbf,nxbf),temp2(nxbf,nxbf))

      ! first run to get dimension
      lwork = -1
      call dsyev( 'V', 'U', nxbf, vpqmat, nxbf, eigval, dlen, lwork, info)

      lwork = int(dlen)
      allocate(work(lwork))

      ! actual run
      call dsyev( 'V', 'U', nxbf, vpqmat, nxbf, eigval, work, lwork, info)

      ! save eigenvectors
      call dcopy(nxbf*nxbf,vpqmat,1,temp2,1)

      ! inverse square root of diagonal matrix 
      do ixbf = 1, nxbf
        if (eigval(ixbf) > zero) then
          eigval(ixbf) = one / dsqrt(eigval(ixbf))
        else
          eigval(ixbf) = zero
        end if
      end do

      ! multiply the eigenvector with the diagonal matrix of eigenvalues
      do jxbf = 1, nxbf
        do ixbf = 1, nxbf
          temp1(ixbf,jxbf) = temp2(ixbf,jxbf) * eigval(jxbf) 
        end do
      end do

      !  Build output matrix 
      call dgemm( 'n', 't', nxbf, nxbf, nxbf, one  &
                , temp1, nxbf, temp2, nxbf         &
                , zero, vpqmat, nxbf )
    else
      !------------------------------------------------------------------------+
      ! Calculate Cholesky decomposition of (P|Q) = L L^T and then invert L 
      ! ... then follows (L^T)^{-1} L^{-1} = (P|Q)^{-1} 
      !------------------------------------------------------------------------+

      call dpotrf('L', nxbf, vpqmat, nxbf, info )
      call dtrtri('L', 'N', nxbf, vpqmat, nxbf, info)

      ! Zero out not referenced values in vpqmat...
      do jxbf = 1, nxbf
        do ixbf = 1, jxbf - 1
          vpqmat(ixbf,jxbf) = zero
        end do
      end do

    end if
  end if


end subroutine
