

subroutine mmd_2int(gout,&
                    aexps,acoefs,anorms,la,ma,na, &
                    cexps,ccoefs,cnorms,lc,mc,nc, &
                    eat,eau,eav,eatuv,cna,&
                    ect,ecu,ecv,ectuv,cnc,&
                    Fn,rtuv,gtuv,gcdtuv,ftuv,pfac,&
                    alpha,dx,dy,dz,ammax,maxl,&
                    nprima,nprimc,nprimik,ncarta,ncartc,memetuv) 
  implicit none


  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  ! constants
  logical, parameter :: usetab = .true.

  ! dimensions
  integer, intent(in) :: nprima,nprimc
  integer, intent(in) :: ncarta,ncartc
  integer, intent(in) :: nprimik
  integer, intent(in) :: ammax,maxl
 
  integer(kli), intent(in) :: memetuv(2)
 
  ! output:
  real(kdp), intent(out) :: gout(ncarta,ncartc)

  ! input:
  real(kdp), intent(in) :: anorms(nprima,ncarta),cnorms(nprimc,ncartc)
  real(kdp), intent(in) :: aexps(nprima,ncarta),cexps(nprimc,ncartc)
  real(kdp), intent(in) :: acoefs(nprima,ncarta),ccoefs(nprimc,ncartc)
  integer,   intent(in) :: la(ncarta),ma(ncarta),na(ncarta),&
                           lc(ncartc),mc(ncartc),nc(ncartc)
  real(kdp), intent(inout) :: eat(memetuv(1),ncarta),&
                              eau(memetuv(1),ncarta),&
                              eav(memetuv(1),ncarta),&
                              ect(memetuv(1),ncartc),&
                              ecu(memetuv(1),ncartc),&
                              ecv(memetuv(1),ncartc),&
                              eatuv(memetuv(2),ncarta),& 
                              ectuv(memetuv(2),ncartc),& 
                              cna(nprima,ncarta),&
                              cnc(nprimc,ncartc)

  real(kdp), intent(inout) :: Fn(0:ammax,nprimik), &
                              rtuv(nprimik*(ammax+1)**3 * (3*(ammax+1)) ) , &
                              gtuv(nprimik*(ammax+1)**3)                  , &
                              gcdtuv(nprima*(1*maxl+2)**3)                , &
                              ftuv(nprimc  *(2*maxl)**3)

  real(kdp), intent(in) :: pfac(nprimik),alpha(nprimik),&
                           dx(nprimik),dy(nprimik),dz(nprimik)

  ! local
  integer   :: i,k,ik,tuv,tuv2,taunuphi,tuvbig,&
               labt,mabu,nabv,lcdt,mcdu,ncdv
  integer   :: icart,kcart
  integer   :: t,u,v,tau,nu,phi
  integer   :: nnc,mmc,llc,&
               nna,mma,lla
  integer   :: tmax,umax,vmax,lmax,nprik
  real(kdp) :: dfac,val, dtemp, dfacij, normab, normcd

  real(kdp), allocatable :: gabcd(:)

  nprik = nprima*nprimc

  allocate(gabcd(nprima))

  do icart = 1, ncarta
    nna = na(icart) 
    mma = ma(icart) 
    lla = la(icart) 

    do kcart = 1, ncartc
      nnc = nc(kcart) 
      mmc = mc(kcart) 
      llc = lc(kcart) 

      !--------------------------------------------------------------------+
      !  Create Hermite Coulomb integrals via recursion from Boys-Function 
      !--------------------------------------------------------------------+

      tmax = lla + llc 
      umax = mma + mmc 
      vmax = nna + nnc 

      lmax = tmax + umax + vmax 

      call rcof_tuvn(rtuv,tmax,umax,vmax,0,alpha,dx,dy,dz,Fn,lmax,nprik)

      !--------------------------------------------------------------------+
      !  Transform to Cartesian Coulomb integrals and contract 
      !--------------------------------------------------------------------+
      
      ! Build:
      !   F_{tau,nu,phi}^{c_r d_s} = (-1)^(tau+nu+phi) * E_{tau,phi,nu}^{c_r d_s}
      call contract_E_RHS(ftuv,ectuv(1,kcart),&
                          llc,0,mmc,0,nnc,0,nnc,mmc,llc,nprimc)

      ! Transform:
      !         (tuv|c_r d_s] = \sum_{tau,phi,nu} (-1)^(tau+nu+phi) E_{tau,phi,nu}^{c_r d_s} R_{t+tau,u+nu,v+phi}
      !   a.k.a (tuv|c_r d_s] = \sum_{tau,phi,nu} F_{tau,phi,nu}^{c_r d_s} R_{t+tau,u+nu,v+phi}
      call contract_HermTUV(gtuv,ftuv,rtuv,pfac,tmax,umax,vmax,eat(1,icart),ect(1,kcart),&
                            nnc,mmc,llc,nna,mma,lla,nprik,nprima,nprimc)

      ! Contract:
      !   (tuv|c d) =  \sum_{rs} C_r C_s (tuv|c_r d_s] 
      call contract_Prim_RHS(gcdtuv,gtuv,cnc(1,kcart),lla,mma,nna,nprima,nprimc)

      ! Transform:
      !   [a_m b_n|cd) = \sum_{tuv} E_{t,u,v}^{a_m b_n} (tuv|c d)
      call contract_E_LHS(gabcd,eatuv(1,icart),gcdtuv,nna,mma,lla,nprima)

      ! Contract:
      !   (ab|cd) = \sum_{mn} C_m C_n [a_m b_n|cd) 
      val = zero
      do i = 1, nprima
        val = val + gabcd(i) * cna(i,icart)    
      end do
 
      gout(icart,kcart) = val

    end do ! kcart
  end do ! icart
  
end subroutine
