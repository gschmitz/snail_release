
subroutine get3ints(xbqint,                                    & ! out
                    verbose,doovlp,tolint,memmax,lmax,         &
                    xshells,shells,                            & ! inp 
                    nxbf,nbf,nxshell,nshell,mxprim,mxcart,     & ! dim
                    mxprimx,mxcartx)                             ! dim 
!-----------------------------------------------------------------------------+
!  Purpose: Calculates the three index integrals
!
!  Gunnar Schmitz 
!-----------------------------------------------------------------------------+

   use type_integ
   use type_cgto
   use type_shell
   use mod_fileutil
   use BoysTable

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   ! constants
   logical,   parameter :: locdbg      = .false.

   ! dimensions
   integer, intent(in) :: nxbf,nbf,nxshell,nshell,mxprim,mxcart,mxprimx,mxcartx

   ! input
   type(shell_info), intent(in)   :: xshells(nxshell),shells(nshell)
   integer,          intent(in)   :: lmax
   integer(kli),     intent(in)   :: memmax
   real(kdp),        intent(in)   :: tolint
   logical,          intent(in)   :: doovlp,verbose

   ! output
   real(kdp),       intent(out)   :: xbqint(nxbf,nbf,nbf)

   ! local
   integer      :: ibf,jbf,kbf,ij,ind,&
                   ishl,jshl,kshl,nbuf,&
                   ibfst,ibfnd,jbfst,jbfnd,&
                   kbfst,kbfnd,&
                   icart,jcart,kcart,&
                   ammax,lammax,ijk,iadr,lmxshlpr

   integer :: maxprim,maxcart

   integer(kli) :: memused

   integer :: maxmtot,maxam

   real(kdp), allocatable :: rab2(:)

   real(kdp) :: dtemp,dtemp1,dijkl,xa,ya,za,xb,yb,zb

   real(kdp), allocatable :: xyzp(:,:,:), Kab(:,:)

   real(kdp) :: alphai, alphaj, alphak, alphal
   integer   :: nprimi, nprimj, nprimk
   integer   :: ncarti, ncartj, ncartk
   integer   :: idx, jdx, kdx, ldx

   integer      :: tmax, umax, vmax
   integer(kli) :: memetuv(2),memboys,memrtuv,memgtuv,memftuv,memgcdtuv
   real(kdp)    :: dmem
 
   type(cgto_info), pointer :: bfi, bfj, bfk

   real(kdp), allocatable :: eabt(:,:,:),eabu(:,:,:),eabv(:,:,:),&
                             ecdt(:,:,:),ecdu(:,:,:),ecdv(:,:,:),&
                             eabtuv(:,:,:),ecdtuv(:,:,:),&
                             Fn(:,:),rtuv(:,:),gtuv(:,:),ftuv(:,:),&
                             gcdtuv(:,:)                           

   ! for integral evaluation
   type(integ_info) :: intopt
   type(TBoysTable) :: boystab

   ! basis function information
   real(kdp), allocatable :: bfiexps(:,:), bficoefs(:,:), bfinorms(:,:), &
                             bfjexps(:,:), bfjcoefs(:,:), bfjnorms(:,:), &
                             bfkexps(:,:), bfkcoefs(:,:), bfknorms(:,:)

   integer,   allocatable :: bfila(:,:), bfima(:,:), bfina(:,:), &
                             bfjla(:,:), bfjma(:,:), bfjna(:,:), &
                             bfkla(:,:), bfkma(:,:), bfkna(:,:)

   real(kdp), allocatable :: gout(:,:)

   real(kdp), allocatable :: cnij(:,:),cnk(:,:)

   real(kdp), allocatable :: o2p(:,:), u(:,:), uA(:,:), uB(:,:)

   integer :: nprimij,nprimkl,nprimijk

   integer   :: iprim,jprim,kprim,lprim,ijkl
   real(kdp) :: p, q, QXYZ(3), PXYZ(3), boysarg, rpq
   real(kdp), allocatable :: alpha(:),dx(:),dy(:),dz(:),pfac(:)

   integer :: ithrd,nthreads

   ! for timings
   real(kdp) :: cpur,cpuf,cpug1,cpug2,cpug3

   ! functions
   real(kdp) :: dist2_3d, product_center_1D, dist
   integer   :: get_max_threads, get_thread_num

   !-----------------------------------------------------------------------+
   ! Define things for parrallel code 
   !-----------------------------------------------------------------------+
   nthreads = get_max_threads()

   !-----------------------------------------------------------------------+
   ! init
   !-----------------------------------------------------------------------+
   memused = 0

   xbqint = zero

   ! integral scheme
   intopt%scheme = INT_MMD

   bfi => null()
   bfj => null()
   bfk => null()

   cpur  = zero
   cpuf  = zero 
   cpug1 = zero 
   cpug2 = zero 
   cpug3 = zero 

   !-----------------------------------------------------------------------+
   ! precompute centers   
   !-----------------------------------------------------------------------+
   allocate(rab2(nshell*(nshell+1)/2)) 

   do ishl = 1, nshell
     do jshl = 1, ishl
      
       xa = shells(ishl)%origin(1); xb = shells(jshl)%origin(1)
       ya = shells(ishl)%origin(2); yb = shells(jshl)%origin(2) 
       za = shells(ishl)%origin(3); zb = shells(jshl)%origin(3) 

       rab2(ishl*(ishl-1)/2 + jshl) = dist2_3d(xa, ya, za, xb, yb, zb)

     end do
   end do

   ! Leave this for the moment and waste memory... (Later adapt routines to handle different mxcart)
   maxcart = max(mxcart,mxcartx) 
   maxprim = max(mxprim,mxprimx)

   !-----------------------------------------------------------------------+
   ! allocate buffer for local basis function info 
   !-----------------------------------------------------------------------+
   allocate( bfiexps(maxprim*maxcart,0:nthreads-1),  &
             bficoefs(maxprim*maxcart,0:nthreads-1), &
             bfinorms(maxprim*maxcart,0:nthreads-1), &
             bfjexps(maxprim*maxcart,0:nthreads-1),  &
             bfjcoefs(maxprim*maxcart,0:nthreads-1), &
             bfjnorms(maxprim*maxcart,0:nthreads-1) )

   allocate( bfkexps(maxprim*maxcart,0:nthreads-1),  &
             bfkcoefs(maxprim*maxcart,0:nthreads-1), & 
             bfknorms(maxprim*maxcart,0:nthreads-1) )

   allocate( bfila(maxcart,0:nthreads-1), bfima(maxcart,0:nthreads-1), bfina(maxcart,0:nthreads-1), &
             bfjla(maxcart,0:nthreads-1), bfjma(maxcart,0:nthreads-1), bfjna(maxcart,0:nthreads-1)  )

   allocate( bfkla(maxcart,0:nthreads-1), bfkma(maxcart,0:nthreads-1), bfkna(maxcart,0:nthreads-1) )
  
   allocate( gout(maxcart*maxcart*maxcart,0:nthreads-1) )

   allocate( cnij(maxprim*maxprim*maxcart*maxcart,0:nthreads-1) )
   allocate( cnk( maxprim*maxcart,0:nthreads-1) )

   !-----------------------------------------------------------------------+
   ! allocate buffer for Integral schemes 
   !-----------------------------------------------------------------------+

   ! McMurrchie Davidson scheme
   if (intopt%scheme == INT_MMD) then
     memetuv(1) = maxprim * (lmax+2) * (lmax+2) * (lmax+2) * 2 

     allocate(eabt(memetuv(1),maxcart*maxcart,0:nthreads-1),&
              eabu(memetuv(1),maxcart*maxcart,0:nthreads-1),&
              eabv(memetuv(1),maxcart*maxcart,0:nthreads-1),&   
              ecdt(memetuv(1),maxcart*maxcart,0:nthreads-1),&
              ecdu(memetuv(1),maxcart*maxcart,0:nthreads-1),&
              ecdv(memetuv(1),maxcart*maxcart,0:nthreads-1)  ) 

     memetuv(2) = maxprim * (lmax+2) * (lmax+2) * (lmax+2) * 8

     allocate(eabtuv(memetuv(2),maxcart*maxcart,0:nthreads-1),&
              ecdtuv(memetuv(2),maxcart*maxcart,0:nthreads-1))

     dmem = memetuv(1) * maxcart * maxcart * nthreads * 6 &
          + memetuv(2) * maxcart * maxcart * nthreads * 2

     lmxshlpr = lmax * 2

     ammax   = 4 * lmax
     memboys = (3*(ammax+1)) * maxprim**4
     memrtuv = maxprim**4    * (lmxshlpr)**3 * (3*(lmxshlpr+1)) 
     memgtuv = maxprim**4    * (lmxshlpr+1)**3
     memftuv = maxprim**2    * (2*lmax)**3

     memgcdtuv = maxprim**2    * (2*lmax+2)**3 

     allocate(Fn(memboys,0:nthreads-1))
     allocate(rtuv(memrtuv,0:nthreads-1))
     allocate(gtuv(memgtuv,0:nthreads-1))
     allocate(ftuv(memftuv,0:nthreads-1))
     allocate(gcdtuv(memgcdtuv,0:nthreads-1)) 

     dmem = dmem + memboys*nthreads + memrtuv*nthreads + memgtuv*nthreads + memftuv*nthreads + memgcdtuv*nthreads

     dmem = dmem * 8.0  / (1024*1024)

     if (verbose) write(6,'(5x,a,f10.2,2x,a)') "Allocated", dmem, " MB for int. evaluation"  

     ! TODO: Check maximal memory
     allocate(o2p(maxprim*maxprim,0:nthreads-1),&
                u(maxprim*maxprim,0:nthreads-1),&
               uA(maxprim*maxprim,0:nthreads-1),&
               uB(maxprim*maxprim,0:nthreads-1))

   end if

   !-----------------------------------------------------------------------+
   ! read in pre-tabulated values for Boys-function 
   !-----------------------------------------------------------------------+

   call allocate_boystable(boystab)

   !-----------------------------------------------------------------------+
   ! calculate integrals (ij|K) 
   !-----------------------------------------------------------------------+

   !$OMP PARALLEL DO SCHEDULE(AUTO) &
   !$OMP DEFAULT(NONE) & 
   !$OMP SHARED(xbqint,shells,xshells,tolint,nshell,nxshell,&
   !$OMP        memetuv,gout,mxcart,maxcart,rab2, &
   !$OMP        bfiexps,bficoefs,bfinorms,bfila,bfima,bfina, &  
   !$OMP        bfjexps,bfjcoefs,bfjnorms,bfjla,bfjma,bfjna, &
   !$OMP        bfkexps,bfkcoefs,bfknorms,bfkla,bfkma,bfkna, &
   !$OMP        eabt,eabu,eabv,eabtuv,cnij, &
   !$OMP        ecdt,ecdu,ecdv,ecdtuv,cnk,  &
   !$OMP        o2p,u,uA,uB,boystab,doovlp,ammax,lmax, &
   !$OMP        Fn,rtuv,gcdtuv,gtuv,ftuv,intopt) &
   !$OMP PRIVATE(ithrd,ishl,jshl,kshl,ibfst,&
   !$OMP         xyzp,Kab,bfi,bfj,bfk,lammax,Pxyz,&
   !$OMP         dijkl,pfac,alpha,dx,dy,dz,ijk,iadr,& 
   !$OMP         ibfnd,nprimi,ncarti,jbfst,jbfnd,nprimj,ncartj,&
   !$OMP         kbfst,kbfnd,nprimk,ncartk,boysarg,rpq,p,q,&
   !$OMP         ibf,jbf,kbf,nprimij,nprimkl,nprimijk) &
   !$OMP REDUCTION(+: cpur,cpuf,cpug1,cpug2,cpug3) 
   do ishl = 1, nshell

     ithrd = get_thread_num() 

     ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
     nprimi = shells(ishl)%nprim; ncarti = shells(ishl)%ncart
     ncarti = shells(ishl)%ncart
     call copybasinfo(bfiexps(1,ithrd), bficoefs(1,ithrd), bfinorms(1,ithrd), & 
                      bfila(1,ithrd), bfima(1,ithrd), bfina(1,ithrd),         &
                      shells, ishl, nprimi, ncarti, nshell)

     do jshl = 1, ishl
       jbfst = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend
       nprimj = shells(jshl)%nprim; ncartj = shells(jshl)%ncart
       ncartj = shells(jshl)%ncart
       call copybasinfo(bfjexps(1,ithrd), bfjcoefs(1,ithrd), bfjnorms(1,ithrd), &
                        bfjla(1,ithrd), bfjma(1,ithrd), bfjna(1,ithrd),         &
                        shells, jshl, nprimj, ncartj, nshell)

       ! calculate the center of the shell pair primitives
       allocate(xyzp(3,nprimi,nprimj),Kab(nprimi,nprimj))
       ! calculate the center of the shell pair primitives
       call GetShellPairInfo(intopt, xyzp, Kab, &
                             eabt(1,1,ithrd), eabu(1,1,ithrd), eabv(1,1,ithrd), eabtuv(1,1,ithrd), cnij(1,ithrd),  &
                             shells, rab2, ishl, jshl, o2p(1,ithrd), u(1,ithrd), uA(1,ithrd), uB(1,ithrd), &
                             nprimi, nprimj, nshell, memetuv, ncarti, ncartj)

       do kshl = 1, nxshell 
         kbfst = xshells(kshl)%ibfstart; kbfnd = xshells(kshl)%ibfend
         nprimk = xshells(kshl)%nprim; ncartk = xshells(kshl)%ncart
         ncartk = xshells(kshl)%ncart
         call copybasinfo(bfkexps(1,ithrd), bfkcoefs(1,ithrd), bfknorms(1,ithrd),&
                          bfkla(1,ithrd), bfkma(1,ithrd), bfkna(1,ithrd),        &
                          xshells, kshl, nprimk, ncartk, nxshell)

         !dijkl = dsqrt(gab(ishl,jshl)) * dsqrt(gab(lshl,kshl)) 
         dijkl = 1000.d0 

         if (dijkl .ge. tolint ) then
           ! Get E coefficients for K aux shell
           call GetAuxShellInfo(intopt,ecdt(1,1,ithrd),ecdu(1,1,ithrd),ecdv(1,1,ithrd),ecdtuv(1,1,ithrd),cnk(1,ithrd),&
                                xshells,kshl,o2p(1,ithrd),u(1,ithrd),uA(1,ithrd),uB(1,ithrd),&
                                nprimk,nxshell,ncartk,memetuv,maxcart)

           !-----------------------------------------------------------------------------------+ 
           ! calculate the Boys function for shell triple (ij|K) 
           !-----------------------------------------------------------------------------------+ 

           nprimij = nprimi * nprimj
           nprimijk = nprimij * nprimk

           allocate(pfac(nprimijk),alpha(nprimijk),dx(nprimijk),dy(nprimijk),dz(nprimijk))

           ! just reference the first basis function in the shells 
           bfi => shells(ishl)%gtos(1) 
           bfj => shells(jshl)%gtos(1)
           bfk => xshells(kshl)%gtos(1)

           ! maximum angular momentum to generate the required integrals
           lammax = bfi%la + bfj%la + bfk%la + bfi%ma + bfj%ma + bfk%ma + bfi%na + bfj%na + bfk%na
           do iprim = 1, bfi%nprim
             do jprim = 1, bfj%nprim
               p = bfi%exps(iprim) + bfj%exps(jprim)
               call product_center(Pxyz,bfi%exps(iprim),bfi%origin,bfj%exps(jprim),bfj%origin)
               do kprim = 1, bfk%nprim

                   ijk = (kprim-1) * bfi%nprim * bfj%nprim + &
                         (jprim-1) * bfi%nprim + iprim 

                   q = bfk%exps(kprim) 
                   alpha(ijk) = p*q / (p+q)
                   rpq = dist(Pxyz,bfk%origin)
                   boysarg = alpha(ijk) * rpq * rpq
                   call BoysFArr(lammax,boysarg,Fn((ijk-1)*(lammax+1) + 1,ithrd),&
                                 boystab%fntab,.true.,boystab%nxvals,boystab%nrange,boystab%kmax)
                   
                   dx(ijk) = Pxyz(1)-bfk%origin(1)
                   dy(ijk) = Pxyz(2)-bfk%origin(2)
                   dz(ijk) = Pxyz(3)-bfk%origin(3)
                   pfac(ijk) = two * pi**(2.5d0) / ( p * q * dsqrt (p+q) )

               end do
             end do
           end do

           if ( intopt%scheme == INT_MMD ) then

             if (doovlp) then
               call ovlp_3int(gout(1,ithrd),&
                              bfiexps(1,ithrd),bficoefs(1,ithrd),bfinorms(1,ithrd),shells(ishl)%origin, &
                              bfila(1,ithrd),bfima(1,ithrd),bfina(1,ithrd), &
                              bfjexps(1,ithrd),bfjcoefs(1,ithrd),bfjnorms(1,ithrd),shells(jshl)%origin, &
                              bfjla(1,ithrd),bfjma(1,ithrd),bfjna(1,ithrd), &
                              bfkexps(1,ithrd),bfkcoefs(1,ithrd),bfknorms(1,ithrd),xshells(kshl)%origin,&
                              bfkla(1,ithrd),bfkma(1,ithrd),bfkna(1,ithrd), &
                              cnij(1,ithrd),cnk(1,ithrd),&
                              nprimi,nprimj,nprimk,&
                              ncarti,ncartj,ncartk,&
                              nprimij,nprimkl,nprimijk)
             else
               ! McMurchie Davidson scheme
               call mmd_3int(gout(1,ithrd),&
                             bfiexps(1,ithrd),bficoefs(1,ithrd),bfinorms(1,ithrd),shells(ishl)%origin,& 
                             bfila(1,ithrd),bfima(1,ithrd),bfina(1,ithrd), &
                             bfjexps(1,ithrd),bfjcoefs(1,ithrd),bfjnorms(1,ithrd),shells(jshl)%origin,&
                             bfjla(1,ithrd),bfjma(1,ithrd),bfjna(1,ithrd), &
                             bfkexps(1,ithrd),bfkcoefs(1,ithrd),bfknorms(1,ithrd),xshells(kshl)%origin,&
                             bfkla(1,ithrd),bfkma(1,ithrd),bfkna(1,ithrd), &
                             eabt(1,1,ithrd),eabu(1,1,ithrd),eabv(1,1,ithrd),eabtuv(1,1,ithrd),cnij(1,ithrd),&
                             ecdt(1,1,ithrd),ecdu(1,1,ithrd),ecdv(1,1,ithrd),ecdtuv(1,1,ithrd),cnk(1,ithrd),&
                             Fn(1,ithrd),rtuv(1,ithrd),gtuv(1,ithrd),gcdtuv(1,ithrd),ftuv(1,ithrd),pfac,&
                             alpha,dx,dy,dz,ammax,lmax,&
                             nprimi,nprimj,nprimk,&
                             ncarti,ncartj,ncartk,&
                             nprimij,nprimkl,nprimijk,&
                             cpur,cpuf,cpug1,cpug2,cpug3,&
                             memetuv,mxcart) 
             end if 
           else
             call quit("Currently only Mc-Murchie Davidson scheme for 3 index integrals!","get3ints")
           end if

           deallocate(pfac,alpha,dx,dy,dz)
         else
           gout(:,ithrd) = zero
         end if

         !---------------------------------------------------------------+
         ! copy local integral to full set
         !---------------------------------------------------------------+
         do kcart = 1, ncartk
           kbf = kcart + kbfst - 1
           do jcart = 1, ncartj
             jbf = jcart + jbfst - 1
             do icart = 1, ncarti
               ibf = icart + ibfst - 1
  
               iadr = (kcart-1) * ncartj * ncarti &
                    + (jcart-1) * ncarti + icart 

               xbqint(kbf,ibf,jbf) = gout(iadr,ithrd)
               xbqint(kbf,jbf,ibf) = gout(iadr,ithrd)
             end do 
           end do
         end do

       end do
       deallocate(xyzp,Kab)
     end do
   end do 
   !$OMP END PARALLEL DO

   bfi => null()
   bfj => null()
   bfk => null()

   deallocate(rab2)

   call deallocate_boystable(boystab)

end subroutine
