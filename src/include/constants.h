 real(kdp), parameter :: pi = 3.14159265359d0
 real(kdp), parameter :: twopi = 6.2831853d0

 real(kdp), parameter :: MACHEP = 0.000000000000000111022302462516
 real(kdp), parameter :: MAXLOG = 709.78271289338

 real(kdp), parameter :: ldpthrs = 0.000000001
