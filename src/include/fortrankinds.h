!-----------------------------------------------------------------------
! kind parameter for fortran variables
! .....................................
!
! kdp : kind value for a real with double precision accuracy
!       (what ever this means for the compiler used... note that
!        it is NOT guaranteed that this is 8 bytes)
!
! kli : kind value for a long integer
!
!-----------------------------------------------------------------------

      integer, parameter :: kdp=kind(13.0d0)
      integer, parameter :: ksp=4

      integer, parameter :: kli=8

