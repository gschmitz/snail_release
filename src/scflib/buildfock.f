 subroutine buildfock(timer,ldirect,fock,fock2bod,diffdens, orb, &
                      gab,intbuf,iter,tolint,hone,&
                      basis,jkbas,use_rijk,gint,nocc,nbf,nspin,memavail,mthreads)

!---------------------------------------------------------------------+
! Purpose: Builds the Fock matrix using the core hamiltonian hone
!          and the coulomb and exchange contribution in gint
!
!---------------------------------------------------------------------+
  
   use type_basis
   use type_int4idx
   use IntegralBuffer
   use type_timer

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   ! dimensions
   integer, intent(in) :: nspin
   integer, intent(in) :: nocc(nspin),nbf,mthreads

   ! output
   real(kdp), intent(out)   :: fock(nbf,nbf,nspin)

   ! Integral buffer
   type(TIntBuffer), intent(inout) :: intbuf(0:mthreads-1)

   ! timer
   type(timer_info), intent(inout) :: timer

   ! input
   real(kdp), intent(inout) :: hone(nbf,nbf),           &
                               gint(nbf,nbf,nspin),     &
                               fock2bod(nbf,nbf,nspin), &
                               diffdens(nbf,nbf,nspin), &
                               orb(nbf,nbf,nspin)
   real(kdp), intent(in)    :: tolint
   logical,   intent(in)    :: ldirect,use_rijk

   type(basis_info), intent(in) :: basis,jkbas

   real(kdp), intent(in) :: gab(basis%nshell,basis%nshell)
   integer,   intent(in) :: iter

   integer(kli),     intent(in) :: memavail

   ! local
   integer   :: ibf,jbf,ncbf,imode,ispin,imdst,imdnd
   integer   :: lmax,nshell,mxprim,mxcart,nscreen
   logical   :: have_schwartz 
   real(kdp) :: sfac

   real(kdp), allocatable :: jmn(:,:),kmn(:,:),jab(:,:,:)

   ! dummy for 4-index integrals 
   type(int4idx) :: twoeint

   ! init
   ncbf = basis%ncbf

   if (ldirect) then
     nshell = basis%nshell
     mxprim = basis%mxprim
     mxcart = basis%mxcart
     lmax   = basis%lmax
     nscreen = 0

     have_schwartz = iter > 1

     ispin = 1

     if (.not.use_rijk) then
       !-----------------------------------------------------------------+
       ! Build  2J - K contribution incrementally
       !-----------------------------------------------------------------+

       if (nspin > 1) then
         allocate(jab(nbf,nbf,nspin))
         jab = zero
       end if

       ! 
       ! Get Fock matrix contributions from integrals
       ! 
       call timer_exec(timer,'init','(mn|rs)')
       if (nspin == 2) then
         do ispin = 1, nspin
           ! Ja/Jb
           call gettwoeints(twoeint,intbuf,jab(1,1,ispin),.true.,2,.true.,.true.,gab,have_schwartz,&
                            diffdens(1,1,ispin),tolint,nscreen,memavail,&
                            lmax,basis%shells,1,.false.,ncbf,nbf,nshell,mxprim,mxcart,mthreads)
           ! K
           call gettwoeints(twoeint,intbuf,fock2bod(1,1,ispin),.true.,3,.true.,.true.,gab,have_schwartz,&
                            diffdens(1,1,ispin),tolint,nscreen,memavail,&
                            lmax,basis%shells,1,.false.,ncbf,nbf,nshell,mxprim,mxcart,mthreads)
           
         end do

         do ispin = 1, nspin 
           call daxpy(nbf*nbf,one,jab(1,1,1),1,fock2bod(1,1,ispin),1)
           call daxpy(nbf*nbf,one,jab(1,1,2),1,fock2bod(1,1,ispin),1)
         end do

       else
         ispin = 1

         ! calculate the 2 body fock matrix contribution 
         ! imode 
         !--------
         !  1 => J + K
         !  2 => J
         !  3 => K
         imdst = 1
         imdnd = 1
 
         ! At least for the molecular size doable with the snail integral code it does not pay off
         ! to calculate Coulomb and Exchange separately... Maybe in the future...
         do imode = imdst, imdnd
           call gettwoeints(twoeint,intbuf,fock2bod(1,1,ispin),.true.,imode,.true.,.false.,gab,have_schwartz,&
                            diffdens(1,1,ispin),tolint,nscreen,memavail,&
                            lmax,basis%shells,1,.false.,ncbf,nbf,nshell,mxprim,mxcart,mthreads)
         end do
       end if
       call timer_exec(timer,'measure','(mn|rs)')
 
       ! 
       ! Symmetrize to get final Fock matrix 
       ! 
       do ispin = 1, nspin
         ! NOTE: the result in fock2bod needs to be symmetrized!!
 
         ! Therefore build final fock as:
         ! F_ij = h_ij + 0.5 * (0.5 (f2b_ij + f2b_ji))
         call dcopy(nbf*nbf,hone,1,fock(1,1,ispin),1)

         call daxpy(nbf*nbf,0.25d0,fock2bod(1,1,ispin),1,fock(1,1,ispin),1)
         
         !$OMP PARALLEL DO SCHEDULE(AUTO) &
         !$OMP DEFAULT(NONE) & 
         !$OMP SHARED(nbf,fock2bod,fock,ispin) &
         !$OMP PRIVATE(jbf)
         do jbf = 1, nbf
           call daxpy(nbf,0.25d0,fock2bod(jbf,1,ispin),nbf,fock(1,jbf,ispin),1)
         end do
         !$OMP END PARALLEL DO
       end do 

       if (nspin > 1) deallocate(jab)
     else
       allocate(jmn(nbf,nbf),kmn(nbf,nbf))

       do ispin = 1, nspin
         call rijkengine(twoeint,jmn,.true.,kmn,.true.,basis,jkbas,tolint,&
                         diffdens(1,1,ispin),orb(1,1,ispin),memavail,nocc(ispin),nbf)      

         ! F_ij <- h_ij
         call dcopy(nbf*nbf,hone,1,fock(1,1,ispin),1)

         ! F^2b_ij += J_ij; UHF case: F^2b_ij += J_ij^a + J_ij^b 
         call daxpy(nbf*nbf, one,jmn,1,fock2bod(1,1,1),1)
         if (nspin > 1) call daxpy(nbf*nbf, one,jmn,1,fock2bod(1,1,2),1)

         ! F^2b_ij -= 0.5 K_ij UHF case: F^2b_ij -= K_ij
         call daxpy(nbf*nbf,-half,kmn,1,fock2bod(1,1,ispin),1)
       end do

       ! Final Fock Matrix 
       call daxpy(nbf*nbf*nspin,one,fock2bod,1,fock,1)
     end if

   else
     !-----------------------------------------------------------------+
     ! Build Fock matrix
     !-----------------------------------------------------------------+
     do ispin = 1, nspin 
       do jbf = 1, nbf
         do ibf = 1, nbf
           fock(ibf,jbf,ispin) = hone(ibf,jbf) + gint(ibf,jbf,ispin)
         end do
       end do
     end do
   end if
 end subroutine

