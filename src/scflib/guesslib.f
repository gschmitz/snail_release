
 subroutine getcoreguess(ldirect,orb,hone,oldfock,xao2o,xao2ot,nbf)
!-------------------------------------------------------------------------------+
! Purpose: Calculates the Hamilton core guess. This means the bare nuclei,
!          without any electrons.
!-------------------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   ! dimensions
   integer, intent(in) :: nbf

   ! input
   real(kdp), intent(in)  :: hone(nbf,nbf),xao2o(nbf,nbf),xao2ot(nbf,nbf)
   logical,   intent(in)  :: ldirect

   ! output
   real(kdp), intent(out) :: orb(nbf,nbf) 
   real(kdp), intent(out) :: oldfock(nbf,nbf)

   real(kdp), allocatable :: work(:,:),eigval(:),orb0(:,:),honep(:,:)

   allocate(work(nbf,nbf),eigval(nbf),orb0(nbf,nbf),honep(nbf,nbf))

   if (ldirect) then
     call dcopy(nbf*nbf,hone,1,oldfock,1)
   end if

   call orthobas(xao2ot, hone, xao2o, honep, work,nbf)

   call diagfock(honep, orb0, eigval,nbf)

   ! orb = X x orb0
   call dgemm('n', 'n', nbf, nbf, nbf, one, xao2o, nbf, orb0, nbf, zero, orb, nbf)

   deallocate(work,eigval,orb0,honep)
 end subroutine

 subroutine getgwhguess(ldirect,orb,hone,ovlp,fock,oldfock,xao2o,xao2ot,nbf)
!-------------------------------------------------------------------------------+
! Purpose: Calculates the Generalized Wolfsberg-Helmholtz guess.
!
!            F_{mn} = K S_{mn} (H_{mm} + H_{nn})/2;  K = 1.75
!
!  It is a simplified extended Hueckel guess and hopefully more accurate
!  than the hamilton core guess
!-------------------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   integer, intent(in) :: nbf

   ! input
   real(kdp), intent(in)  :: hone(nbf,nbf),xao2o(nbf,nbf),xao2ot(nbf,nbf),ovlp(nbf,nbf)

   ! output
   real(kdp), intent(out) :: orb(nbf,nbf) 
   real(kdp), intent(out) :: oldfock(nbf,nbf)

   ! input/output
   logical,   intent(in)    :: ldirect
   real(kdp), intent(inout) :: fock(nbf,nbf)

   ! local
   integer :: ibf,jbf

   real(kdp), allocatable :: work(:,:),eigval(:),orb0(:,:),fockp(:,:)

   allocate(work(nbf,nbf),eigval(nbf),orb0(nbf,nbf),fockp(nbf,nbf))

   do jbf = 1, nbf
     do ibf = 1, jbf-1
       fock(ibf,jbf) = 0.875 * ovlp(ibf,jbf) * (hone(ibf,ibf) + hone(jbf,jbf))
       fock(jbf,ibf) = fock(ibf,jbf) 
     end do
     fock(jbf,jbf) = hone(jbf,jbf) 
   end do

   if (ldirect) then
     call dcopy(nbf*nbf,fock,1,oldfock,1)
     !call dcopy(nbf*nbf,hone,1,oldfock,1)
   end if  

   call orthobas(xao2ot, fock, xao2o, fockp, work,nbf)

   call diagfock(fockp, orb0, eigval, nbf)

   ! orb = X x orb0
   call dgemm('n', 'n', nbf, nbf, nbf, one, xao2o, nbf, orb0, nbf, zero, orb, nbf)

   deallocate(work,eigval,orb0,fockp)
 end subroutine
