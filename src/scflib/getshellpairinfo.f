
subroutine GetShellPairInfo(intopt,&                        ! Option for integral scheme
                            xyz, Kij, &                     ! Output for Obara Saika scheme
                            eabt,eabu,eabv,eabtuv,cnij,&    ! Output for McMurchie-Davidson scheme
                            shells, rab2, ishl, jshl,&      ! input
                            o2p,u,uA,uB,&                   ! scratch 
                            nprimi, nprimj, nshell, &
                            memetuv, ncarti, ncartj)

!------------------------------------------------------------------------+
!  Purpose: Precomputes shell information for different integral schemes
!------------------------------------------------------------------------+

   use type_integ
   use type_cgto
   use type_shell

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   ! dimensions
   integer,      intent(in) :: nprimi, nprimj, nshell, ncarti, ncartj
   integer(kli), intent(in) :: memetuv(2)

   ! output
   real(kdp), intent(out) :: xyz(3,nprimi,nprimj),&
                             Kij(nprimi,nprimj)

   real(kdp), intent(out) :: eabt(memetuv(1),ncarti*ncartj),&
                             eabu(memetuv(1),ncarti*ncartj),&
                             eabv(memetuv(1),ncarti*ncartj),&
                             eabtuv(memetuv(2),ncarti*ncartj),&
                             cnij(nprimi*nprimj,ncarti*ncartj) 

   ! input
   type(integ_info), intent(in)   :: intopt
   type(shell_info), intent(in)   :: shells(nshell)
   integer,          intent(in)   :: ishl, jshl 
   real(kdp),        intent(in)   :: rab2(nshell*(nshell+1)/2)

   ! input/output
   real(kdp),        intent(inout) :: o2p(nprimi*nprimj),u(nprimi*nprimj),&
                                      uA(nprimi*nprimj),uB(nprimi*nprimj)

   ! local
   integer   :: iprim, jprim, icomp, jcomp, ncompi, ncompj,&
                ibfst, ibfnd, jbfst, jbfnd, ijcomp, &
                icart,jcart,ijcart,ijprim,nprimij
   real(kdp) :: alphai, alphaj, zeta, abinv, rij2
   real(kdp) :: distij(3),Kab,di,dj

   integer ::  la, lb, lmax, &
               ma, mb, mmax, & 
               na, nb, nmax, &
               totangm, ncartij 

   integer :: angma,angmb,mxcarta,mxcartb,nbfloc,nbfsloc   

   integer, allocatable :: cartcompi(:,:),cartcompj(:,:)
   
 
   type(cgto_info), pointer :: bfi, bfj
   type(shell_info)         :: shellij,shellj0

   ! functions
   real(kdp) :: product_center_1D

   nprimij = nprimi * nprimj

   ! pre-calculate the product of the primitive norm and coefficients for this shell pair
   if ( intopt%scheme == INT_MMDET ) then
     !
     ! Create new shell with combined angular momenta 
     !

     ! sum of angular momenta
     totangm = shells(ishl)%angm + shells(jshl)%angm 
     ! number of cartesian components in shell (i+j 0|
     ncartij = (totangm + 1) * (totangm + 2) / 2

     ! some dummies
     nbfloc  = 0; nbfsloc = 0

     ! Create shell(s)
     call allocate_shell(shellij,totangm,shells(ishl)%iatom,shells(ishl)%atom,shells(ishl)%atominfo,shells(ishl)%basisname,&
                         shells(ishl)%gtos(1)%exps,shells(ishl)%gtos(1)%coefs,shells(ishl)%origin,&
                         nbfloc,nbfsloc,shells(ishl)%nprim)

     call allocate_shell(shellj0,0,shells(jshl)%iatom,shells(jshl)%atom,shells(jshl)%atominfo,shells(jshl)%basisname,&
                         shells(jshl)%gtos(1)%exps,shells(jshl)%gtos(1)%coefs,shells(jshl)%origin,&
                         nbfloc,nbfsloc,shells(jshl)%nprim)

     ! Precalculate Norms x Contraction coefficients
     do ijcart = 1, ncartij
       do jprim = 1, nprimj
         dj = shellj0%gtos(1)%coefs(jprim) * shells(jshl)%gtos(1)%norms(jprim) 
         !write(6,*) 'norm_j', shellj0%gtos(1)%norms(jprim)
         do iprim = 1, nprimi
           di = shellij%gtos(ijcart)%coefs(iprim) * shells(ishl)%gtos(1)%norms(iprim) 

           !write(6,*) 'norm_i', shellij%gtos(ijcart)%norms(iprim)
         
           ijprim = (jprim-1)*nprimi + iprim
           cnij(ijprim,ijcart) = di * dj
         end do
       end do
     end do

     ! destroy temporary shell again
     call deallocate_shell(shellij)
     call deallocate_shell(shellj0)
   else 

     do jcart = 1, ncartj
       do icart = 1, ncarti
         do jprim = 1, nprimj
           dj = shells(jshl)%gtos(jcart)%coefs(jprim) * shells(jshl)%gtos(jcart)%norms(jprim) 
           !write(6,*) 'norm_j', shells(jshl)%gtos(jcart)%norms(jprim)
           do iprim = 1, nprimi
             di = shells(ishl)%gtos(icart)%coefs(iprim) * shells(ishl)%gtos(icart)%norms(iprim) 
             !write(6,*) 'norm_i', shells(ishl)%gtos(icart)%norms(iprim)

             ijcart = (jcart-1)*ncarti + icart
             ijprim = (jprim-1)*nprimi + iprim

             cnij(ijprim,ijcart) = di * dj
           end do
         end do
       end do
     end do 
   end if

   if (intopt%scheme == INT_OS) then
     ! Obara-Saika Integral algorithm

     do jprim = 1, nprimj
       alphaj = shells(jshl)%gtos(1)%exps(jprim) ! since the bfs in one shell are only different in the angular momentum
                                                 ! we can extract the exponents from any bf in this shell...
       do iprim = 1, nprimi
         alphai = shells(ishl)%gtos(1)%exps(iprim)
         xyz(1,iprim,jprim) = product_center_1D(alphai, shells(ishl)%origin(1), alphaj, shells(jshl)%origin(1))
         xyz(2,iprim,jprim) = product_center_1D(alphai, shells(ishl)%origin(2), alphaj, shells(jshl)%origin(2))
         xyz(3,iprim,jprim) = product_center_1D(alphai, shells(ishl)%origin(3), alphaj, shells(jshl)%origin(3))
     
         zeta  = alphai + alphaj
         abinv = one / zeta
         rij2  = rab2(ishl*(ishl-1)/2 + jshl) 

         Kij(iprim,jprim) = dsqrt(2.0d0) * PI**1.25d0 * abinv * dexp(-alphai * alphaj * abinv * rij2)
      end do
     end do
       
   else if (intopt%scheme == INT_MMD .or. intopt%scheme == INT_MMDET ) then
     ! McMurchie-Davidson Integral algorithm

     !Kab = abinv * dexp(-alphai * alphaj * abinv * rij2)
     Kab = 1.d0 ! Preset it another time...

     ! calculate some shell specific information
     ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
     jbfst = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend

     distij = shells(ishl)%origin - shells(jshl)%origin

     !
     ! calculate Hermite expansion coefficients for shell pairs (ij) 
     !

     bfi => shells(ishl)%gtos(1)
     bfj => shells(jshl)%gtos(1)

     totangm = shells(ishl)%angm + shells(jshl)%angm
     if (intopt%scheme == INT_MMDET) then
       ncompi = (totangm + 1) * (totangm + 2) / 2  
       ncompj = 1

       angma = totangm  
       angmb = 0 
     else
       ncompi = ibfnd - ibfst + 1
       ncompj = jbfnd - jbfst + 1

       angma = shells(ishl)%angm 
       angmb = shells(jshl)%angm
     end if

     mxcarta = (angma+1)*(angma+2)/2
     mxcartb = (angmb+1)*(angmb+2)/2

     allocate(cartcompi(3,mxcarta),&
              cartcompj(3,mxcartb) )

     call shell_getcartcomp(cartcompi,angma)
     call shell_getcartcomp(cartcompj,angmb)

     do icomp = 1, ncompi
       la = cartcompi(1,icomp)
       ma = cartcompi(2,icomp)
       na = cartcompi(3,icomp)

       do jcomp = 1, ncompj
         ijcomp = (jcomp-1)*ncompi + icomp

         lb = cartcompj(1,jcomp)
         mb = cartcompj(2,jcomp)
         nb = cartcompj(3,jcomp)

         lmax = la + lb
         mmax = ma + mb
         nmax = na + nb

         call ecof_tij(eabt(1,ijcomp),la,lb,lmax,distij(1),bfi%exps,bfj%exps,Kab,o2p,u,uA,uB,nprimi*nprimj,nprimi,nprimj)
         call ecof_tij(eabu(1,ijcomp),ma,mb,mmax,distij(2),bfi%exps,bfj%exps,Kab,o2p,u,uA,uB,nprimi*nprimj,nprimi,nprimj)
         call ecof_tij(eabv(1,ijcomp),na,nb,nmax,distij(3),bfi%exps,bfj%exps,Kab,o2p,u,uA,uB,nprimi*nprimj,nprimi,nprimj)

         ! Combine to one tensor
         call BuildETensor(eabtuv(1,ijcomp),eabt(1,ijcomp),eabu(1,ijcomp),eabv(1,ijcomp),  &
                           la,lb,ma,mb,na,nb,lmax,mmax,nmax,nprimij)

       end do
     end do

     deallocate(cartcompi,cartcompj)
   else if (intopt%scheme == INT_HUZ ) then
     ! NOP
   else
     call quit("Unknown integral scheme!","getshellpair")
   end if

end subroutine
