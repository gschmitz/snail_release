
subroutine CopyShellPairInfo(intopt,&                        ! Option for integral scheme
                             xyz,Kij,eabt,eabu,eabv,cnij,&   
                             xyz_in,Kij_in,eabt_in,eabu_in,eabv_in,cnij_in,&
                             nprimi,nprimj,memetuv,ncarti,ncartj)

!------------------------------------------------------------------------+
!  Purpose: Precomputes shell information for different integral schemes
!------------------------------------------------------------------------+

   use type_integ
   use type_cgto
   use type_shell

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   ! dimensions
   integer,      intent(in) :: nprimi, nprimj, ncarti, ncartj
   integer(kli), intent(in) :: memetuv

   ! output
   real(kdp), intent(out) :: xyz(3,nprimi,nprimj),&
                             Kij(nprimi,nprimj)

   real(kdp), intent(out) :: eabt(memetuv,ncarti*ncartj),&
                             eabu(memetuv,ncarti*ncartj),&
                             eabv(memetuv,ncarti*ncartj),&
                             cnij(nprimi*nprimj,ncarti*ncartj) 
   ! input to copy
   real(kdp), intent(in) :: xyz_in(3,nprimi,nprimj),&
                            Kij_in(nprimi,nprimj)

   real(kdp), intent(in) :: eabt_in(memetuv,ncarti*ncartj),&
                            eabu_in(memetuv,ncarti*ncartj),&
                            eabv_in(memetuv,ncarti*ncartj),&
                            cnij_in(nprimi*nprimj,ncarti*ncartj) 

   ! input
   type(integ_info), intent(in)   :: intopt

   ! local
   integer   :: iprim, jprim, icomp, jcomp, ncompi, ncompj,&
                ibfst, ibfnd, jbfst, jbfnd, ijcomp, &
                icart,jcart,ijcart,ijprim
   real(kdp) :: alphai, alphaj, zeta, abinv, rij2
   real(kdp) :: distij(3),Kab,di,dj
 
   type(cgto_info), pointer :: bfi, bfj

   ! functions
   real(kdp) :: product_center_1D

   ! copy the product of the primitive norm and coefficients for this shell pair
   call dcopy(nprimi*nprimj*ncarti*ncartj,cnij_in,1,cnij,1)

   if (intopt%scheme == INT_OS) then
     ! Obara-Saika Integral algorithm

     ! product centers
     call dcopy(3*nprimi*nprimj,xyz_in,1,xyz,1)

     ! pre exponential factor
     call dcopy(nprimi*nprimj,Kij_in,1,Kij,1)     
  
   else if (intopt%scheme == INT_MMD) then
     ! McMurchie-Davidson Integral algorithm

     ! copy E coefficients
     call dcopy(memetuv*ncarti*ncartj,eabt_in,1,eabt,1)
     call dcopy(memetuv*ncarti*ncartj,eabu_in,1,eabu,1)
     call dcopy(memetuv*ncarti*ncartj,eabv_in,1,eabv,1)

   else
     call quit("Unknown integral scheme!","copyshellpairinfo")
   end if

end subroutine
