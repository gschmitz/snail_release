
 subroutine getoneeints(ovlp,hone,shells,atoms,nbf,natoms,nshell,mxcart)
!-----------------------------------------------------------------------------+
!  Purpose: Calculates the one electron integrals
!
!          hone => 1-el hamiltonian: kinetic + electron-nuclear attraction
!
!          ovlp => overlap of basis functions 
!
!-----------------------------------------------------------------------------+

   use type_shell
   use type_cgto
   use type_atom

   implicit none

   include 'fortrankinds.h'

   ! constants
   logical, parameter :: use_new = .true.

   ! dimensions
   integer, intent(in) :: nbf, natoms, nshell, mxcart

   ! input
   type(shell_info), intent(in) :: shells(nshell)
   type(atom_info),  intent(in) :: atoms(natoms)

   ! output
   real(kdp), intent(out) :: ovlp(nbf,nbf),hone(nbf,nbf)
 
   ! local
   integer   :: ibf,jbf,iat,ishl,jshl,ibfst,jbfst,ibfnd,jbfnd,icomp,jcomp

   real(kdp), allocatable :: ints(:,:),sout(:,:),kout(:,:),anorms(:,:),bnorms(:,:)

   real(kdp) :: cna,cnb,alpha,beta,zeta,xyzp(3)
   integer   :: icart,jcart,ncarta,ncartb
   integer   :: ang_a,ang_b,la,ma,na,lb,mb,nb,size_a,size_b
   integer   :: iprim,jprim,nprimi,nprimj

   integer,   allocatable :: cartcompi(:,:),cartcompj(:,:)

   ! functions
   real(kdp) :: product_center_1D

   do jshl = 1, nshell
     jbfst  = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend
     nprimj = shells(jshl)%nprim
     ncartb = shells(jshl)%ncart
     ang_b  = shells(jshl)%angm 

     do ishl = 1, nshell
       ibfst  = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
       nprimi = shells(ishl)%nprim     
       ncarta = shells(ishl)%ncart
       ang_a  = shells(ishl)%angm 
 
       if (.not.use_new) then
         do jbf = jbfst, jbfnd
           jcomp = jbf - jbfst + 1

           do ibf = ibfst, ibfnd
             icomp = ibf - ibfst + 1

             hone(ibf,jbf) = cgto_kinetic( shells(ishl)%gtos(icomp) , &
                                           shells(jshl)%gtos(jcomp) ) 

             ovlp(ibf,jbf) = cgto_overlap( shells(ishl)%gtos(icomp) , &
                                             shells(jshl)%gtos(jcomp) )
             do iat = 1, natoms
               hone(ibf,jbf) = hone(ibf,jbf) + atoms(iat)%charge * &
                               cgto_nuclear( shells(ishl)%gtos(icomp) , &
                                             shells(jshl)%gtos(jcomp) , &
                                           atoms(iat)%xyz )
             end do
           end do
         end do

       else  

         !
         ! Information on angular momenta per basis function in shell
         !
         allocate(cartcompi(3,ncarta),&
                  cartcompj(3,ncartb) )

         call shell_getcartcomp(cartcompi,ang_a)
         call shell_getcartcomp(cartcompj,ang_b)

         allocate(sout(ncarta,ncartb),kout(ncarta,ncartb),anorms(nprimi,ncarta),bnorms(nprimj,ncartb))

         ! norms in shell i
         do icart = 1, ncarta
           do iprim = 1, nprimi
             anorms(iprim,icart) = shells(ishl)%gtos(icart)%norms(iprim)
           end do
         end do

         ! norms in shell j
         do jcart = 1, ncartb
           do jprim = 1, nprimj
             bnorms(jprim,jcart) = shells(jshl)%gtos(jcart)%norms(jprim)
           end do
         end do

         !
         ! Calculate Overlap for shell pair
         !
         call os_overlap_shell(sout,&
                               shells(ishl)%origin,shells(ishl)%gtos(1)%exps,shells(ishl)%gtos(1)%coefs,anorms,cartcompi,ang_a,&
                               shells(jshl)%origin,shells(jshl)%gtos(1)%exps,shells(jshl)%gtos(1)%coefs,bnorms,cartcompj,ang_b,&
                               nprimi,nprimj,ncarta,ncartb)
        
         call os_kinetic_shell(kout,&
                               shells(ishl)%origin,shells(ishl)%gtos(1)%exps,shells(ishl)%gtos(1)%coefs,anorms,cartcompi,ang_a,&
                               shells(jshl)%origin,shells(jshl)%gtos(1)%exps,shells(jshl)%gtos(1)%coefs,bnorms,cartcompj,ang_b,&
                               nprimi,nprimj,ncarta,ncartb)
         do jcart = 1, ncartb
           jbf = jbfst + jcart - 1
           do icart = 1, ncarta
             ibf = ibfst + icart - 1
             ovlp(ibf,jbf) = sout(icart,jcart)
             hone(ibf,jbf) = kout(icart,jcart)
           end do
         end do 
 
         deallocate(sout,kout,anorms,bnorms)

         !
         ! Calculate Nuclear attraction integral for shell pair
         !
         do iat = 1, natoms
           do jprim = 1, nprimj
             beta = shells(jshl)%gtos(1)%prim(jprim)%expo 

             do iprim = 1, nprimi
               alpha = shells(ishl)%gtos(1)%prim(iprim)%expo

               zeta = alpha + beta

               size_a = (ang_a+1)*(ang_a+2)/2
               size_b = (ang_b+1)*(ang_b+2)/2

               allocate(ints(size_a, size_b))
 
               xyzp(1) = product_center_1D(alpha,shells(ishl)%origin(1),beta,shells(jshl)%origin(1))
               xyzp(2) = product_center_1D(alpha,shells(ishl)%origin(2),beta,shells(jshl)%origin(2))
               xyzp(3) = product_center_1D(alpha,shells(ishl)%origin(3),beta,shells(jshl)%origin(3))

               call os_nuclear_shell(ints,shells(ishl)%origin,shells(jshl)%origin, &
                                     atoms(iat)%xyz,xyzp,ang_a,ang_b,alpha,beta,zeta) 

               do jcart = 1, ncartb
                 jbf = jbfst + jcart - 1
                 cnb = shells(jshl)%gtos(jcart)%prim(jprim)%coef &
                     * shells(jshl)%gtos(jcart)%prim(jprim)%norm

                 do icart = 1, ncarta
                   ibf = ibfst + icart - 1
                   cna = shells(ishl)%gtos(icart)%prim(iprim)%coef &
                       * shells(ishl)%gtos(icart)%prim(iprim)%norm
                 
                   hone(ibf,jbf) = hone(ibf,jbf) &
                                 + atoms(iat)%charge * cna * cnb * ints(icart,jcart)                
                 end do
               end do             

               deallocate(ints)
             end do
           end do

         end do

         deallocate(cartcompi,cartcompj)

       end if
     end do
   end do

 end subroutine 

 subroutine getovlp(ovlp,shells,nbf,nshell)
!-----------------------------------------------------------------------------+
!  Purpose: Calculates AO-Overlap 
!
!          ovlp => overlap of basis functions 
!
!-----------------------------------------------------------------------------+

   use type_shell
   use type_cgto

   implicit none

   include 'fortrankinds.h'

   ! constants
   logical, parameter :: use_new = .true.

   ! dimensions
   integer, intent(in) :: nbf,nshell

   ! input
   type(shell_info), intent(in) :: shells(nshell)

   ! output
   real(kdp),       intent(out) :: ovlp(nbf,nbf)

   ! local
   integer :: ibf,jbf,iat,ishl,jshl,ibfst,jbfst,ibfnd,jbfnd,icomp,jcomp

   real(kdp), allocatable :: sout(:,:),anorms(:,:),bnorms(:,:)

   real(kdp) :: alpha,beta,zeta
   integer   :: icart,jcart,ncarta,ncartb
   integer   :: ang_a,ang_b
   integer   :: iprim,jprim,nprimi,nprimj

   integer,   allocatable :: cartcompi(:,:),cartcompj(:,:)

   do jshl = 1, nshell
     jbfst = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend
     nprimj = shells(jshl)%nprim
     ncartb = shells(jshl)%ncart
     ang_b  = shells(jshl)%angm 

     do ishl = 1, nshell
       ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
       nprimi = shells(ishl)%nprim     
       ncarta = shells(ishl)%ncart
       ang_a  = shells(ishl)%angm 
     
       if (use_new) then
         !
         ! Information on angular momenta per basis function in shell
         !
         allocate(cartcompi(3,ncarta),&
                  cartcompj(3,ncartb) )

         call shell_getcartcomp(cartcompi,ang_a)
         call shell_getcartcomp(cartcompj,ang_b)

         allocate(sout(ncarta,ncartb),anorms(nprimi,ncarta),bnorms(nprimj,ncartb))

         ! norms in shell i
         do icart = 1, ncarta
           do iprim = 1, nprimi
             anorms(iprim,icart) = shells(ishl)%gtos(icart)%norms(iprim)
           end do
         end do

         ! norms in shell j
         do jcart = 1, ncartb
           do jprim = 1, nprimj
             bnorms(jprim,jcart) = shells(jshl)%gtos(jcart)%norms(jprim)
           end do
         end do

         !
         ! Calculate Overlap for shell pair
         !
         call os_overlap_shell(sout,&
                               shells(ishl)%origin,shells(ishl)%gtos(1)%exps,shells(ishl)%gtos(1)%coefs,anorms,cartcompi,ang_a,&
                               shells(jshl)%origin,shells(jshl)%gtos(1)%exps,shells(jshl)%gtos(1)%coefs,bnorms,cartcompj,ang_b,&
                               nprimi,nprimj,ncarta,ncartb)
        
         do jcart = 1, ncartb
           jbf = jbfst + jcart - 1
           do icart = 1, ncarta
             ibf = ibfst + icart - 1
             ovlp(ibf,jbf) = sout(icart,jcart)
           end do
         end do 
 
         deallocate(sout,anorms,bnorms,cartcompi,cartcompj)
         
       else  
         do jbf = jbfst, jbfnd
           jcomp = jbf - jbfst + 1

           do ibf = ibfst, ibfnd
             icomp = ibf - ibfst + 1

             ovlp(ibf,jbf) = cgto_overlap( shells(ishl)%gtos(icomp) , &
                                           shells(jshl)%gtos(jcomp) )
           end do
         end do
       end if
     end do
   end do

 end subroutine 

 subroutine getkinints(kin,shells,nbf,nshell,mxcart)
!-----------------------------------------------------------------------------+
!  Purpose: Calculates the kinetic one electron integrals
!
!-----------------------------------------------------------------------------+

   use type_shell
   use type_cgto
   use type_atom

   implicit none

   include 'fortrankinds.h'

   ! dimensions
   integer, intent(in) :: nbf, nshell, mxcart

   ! input
   type(shell_info), intent(in) :: shells(nshell)

   ! output
   real(kdp), intent(out) :: kin(nbf,nbf)
 
   ! local
   integer   :: ibf,jbf,ishl,jshl,ibfst,jbfst,ibfnd,jbfnd,icomp,jcomp


   do jshl = 1, nshell
     jbfst = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend

     do ishl = 1, nshell
       ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
      
       do jbf = jbfst, jbfnd
         jcomp = jbf - jbfst + 1

         do ibf = ibfst, ibfnd
           icomp = ibf - ibfst + 1

           kin(ibf,jbf) = cgto_kinetic( shells(ishl)%gtos(icomp) , &
                                        shells(jshl)%gtos(jcomp) ) 

         end do
       end do
 
     end do
   end do

 end subroutine 
