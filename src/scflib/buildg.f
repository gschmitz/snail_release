
 subroutine buildg(gint,twoeint,dens,nbf,nspin)
!---------------------------------------------------------------------+
! Purpose: Builds coulomb and exchange contribution 2J - K from the
!          4 index ERI integrals in the AO basis
!
!---------------------------------------------------------------------+

   use type_int4idx

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   ! constants
   integer, parameter :: ispin = 1

   integer, intent(in)          :: nbf,nspin

   real(kdp),     intent(inout) :: gint(nbf,nbf,nspin)
   real(kdp),     intent(in)    :: dens(nbf,nbf,nspin)
   type(int4idx), intent(in)    :: twoeint

   ! local
   integer   :: ibf,jbf,kbf,lbf,ind1,ind2
   real(kdp) :: dJ,dK,dKa,dKb,dval

   ! functions
   real(kdp) :: ddot

   if (nspin == 1) then
     !---------------------------------------------------------------+
     ! RHF part G = 2J - K
     !---------------------------------------------------------------+
     if (twoeint%lcompressed) then
       do ibf = 1, nbf
         do jbf = 1, nbf
           gint(ibf,jbf,ispin) = zero
           do kbf = 1, nbf
             do lbf = 1, nbf
               ind1 = int4idx_getind(ibf, jbf, lbf, kbf)
               ind2 = int4idx_getind(ibf, kbf, lbf, jbf)
               gint(ibf,jbf,ispin) = gint(ibf,jbf,ispin) + dens(kbf, lbf,ispin) * & 
                                      (twoeint%dintcomp(ind1) - 0.5d0 * twoeint%dintcomp(ind2))
             end do
           end do
         end do
       end do
     else

       !-----------------------------------------------------------+ 
       ! Build Coulomb J :
       ! 
       !   J_ij = (ij|kl) D_kl
       !   
       !-----------------------------------------------------------+ 
       call dgemm('n','n',nbf*nbf,1,nbf*nbf,one,twoeint%dintfull,nbf*nbf,&
                  dens(1,1,ispin),nbf*nbf,zero,gint(1,1,ispin),nbf*nbf)

       !-----------------------------------------------------------+ 
       ! Build and add Exchange K :
       ! 
       !   K_ij = (ik|lj) D_kl
       !   
       !-----------------------------------------------------------+ 
       do jbf = 1, nbf
         do ibf = 1, jbf
           ! Build Coulomb J
           !dJ = ddot(nbf*nbf,dens(1,1,ispin),1,twoeint%dintfull(ibf,jbf,1,1),nbf*nbf)

           ! Build Exchange K
           dK = ddot(nbf*nbf,dens(1,1,ispin),1,twoeint%dintfull(ibf, 1, 1, jbf),nbf)

           !dval = dJ - half * dK

           dval = gint(ibf,jbf,ispin) - half * dK   

           gint(ibf,jbf,ispin) = dval
           gint(jbf,ibf,ispin) = dval
         end do 
       end do
     end if

   else
     !---------------------------------------------------------------+
     ! UHF part G = J - K
     !---------------------------------------------------------------+
     if (twoeint%lcompressed) then
       do ibf = 1, nbf
         do jbf = 1, nbf
           gint(ibf,jbf,:) = zero
           do kbf = 1, nbf
             do lbf = 1, nbf
               ind1 = int4idx_getind(ibf, jbf, lbf, kbf)
               ind2 = int4idx_getind(ibf, kbf, lbf, jbf)
               ! alpha spin: 
               gint(ibf,jbf,1) = gint(ibf,jbf,1) + dens(kbf, lbf,1) * twoeint%dintcomp(ind1) &
                                                 + dens(kbf, lbf,2) * twoeint%dintcomp(ind1) &
                                                 - dens(kbf, lbf,1) * twoeint%dintcomp(ind2)
               ! beta spin: 
               gint(ibf,jbf,2) = gint(ibf,jbf,2) + dens(kbf, lbf,2) * twoeint%dintcomp(ind1) &
                                                 + dens(kbf, lbf,1) * twoeint%dintcomp(ind1) &
                                                 - dens(kbf, lbf,2) * twoeint%dintcomp(ind2)
             end do
           end do
         end do
       end do
     else
       do jbf = 1, nbf
         do ibf = 1, nbf 
           ! Build Coulomb J (alpha + beta spin)
           dJ =   ddot(nbf*nbf,dens(1,1,1),1,twoeint%dintfull(ibf,jbf,1,1),nbf*nbf) &
                + ddot(nbf*nbf,dens(1,1,2),1,twoeint%dintfull(ibf,jbf,1,1),nbf*nbf)

           ! Build Exchange K (alpha spin)
           dKa = ddot(nbf*nbf,dens(1,1,1),1,twoeint%dintfull(ibf, 1, 1, jbf),nbf)

           ! Build Exchange K (beta spin)
           dKb = ddot(nbf*nbf,dens(1,1,2),1,twoeint%dintfull(ibf, 1, 1, jbf),nbf)

           gint(ibf,jbf,1) = dJ - dKa
           gint(jbf,ibf,2) = dJ - dKb

         end do 
       end do
     end if

   end if
 end subroutine
