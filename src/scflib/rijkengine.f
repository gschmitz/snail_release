!-------------------------------------------------------------------------------------------------+
!  Engine for using RI approximation using the JK fitting basis:
!
!    Generation of
!    -------------
!      -> (mn|kl) AO Integrals (for testing)    ~ O(N^5)
!      -> J_mn Coulomb matrix                   ~ O(N^3)
!      -> K_mn Exchange matrix                  ~ O(N^4)
!
!
! Mote: Like nearly all routines in Snail no batching right now. 
!       If the integrals don't fit into memory it won't work.. 
!
! Gunnar Schmitz, June 2019
!-------------------------------------------------------------------------------------------------+

subroutine rijkengine(twoeint,Jmn,use_rij,Kmn,use_rik, &
                      basis,jkbas,tolint,dens,orb,memmax,nocc,nbf)  ! inp

  use type_cgto
  use type_shell
  use type_basis
  use type_int4idx

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! constants
  logical, parameter :: useOvlp = .false.
  logical, parameter :: verbose = .false.

  ! dimensions
  integer,           intent(in)    :: nocc,nbf

  ! input/output
  type(int4idx) ,    intent(inout) :: twoeint
  real(kdp),         intent(out)   :: Jmn(nbf,nbf),&
                                      Kmn(nbf,nbf)

  ! input
  type(basis_info),  intent(in)  :: basis
  type(basis_info),  intent(in)  :: jkbas
  real(kdp),         intent(in)  :: tolint
  integer(kli),      intent(in)  :: memmax
  logical,           intent(in)  :: use_rij, use_rik
  real(kdp),         intent(in)  :: dens(nbf,nbf), orb(nbf,nbf)

  ! local
  real(kdp), allocatable :: vpqmat(:,:),bqmn(:,:,:),Iqmn(:,:,:)
  real(kdp), allocatable :: xqmi(:,:,:),coeff(:,:),yqmi(:,:,:) 
  real(kdp), allocatable :: cq(:)

  integer :: ixbf,jxbf,imo,jmo,moa,mob
  integer :: nshell,mxcart,mxprim,lmax,nxbf,nxshell,lxmax,iocc
  integer :: info

  logical :: gen_aoint

  real(kdp)    :: cpuint,cpu0,cpux
  integer(kli) :: memused,memavail
  
  !-----------------------------------------------------------------------+
  ! define shortcuts 
  !-----------------------------------------------------------------------+
  nshell = basis%nshell
  mxprim = basis%mxprim
  mxcart = basis%mxcart
  lmax   = basis%lmax

  nxbf    = jkbas%nbf
  nxshell = jkbas%nshell
  lxmax   = jkbas%lmax

  mxcart = max(mxcart,jkbas%mxcart)
  mxprim = max(mxprim,jkbas%mxprim)

  !-----------------------------------------------------------------------+
  !  Build 2 index integrals
  !-----------------------------------------------------------------------+
  allocate(vpqmat(nxbf,nxbf))

  !
  ! Get (P|Q)
  !
  call cpu_time(cpu0)
  call get2ints(vpqmat,                                      & ! out
                .false.,tolint,memmax,lmax,jkbas%shells,     & ! inp 
                nxbf,nxshell,mxprim,mxcart)                    ! dim 
  call cpu_time(cpux)
  cpuint = cpux - cpu0
  if (verbose) write(istdout,'(5x,a,f10.2,/)')  'CPU-time for     (PQ) (sec):', cpuint

  !------------------------------------------------------------------------+
  ! Calculate Cholesky decomposition of (P|Q) = L L^T and than invert L 
  ! ... then follows (L^T)^{-1} L^{-1} = (P|Q)^{-1} 
  !------------------------------------------------------------------------+

  call dpotrf('L', nxbf, vpqmat, nxbf, info )
  call dtrtri('L', 'N', nxbf, vpqmat, nxbf, info)

  ! Zero out not referenced values in vpqmat...
  do jxbf = 1, nxbf
    do ixbf = 1, jxbf - 1
      vpqmat(ixbf,jxbf) = zero
    end do
  end do

  !-----------------------------------------------------------------------+
  !  Build 3 index integrals
  !-----------------------------------------------------------------------+
  allocate(Iqmn(nxbf,nbf,nbf))
  allocate(bqmn(nxbf,nbf,nbf))

  call cpu_time(cpu0)
  call get3ints(Iqmn,                                                 & ! out
                .false.,useOvlp,tolint,memmax,lmax,                   & ! inp
                jkbas%shells,basis%shells,                            & ! inp 
                nxbf,nbf,nxshell,nshell,mxprim,mxcart,mxprim,mxcart)    ! dim 
  call cpu_time(cpux)
  cpuint = cpux - cpu0
  if (verbose) write(istdout,'(5x,a,f10.2,/)')  'CPU-time for   (Q|mn) (sec):', cpuint

  call cpu_time(cpu0)
  !
  ! B(Q,mn) = (PQ)^(-1/2) I(Q,mn)
  !
  call dgemm( 'n', 'n', nxbf, nbf*nbf, nxbf, one  &
            , vpqmat, nxbf, Iqmn, nxbf, zero, bqmn, nxbf )

  gen_aoint = ((.not.use_rij) .and. (.not.use_rik))
 
  if (gen_aoint) then
    ! For testing generate the 4 index AO integrals using the JK fitting basis
 
    ! (mn|kl) = B(Q,mn) B(Q,kl)
    call dgemm( 't', 'n', nbf*nbf, nbf*nbf, nxbf, one &
              , bqmn, nxbf, bqmn, nxbf, zero, twoeint%dintfull, nbf*nbf) 
  end if

  if (use_rij) then
    allocate(cq(nxbf))

    !
    ! C_Q = \sum_kl B(Q,kl) D_kl    ~ O(N_x * N^2)
    !
    call dgemm( 'n', 'n', nxbf, 1, nbf*nbf, one &
              , bqmn, nxbf, dens, nbf*nbf, zero, cq, nxbf)

    !
    ! J_mn = \sum_Q C_Q B(Q,mn)     ~ O(N^2 * N_x)
    !
    call dgemm( 't', 'n', nbf*nbf, 1, nxbf, one  &
              , bqmn, nxbf, cq, nxbf, zero, Jmn, nbf*nbf)  
 
  end if

  if (use_rik) then
    allocate(xqmi(nxbf,nbf,nocc),coeff(nbf,nbf))

    call dcopy(nbf*nbf,orb,1,coeff,1)

    !
    ! X(Q,mi) = B(Q,mn) D_ni                ~ O(N_x * N^2 N_o)  
    !
    call dgemm( 'n', 'n', nxbf*nbf, nocc, nbf, one &
              , bqmn, nxbf*nbf, coeff, nbf, zero, xqmi, nxbf*nbf)

    !
    ! K_mn = \sum_Qi X(Q,mi) X(Q,ni)        ~ O(N_o N^2 * N_x) 
    !
    Kmn = zero
    do iocc = 1, nocc 
      call dgemm('t','n', nbf, nbf, nxbf, two &
                , xqmi(1,1,iocc), nxbf, xqmi(1,1,iocc), nxbf, one, Kmn, nbf)
    end do

  end if

  deallocate(vpqmat,bqmn)
end subroutine



