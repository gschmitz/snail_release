
subroutine copybasinfo(bfiexps, bficoefs, bfinorms, &  ! out: exps, norms, coefficients
                       bfila, bfima, bfina,         &  ! out: angular momenta
                       shells, ishl,                &  ! inp
                       nprim, ncart, nshell )          ! dim
!-----------------------------------------------------------------------------+
!  Purpose: Copies basis info from shell type to a good old array 
!-----------------------------------------------------------------------------+

   use type_cgto
   use type_shell

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   ! dimensions
   integer, intent(in) :: nshell,ncart,nprim

   ! input
   type(shell_info), intent(in)   :: shells(nshell)
   integer,          intent(in)   :: ishl

   ! output
   real(kdp), intent(out) :: bfiexps(nprim,ncart), bficoefs(nprim,ncart), bfinorms(nprim,ncart)
   integer,   intent(out) :: bfila(ncart), bfima(ncart), bfina(ncart) 

   ! local
   integer :: icart, iprim

   do icart = 1, ncart

     call dcopy(nprim,shells(ishl)%gtos(icart)%exps(1), 1,bfiexps(1,icart), 1) 
     call dcopy(nprim,shells(ishl)%gtos(icart)%coefs(1),1,bficoefs(1,icart),1)
     call dcopy(nprim,shells(ishl)%gtos(icart)%norms(1),1,bfinorms(1,icart),1)

     ! angular momenta
     bfila(icart) = shells(ishl)%gtos(icart)%la
     bfima(icart) = shells(ishl)%gtos(icart)%ma
     bfina(icart) = shells(ishl)%gtos(icart)%na
   end do

end subroutine


