

recursive subroutine quicksort(iarr,idx,istart,iend,ninfo,ndim)

  implicit none

  include 'fortrankinds.h'
  
  ! dimension:
  integer,   intent(in) :: ninfo,ndim
  
  ! input:
  integer,   intent(in) :: idx,istart,iend

  ! output:
  integer,   intent(inout) :: iarr(ninfo,ndim)
  
  ! local:
  integer :: i,j,h,x,info
 
  i = iStart
  j = iEnd
  x = iarr(idx,(iStart + iEnd) / 2)
 
  
  do while (i < j)
 
    do while (iarr(idx,i) < x)
      i = i + 1
    end do

    do while (iarr(idx,j) > x)
      j = j - 1
    end do
 
    if (i <= j) Then
      ! swap elements 
      do info = 1, ninfo
        h = iarr(info,i)
        iarr(info,i) = iarr(info,j)
        iarr(info,j) = h
      end do
      i = i + 1
      j = j - 1
    end if
  end do
 
  if (iStart < j) call quicksort( iarr, idx, iStart, j, ninfo, ndim)
  if (i < iEnd)   call quicksort( iarr, idx, i, iEnd, ninfo, ndim)
end subroutine quicksort

