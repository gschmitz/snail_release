subroutine PrintHeader(text)

  implicit none

  include 'stdout.h'

  character(*),   intent(in) :: text
  
  write(istdout,'(/1x,a)') repeat('-',70)
  write(istdout,'(/12x,a)') text 
  write(istdout,'(/1x,a,/)') repeat('-',70)

end
