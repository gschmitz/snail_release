
!--------------------------------------------------------------+
! Performs an orthogonalization using Cholesky decomposition
!
!
!--------------------------------------------------------------+

subroutine cholortho(umat,vmat,ndim)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,  intent(in) :: ndim

  ! output
  real(kdp), intent(out) :: umat(ndim,ndim)

  ! input
  real(kdp), intent(in)  :: vmat(ndim,ndim)

  ! local
  integer :: idx, jdx, info
  real(kdp), allocatable :: xmat(:,:)


  !-----------------------------------------------------------------+
  ! We want to diagoanlize the matrix V. 
  !
  !    V^T V
  !
  ! is positive definite and we can calculate 
  ! 
  !    V^T V = L L^T
  !
  ! after some rearragnements we can form an orthonormal matrix
  !
  !    U = V (L^-1)^T
  !
  ! which spans the same space as V
  !
  !-----------------------------------------------------------------+
  
  allocate(xmat(ndim,ndim))

  ! V^T V
  call dgemm( 't', 'n', ndim, ndim, ndim, one  &
            , vmat, ndim, vmat, ndim, zero, xmat, ndim ) 

  ! V^T V = L L^T
  call dpotrf('L', ndim, xmat, ndim, info ) 

  if (info /= 0) call quit("Problem in dpotrf','cholortho")

  ! Zero out not referenced values ...
  do jdx = 1, ndim
    do idx = 1, jdx - 1
      xmat(idx,jdx) = zero
    end do
  end do

  ! invert L
  call dtrtri('L', 'N', ndim, xmat, ndim, info)

  if (info /= 0) call quit("Problem in dtrtri','cholortho")

  ! Build orthogonal output matrix V (L^-1)^T
  call dgemm( 'n', 't', ndim, ndim, ndim, one  &
            , vmat, ndim, xmat, ndim, zero, umat, ndim ) 

  deallocate(xmat)
end subroutine
