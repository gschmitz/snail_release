!------------------------------------------------------------------+
! Purpose: Expands an integer range in a list 
!
!  Example:

!        Input   |  Output
!        ------------------
!         1,5-7    1,5,6,7
!
!  Input: text character string containing the index range list
!
! Taken and adapted from Rosetta Code: 
!   https://www.rosettacode.org/wiki/Range_expansion#Fortran
!
!------------------------------------------------------------------+
character(len=200) function ExpandRange(text)

  implicit none

  ! scratch
  character(len=200) :: aline  

  ! input 
  character(*), intent(in) :: text         

  ! local
  integer :: n,n1,n2    ! numbers in a range.
  integer :: i,i1       ! steppers.

  integer :: l
  logical :: locdum

  aline = ""

  l = 0 ! no text has been placed.
  i = 1 ! start at the start.

  call forasign ! find something to look at.

  loop1: do while(eatint(n1))              ! if i can grab a first number, a term has begun.
      n2 = n1                              ! make the far end the same.
      if (passby("-")) locdum = eatint(n2) ! a hyphen here is not a minus sign.
      if (l.gt.0) call emit(",")           ! another, after what went before?
      do n = n1,n2,sign(+1,n2 - n1)        ! step through the range, possibly backwards.
        call splot(n)                      ! roll a number.
        if (n.ne.n2) call emit(",")        ! perhaps another follows.
      end do                               ! on to the next number.
      if (.not.passby(",")) exit loop1
    end do loop1

    ExpandRange = aline(1:l) 

    !--------------------------------------------------------------------------------------------+
     contains ! some assistants for the scan to save on repetition and show intent.
    !--------------------------------------------------------------------------------------------+

    !
    ! Find new position to check
    !
    subroutine forasign             
  1   if (i.le.len(text)) then      
        if (text(i:i).le." ") then  
          i = i + 1                 
          go to 1                   
        end if                      
      end if                        
    end subroutine forasign         

    !
    ! advances the scan if a certain character is detected
    ! 
    logical function passby(c) 

      ! input
      character(len=1), intent(in) :: c

      passby = .false.

      if (i.le.len(text)) then
        if (text(i:i).eq.c) then 
          passby = .true.
          i = i + 1
          call forasign
        end if
      end if
    end function passby
 
    ! 
    ! convert text into an integer
    !
    logical function eatint(n)

      ! input/output
      integer, intent(inout) :: n

      ! local
      integer d
      logical neg

      eatint = .false.
      if (i.gt.len(text)) return
      n = 0
      if (passby("+")) then
        neg = .false.
       else
        neg = passby("-")
      end if

      if (i.gt.len(text)) return

 10   d = ichar(text(i:i)) - ichar("0")
      if (0.le.d .and. d.le.9) then
        n = n*10 + d
        i = i + 1
        if (i.le.len(text)) go to 10
      end if
      if (neg) n = -n
      eatint = .true.
      call forasign
    end function eatint
 
    !
    ! Set one character in output character string
    ! 
    subroutine emit(c) 
      
      ! input
      character(len=1), intent(in) :: c

      l = l + 1 
      if (l.gt.len(aline)) stop "ran out of aline!"
      aline(l:l) = c
    end subroutine emit
 
    !
    ! write signed number to output character string
    !
    subroutine splot(n)

      ! input
      integer, intent(in) :: n

      ! local 
      character(len=12) field ! sufficient for 32-bit integers.
      integer i 

      write (field,"(i0)") n
      do i = 1,12
        if (field(i:i).le." ") exit
        call emit(field(i:i))
      end do
    end subroutine splot

end function ExpandRange

