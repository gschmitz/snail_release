subroutine matprt(vec,ndim1,ndim2)
!
! print matrices  of the dimension ndim1,ndim2
!
  include 'fortrankinds.h'
  integer,   intent(in) :: ndim1, ndim2
  real(kdp), intent(in) :: vec(ndim1,ndim2)
  integer maxcol, mincol, i, j
  
  maxcol=0
  do while (maxcol.lt.ndim2)
     mincol=maxcol+1
     maxcol=min(maxcol+5,ndim2)
     write(6,'(/7x,5(5x,i3,6x))') (j,j=mincol,maxcol)
     write(6,*)
     do i=1,ndim1
       write(6,'(i5,1x,5(1x,f13.8))') i,(vec(i,j),j=mincol,maxcol)
     end do
  end do
  
  return
end
