subroutine htosq(n,a,b)
! ---------------------------------------------------------------------
!      expand trigonal matrix b to full matrix a
!      a and b may refer to the same address
! ---------------------------------------------------------------------
   implicit none

   include 'fortrankinds.h'

   integer, intent(in) :: n
   real(kdp), intent(inout) :: a(n,n),b(n*(n+1)/2)

   integer i,j,ioff

   do i=n,1,-1
     ioff=i*(i-1)/2
     do j=i,1,-1
       a(j,i)=b(ioff+j)
     end do
   end do

   do i=1,n
     do j=1,i-1
       a(i,j)=a(j,i)
     end do
   end do

   return
end subroutine htosq
