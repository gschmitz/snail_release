
subroutine numhess(escf,emp2,lscf,ScfCalcDef,lcis,CisCalcDef,lmp2,Mp2CalcDef,&
                   atoms,basis,jkbas,cbas,use_ri,use_rijk, &
                   timer,iprint,intopt,tolint,charge,memmax,&
                   nocc,natoms,nspin)
  use type_atom
  use type_basis
  use type_integ
  use type_timer
  use mod_fileutil

  use type_scfcalcdef
  use type_ciscalcdef
  use type_mp2calcdef

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'
  include 'stdout.h'

  real(kdp), parameter :: delta = 1d-3;


  ! dimensions
  integer, intent(in) :: natoms,nspin

  integer, intent(in) :: nocc(nspin)
  
  ! input/output
  real(kdp),         intent(inout) :: escf,emp2

  ! input
  type(scf_calcdef), intent(in) :: ScfCalcDef
  type(cis_calcdef), intent(in) :: CisCalcDef
  type(mp2_calcdef), intent(in) :: Mp2CalcDef
  type(atom_info),   intent(in) :: atoms(natoms)
  type(basis_info),  intent(in) :: basis
  type(basis_info),  intent(in) :: jkbas 
  type(basis_info),  intent(in) :: cbas 
  integer,           intent(in) :: charge
  integer,           intent(in) :: iprint 
  logical,           intent(in) :: lscf,lmp2,lcis
  integer(kli),      intent(in) :: memmax
  real(kdp),         intent(in) :: tolint
  logical,           intent(in) :: use_ri,use_rijk
  type(integ_info),  intent(in) :: intopt

  ! input/output
  type(timer_info),  intent(inout) :: timer

  ! local
  integer   :: iat,jat,ixyz,jxyz,ipm,ncoord,icoord,jcoord,npm
  real(kdp) :: edisp(4,2),dispi(3),dispj(3),pmi(4),pmj(4),eref(2)
  logical   :: is_diag

  type(basis_info) :: basis_disp,jkbas_disp,cbas_disp
  type(atom_info), allocatable  :: mol_disp(:)

  real(kdp), allocatable :: hess(:)

  ncoord = 3 * natoms

  pmi(1) =  1.d0; pmj(1) =  1.d0
  pmi(2) = -1.d0; pmj(2) = -1.d0
  pmi(3) =  1.d0; pmj(3) = -1.d0
  pmi(4) = -1.d0; pmj(4) =  1.d0

  allocate(mol_disp(natoms),hess(ncoord*(ncoord+1)/2))

  eref(1) = escf
  eref(2) = emp2

  mol_disp = atoms 
 
  icoord = 0
  do iat = 1, natoms
    do ixyz = 1, 3
      icoord = icoord + 1

      jcoord = 0
      do jat = 1, iat
        do jxyz = 1, 3
          jcoord = jcoord + 1

          is_diag = (icoord == jcoord)

          npm = 4
          if (is_diag) npm = 2

          do ipm = 1, npm
            ! create displacement vector
            dispi       = zero
            dispi(ixyz) = delta * pmi(ipm) 

            ! create displacement vector
            dispj = zero
            if (.not.is_diag) dispj(jxyz) = delta * pmj(ipm) 

            ! displace basis
            call copy_basis(basis_disp,basis) 
            call basis_shift_origin(basis_disp,iat,dispi)
            call basis_shift_origin(basis_disp,jat,dispj)

            if (use_rijk) then
              call copy_basis(jkbas_disp,jkbas) 
              call basis_shift_origin(jkbas_disp,iat,dispi)
              call basis_shift_origin(jkbas_disp,jat,dispj)
            end if

            if (use_ri .and. lmp2) then
              call copy_basis(cbas_disp,cbas) 
              call basis_shift_origin(cbas_disp,iat,dispi)
              call basis_shift_origin(cbas_disp,jat,dispj)
            end if

            ! displace molecule
            call at_shift_origin(mol_disp(iat),dispi)
            call at_shift_origin(mol_disp(jat),dispj)

            ! calculate energy for displacement
            call CalcEnergy(edisp(ipm,1),edisp(ipm,2),.true.,lscf,ScfCalcDef,lcis,CisCalcDef,lmp2,Mp2CalcDef, &
                            mol_disp,basis_disp,jkbas_disp,cbas_disp,use_ri,use_rijk,                         &
                            timer,iprint,intopt,tolint,charge,memmax,                                         &
                            nocc,natoms,nspin)
            
            ! restore  
            mol_disp(iat) = atoms(iat)
            mol_disp(jat) = atoms(jat)
          end do
          
          if (is_diag) then
            hess(icoord*(icoord-1)/2+jcoord) = (edisp(1,1) - 2.d0 * eref(1) + edisp(2,1)) / (delta * delta)
            if (lmp2) &
             hess(icoord*(icoord-1)/2+jcoord) = hess(icoord*(icoord-1)/2+jcoord) &
                                              +  (edisp(1,2) - 2.d0 * eref(2) + edisp(2,2)) / (delta * delta)
          else
            hess(icoord*(icoord-1)/2+jcoord) = (edisp(1,1) - edisp(3,1) - edisp(4,1) + edisp(2,1)) / (4.d0 * delta * delta)
            if (lmp2) &
             hess(icoord*(icoord-1)/2+jcoord) = hess(icoord*(icoord-1)/2+jcoord) &
                                              +  (edisp(1,2) - edisp(3,2) - edisp(4,2) + edisp(2,2)) / (4.d0 * delta * delta)
          end if
        end do
      end do
    end do
  end do

  call wrthess(hess,ncoord)

  deallocate(mol_disp)

end subroutine




