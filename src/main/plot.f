!-------------------------------------------------------------------------+
! Purpose: Plots densities, MOs etc.
!
!
! Gunnar Schmitz
!-------------------------------------------------------------------------+

subroutine plot_engine(PltCalcDef,atoms,basis,memmax,filmo,&
                       nocc,nvir,nspin,natoms)   

  use type_atom
  use type_cgto
  use type_shell
  use type_basis
  use mod_fileutil

  use type_pltcalcdef

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! dimensions
  integer, intent(in) :: nspin
  integer, intent(in) :: nocc(nspin),nvir(nspin),natoms

  ! input
  type(plt_calcdef), intent(in) :: PltCalcDef
  type(atom_info),   intent(in) :: atoms(natoms)
  type(basis_info),  intent(in) :: basis
  character(len=80), intent(in) :: filmo
  integer(kli),      intent(in) :: memmax

  ! local
  integer :: nbf,nshell,iat,icube,imo,idx
 
  real(kdp), allocatable :: orb(:,:),orben(:),xyz(:,:),charges(:)
  integer,  allocatable  :: atno(:)
 
  character(len=80) :: filcube
  character(len=5)  :: x1

  !-----------------------------------------------------------------------+
  ! define shortcuts 
  !-----------------------------------------------------------------------+
  nbf    = basis%nbf
  nshell = basis%nshell

  !-----------------------------------------------------------------------+
  ! sanity checks
  !-----------------------------------------------------------------------+

  if (nspin.ne.1) call quit("At the moment only RHF-reference for plotting","plot_engine")

  !-----------------------------------------------------------------------+
  ! allocate memory
  !-----------------------------------------------------------------------+

  allocate(orb(nbf,nbf),orben(nbf))

  !-----------------------------------------------------------------------+
  ! print header information
  !-----------------------------------------------------------------------+
  call PrintHeader("Starting Plotting")

  !-----------------------------------------------------------------------+
  !  read orbitals and their energies
  !-----------------------------------------------------------------------+
  call readmos(orb,orben,filmo,nbf,nbf)

  !-----------------------------------------------------------------------+
  !  plot density, MOs etc. on a grid
  !-----------------------------------------------------------------------+

  ! create information for plotting
  allocate(xyz(3,natoms),atno(natoms),charges(natoms))

  do iat = 1, natoms
    xyz(:,iat)   = atoms(iat)%xyz
    atno(iat)    = atoms(iat)%atono
    charges(iat) = atoms(iat)%charge
  end do

  !
  ! do plotting
  !

  if (PltCalcDef%plot_density) then

    filcube = PltCalcDef%filcube

    write(istdout,'(5x,a,a,/)')  'Writing electron density to file ', filcube

    icube = openfile(filcube,'unknown')

    call shell_EvaluateDensityOnGrid(basis%shells,orb,icube,xyz,atno,nshell,nbf,natoms,nocc,nspin)

    call closefile(icube)
  end if

  if (PltCalcDef%plot_mos) then
    do idx = 1, PltCalcDef%nmos
      imo = PltCalcDef%imos(idx)

      write (x1,'(i3.3)') imo

      filcube = "MO_"// trim(x1) // ".cube" 
    
      write(istdout,'(5x,a11,i3,a9,a,/)')  'Writing MO ', imo, ' to file ', filcube

      icube = openfile(filcube,'unknown')

      call shell_EvaluateMoOnGrid(basis%shells,orb,imo,icube,xyz,atno,nshell,nbf,natoms,nocc,nspin)

      call closefile(icube)
    end do
  end if 

  ! clean up
  deallocate(xyz,atno,charges)
  deallocate(orb,orben)
end subroutine



