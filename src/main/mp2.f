!-------------------------------------------------------------------------+
! Purpose: Computes the MP2 energy  
!
! Gunnar Schmitz
!-------------------------------------------------------------------------+

subroutine mp2engine(escf, emp2,             &  ! input/output
                     silent,                 &  ! inp
                     Mp2CalcDef, basis,      &  ! inp 
                     tolint, have_eri,       &  ! inp
                     twoeint, memmax, filmo, &  ! inp
                     nocc,nvir,nspin)           ! dim

  use type_cgto
  use type_shell
  use type_basis
  use type_int4idx
  use type_integ
  use IntegralBuffer

  use type_mp2calcdef

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! constants
  logical, parameter :: use_contractor = .true.
  integer, parameter :: ndim = 4
  integer, parameter :: iprint = 1
 
  ! SCS values from Grimme
  real(kdp), parameter :: scscos = 1.2d0, scscss = 1.0d0/3.0d0

  ! dimensions
  integer, intent(in) :: nocc,nvir,nspin

  ! output
  real(kdp),         intent(out) :: emp2

  ! input
  real(kdp),         intent(in)  :: escf
  type(basis_info),  intent(in)  :: basis 
  character(len=80), intent(in)  :: filmo
  real(kdp),         intent(in)  :: tolint
  logical,           intent(in)  :: have_eri,silent 
  integer(kli),      intent(in)  :: memmax
  type(mp2_calcdef), intent(in)  :: Mp2CalcDef

  ! input/output
  type(int4idx) ,    intent(inout) :: twoeint

  ! local
  real(kdp), allocatable :: kabij(:,:,:,:),kmnrs(:,:,:,:)
  real(kdp), allocatable :: scratch1(:),scratch2(:)
  real(kdp), allocatable :: orb(:,:),orben(:)

  real(kdp)    :: cpu2ein,cpu0,cpux
  integer      :: nscreen
  integer      :: nbf,ncbf,nshell,mxcart,mxprim,lmax
  integer(kli) :: memused,memavail


  integer :: imo,jmo,moa,mob,nmat,ibf,jbf,kbf,lbf,ind,&
             idm,ndata1,ndata2,nxint,naoint 

  integer :: noct,nfroz

  integer :: iadrsrc,iadrdest

  real(kdp) :: epair,denom,dfac,eij,dtcme,tplus,tminus,&
               pairs,pairt,edoub(2),scsepair

  integer, allocatable :: idims_in(:),idims_out(:)

  real(kdp), allocatable :: gab(:,:)

  type(TIntBuffer), allocatable :: intbuf(:)
  type(integ_info) :: intopt

  logical :: verbose

  real(kdp) :: dummy

  integer :: nthreads,ithrd

  ! functions
  integer :: get_max_threads, get_thread_num

  !-----------------------------------------------------------------------+
  ! Define things for parrallel code 
  !-----------------------------------------------------------------------+
  nthreads = get_max_threads()

  !-----------------------------------------------------------------------+
  ! init
  !-----------------------------------------------------------------------+
  memused = 0
  
  verbose = .not. silent

  !-----------------------------------------------------------------------+
  ! define shortcuts 
  !-----------------------------------------------------------------------+
  nbf    = basis%nbf
  ncbf   = basis%ncbf
  nshell = basis%nshell
  mxprim = basis%mxprim
  mxcart = basis%mxcart
  lmax   = basis%lmax

  !-----------------------------------------------------------------------+
  ! sanity checks
  !-----------------------------------------------------------------------+

  if (nspin.ne.1) call quit("At the moment only RHF-reference for MP2","mp2engine")

  !-----------------------------------------------------------------------+
  ! print header information
  !-----------------------------------------------------------------------+
  if (.not. silent) call PrintHeader('Starting the MP2 calculation')

  !-----------------------------------------------------------------------+
  !  read orbitals and their energies
  !-----------------------------------------------------------------------+

  allocate(orb(nbf,nbf),orben(nbf))

  call readmos(orb,orben,filmo,nbf,nbf)

  !
  ! Apply frozen core approximation?
  !
  nfroz = 0

  if (Mp2CalcDef%use_frozen_core) then
    do imo = 1, nocc
      if ( orben(imo) <= Mp2CalcDef%freeze_point ) then
        nfroz = nfroz + 1
      else
        exit
      end if
    end do

    if (verbose) then
       write(istdout,'(5x,a)')  'Frozen Core Approximation is used'
       write(istdout,'(7x,a,i4,/)') 'No. of frozen occ. orbtials:', nfroz
    end if
  end if

  noct = nocc - nfroz

  !-----------------------------------------------------------------------+
  ! allocate memory
  !-----------------------------------------------------------------------+

  allocate(kabij(nvir,nvir,noct,noct),kmnrs(nbf,nbf,nbf,nbf))

  allocate(scratch1(nbf*nbf*nbf*noct),&
           scratch2(nbf*nbf*noct*noct))

  !-----------------------------------------------------------------------+
  !  calculate ERIs or reuse them
  !-----------------------------------------------------------------------+

  if (.not.have_eri) then

    ! init Buffer to calculate two electron integrals
    allocate(intbuf(0:nthreads-1))
    do ithrd = 0, nthreads - 1
      call allocate_intbuf(intbuf(ithrd),intopt,lmax,mxcart,mxprim,silent)
    end do

    if (verbose) write(istdout,'(5x,a)')  'Start calculation of AO-Integrals...'
    call cpu_time(cpu0)
    memavail = memmax - memused
    allocate(gab(nshell,nshell))
    call gettwoeints(twoeint,intbuf,dummy,.false.,1,.true.,.false.,gab,.false.,dummy,tolint,&
                     nscreen,memavail,lmax,basis%shells,iprint,silent,ncbf,nbf,nshell,mxprim,mxcart,nthreads)
    deallocate(gab)
    call cpu_time(cpux)
    cpu2ein = cpux - cpu0

    if (verbose) write(istdout,'(5x,a,f10.2,/)')  'CPU-time for 2-el Ints (sec):', cpu2ein
  end if

  ! resort AO-Integrals for better dgemm performance
  do lbf = 1, nbf
    do jbf = 1, nbf
      do kbf = 1, nbf
        do ibf = 1, nbf
          kmnrs(ibf,kbf,jbf,lbf) = int4idx_getvalue(twoeint,ibf,jbf,kbf,lbf)
        end do 
      end do
    end do 
  end do

  !-----------------------------------------------------------------------+
  ! use inefficient way to transform AO-integrals to MO-integrals....
  !-----------------------------------------------------------------------+

  if (use_contractor) then
   
    allocate(idims_in(ndim),idims_out(ndim))

    do idm = 1, ndim
      idims_in(idm) = nbf
    end do

    ndata1 = nbf*nbf*nbf*noct
    ndata2 = nbf*nbf*noct*noct
    nxint  = nvir*nvir*noct*noct 
    naoint = nbf*nbf*nbf*nbf

    ! 1.) Transform first occupied index
    call ContractForward(scratch1,idims_out,kmnrs,orb(1,nfroz+1),4,idims_in,ndim,nbf,noct,naoint,ndata1)
    idims_in(4) = noct

    ! 2.) Transform second occupied index
    call ContractForward(scratch2,idims_out,scratch1,orb(1,nfroz+1),3,idims_in,ndim,nbf,noct,ndata1,ndata2)
    idims_in(3) = noct

    ! 3.) Transform first virtual index
    call ContractForward(scratch1,idims_out,scratch2,orb(1,nocc+1),2,idims_in,ndim,nbf,nvir,ndata2,ndata1)
    idims_in(2) = nvir

    ! 4.) Transform second virtual index
    call ContractForward(kabij,idims_out,scratch1,orb(1,nocc+1),1,idims_in,ndim,nbf,nvir,ndata1,nxint)
    idims_in(1) = nvir

    deallocate(idims_in,idims_out)
  else
    ! 1.) Transform first occupied index
    nmat = nbf*nbf*nbf
    call dgemm('n','n',nmat,noct,nbf,one,kmnrs,nmat,orb(1,nfroz+1),nbf,zero,scratch1,nmat)

    ! 2.) Transform second occupied index
    nmat = nbf*nbf
    do imo = 1, noct
      iadrdest = (imo-1)*nmat*noct + 1
      iadrsrc  = (imo-1)*nmat*nbf + 1
      call dgemm('n','n',nmat,noct,nbf,one,scratch1(iadrsrc),nmat,orb(1,nfroz+1),nbf,zero,scratch2(iadrdest),nmat) 
    end do

    ! 3.) Transform first virtual index
    nmat = nbf
    do imo = 1, noct
      do jmo = 1, noct
        iadrsrc  = (imo-1)*nmat*nbf*noct  + (jmo-1)*nmat*nbf + 1
        iadrdest = (imo-1)*nmat*nvir*noct + (jmo-1)*nmat*nvir + 1
        call dgemm('n','n',nmat,nvir,nbf,one,scratch2(iadrsrc),nmat,orb(1,nocc+1),nbf,zero,scratch1(iadrdest),nmat)
      end do 
    end do 

    ! 4.) Transform second virtual index
    nmat = 1
    do imo = 1, noct 
      do jmo = 1, noct
        do moa = 1, nvir
          iadrsrc  = (imo-1)*nbf*nvir*noct + (jmo-1)*nbf*nvir + (moa-1)*nbf  + 1
          call dgemm('n','n',nmat,nvir,nbf,one,scratch1(iadrsrc),nmat,orb(1,nocc+1),nbf,zero,kabij(1,moa,jmo,imo),nmat)
        end do 
      end do 
    end do 

  end if
  
  !-----------------------------------------------------------------------+
  ! We have the exchange integrals now calculate the energy correction...
  !-----------------------------------------------------------------------+

  epair = zero
  denom = one 
  edoub = zero

  !----------------------------------------------------------------------+
  !  E = sum_(i >= j) (2-delta_ij) sum_(ab) L_ab^(ij) t_ab^(ij)
  !
  !  t_ab^(ij) = - K_ab^(ij) / (ea + eb - ei - ej)
  !  L_ab^(Ij) = 2 t_ab^(ij) - t_ab^(ji) 
  !----------------------------------------------------------------------+
  
  do imo = 1, noct
    dfac = two
    do jmo = 1, imo
      if (imo == jmo) dfac = one
      eij = orben(imo+nfroz) + orben(jmo+nfroz)

      pairs = zero
      pairt = zero

      ! conventional FLOP count
      do moa = 1, nvir
        do mob = 1, nvir
          denom =  ( orben(nocc + moa) + orben(nocc + mob) - eij )

          dtcme = (two * kabij(mob,moa,jmo,imo) - kabij(mob,moa,imo,jmo))    
          epair = epair - dfac * dtcme * kabij(mob,moa,jmo,imo) / denom
        end do
      end do 

      ! reduced FLOP count
      do moa = 1, nvir
        do mob = 1, moa - 1
          denom =  ( orben(nocc + moa) + orben(nocc + mob) - eij )

          tplus  = (kabij(mob,moa,jmo,imo) + kabij(mob,moa,imo,jmo))
          tminus = (kabij(mob,moa,jmo,imo) - kabij(mob,moa,imo,jmo))

          pairs = pairs + half * tplus * tplus / denom
          pairt = pairt + thrhalf * tminus * tminus / denom
        end do
        denom =  two *  orben(nocc + moa) - eij 
        tplus  = (kabij(moa,moa,jmo,imo) + kabij(moa,moa,imo,jmo))
        pairs = pairs + 0.25d0 * tplus * tplus / denom
      end do

      edoub(1) = edoub(1) - dfac * pairs
      edoub(2) = edoub(2) - dfac * pairt
    end do
  end do 

  scsepair = scscos * edoub(1) + ((scscos+2.0d0*scscss)/3.0d0) * edoub(2)

  !-----------------------------------------------------------------------+
  ! print MP2 and SCS-MP2 (correlation) energy 
  !-----------------------------------------------------------------------+

  if (.not. silent) then
     call prtenergy(escf,epair,edoub,.true.)
  end if

  emp2 = epair

  deallocate(kabij,kmnrs,scratch1,scratch2)
  deallocate(orb,orben)
  if (.not.have_eri) then
    do ithrd = 0, nthreads - 1
      call deallocate_intbuf(intbuf(ithrd))
    end do
  end if
end subroutine



