
subroutine numgrad(escf,emp2,lscf,ScfCalcDef,lcis,CisCalcDef,lmp2,Mp2CalcDef,&
                   do_opt,OptCalcDef,&
                   atoms,basis,jkbas,cbas,use_ri,use_rijk, &
                   timer,iprint,intopt,tolint,charge,memmax,&
                   nocc,natoms,nspin)
  use type_atom
  use type_basis
  use type_integ
  use type_timer
  use mod_fileutil

  use type_scfcalcdef
  use type_ciscalcdef
  use type_mp2calcdef
  use type_optcalcdef

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'
  include 'stdout.h'

  ! constants
  real(kdp), parameter :: delta = 1d-6;
  real(kdp), parameter :: eps   = 1d-3;

  ! dimensions
  integer, intent(in) :: natoms,nspin

  integer, intent(in) :: nocc(nspin)
  
  ! output
  real(kdp),         intent(inout):: escf,emp2

  ! input
  type(scf_calcdef), intent(in) :: ScfCalcDef
  type(cis_calcdef), intent(in) :: CisCalcDef
  type(mp2_calcdef), intent(in) :: Mp2CalcDef
  type(opt_calcdef), intent(in) :: OptCalcDef
  type(atom_info),   intent(in) :: atoms(natoms)
  type(basis_info),  intent(in) :: basis
  type(basis_info),  intent(in) :: jkbas 
  type(basis_info),  intent(in) :: cbas 
  integer,           intent(in) :: charge
  integer,           intent(in) :: iprint 
  logical,           intent(in) :: lscf,lmp2,lcis,do_opt
  integer(kli),      intent(in) :: memmax
  real(kdp),         intent(in) :: tolint
  logical,           intent(in) :: use_ri,use_rijk
  type(integ_info),  intent(in) :: intopt

  ! input/output
  type(timer_info),  intent(inout) :: timer

  ! local
  integer   :: iat,ixyz,ipm
  integer   :: iter,maxiter
  real(kdp) :: edisp(2,2),eref,disp(3),pm(2),gnrm
  logical   :: converged

  type(basis_info) :: basis_disp,jkbas_disp,cbas_disp,basis_ref,jkbas_ref,cbas_ref
  type(atom_info), allocatable  :: mol_disp(:),mol(:)

  real(kdp), allocatable :: grad(:,:)

  ! functions
  real(kdp) :: dnrm2

  converged = .false.

  pm(1) =  1.d0
  pm(2) = -1.d0

  allocate(mol_disp(natoms),mol(natoms),grad(3,natoms))

  !
  ! Save molecular structure and basis set for current geometry
  !
  mol = atoms
  call copy_basis(basis_ref,basis) 
  if (use_rijk) then
    call copy_basis(jkbas_ref,jkbas) 
  end if
  if (use_ri .and. lmp2) then
    call copy_basis(cbas_ref,cbas) 
  end if

  !
  ! Calculate gradient and/or optimize structure
  !
  if (.not.do_opt) then; maxiter = 1; else; maxiter = OptCalcDef%maxiter; end if
  do iter = 1, maxiter

    mol_disp = mol

    do iat = 1, natoms
      do ixyz = 1, 3
        do ipm = 1, 2

          ! create displacement vector
          disp       = zero
          disp(ixyz) = delta * pm(ipm) 

          ! displace basis
          call copy_basis(basis_disp,basis_ref) 
          call basis_shift_origin(basis_disp,iat,disp)

          if (use_rijk) then
            call copy_basis(jkbas_disp,jkbas_ref) 
            call basis_shift_origin(jkbas_disp,iat,disp)
          end if

          if (use_ri .and. lmp2) then
            call copy_basis(cbas_disp,cbas_ref) 
            call basis_shift_origin(cbas_disp,iat,disp)
          end if

          ! displace molecule
          call at_shift_origin(mol_disp(iat),disp)

          ! calculate energy for displacement
          call CalcEnergy(edisp(ipm,1),edisp(ipm,2),.true.,lscf,ScfCalcDef,lcis,CisCalcDef,lmp2,Mp2CalcDef,&
                          mol_disp,basis_disp,jkbas_disp,cbas_disp,use_ri,use_rijk,   &
                          timer,iprint,intopt,tolint,charge,memmax, &
                          nocc,natoms,nspin)
          
          ! restore  
          mol_disp(iat) = mol(iat)
        end do
        grad(ixyz,iat) = (edisp(1,1) - edisp(2,1)) / (2.d0 * delta)
        if (lmp2) grad(ixyz,iat) = grad(ixyz,iat) + (edisp(1,2) - edisp(2,2)) / (2.d0 * delta)
      end do
    end do

    ! gradient norm
    gnrm =  dnrm2(3*natoms,grad,1) 

    ! Print gradient
    if (do_opt) write(istdout,'(/5x,a,x,a,i5,x,a)') repeat('-',10),'iteration',iter,repeat('-',18)
    write(istdout,'(/,5x,a,f13.8))')  'Numerical Gradient, |dE/dxyz| =', gnrm
    do iat = 1, natoms
       write(istdout,'(5x,a3,f13.8,f13.8,f13.8)') &
           atoms(iat)%elem, grad(1,iat), grad(2,iat), grad(3,iat)

    end do

    if (do_opt) then
      ! update coordinates and basis
      do iat = 1, natoms
         disp(1:3) = -OptCalcDef%alpha * grad(1:3,iat)
         call at_shift_origin(mol(iat),disp)
         call basis_shift_origin(basis_ref,iat,disp)
      end do

      write(istdout,'(5x,a)') 'New coordinates'
      do iat = 1, natoms
         write(istdout,'(5x,a3,f13.8,f13.8,f13.8)') &
             mol(iat)%elem, mol(iat)%xyz(1), mol(iat)%xyz(2), mol(iat)%xyz(3)

      end do

      if (gnrm .lt. eps) then
         converged = .true.
         exit
      end if 
    end if

  end do

  if (do_opt .and. converged) then
     write(istdout,'(/,5x,a,i4,x,a)') 'Convergence (geo) reached in', iter, "iterations"
  else
     write(istdout,'(/,5x,a,i4,x,a)') 'Convergence (geo) NOT reached in', iter, "iterations"
  end if

  !
  ! Clean up and leave
  !
  call deallocate_basis(basis_disp)
  call deallocate_basis(basis_ref)
  call deallocate_basis(jkbas_disp)
  call deallocate_basis(jkbas_ref)
  call deallocate_basis(cbas_disp)
  call deallocate_basis(cbas_ref)

  deallocate(mol,grad,mol_disp)

end subroutine




