!===============================================================================
! The Hartree-Fock engine. 
!
! At the moment implemented
!  - RHF
!  - UHF
!  - DIIS acceleration
!  - direct SCF
!  - RIJK approximation
!
!===============================================================================


subroutine hfengine(escf,ScfCalcDef,silent,&
                    timer,iprint,atoms,basis,jkbas,use_rijk,&
                    intopt,tolint,charge,memmax,            &
                    nocc,natoms,nspin)
  use type_atom
  use type_cgto
  use type_shell
  use type_basis
  use type_int4idx
  use type_queue
  use type_integ
  use type_timer
  use IntegralBuffer
  use mod_fileutil

  use type_scfcalcdef

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'
  include 'stdout.h'

  ! constants
  integer,   parameter :: nminvec = 2
  integer,   parameter :: inot_set = -99

  integer,   parameter :: ispina = 1, &
                          ispinb = 2

  ! dimensions
  integer, intent(in) :: natoms,nspin

  integer, intent(in) :: nocc(nspin)
  
  ! output
  real(kdp),         intent(out) :: escf

  ! input
  type(scf_calcdef), intent(in) :: ScfCalcDef
  type(atom_info),   intent(in) :: atoms(natoms)
  type(basis_info),  intent(in) :: basis
  type(basis_info),  intent(in) :: jkbas 
  integer,           intent(in) :: charge
  integer,           intent(in) :: iprint 
  integer(kli),      intent(in) :: memmax
  real(kdp),         intent(in) :: tolint
  logical,           intent(in) :: use_rijk,silent
  type(integ_info),  intent(in) :: intopt

  ! input/output
  type(timer_info),  intent(inout) :: timer

  ! local
  real(kdp)    :: energy,enuke,eold,ediff,densdev, damp
  real(kdp)    :: epot,ekin,virial 
  logical      :: isconv,isuhf,havefil
  integer      :: m,n,iter,jdx,iadr,ispin,nscreen,ibf,jbf
  integer      :: nbf,ncbf,nshell,mxcart,mxprim,lmax
  integer      :: iat,icube
  integer(kli) :: memused,memavail

  real(kdp)    :: cpu1ein,cpu2ein,cpu0,cpux,cpufock,cpuobas,cpudiag,cpudens
  real(kdp)    :: wal1ein,wal2ein

  real(kdp),allocatable :: ovl(:,:),hone(:,:),gint(:,:,:),fock(:,:,:),fockp(:,:),&
                           xao2o(:,:),xao2ot(:,:),orb0(:,:,:),orb(:,:,:),  &
                           orben(:,:),dens(:,:,:),fnew(:,:,:),dold(:,:,:), & 
                           kin(:,:),fold(:,:,:)


  ! for incremental Fock build
  real(kdp), allocatable :: fock2bod(:,:,:),diffdens(:,:,:)

  ! for 4-index integrals 
  type(int4idx)    :: twoeint
  type(TIntBuffer), allocatable :: intbuf(:)
 
  ! for DIIS
  integer  :: mxdiis,ndiis(nspin),istdiis,ierr
  real(kdp), allocatable :: errmat(:,:)
  real(kdp), allocatable :: diiswei(:)
  integer, allocatable   :: imap(:)
  type(queueinfo)        :: errlst(nspin),focklst(nspin)
  
  
  character(len=80)      :: filmo
  character(len=5)       :: cguess = "GWH"

  real(kdp), allocatable :: gab(:,:)

  real(kdp) :: dummy

  integer :: nthreads,ithrd

  ! functions
  real(kdp) :: getenuke,getenergy,getdensdev,getdamping,getekin
  integer :: get_max_threads, get_thread_num

  !------------------------------------------------------------------------+
  ! sanity check 
  !------------------------------------------------------------------------+
  ! To be added

  !-----------------------------------------------------------------------+
  ! Define things for parrallel code 
  !-----------------------------------------------------------------------+
  nthreads = get_max_threads()

  !------------------------------------------------------------------------+
  ! init some stuff 
  !------------------------------------------------------------------------+
  energy  = zero 
  enuke   = zero 
  eold    = zero 
  ediff   = zero 
  densdev = zero 
  isconv  = .false.

  memused = 0

  isuhf = (nspin == 2)

  nbf    = basis%nbf
  ncbf   = basis%ncbf
  nshell = basis%nshell
  mxprim = basis%mxprim
  mxcart = basis%mxcart
  lmax   = basis%lmax

  cpufock = zero 
  cpuobas = zero 
  cpudiag = zero
  cpudens = zero


  !------------------------------------------------------------------------+
  ! memory allocation 
  !------------------------------------------------------------------------+
  memused = memused + nbf*nbf + nbf*nbf + nbf*nbf*nspin + nbf*nbf*nspin &
                    + nbf*nbf + nbf*nbf + nbf*nbf + nbf*nbf             &
                    + nbf*nbf*nspin + nbf*nbf + nbf*nbf*nspin + nbf*nbf &
                    + nbf*nbf*nspin

  call memcheck(memmax,memused,'HF working arrays I','hfengine')

  allocate(ovl(nbf,nbf),hone(nbf,nbf),gint(nbf,nbf,nspin) ,          &
           fock(nbf,nbf,nspin),fockp(nbf,nbf),xao2o(nbf,nbf),        &
           xao2ot(nbf,nbf),orb0(nbf,nbf,nspin),orb(nbf,nbf,nspin),   &
           orben(nbf,nspin),dens(nbf,nbf,nspin),fnew(nbf,nbf,nspin), &
           dold(nbf,nbf,nspin),fold(nbf,nbf,nspin))  

  if (.not.ScfCalcDef%ldirect) then
    call allocate_int4idx(twoeint,.false.,nbf)
 
    memused = memused + size(twoeint%dintfull)
    call memcheck(memmax,memused,'HF working arrays II','hfengine')
  end if

  if (ScfCalcDef%ldirect) then
    memused = memused + nbf*nbf*nspin*2
    allocate(fock2bod(nbf,nbf,nspin),diffdens(nbf,nbf,nspin))
  else
    allocate(fock2bod(1,1,1),diffdens(1,1,1)) ! for range checker
  end if

  !------------------------------------------------------------------------+
  ! Some output for the user
  !------------------------------------------------------------------------+

  if (.not.silent) then

    write(istdout,'(/1x,a)') repeat('-',70)
    write(istdout,'(5x,a)') 'Starting restricted Hartree-Fock calculcation'

    write(istdout,'(/10x,a27,i4)') 'Number of atoms: ', natoms
    write(istdout,'(10x,a27,i4)')  'Charge of molecule: ', charge

    if (nspin == 1) then
      write(istdout,'(10x,a27,i4)')  'Number of occ. orbitals: ', nocc(ispina)
    else
      write(istdout,'(10x,a27,i4)')  'Number of occ. (a-spin): ', nocc(ispina)
      write(istdout,'(10x,a27,i4)')  'Number of occ. (b-spin): ', nocc(ispinb)
    end if

    write(istdout,'(/20x,a,/)') 'Basis set information:'
    call print_BasisInfo(basis)

    write(istdout,'(/5x,a,es10.2)')   'Integral neglect threshold: ', tolint
    write(istdout,'(/1x,a,/)') repeat('-',70)

  end if

  !------------------------------------------------------------------------+
  ! Integral evaluation
  !------------------------------------------------------------------------+

  ! init Buffer to calculate two electron integrals
  allocate(intbuf(0:nthreads-1))
  do ithrd = 0, nthreads - 1
    call allocate_intbuf(intbuf(ithrd),intopt,lmax,mxcart,mxprim,silent)
  end do

  ! Calculate energy due to nuclear repulsion
  enuke = getenuke(atoms,natoms)

  ! Calculate one electron integrals
  call timer_exec(timer,'init','oneint')
  call getoneeints(ovl,hone,basis%shells,atoms,nbf,natoms,nshell,mxcart)
  call timer_exec(timer,'measure','oneint',cpu1ein,wal1ein)

  ! Calculate two electron integrals
  cpu2ein = zero
  nscreen = 0
  allocate(gab(nshell,nshell))
  memused = memused + nshell*nshell
  if (.not.ScfCalcDef%ldirect) then
    call timer_exec(timer,'init','twoint')
    memavail = memmax - memused
    if (use_rijk) then
      call rijkengine(twoeint,dummy,.false.,dummy,.false.,&
                      basis,jkbas,tolint,dummy,dummy,memmax,nocc,nbf)
    else
      call gettwoeints(twoeint,intbuf,fock,.false.,1,.true.,.false.,gab,.false.,diffdens,tolint,&
                       nscreen,memavail,lmax,basis%shells,iprint,silent,ncbf,nbf,nshell,mxprim,mxcart,nthreads)
    end if
    call timer_exec(timer,'measure','twoint',cpu2ein,wal2ein)
  end if

  !
  ! print timings and other information
  !
  if (.not.silent) then
    write(istdout,'(/5x,a,2(f10.2))') 'CPU-time, WALL-time for 1-el Ints (sec):', cpu1ein, wal1ein
    if (ScfCalcDef%ldirect) then
      write(istdout, '(5x,a)') 'Direct mode for two electron integrals'
    else
      write(istdout, '(5x,a,2(f10.2))') 'CPU-time, WALL-time for 2-el Ints (sec):', cpu2ein, wal2ein 
    end if
    write(istdout,'(/5x,a,f8.2)')     'Screening ratio (%)', real(nscreen,kdp) / real(nbf*nbf*nbf*nbf, kdp) * 100.d0
  end if

  ! Perform symmetric orthogonalisation
  call canorg(xao2o,ovl,nbf)
  xao2ot = transpose(xao2o)

  !------------------------------------------------------------------------+
  ! init stuff for DIIS
  !------------------------------------------------------------------------+
  if (ScfCalcDef%use_diis) then
    mxdiis = 5
    allocate(errmat(nbf,nbf),diiswei(mxdiis),imap(mxdiis))
    memused = memused + nbf*nbf + mxdiis + mxdiis
    istdiis = inot_set 

    do ispin = 1, nspin
      call allocate_queue(errlst(ispin),mxdiis,nbf*nbf)
      call allocate_queue(focklst(ispin),mxdiis,nbf*nbf)
      memused = memused + size(errlst(ispin)%list) + size(focklst(ispin)%list)
    end do

    call memcheck(memmax,memused,'DIIS','hfengine')
  end if


  !------------------------------------------------------------------------+
  ! Read in start orbtials or perform guess 
  !------------------------------------------------------------------------+

  do ispin = 1, nspin

    cguess = ScfCalcDef%cguess

    if (cguess(1:5) == 'REUSE') then
      ! Read exisiting MOs as start guess
      filmo = 'mos'
      ! Check if mos file exists and if not fall back to GWH guess
      inquire(file=filmo, exist=havefil)

      if (havefil) then
        call readmos(orb,orben,filmo,nbf,nbf)
      else
        write(istdout,'(/3x,a)') '!! WARNING !! Could not find MOs for initial guess.'
        write(istdout,'(3x,a)')  'Will use Generalized Wolfsberg Helmholtz guess instead.'
        cguess = 'GWH'
      end if
    end if

    if (cguess(1:3) == 'GWH') then

      ! do generalized Wolfsberg Helmholtz guess
      call getgwhguess(ScfCalcDef%ldirect,orb(1,1,ispin),hone,ovl,fock(1,1,ispin),&
                       fock2bod(1,1,ispin),xao2o,xao2ot,nbf)

    elseif (cguess(1:5) == 'HCORE') then

     ! do hamilton core guess
     call getcoreguess(ScfCalcDef%ldirect,orb(1,1,ispin),hone,fock2bod(1,1,ispin),xao2o,xao2ot,nbf)

    elseif (cguess(1:5) == 'REUSE') then
      ! NOP Already handeld above
    else
      call quit('unknown inital guess','hfengine')
    end if
  end do

  !------------------------------------------------------------------------+
  ! Build initial density 
  !------------------------------------------------------------------------+
 
  do ispin = 1, nspin
    ! Build density Dens = Orb x Orb^T
    call builddens(isuhf, dens(1,1,ispin), orb(1,1,ispin), 1, nocc(ispin), nbf)
 
    ! Save density
    call dcopy(nbf*nbf,dens(1,1,ispin),1,dold(1,1,ispin),1)

    if (ScfCalcDef%ldirect) then
      call dcopy(nbf*nbf,dens(1,1,ispin),1,diffdens(1,1,ispin),1)
      call dcopy(nbf*nbf,zero,0,fock2bod(1,1,ispin),1)
    end if

    ! Save initial orbitals
    call dcopy(nbf*nbf,orb(1,1,ispin),1,orb0(1,1,ispin),1)
  end do

  !------------------------------------------------------------------------+
  ! Start SCF procedure 
  !------------------------------------------------------------------------+

  if (iprint > 1 .and. .not. silent) then
    write(istdout,'(/5x,a5,12x,a6,12x,a,6x,a)') 'Iter:', 'Energy', 'Delta-E','Damp'
    write(istdout,'(5x,a)') repeat('-',55)
  end if

  ndiis(:) = 0
  do iter = 1, ScfCalcDef%maxiter

    !----------------------------------------------------------+
    ! Build G_ij 
    !   RHF =>  2J_ij - K_ij
    !   UHF =>   J_ij - K_ij
    !----------------------------------------------------------+
    call cpu_time(cpu0)
    if (.not.ScfCalcDef%ldirect) then
      call buildg(gint, twoeint, dens, nbf, nspin)
    end if
    call cpu_time(cpux)
    cpufock = cpufock + cpux - cpu0

    !
    ! Build Fock matrix using current Density matrix
    !

    ! F = h + G
    call cpu_time(cpu0)
    call buildfock(timer, ScfCalcDef%ldirect, fock, fock2bod,  &
                   diffdens, orb, gab, intbuf, iter,tolint,    &
                   hone, basis, jkbas, use_rijk, gint,         &
                   nocc, nbf, nspin, memavail, nthreads)
    call cpu_time(cpux)
    cpufock = cpufock + cpux - cpu0

    !
    ! Calculate Energy for current Fock and Density matrix and check convergence
    !
    energy = getenergy(hone, fock, dens, enuke, nbf, nspin)

    ediff = energy - eold ; densdev = getdensdev(dens,dold,nbf,nspin)

    damp = getdamping(ScfCalcDef%dampst,ScfCalcDef%dampstep,iter,ScfCalcDef%dampnd)
  
    if (.not. silent) then 
      select case(iprint)
        case(1)
          write(istdout,'(5x,a11,i4,a11,f14.10,a3)') 'Iteration: ', iter, ' Energy: ', energy, 'Eh'
        case(2) 
          write(istdout,'(5x,i4,4x,f16.10,a3,2x,f16.10,2x,f6.3)') iter, energy, 'Eh', ediff, damp
        case default
          write(istdout,'(5x,a11,i4,a11,f14.10,a3)') 'Iteration: ', iter, ' Energy: ', energy, 'Eh'
      end select
    end if

    If ((dabs(ediff) .lt. ScfCalcDef%scftol).and.(densdev .lt. ScfCalcDef%scftol)) then
      isconv = .true.
      exit 
    end If

    ! Save energy
    eold = energy

    ! Save density matrix
    do ispin = 1, nspin
      call dcopy(nbf*nbf,dens(1,1,ispin),1,dold(1,1,ispin),1)
    end do

    !
    ! Get new orbitals via diagonalizing Fock operator (DIIS and/or damping)
    !
    do ispin = 1, nspin

      if (ScfCalcDef%use_diis) then

        ! save densities for DIIS 
        if (istdiis == inot_set) then
           istdiis = iter - 1
           if (ispin == 1 .and. .not. silent) write(istdout,'(/3x,a,/)') 'Start saving densities for DIIS'
        end if

        ndiis(ispin) = ndiis(ispin) + 1
        if (ndiis(ispin) > mxdiis) ndiis(ispin) = mxdiis

        ! add density and DIIS error to list
        call queue_add(focklst(ispin),fock(1,1,ispin),ierr,nbf*nbf)
        if (ierr /= 0) call quit("Error in focklst","hf_engine")
        
        call getfdserror(errmat,xao2o,xao2ot,fock(1,1,ispin),ovl,dens(1,1,ispin),nbf)
        call queue_add(errlst(ispin),errmat,ierr,nbf*nbf)
        if (ierr /= 0) call quit("Error in errlst","hf_engine")

        ! use DIIS to enhance convergence
        if ( istdiis /= inot_set .and. (iter == (istdiis+nminvec)) ) then

          if (iter == istdiis+nminvec .and. (ispin == 1) .and. .not. silent ) write(istdout,'(/3x,a,/)') 'Start DIIS'

          call getdiisweights(diiswei,errlst(ispin),ndiis(ispin),nbf)

          ! init density
          fnew(:,:,ispin) = zero

          ! get indices in Fock queue
          call queue_getimap(focklst(ispin),imap,mxdiis)

          ! F_new = \sum c_i F_i equivalent to  D_new = \sum c_i D_i since F(\sum c_i D_i) = \sum c_i F(D_i)
          do jdx = 1, ndiis(ispin)
            call daxpy(nbf*nbf,diiswei(jdx),focklst(ispin)%list(1,imap(jdx)),1,fnew(1,1,ispin),1)
          end do

        else
          call dcopy(nbf*nbf,fock(1,1,ispin),1,fnew(1,1,ispin),1)
        end if
      else
        call dcopy(nbf*nbf,fock(1,1,ispin),1,fnew(1,1,ispin),1)
      end if

      ! Mixing/damping 
      if (iter > 1) then 
        do n = 1, nbf
          do m = 1, nbf 
            fock(m, n, ispin) = (one - damp) * fnew(m, n, ispin) + damp * fold(m, n, ispin)
          end do
        end do
      end if

      ! Transform Fock as F' = X^T F X 
      ! (use gint as scratch-> will be overwritten)
      call cpu_time(cpu0)
      call orthobas(xao2ot, fock(1,1,ispin), xao2o, fockp, gint(1,1,ispin), nbf)
      call cpu_time(cpux)
      cpuobas = cpuobas + cpux - cpu0

      ! Diagonalise Fock-matrix
      call cpu_time(cpu0)
      call diagfock(fockp, orb0(1,1,ispin), orben(1,ispin), nbf)
      call cpu_time(cpux)
      cpudiag = cpudiag + cpux - cpu0

      ! orb = X * orb0
      call dgemm('n', 'n', nbf, nbf, nbf, one, xao2o, nbf, orb0(1,1,ispin), nbf, zero, orb(1,1,ispin), nbf)

      ! Build density dens = Orb x Orb^T
      call cpu_time(cpu0)
      call builddens(isuhf, dens(1,1,ispin), orb(1,1,ispin), 1, nocc(ispin), nbf)
      call cpu_time(cpux)
      cpudens = cpudens + cpux - cpu0

      if (ScfCalcDef%ldirect) then
        call dcopy(nbf*nbf,dens(1,1,ispin),1,diffdens(1,1,ispin),1)
        if (use_rijk) then
          fock2bod = zero
        else
          ! calculate difference density for next cycle
          call daxpy(nbf*nbf,-one,dold(1,1,ispin),1,diffdens(1,1,ispin),1)
        end if
      end if

      ! Save Fock matrix
      call dcopy(nbf*nbf,fock(1,1,ispin),1,fold(1,1,ispin),1)

    end do ! ispin loop 


  end do

  !
  ! Print info on convergence
  !
  if (.not. silent) then

    write(istdout,'(/5x,a14,f16.10,a3,/)') 'Final energy: ', energy, 'Eh'

    if (isconv) then
      write(istdout,*) repeat("-",70)
      write(istdout,'(a,i2,a)') "      converged in ", iter, " cycles"
      write(istdout,*) repeat("-",70)
    else
      write(istdout,*) repeat("-",70)
      write(istdout,'(a,i2,a)') "      Energy NOT converged in ", iter, " cycles"
      write(istdout,*) repeat("-",70)
    end if
  end if

  !------------------------------------------------------------------------+
  ! save MOs in human and machine readable form
  !------------------------------------------------------------------------+

  do ispin = 1, nspin  
    call writemos(isuhf,ispin,orb(1,1,ispin),orben(1,ispin),nbf,nbf)
  end do

  !------------------------------------------------------------------------+
  ! save energy and in future other stuff...
  !------------------------------------------------------------------------+
  escf = energy

  !------------------------------------------------------------------------+
  ! Print a summary on the energy in a pretty format...
  !------------------------------------------------------------------------+

  allocate(kin(nbf,nbf))
  call getkinints(kin,basis%shells,nbf,nshell,mxcart) 
  ekin   = getekin(kin,dens,nbf,nspin)
  epot   = energy - ekin
  virial = epot/energy

  if (.not. silent) then

    write(istdout,'(/3x,a,a,a)') '+', repeat("-",51), '+'
    write(istdout,'(3x,a,a,a)') '+', repeat(" ",51), '+'

    write(istdout,'(2x,a2,1x,a28,f16.10,a3,3x,a)') '+','RHF energy:                 ', energy , 'Eh','+'

    write(istdout,'(2x,a2,1x,a28,f16.10,a3,3x,a)') '+','Potential energy:           ', epot, 'Eh','+'
    write(istdout,'(2x,a2,1x,a28,f16.10,a3,3x,a)') '+','Kinetic energy:             ', ekin, 'Eh','+'
    write(istdout,'(2x,a2,1x,a28,f16.10,6x,a)')    '+','Virial Theorem:             ', virial, '+'

    write(istdout,'(3x,a,a,a)') '+', repeat(" ",51), '+'
    write(istdout,'(3x,a,a,a)') '+', repeat("-",51), '+'

  end if

  !------------------------------------------------------------------------+
  ! Calculate dipole moment
  !------------------------------------------------------------------------+
  if (.not.silent) then
    call getdipmom(atoms,basis%shells,dens,natoms,nbf,nspin,nshell,mxcart,mxprim) 
  end if

  !------------------------------------------------------------------------+
  ! Do population analysis
  !------------------------------------------------------------------------+
  if (.not. silent) then
    if (nspin == 1) then
      call getmpa(orb,basis%shells,atoms,nbf,nbf,nshell,nocc,natoms) 
      call getlowdin(orb,basis%shells,atoms,nbf,nbf,nshell,nocc,natoms) 
    end if

    write(istdout,'(/3x,a)')  'Profiling:'
    write(istdout,'(5x,a,f10.2)')  'Fock CPU-time (sec)     : ', cpufock
    write(istdout,'(5x,a,f10.2)')  'Obas CPU-time (sec)     : ', cpuobas
    write(istdout,'(5x,a,f10.2)')  'Diag CPU-time (sec)     : ', cpudiag
    write(istdout,'(5x,a,f10.2)')  'Dens CPU-time (sec)     : ', cpudens
  end if

  !------------------------------------------------------------------------+
  ! clean up and leave.... 
  !------------------------------------------------------------------------+
  deallocate(ovl,hone,gint,fock,fockp,xao2o,xao2ot,orb0,orb,orben,dens,fnew,dold)
  memused = memused - nbf*nbf - nbf*nbf - nbf*nbf*nspin - nbf*nbf*nspin &
                    - nbf*nbf - nbf*nbf - nbf*nbf - nbf*nbf             &
                    - nbf*nbf*nspin - nbf*nbf - nbf*nbf*nspin - nbf*nbf &
                    - nbf*nbf*nspin

  if (.not. ScfCalcDef%ldirect) then
    memused = memused - size(twoeint%dintfull)
    call deallocate_int4idx(twoeint)
  end if

  deallocate(gab)
  memused = memused - nshell*nshell

  if (ScfCalcDef%use_diis) then
    deallocate(errmat,diiswei,imap)
    do ispin = 1, nspin 
      memused = memused - size(errlst(ispin)%list) - size(focklst(ispin)%list)
      call deallocate_queue(errlst(ispin))
      call deallocate_queue(focklst(ispin))
    end do
  end if

  deallocate(diffdens,fock2bod)

  do ithrd = 0, nthreads - 1
    call deallocate_intbuf(intbuf(ithrd))
  end do

end subroutine


function getdamping(alphst,step,iter,alphmin) result(dret)
  implicit none

  include 'fortrankinds.h'

  real(kdp) :: dret

  real(kdp), intent(in) :: alphst,alphmin,step
  integer,   intent(in) :: iter

  ! local
  real(kdp) :: alpha

  alpha = max(alphst - step * iter,alphmin)

  dret = alpha
  return
end function


