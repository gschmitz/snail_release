!-------------------------------------------------------------------------+
! Purpose: Computes the MP2 energy using the RI approximations
!
!          There are some drawbacks with this implementation, but is more
!          meant as a proof of principle implementation. 
!
! Gunnar Schmitz
!-------------------------------------------------------------------------+

subroutine rimp2engine(escf,emp2,                                  &  ! in/out
                       silent,                                     &  ! inp
                       Mp2CalcDef,basis,cbas,tolint,memmax,filmo,  &  ! inp
                       nocc,nvir,nspin)                               ! dim

  use type_cgto
  use type_shell
  use type_basis

  use type_mp2calcdef

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! constants
  logical, parameter :: useEig = .false.
  logical, parameter :: useOvlp = .false.

  ! dimensions
  integer, intent(in) :: nocc,nvir,nspin

  ! output
  real(kdp),         intent(out) :: emp2

  ! input
  real(kdp),         intent(in)  :: escf
  type(basis_info),  intent(in)  :: basis
  type(basis_info),  intent(in)  :: cbas
  character(len=80), intent(in)  :: filmo
  real(kdp),         intent(in)  :: tolint
  integer(kli),      intent(in)  :: memmax
  logical,           intent(in)  :: silent

  type(mp2_calcdef), intent(in)  :: Mp2CalcDef

  ! local
  real(kdp), allocatable :: orb(:,:),orben(:)
  real(kdp), allocatable :: vpqmat(:,:),bqmn(:,:,:),Iqmn(:,:,:) 
  real(kdp), allocatable :: temp1(:,:),temp2(:,:),work(:),eigval(:)

  real(kdp), allocatable :: bqmi(:,:,:),bqai(:,:,:),kabij(:,:,:),kabji(:,:,:)

  real(kdp), allocatable :: spqmat(:,:),cpqmat(:,:)

  integer :: ixbf,jxbf,imo,jmo,moa,mob
  integer :: nbf,nshell,mxcart,mxprim,lmax,nxbf,nxshell,lxmax
  integer :: lwork,info

  integer :: nfroz,noct

  integer :: ithrd,nthreads,nthreads_orig

  real(kdp) :: dlen,denom,dfac,pairs,pairt,edoub(2),scsepair,&
               epair,eij,tplus,tminus

  real(kdp)    :: cpuint,cpu0,cpux
  integer(kli) :: memused,memavail

  logical :: verbose

  ! functions
  integer :: get_max_threads, get_thread_num, mkl_get_max_threads


  !-----------------------------------------------------------------------+
  ! Define things for parrallel code 
  !-----------------------------------------------------------------------+
  nthreads = get_max_threads()
  nthreads_orig = mkl_get_max_threads()

  call mkl_set_num_threads(nthreads)

  !-----------------------------------------------------------------------+
  ! define shortcuts 
  !-----------------------------------------------------------------------+
  nbf    = basis%nbf
  nshell = basis%nshell
  mxprim = basis%mxprim
  mxcart = basis%mxcart
  lmax   = basis%lmax

  nxbf    = cbas%nbf
  nxshell = cbas%nshell
  lxmax   = cbas%lmax

  mxcart = max(mxcart,cbas%mxcart)
  mxprim = max(mxprim,cbas%mxprim)

  verbose = .not. silent

  !-----------------------------------------------------------------------+
  ! sanity checks
  !-----------------------------------------------------------------------+

  if (nspin.ne.1) call quit("At the moment only RHF-reference for MP2","mp2engine")

  !-----------------------------------------------------------------------+
  ! allocate memory
  !-----------------------------------------------------------------------+

  allocate(orb(nbf,nbf),orben(nbf))

  !-----------------------------------------------------------------------+
  ! print header information
  !-----------------------------------------------------------------------+
  if (verbose) call PrintHeader('Starting the RI-MP2 calculation')

  !-----------------------------------------------------------------------+
  !  read orbitals and their energies
  !-----------------------------------------------------------------------+
  call readmos(orb,orben,filmo,nbf,nbf)

  !
  ! Apply frozen core approximation?
  !
  nfroz = 0
  
  if (Mp2CalcDef%use_frozen_core) then
    do imo = 1, nocc
      if ( orben(imo) <= Mp2CalcDef%freeze_point ) then
        nfroz = nfroz + 1
      else
        exit
      end if  
    end do

    if (verbose) then
       write(istdout,'(5x,a)')  'Frozen Core Approximation is used'
       write(istdout,'(7x,a,i4,/)') 'No. of frozen occ. orbtials:', nfroz
    end if
  end if

  noct = nocc - nfroz

  !-----------------------------------------------------------------------+
  !  Build 2 index integrals
  !-----------------------------------------------------------------------+
  allocate(vpqmat(nxbf,nxbf))
  allocate(temp1(nxbf,nxbf))
  allocate(temp2(nxbf,nxbf))
  allocate(eigval(nxbf))

  !
  ! Get (P|Q)
  !
  call cpu_time(cpu0)
  call get2ints(vpqmat,                                  & ! out
                verbose,tolint,memmax,lmax,cbas%shells,  & ! inp 
                nxbf,nxshell,mxprim,mxcart)               ! dim 
  call cpu_time(cpux)
  cpuint = cpux - cpu0
  if (.not. silent) write(istdout,'(5x,a,f10.2,/)')  'CPU-time for     (PQ) (sec):', cpuint

  !
  ! Get (P|Q)^{-1/2} or equivalent variants
  !
  call InvertPQMetric(vpqmat,Mp2CalcDef%use_ovlp,useEig,cbas,nxbf,nxshell)

  !-----------------------------------------------------------------------+
  !  Build 3 index integrals
  !-----------------------------------------------------------------------+
  allocate(Iqmn(nxbf,nbf,nbf))
  allocate(bqmn(nxbf,nbf,nbf))

  call cpu_time(cpu0)
  call get3ints(Iqmn,                                                 & ! out
                verbose,Mp2CalcDef%use_ovlp,tolint,memmax,lmax,       & ! inp
                cbas%shells,basis%shells,                             & ! inp 
                nxbf,nbf,nxshell,nshell,mxprim,mxcart,mxprim,mxcart)    ! dim 
  call cpu_time(cpux)
  cpuint = cpux - cpu0
  if (.not. silent) write(istdout,'(5x,a,f10.2,/)')  'CPU-time for   (Q|mn) (sec):', cpuint

  call cpu_time(cpu0)
  if (.not. silent) write(istdout,'(5x,a)')  'Transforming to MO basis'
  !
  ! B(Q,mn) = (PQ)^(-1/2) I(Q,mn)
  !
  call dgemm( 'n', 'n', nxbf, nbf*nbf, nxbf, one  &
            , vpqmat, nxbf, Iqmn, nxbf, zero, bqmn, nxbf )
 
  !
  ! B(Q,mi) = B(Q,mn) C_n,i
  !
  allocate(bqmi(nxbf,nbf,noct))
  call dgemm( 'n', 'n', nxbf*nbf, noct, nbf, one   &
            , bqmn, nxbf*nbf, orb(1,nfroz+1), nbf  & 
            , zero, bqmi, nxbf*nbf)  

  !
  ! B(Q,ai) = C_m,a B(Q,mi) 
  !
  allocate(bqai(nxbf,nvir,noct))
  do imo = 1, noct
    call dgemm( 'n', 'n', nxbf, nvir, nbf, one &
              , bqmi(1,1,imo), nxbf            &
              , orb(1,nocc+1), nbf, zero, bqai(1,1,imo), nxbf ) 
  end do
  call cpu_time(cpux)
  cpuint = cpux - cpu0
  if (.not. silent) write(istdout,'(5x,a,f10.2,/)')  'CPU-time traf. (Q|ai) (sec):', cpuint

  allocate(kabij(nvir,nvir,0:nthreads-1), &
           kabji(nvir,nvir,0:nthreads-1))

  edoub = zero

  ! Set number of threads in MKL to 1 since the loop is parallelized
  call mkl_set_num_threads(1)

  !$OMP PARALLEL DO SCHEDULE(AUTO) &
  !$OMP DEFAULT(NONE) &
  !$OMP SHARED(nocc,noct,nfroz,nvir,nxbf,bqai,kabij,kabji,orben) &
  !$OMP PRIVATE(ithrd,imo,jmo,moa,mob,pairs,pairt,tplus,tminus,denom,dfac,eij) &
  !$OMP REDUCTION(+: edoub)
  do imo = 1, noct

    ithrd = get_thread_num() 

    dfac = two
    do jmo = 1, imo
      if (imo == jmo) dfac = one
      eij = orben(imo+nfroz) + orben(jmo+nfroz)

      pairs = zero
      pairt = zero

      ! (ai|bj) = B(Q,ai) B(Q,bj)
      call dgemm( 't', 'n', nvir, nvir, nxbf, one &
                , bqai(1,1,imo), nxbf, bqai(1,1,jmo), nxbf, zero, kabij(1,1,ithrd), nvir) 

      ! (aj|bi) = B(Q,aj) B(Q,bi)
      call dgemm( 't', 'n', nvir, nvir, nxbf, one &
                , bqai(1,1,jmo), nxbf, bqai(1,1,imo), nxbf, zero, kabji(1,1,ithrd), nvir) 

      !------------------------------------------------------------------+ 
      ! t_{ab}^{ij} =  - (ai|bj) / (ea + eb - ei - ej)
      ! L_{ab}^{ij} = 2 t_{ab}^{ij} - t_{ab}^{ji} 
      !------------------------------------------------------------------+ 

      do moa = 1, nvir
        do mob = 1, moa - 1
          denom =  ( orben(nocc + moa) + orben(nocc + mob) - eij )

          tplus  = ( kabji(mob,moa,ithrd) + kabij(mob,moa,ithrd) )   
          tminus = ( kabji(mob,moa,ithrd) - kabij(mob,moa,ithrd) )  

          pairs = pairs + half * tplus * tplus / denom
          pairt = pairt + thrhalf * tminus * tminus / denom
        end do
        denom =  two *  orben(nocc + moa) - eij 
        tplus = ( kabji(moa,moa,ithrd) + kabij(moa,moa,ithrd) ) 
        pairs = pairs + 0.25d0 * tplus * tplus / denom
      end do

      edoub(1) = edoub(1) - dfac * pairs
      edoub(2) = edoub(2) - dfac * pairt

    end do
  end do
  !$OMP END PARALLEL DO
 
  epair = edoub(1) + edoub(2)
  emp2  = epair

  !-----------------------------------------------------------------------+
  ! print MP2 and SCS-MP2 (correlation) energy 
  !-----------------------------------------------------------------------+

  if (.not. silent) then
     call prtenergy(escf,epair,edoub,.true.)
  end if

  ! reset threads in MKL
  call mkl_set_num_threads(nthreads_orig)

  deallocate(orb,orben)
  deallocate(vpqmat,bqmn,bqmi,bqai,kabij,kabji)
end subroutine



