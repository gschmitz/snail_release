

subroutine xyzgeo(xyz,natoms,na,nb,nc,deg2rad,geo)
!--------------------------------------------------------------------+
! Purpose: converts from cartesian to internal coordinates
!
!     on input xyz    = cartesian coordinates
!              natoms = number of atoms
!              NA     = indeces of atom ot which atoms are related 
!                       by distance
!              NB     = indeces of atom ot which atoms are related
!                       by angle
!              NC     = indeces of atom ot which atoms are related
!                       by dihedral angle
!
!    on outputgeo  = internal coordinates in a.u., radians and radians
!
!--------------------------------------------------------------------+

    implicit none

    include 'fortrankinds.h'
    include 'numbers.h'

    ! dimensions
    integer, intent(in) :: natoms
    
    ! input
    real(kdp), intent(in) :: xyz(3,natoms)
    real(kdp), intent(in) :: deg2rad

    ! output
    integer,   intent(inout) :: na(natoms), nb(natoms), nc(natoms)
    real(kdp), intent(inout) :: geo(3,natoms)

    ! local
    integer :: iat,jat,kat,lat,iiat
   

    do iat = 2, natoms 
       jat = na(iat)
       kat = nb(iat)
       lat = nc(iat)

       geo(1,iat)= dsqrt((xyz(1,iat)-xyz(1,jat))**2 &
                       + (xyz(2,iat)-xyz(2,jat))**2 &
                       + (xyz(3,iat)-xyz(3,jat))**2)

       if (iat.lt.3) cycle
       iiat = iat
       call getangle(xyz,iiat,jat,kat,geo(2,iat))
       geo(2,iat) = geo(2,iat) * deg2rad
       
       if (iat.lt.4) cycle
       call getdihed(xyz,iiat,jat,kat,lat,geo(3,iat))
       geo(3,iat) = geo(3,iat) * deg2rad
    end do
 
    geo(1,1) = zero 
    geo(2,1) = zero 
    geo(3,1) = zero 
    geo(2,2) = zero 
    geo(3,2) = zero 
    geo(3,3) = zero 
end subroutine

subroutine xyzint(xyz,natoms,na,nb,nc,deg2rad,geo)
!--------------------------------------------------------------------+
!
! Purpose : Determine the internal coordinates of a molecule
!
!        The rules to determine the connectivity are:
!
!        atom i is defined as being at a distance from the nearest 
!        atom j, with atom j already having been defined.
!
!        atom i makes an angle with atom j and atom k, which 
!        already has been defined, and is the nearest to atom j 
!
!        atom i makes a dihedral angle with atoms j, k and l, with l
!        having been definies and being the nearest atom to k 
!
!
!   on input  xyz  = cartesian coordinates
!   on outout geo  = internal coordintates
!
!--------------------------------------------------------------------+
       
    implicit none

    include 'fortrankinds.h'

    ! dimensions
    integer,   intent(in) :: natoms

    ! input:
    real(kdp), intent(in) :: xyz(3,natoms) 
    real(kdp), intent(in) :: deg2rad

    ! output
    integer,   intent(inout) :: na(natoms),nb(natoms),nc(natoms)
    real(kdp), intent(inout) :: geo(3,natoms)

    ! local
    integer   :: nai1,nai2,im1,iat,jat,kat
    real(kdp) :: r,dsum 

    nai1 = 0
    nai2 = 0

    do iat = 1, natoms
       na(iat) = 2
       nb(iat) = 3
       nc(iat) = 4
       im1 = iat - 1
       if (im1.eq.0) cycle
       dsum = 1.D30
       do jat = 1, im1
          r = (xyz(1,iat) - xyz(1,jat))**2 + (xyz(2,iat) - xyz(2,jat))**2 + (xyz(3,iat) - xyz(3,jat))**2
          if (r.lt.dsum.and.na(jat).ne.jat.and.nb(jat).ne.jat) then 
             dsum = r
             kat = jat
          end if
       end do

    ! atom i is nearest to atom k 

       na(iat) = kat
       if (iat.gt.2) nb(iat) = na(kat)
       if (iat.gt.3) nc(iat) = nb(kat)

    ! find any atom to relate to na(iat) 

    end do

    na(1) = 0
    nb(1) = 0
    nc(1) = 0
    nb(2) = 0
    nc(2) = 0
    nc(3) = 0

    call xyzgeo(xyz,natoms,na,nb,nc,deg2rad,geo)
end subroutine


