
subroutine dang(a1,a2,b1,b2,rcos)
!--------------------------------------------------------------------+
! Purpose: determines the angles between the points 
!             (a1,a2), (0,0) and (b1,b2)
!
! output: rcos
!--------------------------------------------------------------------+
    implicit none

    include 'fortrankinds.h'
    include 'numbers.h'
    include 'constants.h'

    ! constants
    real(kdp), parameter :: eps = 1.0D-6

    ! output
    real(kdp), intent(out) :: rcos

    ! input/output 
    real(kdp), intent(inout)  :: a1,a2,b1,b2

    ! local
    real(kdp) :: anorm,bnorm,sinth,costh  

    if ( dabs(a1).lt.eps.and. dabs(a2).lt.eps) then
      rcos = zero 
    elseif ( dabs(b1).lt.eps.and. dabs(b2).lt.eps) then
      rcos = zero
    else
      anorm = one / dsqrt(a1**2+a2**2)
      bnorm = one / dsqrt(b1**2+b2**2)
      a1 = a1 * anorm
      a2 = a2 * anorm
      b1 = b1 * bnorm
      b2 = b2 * bnorm

      sinth = (a1*b2) - (a2*b1)
      costh = a1*b1 + a2*b2

      if (costh.gt. one) costh =  one
      if (costh.lt.-one) costh = -one

      rcos = dacos(costh)

      if ( dabs(rcos).lt.4.0d-4) then
        rcos = zero
      elseif (sinth.gt.zero) then 
        rcos = twopi - rcos
      end if

      rcos = -rcos
    end if
end subroutine

