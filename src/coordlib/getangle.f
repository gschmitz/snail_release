
subroutine getangle(xyz,iat,jat,kat,angle)
!--------------------------------------------------------------------+
! Purpose: Calculates the angle between atoms i,j and k
!
!   on input  xzy = cartesian coordinates
!
!   on output angle = self explenatory
!
!--------------------------------------------------------------------+
       
    implicit none

    include 'fortrankinds.h'
    include 'numbers.h'

    ! input
    real(kdp), intent(in) :: xyz(3,*)
    integer,   intent(in) :: iat,jat,kat

    ! output
    real(kdp), intent(inout) :: angle
 

    ! local
    real(kdp) :: d2ij,d2jk,d2ik,xy,dtemp 

    d2ij = (xyz(1,iat)-xyz(1,jat))**2 &
         + (xyz(2,iat)-xyz(2,jat))**2 &
         + (xyz(3,iat)-xyz(3,jat))**2
    d2jk = (xyz(1,jat)-xyz(1,kat))**2 &
         + (xyz(2,jat)-xyz(2,kat))**2 &
         + (xyz(3,jat)-xyz(3,kat))**2
    d2ik = (xyz(1,iat)-xyz(1,kat))**2 &
         + (xyz(2,iat)-xyz(2,kat))**2 &
         + (xyz(3,iat)-xyz(3,kat))**2

    xy = dsqrt(d2ij * d2jk)

    dtemp = half * (d2ij + d2jk - d2ik) / xy

    if (dtemp .gt.  one) dtemp =  one
    if (dtemp .lt. -one) dtemp = -one

    angle = dacos( dtemp )
end subroutine 

