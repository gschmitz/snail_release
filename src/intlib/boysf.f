!============================================================================================
! Routines to compute the Boys-Function, which is needed for the nuclear attraction
! and the electron repulsion integral. 
!
!
!============================================================================================

 function BoysF(m0,x0,fntab,usetab,nxvals,nrange,kmax) result(dret)
 !-------------------------------------------------------------------------------------+
 ! Purpose: Calculates the Boys function $F_m(x) = \int_0^1 t^{2m}e^{-xt^2}\, dt.$
 !          for a given m (m0) and x (x0) combining different strategies:
 !
 !           1. Use a six-term Taylor expansion around pre-tabulated values
 !           2. Asymptotic formula for large x (> 40.)
 !           3. Asymptotic formula for small x (< 5.5)
 !           4. Numerical evaluation for remaining cases
 !
 !--------------------------------------------------------------------------------------+

   implicit none

   include 'fortrankinds.h'
   include 'constants.h'
   include 'numbers.h'

   real(kdp) :: dret

   ! dimensions
   integer,   intent(in) :: nxvals,kmax,nrange

   real(kdp), intent(in) :: x0
   integer,   intent(in) :: m0

   logical,   intent(in) :: usetab
   real(kdp), intent(in) :: fntab(0:kmax,nxvals)

   ! local
   real(kdp) :: xk,fm,kf,gamln,x,val
   integer   :: k,m
   ! functions
   real(kdp) :: IGamma_norm,doublefact,GammaLn,BoysFTabulated

   x = x0; m = m0

   k = 0
   fm = zero
   kf = one
   xk = one

   !if (x < real(nrange,kdp) .and. m < kmax - 7 .and. usetab) then
   !if (x < 30.d0 .and. m < kmax - 7 .and. usetab) then
   if (x < 30.d0 .and. m < kmax  .and. usetab) then
   !if (.false.) then
     !---------------------------------------------------+
     ! extrapolate using tabulated data
     !---------------------------------------------------+
   
     dret = BoysFTabulated(m,x,fntab,nxvals,nrange,kmax) 

   else

     ! Check if we are in the regime where the Taylor series is ok
     if (x .le. 5.5d0) then
       x = -x
 
       do while (k .le. 38)
         ! F_m(x) = \sum_{k=0}^\infty \frac{(-x)^k}{k! (2m+2k+1)}
         fm = fm + xk / (kf * (2.0d0 * real(m + k,kdp) + 1.d0))
         k  = k + 1
         xk = xk * x
         kf = kf * k

       end do

       dret = fm
     elseif (x .ge. 40) then
       ! use asymptotic expansion, which is precise to <1e-16 for F_n(x), n = 0..60
       dret = doublefact(2 * m - 1) / 2**(m+1) * dsqrt(PI / x**(2.0d0*m+1))
     else
       ! Need to use excat formula
       gamln =  GammaLn(m + 0.5d0)
       dret = 0.5d0 * dexp(gamln) * x**(-m-0.5d0) * IGamma_norm(m + 0.5d0, x)
     end if
   end if

   return 
 end function

 function BoysFTabulated(m0,x0,fntab,nxvals,nrange,kmax) result(dret)
 !-------------------------------------------------------------------------------------+
 ! Purpose: Calculates the Boys function $F_m(x) = \int_0^1 t^{2m}e^{-xt^2}\, dt.$
 !          for a given m (m0) and x (x0) using  a six-term Taylor expansion 
 !          around pre-tabulated values
 !
 !--------------------------------------------------------------------------------------+

   implicit none

   include 'fortrankinds.h'
   include 'constants.h'
   include 'numbers.h'

   real(kdp) :: dret

   ! constants
   real(kdp), parameter :: c3 = 1.d0 / (2.d0*3.0d0),& 
                           c4 = 1.d0 / (2.d0*3.0d0*4.0d0),& 
                           c5 = 1.d0 / (2.d0*3.0d0*4.0d0*5.0d0),& 
                           c6 = 1.d0 / (2.d0*3.0d0*4.0d0*5.0d0*6.0d0) 

   ! dimensions
   integer,   intent(in) :: nxvals,nrange,kmax

   real(kdp), intent(in) :: x0
   integer,   intent(in) :: m0
  
   real(kdp), intent(in) :: fntab(0:kmax,nxvals)

   ! local
   integer   :: idx,k
   real(kdp) :: dx,xt,diff,factorial,dval
   real(kdp) :: d1,d2,d3,d4,d5,d6

   dx   = 0.1d0                              ! spacing in tabulated data
   idx  = int((x0 + 0.5d0 * dx) / dx) + 1    ! index of nearest tabulated x-value 
   xt   = (idx-1) * dx                       ! nearest tabulated x-value
   diff = x0 - xt                            ! difference between actual 
                                             !  and tabulated x-value 

   dret = zero
   factorial = one

   !-----------------------------------------------+
   ! first code
   !-----------------------------------------------+

   !do k = 0, 6 
   !  if (k > 0) then
   !    factorial = factorial * real(k,kdp)
   !  end if

   !  dval = fntab(m0 + k, idx) * (-diff)**k / factorial

   !  dret = dret + dval
 
   !end do

   !-----------------------------------------------+
   ! second code (loop unrolling)
   !-----------------------------------------------+
   !dret = dret + fntab(m0, idx)  
   !dret = dret + fntab(m0 + 1, idx) * (-diff)  
   !dret = dret + fntab(m0 + 2, idx) *   diff **2 / 2.d0 
   !dret = dret + fntab(m0 + 3, idx) * (-diff)**3 / (2.d0*3.0d0) 
   !dret = dret + fntab(m0 + 4, idx) *   diff **4 / (2.d0*3.0d0*4.0d0) 
   !dret = dret + fntab(m0 + 5, idx) * (-diff)**5 / (2.d0*3.0d0*4.0d0*5.0d0) 
   !dret = dret + fntab(m0 + 6, idx) *   diff **6 / (2.d0*3.0d0*4.0d0*5.0d0*6.0d0) 

   !-----------------------------------------------+
   ! third code (loop unrolling, constant folding)
   !-----------------------------------------------+

   d1 = -diff
   d2 = d1 * (-diff)
   d3 = d2 * (-diff) 
   d4 = d3 * (-diff) 
   d5 = d4 * (-diff) 
   d6 = d5 * (-diff) 

   dret = dret + fntab(m0, idx)  
   dret = dret + fntab(m0 + 1, idx) * d1 
   dret = dret + fntab(m0 + 2, idx) * d2 * 0.5d0  
   dret = dret + fntab(m0 + 3, idx) * d3 * c3 
   dret = dret + fntab(m0 + 4, idx) * d4 * c4 
   dret = dret + fntab(m0 + 5, idx) * d5 * c5 
   dret = dret + fntab(m0 + 6, idx) * d6 * c6 

   return
 end function

 subroutine WriteTabBoys(homedir)
 !-------------------------------------------------------------------------------------+
 ! Purpose: Generates a file fboys-tab.dat with pre-tabulated values and saves it 
 !          the directory homedir. 
 !--------------------------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'
   include 'constants.h'
   include 'numbers.h'

   ! parameter
   character(len=80) :: fdatabase = "fboys-tab.dat"
  
   ! input
   character(len=255) :: homedir
 
   ! local
   integer   :: nrange,nxvals,iunftab,kmax,idx,k,reclen,irec
   real(kdp) :: xval,dx

   real(kdp), allocatable :: fntab(:,:)

   ! dummies
   integer   :: idum
   real(kdp) :: dummy

   nrange = 50
   nxvals = 500
   kmax   = 50
   dx     = 0.1d0 ! spacing 

   allocate(fntab(0:kmax,nxvals))

   do idx = 1, nxvals
     xval = (idx-1) * dx  
     call BoysFArr(kmax,xval,fntab(0,idx),fntab,.false.,idum,idum,idum)
   end do

   ! open file for tabulated data
   iunftab = 9
   reclen = (kmax+1) * sizeof(dummy) !* 2 
   open(iunftab,file=trim(homedir) // '/bin/' // trim(fdatabase),form='unformatted',access='direct',recl=reclen) 

   irec = 0
   do idx = 1, nxvals
     irec = irec + 1
     write(iunftab,rec=irec) (fntab(k,idx),k=0,kmax)
   end do
   close(iunftab)

   ! clean up and leave
   deallocate(fntab)
 end subroutine

 subroutine BoysFArr(mmax,x,bf,fntab,usetab,nxvals,nrange,kmax)
 !-------------------------------------------------------------------------------------+
 ! Purpose: Calculates the Boys function $F_m(x) = \int_0^1 t^{2m}e^{-xt^2}\, dt.$
 !          for a given x to all orders 0 ... mmax. For the highest order the 
 !          Boys function is evaluated explicitly and afterwards the down-wards 
 !          recurrence relation is used.
 !
 !--------------------------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'

   ! dimensions
   integer,   intent(in)  :: mmax,nxvals,nrange,kmax

   ! input
   real(kdp), intent(in)  :: x
   real(kdp), intent(out) :: bf(0:mmax)
   real(kdp), intent(in)  :: fntab(0:kmax,nxvals)
   logical,   intent(in)  :: usetab

   ! local
   integer   :: m
   real(kdp) :: emx
   ! functions
   real(kdp) :: BoysF

   emx = dexp(-x)

   ! Fill in the highest value
   bf(mmax) = BoysF(mmax, x, fntab, usetab, nxvals, nrange, kmax)

   if (mmax .gt.0) then
     ! Peform downward recursion to obtain the other values
     do m = mmax - 1, 0, -1
        bf(m) =  (2.0d0 * x * bf(m + 1) + emx) / (2.0d0 * m + 1.0d0)
     end do 
   end if 

 end subroutine


 function  IGamma_norm(a,x) result(dret)
 !-------------------------------------------------------------------------------------+
 !  Normalised incomplete Gamma function
 !  P(n,x) = 1/Gamma(n) \int_{0}^{\infty} exp(-t)t^{n-1}dt
 !-------------------------------------------------------------------------------------+
 
   implicit none

   include 'fortrankinds.h'
   include 'constants.h'  
   include 'numbers.h'

   real(kdp) :: dret

   real(kdp), intent(in) :: a,x

   real(kdp) :: ans, ax, r, c

   ! functions
   real(kdp) :: GammaLn

   if (x .lt. zero .or. a .le. zero ) then
     dret = zero
   elseif (x.eq.zero) then
     dret = zero
   else
     ax = a * dlog(x) - x - GammaLn(a)
     if (MAXLOG < -ax) then
       ! error underflow
       dret = zero
     else
       ax = dexp(ax)
       r = a
       c = one
       ans = one
       do while  (MACHEP * ans .lt. C)
         r = r + one
         c = c * x / r
         ans = ans + c
       end do
       dret = ans * ax / a
     end if
   end if

   return
 end function

 function GammaLn(x) result(dret)
 !-------------------------------------------------------------------------------------+
 !  Ln (Gamma) function
 !  Input
 !  dX = function argument; dX > 0
 ! Output
 !  GAMMALN = Ln (Gamma) function
 !-------------------------------------------------------------------------------------+

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'  
 
   real(kdp) :: dret

   real(kdp), intent(in) :: x

   real(kdp) :: ser, tmp, y, xtmp, stp, cof(6)
   integer   :: j

   COF(1) = 76.1800917294715d0
   COF(2) = -86.5053203294168d0
   COF(3) = 24.0140982408309d0
   COF(4) = -1.23173957245015d0
   COF(5) = 0.00120865097386618d0
   COF(6) = -0.000005395239384953d0

   STP = 2.506628274631d0

   XTMP = X

   Y = XTMP

   TMP = XTMP + 5.5d0

   TMP = (XTMP + 0.5d0) * dlog(TMP) - TMP   

   SER = 1.00000000019001d0

   do j = 1, 6
     y = y + one
     ser = ser + cof(j) / y
   end do

   dret = tmp + dlog(stp * ser / xtmp)

   return
 end function
