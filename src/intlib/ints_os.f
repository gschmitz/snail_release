!========================================================================
! Obara Saika scheme for the evaluation of molecular integrals
!
! Implemented:
!  - Overlap integrals
!  - Integrals over the kinetic energy operator
!  - Nuclear attraction integral
!  - Coulomb integral (but other file)
!
!========================================================================

 subroutine os_overlap_shell(ovlp,&
                             ax,aexps,acoefs,anorms,cartcompa,ang_a,&
                             bx,bexps,bcoefs,bnorms,cartcompb,ang_b,&
                             nprimi,nprimj,ncarta,ncartb)

    implicit none

    include 'fortrankinds.h'
    include 'numbers.h'

    ! dimensions
    integer, intent(in) :: ang_a, ang_b, nprimi, nprimj, ncarta, ncartb

    ! output
    real(kdp), intent(out) :: ovlp(ncarta,ncartb)

    ! input
    real(kdp), intent(in) :: ax(3),bx(3)
    real(kdp), intent(in) :: aexps(nprimi),acoefs(nprimi),anorms(nprimi,ncarta), &
                             bexps(nprimj),bcoefs(nprimj),bnorms(nprimj,ncartb)

    integer,   intent(in) :: cartcompa(3,ncarta),cartcompb(3,ncartb)

    ! local
    integer   :: iprim,jprim,icart,jcart
    integer   :: la,ma,na,lb,mb,nb
    real(kdp) :: alpha,beta,zeta,mu,px(3)

    real(kdp), allocatable :: sx(:,:),sy(:,:),sz(:,:)

    ! functions 
    real(kdp) :: product_center_1D

    ovlp = zero

    allocate(sx(0:ang_a,0:ang_b),sy(0:ang_a,0:ang_b),sz(0:ang_a,0:ang_b))

    do iprim = 1, nprimi
      do jprim = 1, nprimj

        alpha = aexps(iprim)
        beta  = bexps(jprim)

        zeta = alpha + beta
        mu   = alpha * beta / zeta

        px(1) = product_center_1D(alpha,ax(1),beta,bx(1))
        px(2) = product_center_1D(alpha,ax(2),beta,bx(2))
        pX(3) = product_center_1D(alpha,ax(3),beta,bx(3))

        call os_overlap_1D_shell(sx, ang_a, ang_b, px(1)-ax(1), px(1)-bx(1), zeta, mu, ax(1), bx(1))
        call os_overlap_1D_shell(sy, ang_a, ang_b, px(2)-ax(2), px(2)-bx(2), zeta, mu, ax(2), bx(2))
        call os_overlap_1D_shell(sz, ang_a, ang_b, px(3)-ax(3), px(3)-bx(3), zeta, mu, ax(3), bx(3))

        do jcart = 1, ncartb
          lb = cartcompb(1,jcart)
          mb = cartcompb(2,jcart)
          nb = cartcompb(3,jcart)

          do icart = 1, ncarta
            la = cartcompa(1,icart)
            ma = cartcompa(2,icart)
            na = cartcompa(3,icart)

            ovlp(icart,jcart) = ovlp(icart,jcart) +  anorms(iprim,icart) * acoefs(iprim) &
                                                   * bnorms(jprim,jcart) * bcoefs(jprim) &
                                                   * sx(la,lb) * sy(ma,mb) * sz(na,nb)
          end do
        end do
      end do
    end do
 
    deallocate(sx,sy,sz)
 end subroutine

 function os_overlap(alpha,la,ma,na,ax,beta,lb,mb,nb,bx) result(dret)
   implicit none

   include 'fortrankinds.h'

    real(kdp) :: dret

    real(kdp), intent(in) :: alpha,beta,ax(3),bx(3)
    integer,   intent(in) :: la,ma,na,lb,mb,nb

    real(kdp) :: zeta,mu,px(3),sx,sy,sz

    real(kdp), allocatable :: arrdum(:)

    ! functions
    real(kdp) :: product_center_1D,os_overlap_1D

    allocate(arrdum(1))

    zeta =  alpha + beta
    mu   = alpha * beta / zeta

    px(1) = product_center_1D(alpha,ax(1),beta,bx(1))
    px(2) = product_center_1D(alpha,ax(2),beta,bx(2))
    pX(3) = product_center_1D(alpha,ax(3),beta,bx(3))

    sx =  os_overlap_1D(la, lb, px(1)-ax(1), px(1)-bx(1), zeta, mu, ax(1), bx(1), arrdum, .false.)
    sy =  os_overlap_1D(ma, mb, px(2)-ax(2), px(2)-bx(2), zeta, mu, ax(2), bx(2), arrdum, .false.)
    sz =  os_overlap_1D(na, nb, px(3)-ax(3), px(3)-bx(3), zeta, mu, ax(3), bx(3), arrdum, .false.)

    dret = sx * sy * sz

    deallocate(arrdum)
    return
 end function

 function os_overlap_1D(la,lb,PAx,PBx,zeta,mu,Ax,Bx,Sij,SaveSij) result(dret)
   implicit none

   include 'fortrankinds.h'

   real(kdp) :: dret

   integer,   intent(in)   :: la,lb
   real(kdp), intent(in)   :: PAx,PBx,zeta,mu,Ax,Bx
   real(kdp), intent(out)  :: Sij(0:la,0:lb)
   logical,   intent(in)   :: SaveSij

   ! local
   real(kdp) :: S00,Xab2,o2z
   integer   :: l,m,lidx,midx

   real(kdp), allocatable :: S(:,:)

   ! functions
   real(kdp) :: GetS00

   allocate(S(0:la,0:lb))

   Xab2 = (Ax - Bx) * (Ax - Bx)
   S00  = GetS00(zeta, mu, Xab2)
   o2z  =  1.0d0 / (2.0d0 * zeta) 

   S(0,0) = S00

   ! Do some special cases to avoid checks inside the loop
   if (la .gt. 0) S(1, 0) = PAx * S(0, 0)
   if (lb .gt. 0) S(0, 1) = PBx * S(0, 0)
   if (la .gt. 0 .And. lb .gt. 0) S(1, 1) = PAx * S(0, 1) + o2z * S(0, 0)

   if (la .gt. 1 .or. lb .gt. 1) then
      ! Use recurrence realations to arrive at Slm
      do m = 0, lb
        do l = 0, la
          ! Calculate S_{l+1,m} from S_{l,m}
          lidx = max(l - 1, 0); midx = max(m - 1, 0)
          if (l.lt.la) then
            S(l + 1, m) = PAx * S(l, m) + o2z * (l * S(lidx, m) + m * S(l, midx))
          end if
          ! Calculate S_{l,m+1} from S_{l,m}
          if (m.lt.lb) then
            S(l, m + 1) = PBx * S(l, m) + o2z * (l * S(lidx, m) + m * S(l, midx))
          end if
        end do
      end do
    end if

    if (SaveSij) call dcopy((la + 1) * (lb + 1), S(0,0), 1, Sij(0,0),1)

    dret = S(la,lb)
   return
 end function

 subroutine os_overlap_1D_shell(S,la,lb,PAx,PBx,zeta,mu,Ax,Bx) 
   implicit none

   include 'fortrankinds.h'

   integer,   intent(in)   :: la,lb
   real(kdp), intent(in)   :: PAx,PBx,zeta,mu,Ax,Bx
   real(kdp), intent(out)  :: S(0:la,0:lb)

   ! local
   real(kdp) :: S00,Xab2,o2z
   integer   :: l,m,lidx,midx

   ! functions
   real(kdp) :: GetS00

   Xab2 = (Ax - Bx) * (Ax - Bx)
   S00  = GetS00(zeta, mu, Xab2)
   o2z  =  1.0d0 / (2.0d0 * zeta) 

   S(0,0) = S00

   ! Do some special cases to avoid checks inside the loop
   if (la .gt. 0) S(1, 0) = PAx * S(0, 0)
   if (lb .gt. 0) S(0, 1) = PBx * S(0, 0)
   if (la .gt. 0 .And. lb .gt. 0) S(1, 1) = PAx * S(0, 1) + o2z * S(0, 0)

   if (la .gt. 1 .or. lb .gt. 1) then
      ! Use recurrence realations to arrive at Slm
      do m = 0, lb
        do l = 0, la
          ! Calculate S_{l+1,m} from S_{l,m}
          lidx = max(l - 1, 0); midx = max(m - 1, 0)
          if (l.lt.la) then
            S(l + 1, m) = PAx * S(l, m) + o2z * (l * S(lidx, m) + m * S(l, midx))
          end if
          ! Calculate S_{l,m+1} from S_{l,m}
          if (m.lt.lb) then
            S(l, m + 1) = PBx * S(l, m) + o2z * (l * S(lidx, m) + m * S(l, midx))
          end if
        end do
      end do
    end if

 end subroutine 

 function GetS00(zeta,mu,Xab2) result(dret)
   implicit none

   include 'fortrankinds.h'
   include 'constants.h'

   real(kdp) :: dret

   real(kdp), intent(in) :: zeta,mu,Xab2

   dret = dsqrt(PI / zeta) * dexp(-mu * Xab2)
   return
 end function

 function GetT00(zeta,mu,Xab2,alpha,Xpa2,fact) result(dret)
   implicit none

   include 'fortrankinds.h'

   real(kdp) :: dret

   real(kdp), intent(in) :: zeta,mu,Xab2,alpha,Xpa2,fact

   real(kdp) :: S00

   ! functions
   real(kdp) :: GetS00  

   S00 = GetS00(zeta, mu, Xab2) 

   dret = alpha * (1.0d0 - 2.0d0 * alpha * (Xpa2 + fact)) * S00 
   return
 end function


 function os_kinetic_1d(alpha,la,beta,lb,Pax,PBx,zeta,mu,Ax,Bx,S) result(dret)
   implicit none

   include 'fortrankinds.h'

   real(kdp) :: dret

   integer,   intent(in) :: la,lb
   real(kdp), intent(in) :: alpha,beta,Pax,PBx,zeta,mu,Ax,Bx,S(0:la,0:lb)


   real(kdp) :: alp_zet,bet_zet,twoalp,twobet,o2z,Xab2,Xpa2,T00
   integer   :: l,m,lidx,midx

   real(kdp), allocatable :: T(:,:)

   real(kdp) :: GetT00

   allocate(T(0:la,0:lb))

   alp_zet = alpha / zeta
   bet_zet = beta / zeta
   twoalp  = 2.0d0 * alpha
   twobet  = 2.0d0 * beta
   o2z     = 1.0d0 / (2.0d0 * zeta)

   Xab2 = (Ax - Bx) * (Ax - Bx)
   Xpa2 = PAx * PAx
   T00  = GetT00(zeta, mu, Xab2, alpha, Xpa2, o2z)

   T(0,0) = T00

   ! Do some special cases to avoid checks inside the loop
   if (la .gt. 0) T(1, 0) = PAx * T(0, 0) + bet_zet * twoalp * S(1, 0)
   if (lb .gt. 0)  T(0, 1) = PBx * T(0, 0) + alp_zet * twobet * S(0, 1)
   if (la .gt. 0 .and. lb .gt. 0) T(1, 1) = PAx * T(0, 1) + o2z * T(0, 0) + bet_zet * twoalp * S(1, 1)

   if (la .gt. 1 .or. lb .gt. 1) then
     do m = 0, lb
       do l = 0, la
          lidx = max(l-1,0); midx = max(m-1,0)
          ! Calculate T_{l+1,m} from T_{l,m}
          if (l.lt.la) then
            T(l + 1, m) = PAx * T(l, m) + o2z * (l * T(lidx, m) + m * T(l, midx)) &
                                  + bet_zet * (twoalp * S(l + 1, m) - l * S(lidx, m))
          end if
          ! Calculate T_{l,m+1} from T_{l,m}
          if (m.lt.lb) then
            T(l, m + 1) = PBx * T(l, m) + o2z * (l * T(lidx, m) + m * T(l, midx)) &
                                  + alp_zet * (twobet * S(l, m + 1) - m * S(l, midx))
          end if
       end do
     end do

   end if

   dret = T(la,lb)

   deallocate(T)
   return
 end function

 subroutine os_kinetic_1d_shell(T,alpha,la,beta,lb,Pax,PBx,zeta,mu,Ax,Bx,S) 
   implicit none

   include 'fortrankinds.h'

   ! dimensions
   integer,   intent(in) :: la,lb
  
   ! output
   real(kdp), intent(out) :: T(0:la,0:lb)

   ! input
   real(kdp), intent(in) :: alpha,beta,Pax,PBx,zeta,mu,Ax,Bx,S(0:la,0:lb)

   ! local
   real(kdp) :: alp_zet,bet_zet,twoalp,twobet,o2z,Xab2,Xpa2,T00
   integer   :: l,m,lidx,midx

   ! functions
   real(kdp) :: GetT00


   alp_zet = alpha / zeta
   bet_zet = beta / zeta
   twoalp  = 2.0d0 * alpha
   twobet  = 2.0d0 * beta
   o2z     = 1.0d0 / (2.0d0 * zeta)

   Xab2 = (Ax - Bx) * (Ax - Bx)
   Xpa2 = PAx * PAx
   T00  = GetT00(zeta, mu, Xab2, alpha, Xpa2, o2z)

   T(0,0) = T00

   ! Do some special cases to avoid checks inside the loop
   if (la .gt. 0) T(1, 0) = PAx * T(0, 0) + bet_zet * twoalp * S(1, 0)
   if (lb .gt. 0)  T(0, 1) = PBx * T(0, 0) + alp_zet * twobet * S(0, 1)
   if (la .gt. 0 .and. lb .gt. 0) T(1, 1) = PAx * T(0, 1) + o2z * T(0, 0) + bet_zet * twoalp * S(1, 1)

   if (la .gt. 1 .or. lb .gt. 1) then
     do m = 0, lb
       do l = 0, la
          lidx = max(l-1,0); midx = max(m-1,0)
          ! Calculate T_{l+1,m} from T_{l,m}
          if (l.lt.la) then
            T(l + 1, m) = PAx * T(l, m) + o2z * (l * T(lidx, m) + m * T(l, midx)) &
                                  + bet_zet * (twoalp * S(l + 1, m) - l * S(lidx, m))
          end if
          ! Calculate T_{l,m+1} from T_{l,m}
          if (m.lt.lb) then
            T(l, m + 1) = PBx * T(l, m) + o2z * (l * T(lidx, m) + m * T(l, midx)) &
                                  + alp_zet * (twobet * S(l, m + 1) - m * S(l, midx))
          end if
       end do
     end do

   end if

 end subroutine


 function os_kinetic(alpha,la,ma,na,Ax,beta,lb,mb,nb,Bx) result(dret)
   implicit none

   include 'fortrankinds.h'

   real(kdp) :: dret

   integer,   intent(in) :: la,ma,na,lb,mb,nb
   real(kdp), intent(in) :: alpha,beta,Ax(3),Bx(3)

   real(kdp), allocatable :: Sxij(:,:),Syij(:,:),Szij(:,:)
   real(kdp) :: Px(3),Sx,Sy,Sz,Tx,Ty,Tz,mu,zeta

   ! functions
   real(kdp) :: os_overlap_1D, os_kinetic_1D,product_center_1D

   allocate(Sxij(0:la,0:lb),Syij(0:ma,0:mb),Szij(0:na,0:nb))

   ! calculate product center
   Px(1) = product_center_1D(alpha,Ax(1),beta,Bx(1))
   Px(2) = product_center_1D(alpha,Ax(2),beta,Bx(2))
   PX(3) = product_center_1D(alpha,Ax(3),beta,Bx(3))
 
   zeta = alpha + beta
   mu   = alpha * beta / zeta

   ! For the x coordinate
   Sx = os_overlap_1D(la, lb, Px(1)-Ax(1), Px(1)-Bx(1), &
                      zeta, mu, Ax(1), Bx(1), Sxij, .True.)
   Tx = os_kinetic_1D(alpha, la, beta, lb, Px(1)-Ax(1), Px(1)-Bx(1), &
                      zeta, mu, Ax(1), Bx(1), Sxij)
   ! For the y coordinate
   Sy = os_overlap_1D(ma, mb, Px(2)-Ax(2), Px(2)-Bx(2), &
                      zeta, mu, Ax(2), Bx(2), Syij, .True.)
   Ty = os_kinetic_1D(alpha, ma, beta, mb, Px(2)-Ax(2), Px(2)-Bx(2), &
                      zeta, mu, Ax(2), Bx(2), Syij)
   ! For the z coordinate
   Sz = os_overlap_1D(na, nb, Px(3)-Ax(3), Px(3)-Bx(3), &
                      zeta, mu, Ax(3), Bx(3), Szij, .True.)
   Tz = os_kinetic_1D(alpha, na, beta, nb, Px(3)-Ax(3), Px(3)-Bx(3), &
                      zeta, mu, Ax(3), Bx(3), Szij)


   dret = (Tx * Sy * Sz + Sx * Ty * Sz + Sx * Sy * Tz)

   deallocate(Sxij,Syij,Szij)
   return 
 end function

 subroutine os_kinetic_shell(kin,&
                             ax,aexps,acoefs,anorms,cartcompa,ang_a,&
                             bx,bexps,bcoefs,bnorms,cartcompb,ang_b,&
                             nprimi,nprimj,ncarta,ncartb)

    implicit none

    include 'fortrankinds.h'
    include 'numbers.h'

    ! dimensions
    integer, intent(in) :: ang_a, ang_b, nprimi, nprimj, ncarta, ncartb

    ! output
    real(kdp), intent(out) :: kin(ncarta,ncartb)

    ! input
    real(kdp), intent(in) :: ax(3),bx(3)
    real(kdp), intent(in) :: aexps(nprimi),acoefs(nprimi),anorms(nprimi,ncarta), &
                             bexps(nprimj),bcoefs(nprimj),bnorms(nprimj,ncartb)

    integer,   intent(in) :: cartcompa(3,ncarta),cartcompb(3,ncartb)

    ! local
    integer   :: iprim,jprim,icart,jcart
    integer   :: la,ma,na,lb,mb,nb
    real(kdp) :: alpha,beta,zeta,mu,px(3)

    real(kdp), allocatable :: sx(:,:),sy(:,:),sz(:,:)
    real(kdp), allocatable :: tx(:,:),ty(:,:),tz(:,:)

    ! functions 
    real(kdp) :: product_center_1D

    kin = zero

    allocate(sx(0:ang_a,0:ang_b),sy(0:ang_a,0:ang_b),sz(0:ang_a,0:ang_b))
    allocate(tx(0:ang_a,0:ang_b),ty(0:ang_a,0:ang_b),tz(0:ang_a,0:ang_b))

    do iprim = 1, nprimi
      do jprim = 1, nprimj

        alpha = aexps(iprim)
        beta  = bexps(jprim)

        zeta = alpha + beta
        mu   = alpha * beta / zeta

        px(1) = product_center_1D(alpha,ax(1),beta,bx(1))
        px(2) = product_center_1D(alpha,ax(2),beta,bx(2))
        pX(3) = product_center_1D(alpha,ax(3),beta,bx(3))

        ! overlap integrals
        call os_overlap_1D_shell(sx, ang_a, ang_b, px(1)-ax(1), px(1)-bx(1), zeta, mu, ax(1), bx(1))
        call os_overlap_1D_shell(sy, ang_a, ang_b, px(2)-ax(2), px(2)-bx(2), zeta, mu, ax(2), bx(2))
        call os_overlap_1D_shell(sz, ang_a, ang_b, px(3)-ax(3), px(3)-bx(3), zeta, mu, ax(3), bx(3))

        ! kinetic integrals
        call os_kinetic_1D_shell(tx, alpha, ang_a, beta, ang_b, px(1)-ax(1), px(1)-bx(1), zeta, mu, ax(1), bx(1), sx)
        call os_kinetic_1D_shell(ty, alpha, ang_a, beta, ang_b, px(2)-ax(2), px(2)-bx(2), zeta, mu, ax(2), bx(2), sy)
        call os_kinetic_1D_shell(tz, alpha, ang_a, beta, ang_b, px(3)-ax(3), px(3)-bx(3), zeta, mu, ax(3), bx(3), sz)

        do jcart = 1, ncartb
          lb = cartcompb(1,jcart)
          mb = cartcompb(2,jcart)
          nb = cartcompb(3,jcart)

          do icart = 1, ncarta
            la = cartcompa(1,icart)
            ma = cartcompa(2,icart)
            na = cartcompa(3,icart)

            kin(icart,jcart) = kin(icart,jcart) +  anorms(iprim,icart) * acoefs(iprim) &
                                                 * bnorms(jprim,jcart) * bcoefs(jprim) &
                                                 * ( tx(la,lb) * sy(ma,mb) * sz(na,nb) &
                                                   + sx(la,lb) * ty(ma,mb) * sz(na,nb) &
                                                   + sx(la,lb) * sy(ma,mb) * tz(na,nb) &
                                                   ) 
                                                      
          end do
        end do
      end do
    end do
 
    deallocate(sx,sy,sz)
    deallocate(tx,ty,tz)
 end subroutine

 function os_getind(l,m,n) result(ind)
   implicit none

   integer :: ind

   integer, intent(in) :: l,m,n
   integer :: ii,jj

   ii = m + n
   jj = n

   !  return index in one dimensional array
   ind = ii * (ii + 1) / 2 + jj + 1 
   return
 end function

 function os_nuclear(Ax,norma,la,ma,na,alpha,Bx,normb,lb,mb,nb,beta,NucX) result(dret)
   implicit none

   include 'fortrankinds.h'

   real(kdp) :: dret

   real(kdp), intent(in) :: Ax(3),Bx(3),NucX(3)
   real(kdp), intent(in) :: norma,alpha,normb,beta
   integer,   intent(in) :: la,ma,na,lb,mb,nb

   ! local
   integer   :: am_a, am_b, idx1, idx2, nsize1, nsize2
   real(kdp) :: zeta,Px(3) 
   real(kdp), allocatable :: ints(:,:) 

   ! functions
   real(kdp) :: product_center_1D 
   integer   :: os_getind

   am_a = la + ma + na
   am_b = lb + mb + nb

   zeta = alpha + beta

   Px(1) = product_center_1D(alpha,Ax(1),beta,Bx(1))
   Px(2) = product_center_1D(alpha,Ax(2),beta,Bx(2))
   PX(3) = product_center_1D(alpha,Ax(3),beta,Bx(3))
   
   nsize1 = (am_a+1)*(am_a+2)/2
   nsize2 = (am_b+1)*(am_b+2)/2 

   allocate(ints(nsize1,nsize2))

   call os_nuclear_shell(ints,Ax,Bx,Nucx,Px,am_a,am_b,alpha,beta,zeta)

   idx1 = os_getind(la, ma, na)
   idx2 = os_getind(lb, mb, nb)

   dret = ints(idx1,idx2) * norma * normb

   deallocate(ints)
   return
 end function


 subroutine os_nuclear_shell(Vnai,xyza,xyzb,xyz_nuc,xyzp,am_a,am_b,alpha,beta,zeta)
 !---------------------------------------------------------------------------------+ 
 ! Obara Saika scheme for nuclear attraction integrals for the given shell pair
 !
 !---------------------------------------------------------------------------------+ 
   implicit none

   include 'fortrankinds.h'
   include 'constants.h'
   include 'numbers.h'
 

   real(kdp), intent(in)  :: xyza(3),xyzb(3),xyz_nuc(3),xyzp(3)
   real(kdp), intent(in)  :: alpha,beta,zeta
   integer,   intent(in)  :: am_a,am_b
   real(kdp), intent(out) :: Vnai((am_a+1)*(am_a+2)/2,(am_b+1)*(am_b+2)/2)


   ! local
   real(kdp) :: o2z,PAx,PAy,PAz,PBx,PBy,PBz,PCx,PCy,PCz,rab2,rpc2,boysarg,prefac
   integer   :: mmax,size_a,size_b,Nan,Nam,Nal,Nbn,Nbm,Nbl,m,lambdab,lambdaa,&
                ii, jj, kk, ll,lb, mb, nb, la, ma, na, aidx, bidx, ia, ib
   real(kdp), allocatable :: ints(:,:,:),bf(:),fntab(:,:)

   ! dummies
   real(kdp) :: dummy
   integer   :: idummy

   ! functions
   real(kdp) :: dist2

   idummy = 1

   o2z  = one / (two * zeta)

   PAx  = xyzp(1) - xyza(1)
   PAy  = xyzp(2) - xyza(2)
   PAz  = xyzp(3) - xyza(3)

   PBx  = xyzp(1) - xyzb(1)
   PBy  = xyzp(2) - xyzb(2)
   PBz  = xyzp(3) - xyzb(3)

   PCx  = xyzp(1) - xyz_nuc(1)
   PCy  = xyzp(2) - xyz_nuc(2)
   PCz  = xyzp(3) - xyz_nuc(3)

   rab2 = dist2(xyza, xyzb)
   rpc2 = dist2(xyzp, xyz_nuc)

   ! Sum of angular momenta
   mmax = am_a + am_b

   size_a = (am_a + 1) * (am_a + 1) * am_a
   size_b = (am_b + 1) * (am_b + 1) * am_b

   allocate(ints(0:size_a, 0:size_b, 0:mmax)) 

   ! Helpers for computing indices on work array
   Nan = 1
   Nam = am_a + 1
   Nal = Nam * Nam

   Nbn = 1
   Nbm = am_b + 1
   Nbl = Nbm * Nbm

   ! Argument of Boys' function
   boysarg = zeta * rpc2
   allocate(bf(0:mmax))
   allocate(fntab(0:0,1)) ! dummy
   call BoysFArr(mmax,boysarg,bf,fntab,.false.,idummy,idummy,idummy)
   deallocate(fntab)

   ! Constant prefactor for auxiliary integrals
   prefac = two * PI / zeta * dexp(-alpha * beta * rab2 / zeta)


   ! Initialize integral array (0_A | A(0) | 0_B)^(m)
   do m = 0, mmax
     ints(0, 0, m) = prefac * bf(m)
   end do

   ! Increase angular momentum on right hand side.

   ! Loop over total angular momentum
   do lambdab = 1,  am_b
     ! Loop over angular momentum functions belonging to this shell
     do ii = 0,  lambdab
       lb = lambdab - ii

       do jj = 0, ii
         mb = ii - jj
         nb = jj

         ! index in array
         bidx = lb * Nbl + mb * Nbm + nb * Nbn

         if (nb > 0) then
           do m = 0, mmax - lambdab
             ints(0, bidx, m) = PBz * ints(0, bidx - Nbn, m) - PCz * ints(0, bidx - Nbn, m + 1)
           end do

           if (nb > 1) then
             do m = 0, mmax - lambdab
               ints(0, bidx, m) = ints(0, bidx, m) + o2z * (nb - 1) * (ints(0, bidx - 2 * Nbn, m) &
                                  - ints(0, bidx - 2 * Nbn, m + 1))
             end do
           end if

         elseif (mb > 0) then
           do m = 0, mmax - lambdab
             ints(0, bidx, m) = PBy * ints(0, bidx - Nbm, m) - PCy * ints(0, bidx - Nbm, m + 1)
           end do
           if (mb > 1) then
             do m = 0, mmax - lambdab
               ints(0, bidx, m) = ints(0, bidx, m) + o2z * (mb - 1) * (ints(0, bidx - 2 * Nbm, m) &
                                  - ints(0, bidx - 2 * Nbm, m + 1))
             end do
           end if
         elseif (lb > 0) then

           do m = 0, mmax - lambdab
             ints(0, bidx, m) = PBx * ints(0, bidx - Nbl, m) - PCx * ints(0, bidx - Nbl, m + 1)
           end do
           if (lb > 1) then
             do m = 0, mmax - lambdab
               ints(0, bidx, m) = ints(0, bidx, m) + o2z * (lb - 1) * (ints(0, bidx - 2 * Nbl, m) &
                                  - ints(0, bidx - 2 * Nbl, m + 1))
             end do
           end if
         else
           call quit("Something went wrong in the Obara-Saika", "nuclear attraction integral algorithm.")
         end if
       end do 
     end do
   end do

   ! Now, increase the angular momentum of the left-hand side, too.

   ! Loop over total angular momentum of RHS
   do lambdab = 0, am_b
     ! Loop over the functions belonging to the shell
     do ii = 0, lambdab
       lb = lambdab - ii
       do jj = 0, ii
         mb = ii - jj
         nb = jj

         ! RHS index is
         bidx = lb * Nbl + mb * Nbm + nb * Nbn

         ! Loop over total angular momentum of LHS
         do lambdaa = 1, am_a
           ! Loop over angular momentum of second shell
           do kk = 0, lambdaa
             la = lambdaa - kk
             do ll = 0, kk
               ma = kk - ll
               na = ll

               ! LHS index is
               aidx = la * Nal + ma * Nam + na * Nan

               if (na > 0) then

                 do m = 0, mmax - lambdaa - lambdab
                   ints(aidx, bidx, m) = PAz * ints(aidx - Nan, bidx, m) - PCz * ints(aidx - Nan, bidx, m + 1)
                 end do

                 if (na > 1) then
                   do m = 0, mmax - lambdaa - lambdab
                     ints(aidx, bidx, m) = ints(aidx, bidx, m) + o2z * (na - 1) * (ints(aidx - 2 * Nan, bidx, m) &
                                             - ints(aidx - 2 * Nan, bidx, m + 1))
                   end do
                 end if

                 if (nb > 0) then
                   do m = 0, mmax - lambdaa - lambdab
                     ints(aidx, bidx, m) = ints(aidx, bidx, m) + o2z * nb * (ints(aidx - Nan, bidx - Nbn, m) &
                                             - ints(aidx - Nan, bidx - Nbn, m + 1))
                   end do
                 end if

               elseif (ma > 0) then

                 do m = 0, mmax - lambdaa - lambdab
                   ints(aidx, bidx, m) = PAy * ints(aidx - Nam, bidx, m) - PCy * ints(aidx - Nam, bidx, m + 1)
                 end do

                 if (ma > 1) then
                   do m = 0, mmax - lambdaa - lambdab
                     ints(aidx, bidx, m) = ints(aidx, bidx, m) + o2z * (ma - 1) * (ints(aidx - 2 * Nam, bidx, m) &
                                             - ints(aidx - 2 * Nam, bidx, m + 1))
                   end do
                 end if

                 if (mb > 0) then
                   do m = 0, mmax - lambdaa - lambdab
                     ints(aidx, bidx, m) = ints(aidx, bidx, m) + o2z * mb * (ints(aidx - Nam, bidx - Nbm, m) &
                                             - ints(aidx - Nam, bidx - Nbm, m + 1))
                   end do
                 end if

               elseif (la > 0) then

                 do m = 0, mmax - lambdaa - lambdab
                   ints(aidx, bidx, m) = PAx * ints(aidx - Nal, bidx, m) - PCx * ints(aidx - Nal, bidx, m+1)
                 end do

                 if (la > 1) then
                   do m = 0, mmax - lambdaa - lambdab
                     ints(aidx, bidx, m) = ints(aidx, bidx, m) + o2z * (la-1) * (ints(aidx-2*Nal, bidx, m) &
                                           - ints(aidx-2*Nal, bidx, m+1))
                   end do
                 end if

                 if (lb > 0) then
                   do m = 0, mmax - lambdaa - lambdab
                     ints(aidx, bidx, m) = ints(aidx, bidx, m) + o2z * lb * (ints(aidx-Nal, bidx-Nbl, m) &
                                           - ints(aidx-Nal, bidx-Nbl, m+1))
                    end do
                 end if
               else
                 call quit("Something went wrong in the Obara-Saika","nuclear attraction integral algorithm.") 
               end if
             end do 
           end do
         end do
       end do
     end do
   end do

   ! Size of returned array
   na = (am_a + 1) * (am_a + 2) / 2 
   nb = (am_b + 1) * (am_b + 2) / 2

   !----------------------------------------------------------------+
   ! Fill in array
   !----------------------------------------------------------------+

   ! Index of left basis function
   ia = 1
   
   ! init
   Vnai = zero 

   ! Loop over basis functions on this shell
   do ii = 0, am_a
       la = am_a - ii
       do jj = 0, ii
           ma = ii - jj
           na = jj
           ! Index in working array
           aidx = la * Nal + ma * Nam + na

           ! Index of right basis function
           ib = 1

           ! Loop over angular momentum of second shell
           do kk = 0, am_b
               lb = am_b - kk
               do ll = 0, kk
                   mb = kk - ll
                   nb = ll
                   ! Other index is
                   bidx = lb * Nbl + mb * Nbm + nb

                   ! Store result
                   Vnai(ia, ib) = - ints(aidx, bidx, 0)

                   ! Increment index of basis function
                   ib = ib + 1
               end do
           end do
           ia = ia + 1
       end do
   end do

 end subroutine

 function os_contr_coulomb(aexps,acoefs,anorms,xyza,la,ma,na, &
                           bexps,bcoefs,bnorms,xyzb,lb,mb,nb, &
                           cexps,ccoefs,cnorms,xyzc,lc,mc,nc, &
                           dexps,dcoefs,dnorms,xyzd,ld,md,nd, &
                           nprima,nprimb,nprimc,nprimd,&
                           ibf,jbf,kbf,lbf,xyzp,xyzq,Kab,Kcd,&
                           vrr_terms,maxmtot,maxam,nbuf,&
                           fntab,nxvals,nrange,kmax) result(dret)
 !--------------------------------------------------------------------------------+
 ! Wrapper for the routine contr_hrr, which uses the Head-Gordon Pople scheme
 ! (based on Obara-Saika) to evaluate electron repulsion integrals
 !--------------------------------------------------------------------------------+
   implicit none
 
   include 'fortrankinds.h'
 
   ! output:
   real(kdp) :: dret
 
   ! dimensions
   integer, intent(in) :: nprima,nprimb,nprimc,nprimd
   integer, intent(in) :: maxmtot,maxam,nbuf
   integer, intent(in) :: nxvals,nrange,kmax
 
   ! scratch
   real(kdp), intent(inout) :: vrr_terms(nbuf)

   ! input:
   real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3)
   real(kdp), intent(in) :: xyzp(3,nprima,nprimb),xyzq(3,nprimc,nprimd),&
                            Kab(nprima,nprimb),Kcd(nprimc,nprimd)
   real(kdp), intent(in) :: anorms(nprima),bnorms(nprimb),cnorms(nprimc),dnorms(nprimd)
   real(kdp), intent(in) :: aexps(nprima),bexps(nprimb),cexps(nprimc),dexps(nprimd)
   real(kdp), intent(in) :: acoefs(nprima),bcoefs(nprimb),ccoefs(nprimc),dcoefs(nprimd)
   integer,   intent(in) :: la,ma,na,lb,mb,nb,&
                            lc,mc,nc,ld,md,nd,&
                            ibf,jbf,kbf,lbf

   real(kdp), intent(in) :: fntab(0:kmax,nxvals)

   ! local
   real(kdp) :: rab2,rcd2
 
   ! functions
   real(kdp) :: dist2

   rab2 = dist2(xyza,xyzb)
   rcd2 = dist2(xyzc,xyzd)
 
   ! invoke Head-Gordon Pople (based on Obara-Saika) recurrence relations
   call contr_hrr(dret,&
                  nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                  nprimb, xyzb, bnorms, lb, mb, nb, bexps, bcoefs, &
                  nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                  nprimd, xyzd, dnorms, ld, md, nd, dexps, dcoefs, &
                  ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                  vrr_terms,maxmtot,maxam,nbuf, &
                  fntab, nxvals, nrange, kmax)
 
   return
 end function

 subroutine os_eri(dret,&
                   aexps,acoefs,anorms,xyza,la,ma,na, &
                   bexps,bcoefs,bnorms,xyzb,lb,mb,nb, &
                   cexps,ccoefs,cnorms,xyzc,lc,mc,nc, &
                   dexps,dcoefs,dnorms,xyzd,ld,md,nd, &
                   nprima,nprimb,nprimc,nprimd,rij2,&
                   ibf,jbf,kbf,lbf,&
                   ishl,jshl,kshl,lshl,xyzp,xyzq,Kab,Kcd,&
                   vrr_terms,maxmtot,maxam,nbuf,&
                   fntab,nxvals,nrange,kmax,nshell) 
 !--------------------------------------------------------------------------------+
 ! Wrapper for the routine contr_hrr, which uses the Head-Gordon Pople scheme
 ! (based on Obara-Saika) to evaluate electron repulsion integrals
 !--------------------------------------------------------------------------------+
   implicit none
 
   include 'fortrankinds.h'
 
   ! output:
   real(kdp), intent(out) :: dret
 
   ! dimensions
   integer, intent(in) :: nprima,nprimb,nprimc,nprimd
   integer, intent(in) :: maxmtot,maxam,nbuf
   integer, intent(in) :: nxvals,nrange,kmax,nshell
 
   ! scratch
   real(kdp), intent(inout) :: vrr_terms(nbuf)

   ! input:
   real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3)
   real(kdp), intent(in) :: xyzp(3,nprima,nprimb),&
                            xyzq(3,nprimc,nprimd),&
                            Kab(nprima,nprimb),&
                            Kcd(nprimc,nprimd)
   real(kdp), intent(in) :: anorms(nprima),bnorms(nprimb),cnorms(nprimc),dnorms(nprimd)
   real(kdp), intent(in) :: aexps(nprima),bexps(nprimb),cexps(nprimc),dexps(nprimd)
   real(kdp), intent(in) :: acoefs(nprima),bcoefs(nprimb),ccoefs(nprimc),dcoefs(nprimd)
   integer,   intent(in) :: la,ma,na,lb,mb,nb,&
                            lc,mc,nc,ld,md,nd,&
                            ibf,jbf,kbf,lbf,&
                            ishl,jshl,kshl,lshl

   real(kdp), intent(in) :: fntab(0:kmax,nxvals)
   real(kdp), intent(in) :: rij2(nshell*(nshell+1)/2)

   ! local
   real(kdp) :: rab2,rcd2
 
   rab2 = rij2(ishl*(ishl-1)/2 + jshl) ! dist2(xyza,xyzb)
   rcd2 = rij2(kshl*(kshl-1)/2 + lshl) ! dist2(xyzc,xyzd)

   ! invoke Head-Gordon Pople (based on Obara-Saika) recurrence relations
   call contr_hrr(dret,&
                  nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                  nprimb, xyzb, bnorms, lb, mb, nb, bexps, bcoefs, &
                  nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                  nprimd, xyzd, dnorms, ld, md, nd, dexps, dcoefs, &
                  ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                  vrr_terms,maxmtot,maxam,nbuf, &
                  fntab, nxvals, nrange, kmax)
 
 end subroutine

 subroutine os_eri_shell(gout,&
                         aexps,acoefs,anorms,xyza,la,ma,na, &
                         bexps,bcoefs,bnorms,xyzb,lb,mb,nb, &
                         cexps,ccoefs,cnorms,xyzc,lc,mc,nc, &
                         dexps,dcoefs,dnorms,xyzd,ld,md,nd, &
                         nprima,nprimb,nprimc,nprimd,&
                         ncarta,ncartb,ncartc,ncartd,&
                         rij2,ibf,jbf,kbf,lbf,&
                         ishl,jshl,kshl,lshl,xyzp,xyzq,Kab,Kcd,&
                         vrr_terms,maxmtot,maxam,nbuf,&
                         fntab,nxvals,nrange,kmax,nshell) 
 !--------------------------------------------------------------------------------+
 ! Wrapper for the routine contr_hrr, which uses the Head-Gordon Pople scheme
 ! (based on Obara-Saika) to evaluate electron repulsion integrals.
 ! Integrals for a shell (cartesian components) are currently done stupidly...
 ! In a future version the loops have to be integrated in contr_hrr 
 ! to reuse more intermediates....
 !--------------------------------------------------------------------------------+
   implicit none
 
   include 'fortrankinds.h'
 
   ! dimensions
   integer, intent(in) :: nprima,nprimb,nprimc,nprimd
   integer, intent(in) :: ncarta,ncartb,ncartc,ncartd
   integer, intent(in) :: maxmtot,maxam,nbuf
   integer, intent(in) :: nxvals,nrange,kmax,nshell

   ! output:
   real(kdp), intent(out) :: gout(ncarta,ncartb,ncartc,ncartd) 
 
   ! scratch
   real(kdp), intent(inout) :: vrr_terms(nbuf)

   ! input:
   real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3)
   real(kdp), intent(in) :: xyzp(3,nprima,nprimb),&
                            xyzq(3,nprimc,nprimd),&
                            Kab(nprima,nprimb),&
                            Kcd(nprimc,nprimd)
   real(kdp), intent(in) :: anorms(nprima,ncarta),bnorms(nprimb,ncartb),&
                            cnorms(nprimc,ncartc),dnorms(nprimd,ncartd)
   real(kdp), intent(in) :: aexps(nprima,ncarta),bexps(nprimb,ncartb),&
                            cexps(nprimc,ncartc),dexps(nprimd,ncartd)
   real(kdp), intent(in) :: acoefs(nprima,ncarta),bcoefs(nprimb,ncartb),&
                            ccoefs(nprimc,ncartc),dcoefs(nprimd,ncartd)
   integer,   intent(in) :: la(ncarta),ma(ncarta),na(ncarta),&
                            lb(ncartb),mb(ncartb),nb(ncartb),&
                            lc(ncartc),mc(ncartc),nc(ncartc),&
                            ld(ncartd),md(ncartd),nd(ncartd),&
                            ibf,jbf,kbf,lbf,&
                            ishl,jshl,kshl,lshl

   real(kdp), intent(in) :: fntab(0:kmax,nxvals)
   real(kdp), intent(in) :: rij2(nshell*(nshell+1)/2)

   ! local
   real(kdp) :: rab2,rcd2
   integer   :: icart,jcart,kcart,lcart
 
   ! functions
   real(kdp) :: dist2

   rab2 = rij2(ishl*(ishl-1)/2 + jshl) ! dist2(xyza,xyzb)
   rcd2 = rij2(kshl*(kshl-1)/2 + lshl) ! dist2(xyzc,xyzd)

   do icart = 1, ncarta
     do jcart = 1, ncartb
       do kcart = 1, ncartc
         do lcart = 1, ncartd
 
           ! invoke Head-Gordon Pople (based on Obara-Saika) recurrence relations
           call contr_hrr(gout(icart,jcart,kcart,lcart),&
                          nprima, xyza, anorms(1,icart), la(icart), ma(icart), na(icart), aexps(1,icart), acoefs(1,icart), &
                          nprimb, xyzb, bnorms(1,jcart), lb(jcart), mb(jcart), nb(jcart), bexps(1,jcart), bcoefs(1,jcart), &
                          nprimc, xyzc, cnorms(1,kcart), lc(kcart), mc(kcart), nc(kcart), cexps(1,kcart), ccoefs(1,kcart), &
                          nprimd, xyzd, dnorms(1,lcart), ld(lcart), md(lcart), nd(lcart), dexps(1,lcart), dcoefs(1,lcart), &
                          ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                          vrr_terms,maxmtot,maxam,nbuf, &
                          fntab, nxvals, nrange, kmax)
         end do
       end do
     end do
   end do
 end subroutine

 function os_multipole(alpha,la,ma,na,ax,beta,lb,mb,nb,bx,xx,lx,mx,nx) result(dret)
   implicit none

   include 'fortrankinds.h'

    real(kdp) :: dret

    real(kdp), intent(in) :: alpha,beta,ax(3),bx(3),xx(3)
    integer,   intent(in) :: la,ma,na,lb,mb,nb,lx,mx,nx

    real(kdp) :: zeta,mu,px(3),sx,sy,sz

    ! functions
    real(kdp) :: product_center_1D,os_multipole_1D

    zeta =  alpha + beta
    mu   = alpha * beta / zeta

    px(1) = product_center_1D(alpha,ax(1),beta,bx(1))
    px(2) = product_center_1D(alpha,ax(2),beta,bx(2))
    pX(3) = product_center_1D(alpha,ax(3),beta,bx(3))

    sx =  os_multipole_1D(la, lb, lx, px(1)-ax(1), px(1)-bx(1), px(1)-xx(1), zeta, mu, ax(1), bx(1))
    sy =  os_multipole_1D(ma, mb, mx, px(2)-ax(2), px(2)-bx(2), px(2)-xx(2), zeta, mu, ax(2), bx(2))
    sz =  os_multipole_1D(na, nb, nx, px(3)-ax(3), px(3)-bx(3), px(3)-xx(3), zeta, mu, ax(3), bx(3))
    dret = sx * sy * sz

    return
 end function

 function os_multipole_1d(la,lb,nx,PAx,PBx,PXx,zeta,mu,Ax,Bx) result(dret)
   implicit none
   !-----------------------------------------------------------------------------+
   ! Purpose: computes 1D-multipole integrals S_ab^nx (a|x^nx|b) by Obara-Saika
   !          recurrence relations. We are lazy and invoke os_overlap_1d to get
   !          S_ab^0. The needed recurrence realtions then are
   !
   !          S_ij^e+1 = PXx S_ij^e + 1/(2*zeta) 
   !                             (i S_i-1j^e + j S_ij-1^e + e S_ij^e-1 ) 
   !-----------------------------------------------------------------------------+

   include 'fortrankinds.h'

   real(kdp) :: dret

   integer,   intent(in)   :: la,lb,nx
   real(kdp), intent(in)   :: PAx,PBx,PXx,zeta,mu,Ax,Bx

   ! local
   real(kdp) :: o2z,term,dtemp
   integer   :: ix 

   real(kdp), allocatable :: S(:,:,:)

   ! functions
   real(kdp) :: os_overlap_1d

   allocate(S(0:la,0:lb,0:nx))

   o2z  =  1.0d0 / (2.0d0 * zeta) 

   ! get S_ab^0.
   dtemp = os_overlap_1D(la,lb,PAx,PBx,zeta,mu,Ax,Bx,S,.true.) 

   ! now generate remaining powers
   term = 0.0d0
   do ix = 1, nx
     if (la > 0)   term = term + la * S(la-1,lb,ix-1)
     if (lb > 0)   term = term + lb * S(la,lb-1,ix-1)
     if (ix-1 > 0) term = term + (ix-1) * S(la,lb,ix-2)
     
     S(la,lb,ix) = PXx * S(la,lb,ix-1) * o2z * term
   end do

   dret = S(la,lb,nx)

   deallocate(S)

   return
 end function
