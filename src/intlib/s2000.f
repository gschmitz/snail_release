!
! Calculate (ds|ss) integrals by differentiating the (ss|ss) integrals
!
! Note: ! NOT WORKING RIGHT NOW !
!
! Info: 
! -----
!
! d_x   = P_x - Q_x
! dij   = A_x - B_x
! alpha = p*q/(p+q)                      => rho in notes 
! pfac  = 2 pi^(5/2) * (sqrt(p+q)/(pq))  => theta in notes
!
pure subroutine s2000(gout,&
                      Kab,cnab,Kcd,cncd,&
                      aexp,bexp,dij,&
                      Fn,pfac,dx,dy,dz,&
                      nprimi,nprimj,nprimij,&
                      nprimkl,nprimijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer, intent(in) :: nprimi,nprimj,nprimij,nprimkl,nprimijkl

  ! output:
  real(kdp), intent(out) :: gout(6)

  ! input
  real(kdp), intent(in) :: Kab(nprimij),&
                           Kcd(nprimkl) 
  real(kdp), intent(in) :: cnab(nprimij),cncd(nprimkl),dij(3),&
                           aexp(nprimi),bexp(nprimj)
  real(kdp), intent(in) :: Fn(0:2,nprimijkl)  
  real(kdp), intent(in) :: pfac(nprimijkl),&
                           dx(nprimijkl),dy(nprimijkl),dz(nprimijkl)

  ! local
  real(kdp) :: dfac
  real(kdp) :: xint0, xint1, xint2, a2
  real(kdp) :: ax2,axy,axz,ay2,ayx,ayz,az2,axx,ayy,azz
  real(kdp) :: bx2,bxy,bxz,by2,byx,byz,bz2,bxx,byy,bzz
  real(kdp) :: cxx,cxy,cxz,cyx,czz,cyz,cyy
  real(kdp) :: x2s,xys,xzs,y2s,yzs,z2s

  integer :: ijp,klp,ijkl,iprim,jprim

  real(kdp), allocatable :: p(:),u(:)

  allocate(p(nprimij),u(nprimij))

  do jprim = 1, nprimj
    do iprim = 1, nprimi
      ijp = (jprim-1)*nprimi + iprim

      p(ijp) = aexp(iprim) + bexp(jprim)
      u(ijp) = aexp(iprim) * bexp(jprim) / p(ijp)

    end do
  end do

  ! x^2 : (00|00) A^x2 + (00|00)^(1) B^x2 + (00|00)^(2) C^xx
  ! xy  : (00|00) A^xy + (00|00)^(1) B^xy + (00|00)^(2) C^xy
  ! xz  : (00|00) A^xz + (00|00)^(1) B^xz + (00|00)^(2) C^xz
  ! y^2 : (00|00) A^y2 + (00|00)^(1) B^y2 + (00|00)^(2) C^yy
  ! yz  : (00|00) A^yx + (00|00)^(1) B^yz + (00|00)^(2) C^yz
  ! z^2 : (00|00) A^z2 + (00|00)^(1) B^z2 + (00|00)^(2) C^zz
  x2s = zero ; xys = zero ; xzs = zero ; y2s = zero ; yzs = zero ; z2s = zero

  do iprim = 1, nprimi

    a2 = aexp(iprim) * aexp(iprim)

    do jprim = 1, nprimj
      ijp = (jprim-1)*nprimi + iprim

      dfac = u(ijp) * u(ijp) / a2
      axx  = dfac * dij(1) * dij(1)
      axy  = dfac * dij(1) * dij(2)
      axz  = dfac * dij(1) * dij(3)
      ayy  = dfac * dij(2) * dij(2)
      ayz  = dfac * dij(2) * dij(3)
      azz  = dfac * dij(3) * dij(3)

      dfac = - u(ijp)/(two * a2) + one / (two * aexp(iprim))
      ax2  = axx + dfac 
      ay2  = ayy + dfac 
      az2  = azz + dfac 

      dfac = u(ijp) / (aexp(ijp) * p(ijp))
      bxx  = dfac * (dx(ijp) * dij(1) + dx(ijp) * dij(1) )
      bxy  = dfac * (dx(ijp) * dij(2) + dy(ijp) * dij(1) )
      bxz  = dfac * (dx(ijp) * dij(3) + dz(ijp) * dij(1) )
      byy  = dfac * (dy(ijp) * dij(2) + dy(ijp) * dij(2) )
      byz  = dfac * (dy(ijp) * dij(3) + dz(ijp) * dij(2) )
      bzz  = dfac * (dz(ijp) * dij(3) + dz(ijp) * dij(3) )

      dfac = - one/(two * p(ijp) * p(ijp))
      bx2  = bxx + dfac
      by2  = byy + dfac
      bz2  = bzz + dfac

      dfac = one / (p(ijp) * p(ijp))
      cxx  = dfac * dx(ijp) * dx(ijp) 
      cxy  = dfac * dx(ijp) * dy(ijp) 
      cxz  = dfac * dx(ijp) * dz(ijp) 
      cyy  = dfac * dy(ijp) * dy(ijp) 
      cyz  = dfac * dy(ijp) * dz(ijp) 
      czz  = dfac * dz(ijp) * dz(ijp) 


      do klp = 1, nprimkl

        ijkl = (klp-1) * nprimij + ijp  

        dfac  = pfac(ijkl) * Kab(ijp) * Kcd(ijp)
        xint0 = Fn(0,ijkl) * dfac 
        xint1 = Fn(1,ijkl) * dfac 
        xint2 = Fn(2,ijkl) * dfac 

        x2s = x2s + cnab(ijp) * cncd(klp) * (xint0 * ax2 + xint1 * bx2 + xint2 * cxx ) 
        xys = xys + cnab(ijp) * cncd(klp) * (xint0 * axy + xint1 * bxy + xint2 * cxy ) 
        xzs = xzs + cnab(ijp) * cncd(klp) * (xint0 * axz + xint1 * bxz + xint2 * cxz ) 
        y2s = y2s + cnab(ijp) * cncd(klp) * (xint0 * ay2 + xint1 * by2 + xint2 * cyy ) 
        yzs = yzs + cnab(ijp) * cncd(klp) * (xint0 * ayz + xint1 * byz + xint2 * cyz )
        z2s = z2s + cnab(ijp) * cncd(klp) * (xint0 * az2 + xint1 * bz2 + xint2 * czz ) 

      end do
    end do
  end do

  gout(1) = x2s
  gout(2) = xys 
  gout(3) = xzs
  gout(4) = y2s
  gout(5) = yzs
  gout(6) = z2s

end subroutine
