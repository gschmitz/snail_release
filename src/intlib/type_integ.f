module type_integ

  implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  !
  ! Define identifier for used integral evaluation schemes
  !
  integer, parameter :: INT_HUZ   = 0  ! Huzinaga type pof evaluation (extremely slow)
  integer, parameter :: INT_MMD   = 1  ! McMurchie Davidson scheme (currently default)
  integer, parameter :: INT_OS    = 2  ! Obara Saika scheme
  integer, parameter :: INT_MMDET = 3  ! McMurchie Davidson scheme with electron transfer relation

  !
  ! Define the type which holds the information for integral evaluation and storing 
  !
  type integ_info

    integer :: scheme = 1            ! Which scheme is used to evaluate integrals
 
  end type

  public :: integ_info, allocate_integ, deallocate_integ, &
            INT_HUZ, INT_MMD, INT_OS, INT_MMDET


!-----------------------------------------------------------------
  contains
!-----------------------------------------------------------------

  subroutine allocate_integ(integ)

    implicit none

    include 'fortrankinds.h'

    type(integ_info), intent(inout)   :: integ

  end subroutine


  subroutine deallocate_integ(integ)

    implicit none

    include 'fortrankinds.h'

    type(integ_info), intent(inout)   :: integ

  end subroutine


end module
