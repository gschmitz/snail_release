
module BoysTable 


 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  type TBoysTable

    real(kdp) :: dx = 0.1d0

    integer   :: kmax 
    integer   :: nrange 
    integer   :: nxvals 
  
    real(kdp), pointer :: fntab(:,:)  
  end type

  public :: TBoysTable, allocate_boystable, deallocate_boystable 

!------------------------------------------------------------------------------!
  contains
!------------------------------------------------------------------------------!

  subroutine allocate_boystable(boystab)
 
    implicit none

    include 'fortrankinds.h'


    ! input/output
    type(TBoysTable), intent(inout) :: boystab

    ! local
    character(len=255) :: homedir
    character(len=80)   :: fdatabase 

    integer   :: nrange, kmax, nxvals
    integer   :: irec, iunftab, idx, reclen, k
    logical   :: lexist
    real(kdp) :: dummy
  
    call get_environment_variable("SNAILDIR", homedir)

    fdatabase = trim(homedir) // "/bin/fboys-tab.dat"

    nrange = 50
    nxvals = 500
    kmax   = 50

    boystab%dx = 0.1d0
    boystab%nrange = nrange
    boystab%kmax   = kmax
    boystab%nxvals = nxvals

    allocate(boystab%fntab(0:kmax,nxvals))

    inquire(file=trim(fdatabase), exist=lexist)

    if (lexist) then
      reclen = (kmax+1) * sizeof(dummy) !* 2 
      iunftab = 9
      open(iunftab,file=trim(fdatabase),form='unformatted',access='direct',recl=reclen) 

      irec = 0
      do idx = 1, nxvals
        irec = irec + 1
        read(iunftab,rec=irec) (boystab%fntab(k,idx),k=0,kmax)
      end do
      close(iunftab)
    else
      call quit("Did not find tabulated values for Boys function!","allocate_boystable") 
    end if

  end subroutine

  subroutine deallocate_boystable(boystab)

    implicit none

    include 'fortrankinds.h'

    type(TBoysTable), intent(inout) :: boystab

    ! local
    integer :: nrange, kmax, nxvals

    nrange = 0
    kmax   = 0
    nxvals = 0

    boystab%dx = 0.1d0
    boystab%nrange = nrange
    boystab%kmax   = kmax
    boystab%nxvals = nxvals

    deallocate(boystab%fntab)

  end subroutine

end module

