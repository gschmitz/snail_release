

pure subroutine ecof_ijt(ecof,i,j,t,Qx,a,b,Kab,nprimpairs,nprimi,nprimj)
!-----------------------------------------------------------------------+
! Purpose: Generate Hermite expansion coefficients 
!          
! (Will be removed since the order in ecof_tij gives better memory access)
!
! Gunnar Schmitz 
!-----------------------------------------------------------------------+
  implicit none 


  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: nprimpairs,nprimi,nprimj

  ! input
  integer,   intent(in) :: i, j, t
  real(kdp), intent(in) :: Qx, a(nprimi), b(nprimj), Kab(nprimpairs)

  ! output
  real(kdp), intent(out) :: ecof(nprimpairs,0:i,0:j,0:t)

  ! local
  real(kdp) :: t1 
  integer   :: ii, jj, tt, ij, k, iprim, jprim, ijprim

  real(kdp) :: p
  real(kdp), allocatable ::  o2p(:), u(:), uA(:), uB(:)

  allocate(o2p(nprimpairs),u(nprimpairs),uA(nprimpairs),uB(nprimpairs))

  do jprim = 1, nprimj
    do iprim = 1, nprimi
      ijprim = (jprim-1)*nprimi + iprim

      p = a(iprim) + b(jprim)
      u(ijprim) = a(iprim) * b(jprim) / p

      uA(ijprim) = - (u(ijprim) * Qx / a(iprim)) 
      uB(ijprim) =   (u(ijprim) * Qx / b(jprim))
 
      o2p(ijprim) = (one / (two*p)) 
     
    end do
  end do 

  ! init : E^00_0 = K_AB
  ecof = zero
  !ecof(0,0,0) = Kab !dexp(-u * Qx * Qx)

  do ii = 0, i
   
    !-------------------------------------------------------------------------+
    ! E^i+1,j_t = 1/(2p) E^ij_t-1 - u*Qx/a E^ij_t + (t+1) * E^ij_t+1 
    !-------------------------------------------------------------------------+
    if (ii <= 2) then
   
      ! do E(0,0,0), E(1,0,0), E(1,0,1), E(2,0,0), E(2,0,1), E(2,0,2)
      if ( ii == 0 ) then
        do k = 1, nprimpairs 
          ecof(k,0,0,0) = dexp(-u(k) * Qx * Qx) ! Kab(k)
        end do
      elseif (ii == 1) then
        do k = 1, nprimpairs
          ecof(k,1,0,0) = uA(k)  * ecof(k,0,0,0)
          ecof(k,1,0,1) = o2p(k) * ecof(k,0,0,0)
        end do
      elseif (ii == 2) then
        do k = 1, nprimpairs
          ecof(k,2,0,0) = uA(k)  * ecof(k,1,0,0) + o2p(k) * ecof(k,0,0,0)
          ecof(k,2,0,1) = o2p(k) * ecof(k,1,0,0) +  uA(k) * ecof(k,1,0,1)
          ecof(k,2,0,2) = o2p(k) * ecof(k,1,0,1)
        end do
      end if
    else

      ! do E(I,0,T) special cases (T=0, T=I-1 and T=I)
      do k = 1, nprimpairs 
        ecof(k,ii,0,0)    = uA(k)  * ecof(k,ii-1,0,0)    +         ecof(k,ii-1,0,1)
        ecof(k,ii,0,ii-1) = o2p(k) * ecof(k,ii-1,0,ii-2) + uA(k) * ecof(k,ii-1,0,ii-1)
        ecof(k,ii,0,ii)   = o2p(k) * ecof(k,ii-1,0,ii-1)
      end do

      ! do remaining E(I,0,T) cases
      do tt = 1, ii - 2
        t1 = tt + 1
        do k = 1, nprimpairs
          ecof(k,ii,0,tt) = o2p(k) * ecof(k,ii-1,0,tt-1) + uA(k) * ecof(k,ii-1,0,tt) + t1 * ecof(k,ii-1,0,tt+1)
        end do
      end do
    end if

    
    !-------------------------------------------------------------------------+
    ! E^i,j+1_t = 1/(2p) E^ij_t-1 + u*Qx/b E^ij_t + (t+1) * E^ij_t+1 
    !-------------------------------------------------------------------------+
    do jj = 1, j
      ij = ii + jj

      if ( ij <= 2) then
        ! do E(0,1,0), E(0,1,1), E(0,2,0), E(0,2,2) etc.
        if (ij == 1) then
          do k = 1, nprimpairs
            ecof(k,0,1,0) =  uB(k)  * ecof(k,0,0,0)
            ecof(k,0,1,1) =  o2p(k) * ecof(k,0,0,0)
          end do
        elseif (ij == 2) then
          if (ii == 0) then
            do k = 1, nprimpairs
              ecof(k,0,2,0) =  uB(k)  * ecof(k,0,1,0) +  o2p(k) * ecof(k,0,0,0)
              ecof(k,0,2,1) =  o2p(k) * ecof(k,0,1,0) +  uB(k)  * ecof(k,0,1,1)  
              ecof(k,0,2,2) =  o2p(k) * ecof(k,0,1,1)
            end do
          else
            do k = 1, nprimpairs
              ecof(k,1,1,0) =  uA(k) * ecof(k,0,1,0) +         ecof(k,0,1,1) ! actually use here recurrence relation for i
              ecof(k,1,1,1) = o2p(k) * ecof(k,0,1,0) + uA(k) * ecof(k,0,1,1) ! to ensure that we have all needed intermediates
              ecof(k,1,1,2) = o2p(k) * ecof(k,0,1,1) 
            end do
          end if
        end if
      else

        ! do E(I,J,T) special cases (T=0, T=I+J-1 and T=I+J)
        do k = 1, nprimpairs
          ecof(k,ii,jj,0)    = uB(k)  * ecof(k,ii,jj-1,0)    +         ecof(k,ii,jj-1,1)
          ecof(k,ii,jj,ij-1) = o2p(k) * ecof(k,ii,jj-1,ij-2) + uB(k) * ecof(k,ii,jj-1,ij-1)
          ecof(k,ii,jj,ij)   = o2p(k) * ecof(k,ii,jj-1,ij-1)
        end do

        ! do remaining E(I,J,T) cases
        do tt = 1, ij - 2
          t1 = tt + 1
          do k = 1, nprimpairs
            ecof(k,ii,jj,tt) = o2p(k) * ecof(k,ii,jj-1,tt-1) + uB(k) * ecof(k,ii,jj-1,tt) + t1 * ecof(k,ii,jj-1,tt+1)
          end do
        end do
     end if
    end do
  end do

  deallocate(o2p,u,uA,uB)
 
end subroutine

pure subroutine ecof_tij(ecof,i,j,t,Qx,a,b,Kab,o2p,u,uA,uB,nprimpairs,nprimi,nprimj)
!-----------------------------------------------------------------------+
! Purpose: Generate Hermite expansion coefficients
!
! Gunnar Schmitz 
!-----------------------------------------------------------------------+
  implicit none 

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: nprimpairs,nprimi,nprimj

  ! input
  integer,   intent(in) :: i, j, t
  real(kdp), intent(in) :: Qx, a(nprimi), b(nprimj), Kab(nprimpairs)

  ! output
  real(kdp), intent(out) :: ecof(nprimpairs,0:t,0:i,0:j)

  ! scratch
  real(kdp), intent(inout) :: o2p(nprimpairs),u(nprimpairs),uA(nprimpairs),uB(nprimpairs)

  ! local
  real(kdp) :: t1 
  integer   :: ii, jj, tt, ij, k, iprim, jprim, ijprim

  real(kdp) :: p

  do jprim = 1, nprimj
    do iprim = 1, nprimi
      ijprim = (jprim-1)*nprimi + iprim

      p = a(iprim) + b(jprim)
      u(ijprim) = a(iprim) * b(jprim) / p

      uA(ijprim) = - (u(ijprim) * Qx / a(iprim)) 
      uB(ijprim) =   (u(ijprim) * Qx / b(jprim))
 
      o2p(ijprim) = (one / (two*p)) 
     
    end do
  end do 

  ! init : E^00_0 = K_AB
  ecof = zero
  !ecof(0,0,0) = Kab !dexp(-u * Qx * Qx)

  do ii = 0, i
   
    !-------------------------------------------------------------------------+
    ! E^i+1,j_t = 1/(2p) E^ij_t-1 - u*Qx/a E^ij_t + (t+1) * E^ij_t+1 
    !-------------------------------------------------------------------------+
    if (ii <= 2) then
   
      ! do E(0,0,0), E(1,0,0), E(1,0,1), E(2,0,0), E(2,0,1), E(2,0,2)
      if ( ii == 0 ) then
        do k = 1, nprimpairs 
          ecof(k,0,0,0) = dexp(-u(k) * Qx * Qx) ! Kab(k)
        end do
      elseif (ii == 1) then
        do k = 1, nprimpairs
          ecof(k,0,1,0) = uA(k)  * ecof(k,0,0,0)
          ecof(k,1,1,0) = o2p(k) * ecof(k,0,0,0)
        end do
      elseif (ii == 2) then
        do k = 1, nprimpairs
          ecof(k,0,2,0) = uA(k)  * ecof(k,0,1,0) + o2p(k) * ecof(k,0,0,0)
          ecof(k,1,2,0) = o2p(k) * ecof(k,0,1,0) +  uA(k) * ecof(k,1,1,0)
          ecof(k,2,2,0) = o2p(k) * ecof(k,1,1,0)
        end do
      end if
    else

      ! do E(T,I,0) special cases (T=0, T=I-1 and T=I)
      do k = 1, nprimpairs 
        ecof(k,0   ,ii,0) = uA(k)  * ecof(k,0   ,ii-1,0) +         ecof(k,1   ,ii-1,0)
        ecof(k,ii-1,ii,0) = o2p(k) * ecof(k,ii-2,ii-1,0) + uA(k) * ecof(k,ii-1,ii-1,0)
        ecof(k,ii  ,ii,0) = o2p(k) * ecof(k,ii-1,ii-1,0)
      end do

      ! do remaining E(T,I,0) cases
      do tt = 1, ii - 2
        t1 = tt + 1
        do k = 1, nprimpairs
          ecof(k,tt,ii,0) = o2p(k) * ecof(k,tt-1,ii-1,0) + uA(k) * ecof(k,tt,ii-1,0) + t1 * ecof(k,tt+1,ii-1,0)
        end do
      end do
    end if

    
    !-------------------------------------------------------------------------+
    ! E^i,j+1_t = 1/(2p) E^ij_t-1 + u*Qx/b E^ij_t + (t+1) * E^ij_t+1 
    !-------------------------------------------------------------------------+
    do jj = 1, j
      ij = ii + jj

      if ( ij <= 2) then
        ! do E(0,1,0), E(0,1,1), E(0,2,0), E(0,2,2) etc.
        if (ij == 1) then
          do k = 1, nprimpairs
            ecof(k,0,0,1) =  uB(k)  * ecof(k,0,0,0)
            ecof(k,1,0,1) =  o2p(k) * ecof(k,0,0,0)
          end do
        elseif (ij == 2) then
          if (ii == 0) then
            do k = 1, nprimpairs
              ecof(k,0,0,2) =  uB(k)  * ecof(k,0,0,1) +  o2p(k) * ecof(k,0,0,0)
              ecof(k,1,0,2) =  o2p(k) * ecof(k,0,0,1) +  uB(k)  * ecof(k,1,0,1)  
              ecof(k,2,0,2) =  o2p(k) * ecof(k,1,0,1)
            end do
          else
            do k = 1, nprimpairs
              ecof(k,0,1,1) =  uA(k) * ecof(k,0,0,1) +         ecof(k,1,0,1) ! actually use here recurrence relation for i
              ecof(k,1,1,1) = o2p(k) * ecof(k,0,0,1) + uA(k) * ecof(k,1,0,1) ! to ensure that we have all needed intermediates
              ecof(k,2,1,1) = o2p(k) * ecof(k,1,0,1) 
            end do
          end if
        end if
      else

        ! do E(T,I,J) special cases (T=0, T=I+J-1 and T=I+J)
        do k = 1, nprimpairs
          ecof(k,0   ,ii,jj) = uB(k)  * ecof(k,0   ,ii,jj-1) +         ecof(k,1   ,ii,jj-1)
          ecof(k,ij-1,ii,jj) = o2p(k) * ecof(k,ij-2,ii,jj-1) + uB(k) * ecof(k,ij-1,ii,jj-1)
          ecof(k,ij  ,ii,jj) = o2p(k) * ecof(k,ij-1,ii,jj-1)
        end do

        ! do remaining E(T,I,J) cases
        do tt = 1, ij - 2
          t1 = tt + 1
          do k = 1, nprimpairs
            ecof(k,tt,ii,jj) = o2p(k) * ecof(k,tt-1,ii,jj-1) + uB(k) * ecof(k,tt,ii,jj-1) + t1 * ecof(k,tt+1,ii,jj-1)
          end do
        end do
     end if
    end do
  end do

end subroutine


subroutine rcof_tuvn(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl) 
!-----------------------------------------------------------------------+
! Purpose: Calculate Coulomb integrals in Hermite basis
!
! Gunnar Schmitz 
!-----------------------------------------------------------------------+

  implicit none 

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax, nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)
  
  ! local
  integer   :: i, it, iu, iv, m, k, ntot, itype, im
  integer   :: itm1, itm2, ium1, ium2, ivm1, ivm2
  real(kdp) :: dtm1, dum1, dvm1

  ! functions
  real(kdp) :: bin_exp

  ntot = t + u + v

  itype = 100 * t + 10 * u + v                      ! works only if t, u, v are smaller than 10 which should usually be fullfilled..
  if (t >= 10 .or. u >= 10 .or. v >= 10) itype = -1 ! .... but ensure that this will never be a problem

  select case (itype)
 
     case(000)
     
       call r0_0_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(100)
     
       call r1_0_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(010)
     
       call r0_1_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(001)
     
       call r0_0_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(110)
     
       call r1_1_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(101)
     
       call r1_0_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(011)
     
       call r0_1_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(111)
     
       call r1_1_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(200)
     
       call r2_0_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(020)
     
       call r0_2_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(002)
     
       call r0_0_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(210)
     
       call r2_1_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(120)
     
       call r1_2_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(201)
     
       call r2_0_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(021)
     
       call r0_2_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(102)
     
       call r1_0_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(012)
     
       call r0_1_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(211)
     
       call r2_1_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(121)
     
       call r1_2_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(112)
     
       call r1_1_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(220)
     
       call r2_2_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(202)
     
       call r2_0_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(022)
     
       call r0_2_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(221)
     
       call r2_2_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(212)
     
       call r2_1_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(122)
     
       call r1_2_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(222)
     
       call r2_2_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(300)
     
       call r3_0_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(030)
     
       call r0_3_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(003)
     
       call r0_0_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(310)
     
       call r3_1_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(130)
     
       call r1_3_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(301)
     
       call r3_0_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(031)
     
       call r0_3_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(103)
     
       call r1_0_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(013)
     
       call r0_1_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(311)
     
       call r3_1_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(131)
     
       call r1_3_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(113)
     
       call r1_1_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(320)
     
       call r3_2_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(230)
     
       call r2_3_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(302)
     
       call r3_0_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(032)
     
       call r0_3_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(203)
     
       call r2_0_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(023)
     
       call r0_2_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(321)
     
       call r3_2_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(231)
     
       call r2_3_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(312)
     
       call r3_1_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(132)
     
       call r1_3_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(213)
     
       call r2_1_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(123)
     
       call r1_2_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(322)
     
       call r3_2_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(232)
     
       call r2_3_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(223)
     
       call r2_2_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(330)
     
       call r3_3_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(303)
     
       call r3_0_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(033)
     
       call r0_3_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(331)
     
       call r3_3_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(313)
     
       call r3_1_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(133)
     
       call r1_3_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(332)
     
       call r3_3_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(323)
     
       call r3_2_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(233)
     
       call r2_3_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)
     
     case(333)
     
       call r3_3_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

    case default

      ! init : R_000^n = (-2 p)^n * F_n(x)
      rcof = zero
      do i = 0, lmax
        do k = 1, nprijkl
          !rcof(k,0,0,0,i) = (-two*p(k))**(i) * Fn(i,k)
          rcof(k,0,0,0,i) = bin_exp( -two*p(k), i ) * Fn(i,k)
        end do
      end do

      if (v > 0) then
        do m = lmax, 1, -1
          im = m-1
          do k = 1, nprijkl
            rcof(k,0,0,1,im) =  PCz(k) * rcof(k,0,0,0,m)
          end do
        end do
      end if

      do m = lmax, 1, -1
 
        ! calculate some common indices
        im = m-1

        ! R_t+1,u,v^n = t R^n_t-1,u,v + X_PC R^n+1,t,u,v
        do iv = 2, v
           ivm1 = (iv-1)
           ivm2 = (iv-2)
           dvm1 = real(ivm1,kdp)  
           do k = 1, nprijkl
             rcof(k,0,0,iv,im) =  PCz(k) * rcof(k,0,0,ivm1,m) + dvm1 * rcof(k,0,0,ivm2,m) 
           end do
        end do
 
        ! R_t,u+1,v^n = u R^n_t,u-1,v + Y_PC R^n+1,t,u,v
        if (u > 0) then
          do iv = 0, v
            do k = 1, nprijkl
              rcof(k,0,1,iv,im) = PCy(k) * rcof(k,0,0,iv,m)
            end do
          end do
        end if
        do iv = 0, v
          do iu = 2, u
            ium1 = (iu-1)
            ium2 = (iu-2)
            dum1 = real(ium1,kdp) 
            do k = 1, nprijkl
              rcof(k,0,iu,iv,im) = PCy(k) * rcof(k,0,ium1,iv,m) + dum1 * rcof(k,0,ium2,iv,m)
            end do
          end do
        end do 

        ! R_t,u,v+1^n = v R^n_t,u,v-1 + Z_PC R^n+1,t,u,v
        if (t > 0) then
          do iv = 0, v
            do iu = 0, u
              do k = 1, nprijkl
                rcof(k,1,iu,iv,im) = PCx(k) * rcof(k,0,iu,iv,m)
              end do
            end do
          end do
        end if
        do iv = 0, v
           do iu = 0, u
             do it = 2, t
               itm1 = (it-1)
               itm2 = (it-2)
               dtm1 = real(itm1,kdp)
               do k = 1, nprijkl
                 rcof(k,it,iu,iv,im) =  PCx(k) * rcof(k,itm1,iu,iv,m) + dtm1 * rcof(k,itm2,iu,iv,m)
               end do
             end do
          end do
        end do
      end do
  end select

end subroutine


pure function GenerateID(tmax,umax,vmax,mxdim) result(iint)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  integer :: iint

  integer, intent(in)  :: mxdim
  integer, intent(in)  :: tmax,umax,vmax
    
  integer :: iexpo, imult1, imult2  

  iexpo = int(floor(log10(real(mxdim,kdp)))) + 1
  imult1 = 10**iexpo
  imult2 = 10**(iexpo+1)

  iint = tmax * imult2 * 100 + umax * imult1 * 10 + vmax 
  !iint = tmax * 1000 + umax * 100 + vmax 

  return
end function

!----------------------------------------------------------------------------+
! Purpose: Sets up the loop structure for the integral evaluation using the
!          McMurchie Davidson scheme. It is intended to allow re-use of
!          the Coulomb Hermite integrals R(t,u,v) integrals
!
! Gunnar Schmitz, October 2017
!----------------------------------------------------------------------------+
subroutine mmd_SetupOrder(loopinfo,mxrun,cartcompij,cartcompkl,&
                          use_transfer,equalij,equalkl,        &
                          angshl,shellp,shellq,                &
                          la,ma,na,lb,mb,nb,lc,mc,nc,ld,md,nd, &
                          ncarta,ncartb,ncartc,ncartd)

  implicit none

  ! dimensions
  integer, intent(in)    :: ncarta,ncartb,ncartc,ncartd

  integer, intent(in) :: la(ncarta),ma(ncarta),na(ncarta),&
                         lb(ncartb),mb(ncartb),nb(ncartb),&
                         lc(ncartc),mc(ncartc),nc(ncartc),&
                         ld(ncartd),md(ncartd),nd(ncartd)

  ! input
  logical, intent(in)    :: use_transfer,equalij,equalkl
  integer, intent(in)    :: angshl,shellp,shellq  

  ! scratch
  integer, intent(inout) :: cartcompij(3,ncarta*ncartb),cartcompkl(3,ncartc*ncartd)

  ! output
  integer, intent(out)   :: loopinfo(5,ncarta*ncartb*ncartc*ncartd)
  integer, intent(out)   :: mxrun

  ! functions
  integer   :: GenerateID

  ! local
  integer   :: icart,jcart,kcart,lcart
  integer   :: mxcartb, mxcartd
  integer   :: ia,ja,ic,jc
  integer   :: tmax,umax,vmax
  integer   :: iid,maxtuv

  integer   :: ncd,mcd,lcd,&
               nab,mab,lab,&
               nna,mma,lla,&
               nnb,mmb,llb,&
               nnc,mmc,llc,&
               nnd,mmd,lld


  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
  ! Do statistics run:
  !   1) setup loop information for loop over cartesian components
  !   2) sort list to enable reuse of intermediates (R_t,u,v)   
  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
  mxrun = 0

  maxtuv = angshl * 2

  if (use_transfer) then

    icart = 0
    do ia = 0, shellp  ! sum of angular momenta in (bra| shell
      lla = shellp - ia
      do ja = 0, ia    
        mma = ia - ja
        nna = ja

        icart = icart + 1

        cartcompij(1,icart) = lla
        cartcompij(2,icart) = mma
        cartcompij(3,icart) = nna

        kcart = 0
        do ic = 0, shellq  ! sum of angular momenta in |ket) shell
          llc = shellq - ic
          do jc = 0, ic    
            mmc = ic - jc
            nnc = jc

            kcart = kcart + 1

            cartcompkl(1,kcart) = llc
            cartcompkl(2,kcart) = mmc
            cartcompkl(3,kcart) = nnc
  
            tmax = lla + llc 
            umax = mma + mmc
            vmax = nna + nnc

            iid = GenerateID(tmax,umax,vmax,maxtuv)

            mxrun = mxrun + 1
            loopinfo(1,mxrun) = iid
            loopinfo(2,mxrun) = icart
            loopinfo(3,mxrun) = 1
            loopinfo(4,mxrun) = kcart
            loopinfo(5,mxrun) = 1 
          end do
        end do

      end do
    end do

  else
    do kcart = 1, ncartc
      nnc = nc(kcart)
      mmc = mc(kcart)
      llc = lc(kcart)

      mxcartd = ncartd
      if (equalkl) mxcartd = kcart
      do lcart = 1, mxcartd 
        nnd = nd(lcart)
        mmd = md(lcart)
        lld = ld(lcart)

        ncd = nnc + nnd
        mcd = mmc + mmd
        lcd = llc + lld

        do icart = 1, ncarta
          nna = na(icart)
          mma = ma(icart)
          lla = la(icart)

          mxcartb = ncartb
          if (equalij) mxcartb = icart
          do jcart = 1, mxcartb
            nnb = nb(jcart)
            mmb = mb(jcart)
            llb = lb(jcart)  

            nab = nna + nnb
            mab = mma + mmb
            lab = lla + llb

            tmax = lab + lcd 
            umax = mab + mcd
            vmax = nab + ncd

            iid = GenerateID(tmax,umax,vmax,maxtuv)

            mxrun = mxrun + 1
            loopinfo(1,mxrun) = iid
            loopinfo(2,mxrun) = icart
            loopinfo(3,mxrun) = jcart
            loopinfo(4,mxrun) = kcart
            loopinfo(5,mxrun) = lcart

          end do
        end do
      end do  
    end do
  end if
 
  ! sort loopinfo according to iid 
  call heapsort(loopinfo,1,5,mxrun)
  !call quicksort(loopinfo,1,1,mxrun,5,mxrun)

end subroutine

!----------------------------------------------------------------------------+
! Purpose: Calculates the Electron Repulsion Integral for a quartet of
!          contracted Gaussians using the McMurchie-Davidson scheme
!
! Gunnar Schmitz, October 2017
!----------------------------------------------------------------------------+
subroutine mmd_eri(gout,&
                   aexps,acoefs,anorms,xyza,la,ma,na, &
                   bexps,bcoefs,bnorms,xyzb,lb,mb,nb, &
                   cexps,ccoefs,cnorms,xyzc,lc,mc,nc, &
                   dexps,dcoefs,dnorms,xyzd,ld,md,nd, &
                   eabtuv,cnab,ecdtuv,cncd,&
                   use_transfer,ndimab,ndimcd,&
                   intbuf,pfac,alpha,dx,dy,dz,&
                   nprima,nprimb,nprimc,nprimd,&
                   ncarta,ncartb,ncartc,ncartd,&
                   nprimij,nprimkl,nprimijkl,&
                   cpur,cpuf,cpug1,cpug2,cpug3,&
                   memetuv) 

  use IntegralBuffer

  implicit none


  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'
 

  ! dimensions
  integer, intent(in) :: nprima,nprimb,nprimc,nprimd
  integer, intent(in) :: ncarta,ncartb,ncartc,ncartd
  integer, intent(in) :: nprimij,nprimkl,nprimijkl

  integer, intent(in) :: ndimab,ndimcd
 
  integer(kli), intent(in) :: memetuv
 
  ! output:
  real(kdp), intent(out) :: gout(ncarta,ncartb,ncartc,ncartd)

  ! scratch:
  type(TIntBuffer), intent(inout) :: intbuf

  ! input:
  real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3)
  real(kdp), intent(in) :: anorms(nprima,ncarta),bnorms(nprimb,ncartb),&
                           cnorms(nprimc,ncartc),dnorms(nprimd,ncartd)
  real(kdp), intent(in) :: aexps(nprima,ncarta),bexps(nprimb,ncartb),&
                           cexps(nprimc,ncartc),dexps(nprimd,ncartd)
  real(kdp), intent(in) :: acoefs(nprima,ncarta),bcoefs(nprimb,ncartb),&
                           ccoefs(nprimc,ncartc),dcoefs(nprimd,ncartd)
  integer,   intent(in) :: la(ncarta),ma(ncarta),na(ncarta),&
                           lb(ncartb),mb(ncartb),nb(ncartb),&
                           lc(ncartc),mc(ncartc),nc(ncartc),&
                           ld(ncartd),md(ncartd),nd(ncartd)
  real(kdp), intent(in) :: eabtuv(memetuv,ncarta*ncartb),&
                           ecdtuv(memetuv,ncartc*ncartd),&
                           cnab(nprimij,ncarta*ncartb),& 
                           cncd(nprimkl,ncartc*ncartd)

  logical,   intent(in) :: use_transfer

  real(kdp), intent(in) :: pfac(nprimijkl),alpha(nprimijkl),&
                           dx(nprimijkl),dy(nprimijkl),dz(nprimijkl)


  real(kdp), intent(inout) :: cpur,cpuf,cpug1,cpug2,cpug3

  ! local
  integer   :: ijp,klp
  integer   :: icart,jcart,kcart,lcart,ijcart,klcart,iprim
  integer   :: ncd,mcd,lcd,&
               nab,mab,lab,&
               nna,mma,lla,&
               nnb,mmb,llb,&
               nnc,mmc,llc,&
               nnd,mmd,lld
  integer   :: tmax,umax,vmax,lmax,nprijkl,nprimijk
  integer   :: il,jl,kl,ll,angshl
  integer   :: iint
  real(kdp) :: val
  real(kdp) :: cpu0,cpux
  real(kdp) :: dij(3),dkl(3)

  logical   :: atom12,atom23,atom34,atomic,case1
  integer   :: shellp,shellq,shellsum
  logical   :: equalij,equalkl,equalijkl,skipit

  integer :: mxrun,iid,irun,iidold
  integer :: m0n0

  ! init

  il = la(1) + ma(1) + na(1) 
  jl = lb(1) + mb(1) + nb(1) 
  kl = lc(1) + mc(1) + nc(1) 
  ll = ld(1) + md(1) + nd(1) 

  iint = il * 1000 + jl * 100 + kl * 10 + ll

  angshl = il + jl + kl + ll

  nprimijk = nprima*nprimb*nprimc
  nprijkl  = nprima*nprimb*nprimc*nprimd

  !------------------------------------------------------------------------------------------+
  ! decide on an early stage if integral is vanishing...
  !------------------------------------------------------------------------------------------+

  ! idea taken from libERD from Flocke et al.
  atom12 = (xyza(1) == xyzb(1)) .and. (xyza(2) == xyzb(2)) .and. (xyza(3) == xyzb(3))
  atom23 = (xyzb(1) == xyzc(1)) .and. (xyzb(2) == xyzc(2)) .and. (xyzb(3) == xyzc(3))
  atom34 = (xyzc(1) == xyzd(1)) .and. (xyzc(2) == xyzd(2)) .and. (xyzc(3) == xyzd(3))

  atomic = (atom12 .and. atom34 .and. atom23)

  shellp = il + jl  
  shellq = kl + ll 
  shellsum = shellp + shellq

  case1 = mod(shellsum,2) .eq. 1

  if (atomic .and. (case1)) then
    gout = zero
    return
  end if

  !------------------------------------------------------------------------------------------+
  ! figure out if for shell pairs ij and kl i = j and k = l  
  !------------------------------------------------------------------------------------------+

  equalij = atom12

  if (equalij) then
    equalij =  (il == jl) .and. (nprima == nprimb) .and. (ncarta == ncartb)
    if (equalij) then
      do iprim = 1, nprima
        equalij = equalij .and. (aexps(iprim,1) == bexps(iprim,1))
      end do
      if (equalij) then
        do jcart = 1, ncarta
          if (equalij) then
            do iprim = 1, nprima
              equalij = equalij .and. (acoefs(iprim,jcart) == bcoefs(iprim,jcart))
            end do
          end if
        end do
      end if
    end if
  end if

  equalkl = atom34

  if (equalkl) then
    equalkl =  (kl == ll) .and. (nprimc == nprimd) .and. (ncartc == ncartd)
    if (equalkl) then
      do iprim = 1, nprimc
        equalkl = equalkl .and. (cexps(iprim,1) == dexps(iprim,1))
      end do
      if (equalkl) then
        do jcart = 1, ncartc
          if (equalkl) then
            do iprim = 1, nprimc
              equalkl = equalkl .and. (ccoefs(iprim,jcart) == dcoefs(iprim,jcart))
            end do
          end if
        end do
      end if
    end if
  end if

  equalijkl = equalij .and. equalkl

  !
  ! precompute some distances
  !
  dij(1) = (xyza(1) - xyzb(1)); dkl(1) = (xyzc(1) - xyzd(1)) 
  dij(2) = (xyza(2) - xyzb(2)); dkl(2) = (xyzc(2) - xyzd(2)) 
  dij(3) = (xyza(3) - xyzb(3)); dkl(3) = (xyzc(3) - xyzd(3)) 

  select case(iint)

   !---------------------------------------------------------------------------+  
   !  Spcial cases
   !---------------------------------------------------------------------------+  

    case(0000) ! (ss|ss) 
      call s0000(gout,&
                 eabtuv,cnab,&
                 ecdtuv,cncd,intbuf%Fn,pfac,&
                 nprimij,nprimkl,nprimijkl) 
   
     case(1000) ! (ps|ss) 
       call s1000(gout,&
                  eabtuv(1,1),eabtuv(1,2),eabtuv(1,3),&
                  cnab,ecdtuv,cncd,&
                  intbuf%Fn,intbuf%rtuv,&   ! rtuv is passed here since it has enough space
                  pfac,alpha,dx,dy,dz,  &   ! to do all integrals in one shot
                  nprima,nprimb,nprimc,nprimd,&
                  nprimij,nprimkl,nprimijkl) 

     !case(2000)
     !  call s2000(gout,&
     !             eabt,eabu,eabv,cnab,&
     !             ecdt,ecdu,ecdv,cncd,&
     !             aexps,bexps,dij,&
     !             intbuf%Fn,pfac,dx,dy,dz,&
     !             nprima,nprimb,nprimij,&
     !             nprimkl,nprimijkl)
     !  write(6,*) '(dss|ss)'
     !  call matprt(gout,1,6)

   !---------------------------------------------------------------------------+  
    case default    ! General cases...
   !---------------------------------------------------------------------------+  

      ! Setup loops over cartesian components (in loopinfo) in the hopefully most efficient way
      call mmd_SetupOrder(intbuf%loopinfo,mxrun,               &
                          intbuf%cartcompij,intbuf%cartcompkl, &
                          use_transfer,equalij,equalkl,        &
                          angshl,shellp,shellq,                &
                          la,ma,na,lb,mb,nb,lc,mc,nc,ld,md,nd, &
                          ncarta,ncartb,ncartc,ncartd)

      ! init output array
      gout = zero

      ! for reusing R_t,u,v
      iidold = -1

      if (use_transfer) then
        nnb = 0 
        mmb = 0 
        llb = 0   

        nnd = 0 
        mmd = 0 
        lld = 0 
      end if

      !------------------------------------------------------------------------+
      ! Now loop over cartesian components 
      !------------------------------------------------------------------------+
      loop_cartcomp: do irun = 1, mxrun

        ! extract information on loop cycle
        iid   = intbuf%loopinfo(1,irun)
        icart = intbuf%loopinfo(2,irun) 
        jcart = intbuf%loopinfo(3,irun) 
        kcart = intbuf%loopinfo(4,irun) 
        lcart = intbuf%loopinfo(5,irun) 

        ! set dimensions for this loop 
        if (use_transfer) then
          lla = intbuf%cartcompij(1,icart)
          mma = intbuf%cartcompij(2,icart)
          nna = intbuf%cartcompij(3,icart)

          nab = nna 
          mab = mma 
          lab = lla 

          llc = intbuf%cartcompkl(1,kcart)
          mmc = intbuf%cartcompkl(2,kcart)
          nnc = intbuf%cartcompkl(3,kcart)

          ncd = nnc 
          mcd = mmc 
          lcd = llc 

        else
          nna = na(icart) ;  nnb = nb(jcart)
          mma = ma(icart) ;  mmb = mb(jcart)
          lla = la(icart) ;  llb = lb(jcart)

          nab = nna + nnb
          mab = mma + mmb
          lab = lla + llb

          nnc = nc(kcart) ; nnd = nd(lcart)
          mmc = mc(kcart) ; mmd = md(lcart)
          llc = lc(kcart) ; lld = ld(lcart)

          ncd = nnc + nnd
          mcd = mmc + mmd
          lcd = llc + lld
        end if

        ! set additional indices  
        ijcart = (jcart-1)*ncarta + icart
        klcart = (lcart-1)*ncartc + kcart

        !
        ! Construct preexponential factor for screening
        !
        skipit = .true.
        do ijp = 1, nprimij
          !intbuf%Kab(ijp) = eabt(ijp,ijcart) * eabu(ijp,ijcart) * eabv(ijp,ijcart)
          intbuf%Kab(ijp) = 10.d+10
          if (intbuf%Kab(ijp) > 1.d-12) skipit = .false.
        end do
        do klp = 1, nprimkl
          !intbuf%Kcd(klp) = ecdt(klp,klcart) * ecdu(klp,klcart) * ecdv(klp,klcart)
          intbuf%Kcd(klp) = 10.d+10
          if (intbuf%Kcd(klp) > 1.d-12) skipit = .false.
        end do

        if (skipit) cycle 

        !--------------------------------------------------------------------+
        !  Create Hermite Coulomb integrals via recursion from Boys-Function 
        !                       Scale as O(p^4 l^4)
        !--------------------------------------------------------------------+

        tmax = lab + lcd 
        umax = mab + mcd
        vmax = nab + ncd

        lmax = tmax + umax + vmax 

#ifdef TIME_MMD
        call cpu_time(cpu0)
#endif
        if (iid /= iidold) &
          call rcof_tuvn(intbuf%rtuv,tmax,umax,vmax,0,alpha,dx,dy,dz,intbuf%Fn,lmax,nprijkl)
#ifdef TIME_MMD
        call cpu_time(cpux)
        cpur = cpur + cpux - cpu0
#endif

        iidold = iid
        !--------------------------------------------------------------------+
        !  Transform to Cartesian Coulomb integrals and contract 
        !--------------------------------------------------------------------+
        
        ! 1a) Build:
        !   F_{tau,nu,phi}^{c_r d_s} = (-1)^(tau+nu+phi) * E_{tau,phi,nu}^{c_r d_s}
        !
        !   Scales as O(p^2 l^3) 

#ifdef TIME_MMD
        call cpu_time(cpu0)
#endif
        call contract_E_RHS(intbuf%ftuv,ecdtuv(1,klcart),llc,lld,mmc,mmd,nnc,nnd,ncd,mcd,lcd,nprimkl)

#ifdef TIME_MMD
        call cpu_time(cpux)
        cpuf = cpuf + cpux - cpu0
#endif

        ! 1b) Transform:
        !         (tuv|c_r d_s] = \sum_{tau,phi,nu} (-1)^(tau+nu+phi) E_{tau,phi,nu}^{c_r d_s} R_{t+tau,u+nu,v+phi}
        !   a.k.a (tuv|c_r d_s] = \sum_{tau,phi,nu} F_{tau,phi,nu}^{c_r d_s} R_{t+tau,u+nu,v+phi}
        !
        !          Scales as O(p^4 l^8)

#ifdef TIME_MMD
        call cpu_time(cpu0)
#endif
        call contract_HermTUV(intbuf%gtuv,intbuf%ftuv,intbuf%rtuv,pfac,tmax,umax,vmax,intbuf%Kab,intbuf%Kcd,&
                              ncd,mcd,lcd,nab,mab,lab,nprimijkl,nprimij,nprimkl)

#ifdef TIME_MMD
        call cpu_time(cpux)
        cpug1 = cpug1 + cpux - cpu0
#endif

        ! 2) Contract:
        !   (tuv|c d) =  \sum_{rs} C_r C_s (tuv|c_r d_s] 
        !
        !   Scales as O(p^4 l^5 c)

#ifdef TIME_MMD
        call cpu_time(cpu0)
#endif

        call contract_Prim_RHS(intbuf%gcdtuv,intbuf%gtuv,cncd(1,klcart),lab,mab,nab,nprimij,nprimkl)

#ifdef TIME_MMD
        call cpu_time(cpux)
        cpug2 = cpug2 + cpux - cpu0
#endif

        ! 3) Transform:
        !   [a_m b_n|cd) = \sum_{tuv} E_{t,u,v}^{a_m b_n} (tuv|c d)
        !
        !   Scales as O(p^2 l^7 c^2)

#ifdef TIME_MMD
        call cpu_time(cpu0)
#endif

        call contract_E_LHS(intbuf%gabcd,eabtuv(1,ijcart),intbuf%gcdtuv,nab,mab,lab,nprimij)

#ifdef TIME_MMD
        call cpu_time(cpux)
        cpug3 = cpug3 + cpux - cpu0
#endif

        ! 4) Contract:
        !   (ab|cd) = \sum_{mn} C_m C_n [a_m b_n|cd) 
        !
        !   Scales as O(p^2 l^4 c^3)
        val = zero
        do ijp = 1, nprimij
          val = val + intbuf%gabcd(ijp) * cnab(ijp,ijcart)    
        end do

        if (use_transfer) then
          m0n0 = (kcart-1)*ndimab + icart

          !intbuf%gm0n0(m0n0) = val 
          intbuf%gm0n0(lla,mma,nna,llc,mmc,nnc) = val 
        else 
          gout(icart,jcart,kcart,lcart) = val
 
          if (equalij)   gout(jcart,icart,kcart,lcart) = val
          if (equalkl)   gout(icart,jcart,lcart,kcart) = val
          if (equalijkl) gout(jcart,icart,lcart,kcart) = val
        end if
      end do loop_cartcomp

      !if (iint == 2000) then
      ! write(6,*) '(ds|ss)'
      ! call matprt(gout,1,6)
      !end if

      !
      ! We have now the integrals (a+b 0|c+d 0) and we will use the transfer relation to get the integrals (a b|c d)
      !
      if (use_transfer) then

        do lcart = 1, ncartd
          nnd = nd(lcart); mmd = md(lcart); lld = ld(lcart)
          do kcart = 1, ncartc
            nnc = nc(kcart); mmc = mc(kcart); llc = lc(kcart)
            do jcart = 1, ncartb
              nnb = nb(jcart); mmb = mb(jcart); llb = lb(jcart)  
              do icart = 1, ncarta
                nna = na(icart); mma = ma(icart); lla = la(icart)

                if (jl == 1 .and. ll == 0 ) &
                call single_hrr(val,intbuf%gm0n0,dij,dkl, &
                                lla,mma,nna,  &
                                llb,mmb,nnb,  &
                                llc,mmc,nnc,  &
                                lld,mmd,nnd,  &
                                intbuf%maxam,ndimcd)
                                !ndimab,ndimcd)
              
                gout(icart,jcart,kcart,lcart) = val
              end do
            end do
          end do 
        end do 
      end if

      !if (jl == 1 .and. ll == 0 ) then
      !  write(6,*) "iint",iint
      !  call matprt(gout,ncarta*ncartb,ncartc*ncartd)
      !end if
  end select 
end subroutine

pure function GetPreExpFact(ecof,lab,mab,nab,nprimij) result(dret)

  implicit none

  include 'fortrankinds.h'

  ! dimensions
  integer, intent(in) :: nprimij,nab,mab,lab

  ! input
  real(kdp), intent(in) :: ecof(nprimij,0:lab,0:mab,0:nab) 

  ! local 
  integer   :: ijp
  real(kdp) :: dret

  do ijp = 1, nprimij
    dret = dret + ecof(ijp,0,0,0)
  end do

end function

pure subroutine BuildETensor(ecof,eabt,eabu,eabv,    &
                             lla,llb,mma,mmb,nna,nnb,&
                             lab,mab,nab,nprimij)

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'
  include 'numbers.h'
 
  ! dimensions
  integer, intent(in) :: nab,mab,lab,nprimij
  integer, intent(in) :: lla,llb,mma,mmb,nna,nnb

  ! output
  real(kdp), intent(out) :: ecof(nprimij,0:lab,0:mab,0:nab) 

  ! input
  real(kdp), intent(in) :: eabt(nprimij,0:lla+llb,0:lla,0:llb),&
                           eabu(nprimij,0:mma+mmb,0:mma,0:mmb),&
                           eabv(nprimij,0:nna+nnb,0:nna,0:nnb)

  ! local
  integer   :: t, u, v, ijp

  do v = 0, nab
    do u = 0, mab
      do t = 0, lab

        do ijp = 1, nprimij
  
          ecof(ijp,t,u,v) =  eabt(ijp,t,lla,llb)   &
                          *  eabu(ijp,u,mma,mmb)   &
                          *  eabv(ijp,v,nna,nnb)   
  
        end do
      end do
    end do
  end do

end subroutine


!-------------------------------------------------------------------------------+
! Purpose: Contracts E coefficients for the RHS
!
!   F_{tau,nu,phi}^{c_r d_s} = (-1)^(tau+nu+phi) * E_{tau,phi,nu}^{c_r d_s}
!
!   Scales as O(p^2 l^3)
! 
!-------------------------------------------------------------------------------+
pure subroutine contract_E_RHS(ftuv,ecdtuv,            &
                               llc,lld,mmc,mmd,nnc,nnd,&
                               ncd,mcd,lcd,nprimkl)

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'
  include 'numbers.h'
 
  ! dimensions
  integer, intent(in) :: ncd,mcd,lcd,nprimkl
  integer, intent(in) :: llc,lld,mmc,mmd,nnc,nnd

  ! output
  real(kdp), intent(out) :: ftuv(nprimkl,0:lcd,0:mcd,0:ncd) 

  ! input
  real(kdp), intent(in)  :: ecdtuv(nprimkl,0:lcd,0:mcd,0:ncd)

  ! local
  real(kdp) :: dfac
  integer   :: phi, nu, tau, klp, ifac

  do phi = 0, ncd
    do nu = 0, mcd
      do tau = 0, lcd
        !dfac = (-1.0)**(tau+nu+phi)
        !dfac = (-1.0)**(mod(tau+nu+phi,2)) 
        ifac = 1 - ishft(iand(tau+nu+phi,1),1)
        dfac = real(ifac,kdp)

        do klp = 1, nprimkl
  
          ftuv(klp,tau,nu,phi) = dfac * ecdtuv(klp,tau,nu,phi)   
  
        end do
      end do
    end do
  end do

end subroutine


!------------------------------------------------------------------------------------------------------------+
!  Purpose: Contracts modified E coeff tensor with Hermite integrals
!
!         (tuv|c_r d_s] = \sum_{tau,phi,nu} (-1)^(tau+nu+phi) E_{tau,phi,nu}^{c_r d_s} R_{t+tau,u+nu,v+phi}
!   a.k.a (tuv|c_r d_s] = \sum_{tau,phi,nu} F_{tau,phi,nu}^{c_r d_s} R_{t+tau,u+nu,v+phi}
!
!          Scales as O(p^4 l^8)
!
!------------------------------------------------------------------------------------------------------------+
pure subroutine contract_HermTUV(gtuv,ftuv,rtuv,pfac,tmax,umax,vmax,Kab,Kcd,&
                                 ncd,mcd,lcd,nab,mab,lab,nprimijkl,nprimij,nprimkl)

! NOTE: It seems to be more efficient to address the arrays as one dimensonal
!       The performance difference is really measureable 

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'
  include 'numbers.h'
 
  ! dimensions
  integer, intent(in) :: nprimijkl,nprimij,nprimkl
  integer, intent(in) :: ncd,mcd,lcd,nab,mab,lab
  integer, intent(in) :: tmax,umax,vmax
  
  ! output
  !real(kdp), intent(out) :: gtuv(nprimijkl,0:lab,0:mab,0:nab)
  real(kdp), intent(out) :: gtuv(nprimijkl*(lab+1)*(mab+1)*(nab+1))
 
  ! input 
  !real(kdp), intent(in)  :: ftuv(nprimkl,0:lcd,0:mcd,0:ncd),      &
  !                          rtuv(nprimijkl,0:tmax,0:umax,0:vmax), &
  !                          pfac(nprimijkl) 
  real(kdp), intent(in)  :: ftuv(nprimkl*(lcd+1)*(mcd+1)*(ncd+1)),      &
                            rtuv(nprimijkl*(tmax+1)*(umax+1)*(vmax+1)), &
                            pfac(nprimijkl) 
  real(kdp), intent(in)  :: Kab(nprimij),Kcd(nprimkl)

  ! local
  real(kdp) :: dfac
  integer   :: phi,nu,tau,t,u,v,ijp,klp,ijkl

  integer   :: voffijkl, uoffijkl, &
               uoff, voff, toff, tuv, tuvbig, taunuphi

  voffijkl = ( (tmax+1) * (umax+1) * nprimijkl )
  uoffijkl = ( (tmax+1) * nprimijkl )
  gtuv = zero
  do klp = 1, nprimkl

    if (Kcd(klp).lt.1.d-16) cycle
                
    ijkl = (klp-1) * nprimij  
                 
    do ijp = 1, nprimij
      ijkl = ijkl + 1

      if (Kab(ijp).lt.1.d-16) cycle

      taunuphi = klp - nprimkl
      do phi = 0, ncd
        do nu = 0, mcd
          do tau = 0, lcd
 
            taunuphi = taunuphi + nprimkl

            dfac = ftuv(taunuphi) * pfac(ijkl)

            tuv = ijkl - nprimijkl  
            do v = 0, nab
              voff = (v+phi) * voffijkl
              do u = 0, mab
                uoff = (u+nu) * uoffijkl 
                do t = 0, lab
                  toff = (t+tau) * nprimijkl
  
                  tuv = tuv + nprimijkl

                  tuvbig = voff + uoff + toff + ijkl
 
                  gtuv(tuv) = gtuv(tuv) + dfac * rtuv(tuvbig) 
                end do
              end do
            end do
          end do
        end do
      end do

    end do
  end do

end subroutine 

pure subroutine contract_HermTUV_old(gtuv,ftuv,rtuv,pfac,tmax,umax,vmax,Kab,Kcd,&
                                     ncd,mcd,lcd,nab,mab,lab,nprimijkl,nprimij,nprimkl)

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'
  include 'numbers.h'

  ! constants
  logical, parameter :: outer_ijkl = .false.
 
  ! dimensions
  integer, intent(in) :: nprimijkl,nprimij,nprimkl
  integer, intent(in) :: ncd,mcd,lcd,nab,mab,lab
  integer, intent(in) :: tmax,umax,vmax
  
  ! output
  real(kdp), intent(out) :: gtuv(nprimijkl,0:lab,0:mab,0:nab)
 
  ! input 
  real(kdp), intent(in)  :: ftuv(nprimkl,0:lcd,0:mcd,0:ncd),      &
                            rtuv(nprimijkl,0:tmax,0:umax,0:vmax), &
                            pfac(nprimijkl) 
  real(kdp), intent(in)  :: Kab(nprimij),Kcd(nprimkl)

  ! local
  real(kdp) :: dfac
  integer   :: phi,nu,tau,t,u,v,ijp,klp,ijkl

  gtuv = zero

  if (outer_ijkl) then

    do klp = 1, nprimkl

      if (Kcd(klp).lt.1.d-16) cycle
                  
      ijkl = (klp-1) * nprimij  
                   
      do ijp = 1, nprimij
        ijkl = ijkl + 1

        if (Kab(ijp).lt.1.d-16) cycle

        do phi = 0, ncd
          do nu = 0, mcd
            do tau = 0, lcd
 
              dfac = ftuv(klp,tau,nu,phi) * pfac(ijkl)

              do v = 0, nab
                do u = 0, mab
                  do t = 0, lab
                    gtuv(ijkl,t,u,v) = gtuv(ijkl,t,u,v) + dfac * rtuv(ijkl,t+tau,u+nu,v+phi) 
                  end do
                end do
              end do
            end do
          end do
        end do

      end do
    end do
  else
    do phi = 0, ncd
      do nu = 0, mcd
        do tau = 0, lcd
          do v = 0, nab
            do u = 0, mab
              do t = 0, lab
                do klp = 1, nprimkl

                  dfac = ftuv(klp,tau,nu,phi)

                  !if (Kcd(klp).lt.1.d-16) cycle
                              
                  ijkl = (klp-1) * nprimij  
                               
                  do ijp = 1, nprimij
                    ijkl = ijkl + 1

                    !if (Kab(ijp).lt.1.d-16) cycle

                    gtuv(ijkl,t,u,v) = gtuv(ijkl,t,u,v) + dfac * pfac(ijkl) * rtuv(ijkl,t+tau,u+nu,v+phi) 
                  end do
                end do
              end do
            end do
          end do
        end do

      end do
    end do


  end if

end subroutine 

!------------------------------------------------------------------------------+
! Purpose: Contracts the primitives for the RHS (Ket)
!
!   (tuv|c d) =  \sum_{rs} C_r C_s (tuv|c_r d_s] 
!
!   Scales as O(p^4 l^5 c)
!
!------------------------------------------------------------------------------+
pure subroutine contract_Prim_RHS(gcdtuv,gtuv,cncd,lab,mab,nab,nprimij,nprimkl)

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'
  include 'numbers.h'
 
  ! dimensions
  integer, intent(in) :: nprimij,nprimkl,lab,mab,nab

  ! output
  real(kdp), intent(out) :: gcdtuv(nprimij,(1+lab)*(1+mab)*(1+nab))

  ! input
  real(kdp), intent(in) :: gtuv(nprimij*nprimkl,(1+lab)*(1+mab)*(1+nab)), cncd(nprimkl)

  ! local
  integer   :: ijp,klp,ijkl,tuv,ntuv
  real(kdp) :: dfac
 
  gcdtuv = zero
  ntuv = (1+lab)*(1+mab)*(1+nab)

  do tuv = 1, ntuv
    do klp = 1, nprimkl
      dfac = cncd(klp)
      ijkl = (klp - 1) * nprimij

      do ijp = 1, nprimij
        ijkl = ijkl + 1
        gcdtuv(ijp,tuv) =  gcdtuv(ijp,tuv) + gtuv(ijkl,tuv) * dfac
      end do
    end do
  end do

end subroutine

!------------------------------------------------------------------+
!  Purpose: Contracts LHS with E coefficients
!
!   [a_m b_n|cd) = \sum_{tuv} E_{t,u,v}^{a_m b_n} (tuv|c d)
!
!   Scales as O(p^2 l^7 c^2)
!
!------------------------------------------------------------------+
pure subroutine contract_E_LHS(gabcd,eabtuv,gcdtuv,     &
                               nab,mab,lab,nprimij)

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'
  include 'numbers.h'
 
  ! dimensions
  integer, intent(in) :: nab,mab,lab,nprimij

  ! output
  real(kdp), intent(out) :: gabcd(nprimij) 

  ! input
  real(kdp), intent(in) :: gcdtuv(nprimij,(lab+1)*(mab+1)*(nab+1))
  real(kdp), intent(in) :: eabtuv(nprimij,(lab+1)*(mab+1)*(nab+1))


  ! local
  integer   :: ijp,ituv 

  gabcd = zero
  do ituv = 1, (lab+1)*(mab+1)*(nab+1)
    do ijp = 1, nprimij
      gabcd(ijp) = gabcd(ijp) + gcdtuv(ijp,ituv) * eabtuv(ijp,ituv)
    end do
  end do

end subroutine

!----------------------------------------------------------------------+
!  Purpose: Calculates dipole moment integrals
!
!----------------------------------------------------------------------+
subroutine mmd_dipole(mu,&
                      aexps,acoefs,anorms,xyza,la,ma,na, &
                      bexps,bcoefs,bnorms,xyzb,lb,mb,nb, &
                      xyzc,direction,nprima,nprimb,ncarta,ncartb)

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in)  :: nprima,nprimb,ncarta,ncartb

  ! output
  real(kdp), intent(out) :: mu(ncarta,ncartb)

  ! input
  integer,   intent(in) :: la(ncarta),ma(ncarta),na(ncarta),&
                           lb(ncartb),mb(ncartb),nb(ncartb)
  real(kdp), intent(in) :: xyza(3), xyzb(3), xyzc(3)
  real(kdp), intent(in) :: anorms(nprima,ncarta), bnorms(nprimb,ncartb)
  real(kdp), intent(in) :: aexps (nprima,ncarta), bexps( nprimb,ncartb)
  real(kdp), intent(in) :: acoefs(nprima,ncarta),bcoefs(nprimb,ncartb)

  character(len=1), intent(in) :: direction

  ! local
  real(kdp) :: Pxyz(3),xpc,ypc,zpc
  real(kdp) :: D,S1,S2,S3,Kab,dx,dy,dz
  integer   :: iprim,jprim,ijp,lmax,nmax,mmax,nprimpairs

  integer   :: icart,jcart
  integer   :: lla,mma,nna,llb,mmb,nnb

  real(kdp), allocatable :: elab(:,:,:,:),&
                            emab(:,:,:,:),&
                            enab(:,:,:,:)

  real(kdp), allocatable ::  o2p(:), u(:), uA(:), uB(:)

  ! init
  mu = zero
  nprimpairs = nprima*nprimb

  allocate(o2p(nprimpairs),u(nprimpairs),uA(nprimpairs),uB(nprimpairs))

  dx = xyza(1) - xyzb(1)
  dy = xyza(2) - xyzb(2)
  dz = xyza(3) - xyzb(3)

  do icart = 1, ncarta
    lla = la(icart)
    mma = ma(icart)
    nna = na(icart)

    do jcart = 1, ncartb
      llb = lb(jcart)
      mmb = mb(jcart)
      nnb = nb(jcart)

      lmax = lla + llb
      mmax = mma + mmb
      nmax = nna + nnb

      if (direction == 'x') lmax = max(lmax,1)
      if (direction == 'y') mmax = max(mmax,1)
      if (direction == 'z') nmax = max(nmax,1)

      allocate(elab(nprima*nprimb,0:lmax,0:lla,0:llb))
      allocate(emab(nprima*nprimb,0:mmax,0:mma,0:mmb))
      allocate(enab(nprima*nprimb,0:nmax,0:nna,0:nnb))

      call ecof_tij(elab,lla,llb,lmax,dx,aexps(1,icart),bexps(1,jcart),Kab,o2p,u,uA,uB,nprimpairs,nprima,nprimb)
      call ecof_tij(emab,mma,mmb,mmax,dy,aexps(1,icart),bexps(1,jcart),Kab,o2p,u,uA,uB,nprimpairs,nprima,nprimb)
      call ecof_tij(enab,nna,nnb,nmax,dz,aexps(1,icart),bexps(1,jcart),Kab,o2p,u,uA,uB,nprimpairs,nprima,nprimb)

      do iprim = 1, nprima
        do jprim = 1, nprimb

          ijp = (jprim - 1) * nprima + iprim 

          call product_center(Pxyz,aexps(iprim,icart),xyza,bexps(jprim,jcart),xyzb) 

          if (direction == 'x') then
            xpc = Pxyz(1) - xyzc(1) 

            D  = elab(ijp,1,lla,llb) + xpc * elab(ijp,0,lla,llb) 
            S2 = emab(ijp,0,mma,mmb) 
            S3 = enab(ijp,0,nna,nnb) 

            mu(icart,jcart) = mu(icart,jcart) + acoefs(iprim,icart) * anorms(iprim,icart) &
                                              * bcoefs(jprim,jcart) * bnorms(jprim,jcart) &
                                              * D*S2*S3*(pi/(aexps(iprim,icart)+bexps(jprim,jcart)))**(1.5)

          else if (direction == 'y') then
            ypc = Pxyz(2) - xyzc(2) 

            S1 = elab(ijp,0,lla,llb) 
            D  = emab(ijp,1,mma,mmb) + ypc * emab(ijp,0,mma,mmb)
            S3 = enab(ijp,0,nna,nnb)

            mu(icart,jcart) = mu(icart,jcart) + acoefs(iprim,icart) * anorms(iprim,icart) &
                                              * bcoefs(jprim,jcart) * bnorms(jprim,jcart) &
                                              * S1*D*S3*(pi/(aexps(iprim,icart)+bexps(jprim,jcart)))**(1.5)

          else if (direction == 'z') then
            zpc = Pxyz(3) - xyzc(3) 

            S1 = elab(ijp,0,lla,llb)
            S2 = emab(ijp,0,mma,mmb)
            D  = enab(ijp,1,nna,nnb) + zpc * enab(ijp,0,nna,nnb)

            mu(icart,jcart) = mu(icart,jcart) + acoefs(iprim,icart) * anorms(iprim,icart) &
                                              * bcoefs(jprim,jcart) * bnorms(jprim,jcart) &
                                              * S1*S2*D*(pi/(aexps(iprim,icart)+bexps(jprim,jcart)))**(1.5)

          end if
        end do
      end do
      deallocate(elab,emab,enab)
    
    end do
  end do

end subroutine

recursive subroutine single_hrr(dret,gin,dij,dkl, &
                                la,ma,na, &
                                lb,mb,nb, &
                                lc,mc,nc, &
                                ld,md,nd, &
                                ndimab,ndimcd)

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'

  ! output
  real(kdp), intent(out) :: dret

  ! parameter
  real(kdp), parameter  :: thrcoord = 1.d-16 

  ! dimensions
  integer,   intent(in) :: ndimab,ndimcd

  ! input
  real(kdp), intent(in) :: dij(3),dkl(3)
  integer,   intent(in) :: la,ma,na,lb,mb,nb, &
                           lc,nc,mc,ld,md,nd


  !real(kdp), intent(in) :: gin(ndimab,ndimcd)
  real(kdp), intent(in) :: gin(0:ndimab,0:ndimab,0:ndimab,0:ndimab,0:ndimab,0:ndimab)

  ! functions
  integer :: getind

  ! local
  real(kdp) :: dist,dtemp
  integer   :: ia,ic

  !------------------------------------------------------------------------------------------+
  ! use horizontal reccurence relation to transfer cartesian powers for 1. electron:
  !    (a(b+1)|cd) = ((a+1)b|cd) + X_ab (ab|cd)
  !------------------------------------------------------------------------------------------+
  if (lb > 0) then
    call single_hrr(dret, gin, dij, dkl, &
                    la + 1, ma, na,  &
                    lb - 1, mb, nb,  &
                    lc, mc, nc,  &
                    ld, md, nd,  &
                    ndimab,ndimcd)

    write(6,*) "dret", dret
    dist = dij(1) 
    !if (dabs(dist) >= thrcoord) then
      call single_hrr(dtemp, gin, dij, dkl, &
                      la, ma, na,  &
                      lb - 1, mb, nb, &
                      lc, mc, nc,  &
                      ld, md, nd,  &
                      ndimab,ndimcd)

      dret = dret + dist * dtemp
      write(6,*) "dist,dtemp", dist,dtemp
    !end if
    return 
  end if

  if (mb > 0) then
    call  single_hrr(dret, gin, dij, dkl, &
                     la, ma + 1, na,  &
                     lb, mb - 1, nb,  &
                     lc, mc, nc,  &
                     ld, md, nd,  &
                     ndimab,ndimcd)

    dist = dij(2)
    if (dabs(dist) >= thrcoord) then
      call single_hrr(dtemp, gin, dij, dkl, &
                      la, ma, na,  &
                      lb, mb - 1, nb,  &
                      lc, mc, nc, &
                      ld, md, nd, &
                      ndimab,ndimcd)

      dret = dret + dist * dtemp 
    end if
    return 
  end if

  if (nb > 0) then
    call single_hrr(dret, gin, dij, dkl, &
                    la, ma, na + 1, &
                    lb, mb, nb - 1, &
                    lc, mc, nc,  &
                    ld, md, nd,  &
                    ndimab,ndimcd)

    dist = dij(3)
    if (dabs(dist) >= thrcoord) then
      call single_hrr(dtemp, gin, dij, dkl, &
                      la, ma, na, &
                      lb, mb, nb - 1,  &
                      lc, mc, nc,  &
                      ld, md, nd,  &
                      ndimab,ndimcd)

      dret = dret + dist * dtemp
    end if
    return 
  end if

  !------------------------------------------------------------------------------------------+
  ! use horizontal reccurence relation to transfer cartesian powers for 2. electron:
  !   (ab|c(d+1)) = (ab|(c+1)d) + X_cd (ab|cd)
  !------------------------------------------------------------------------------------------+
  if (ld > 0) then
    call single_hrr(dret, gin, dij, dkl, &
                    la, ma, na, &
                    lb, mb, nb, &
                    lc + 1, mc, nc, &
                    ld - 1, md, nd, &
                    ndimab,ndimcd)

    dist = dkl(1)
    if (dabs(dist) >= thrcoord) then
      call single_hrr(dtemp, gin, dij, dkl, &
                      la, ma, na,  &
                      lb, mb, nb,  &
                      lc, mc, nc,  &
                      ld - 1, md, nd,&
                      ndimab,ndimcd)

      dret = dret + dist * dtemp
    end if
    return 
  end if

  if (md > 0) then
    call single_hrr(dret, gin, dij, dkl, &
                    la, ma, na, &
                    lb, mb, nb, &
                    lc, mc + 1, nc, &
                    ld, md - 1, nd, &
                    ndimab,ndimcd)

    dist = dkl(2)
    if (dabs(dist) >= thrcoord) then 
          call single_hrr(dtemp, gin, dij, dkl,&
                          la, ma, na, &
                          lb, mb, nb, &
                          lc, mc, nc, &
                          ld, md - 1, nd, &
                          ndimab,ndimcd)

          dret = dret + dist * dtemp
    end if
    return 
  end if

  if (nd > 0) then
    call single_hrr(dret, gin, dij, dkl, &
                    la, ma, na, &
                    lb, mb, nb, &
                    lc, mc, nc + 1, &
                    ld, md, nd - 1, &
                    ndimab,ndimcd)

    dist = dkl(3)
    if (dabs(dist) >= thrcoord) then
      call single_hrr(dtemp, gin, dij, dkl, &
                      la, ma, na,  &
                      lb, mb, nb,  &
                      lc, mc, nc,  &
                      ld, md, nd - 1,  &
                      ndimab,ndimcd)

      dret = dret + dist * dtemp
    end if
    return 
  end if


  !------------------------------------------------------------------------------------------+
  ! return contracted integral (a0|c0) = sum [a0|c0]
  !------------------------------------------------------------------------------------------+
  ia = getind(la,ma,na)
  ic = getind(lc,mc,nc)

  !dret = gin(ia,ic)
  dret = gin(la,ma,na,lc,mc,nc)
  return
end subroutine

