!-------------------------------------------------------------------------------------+
!  Purpose: Specialized code to calculate for a shell quadruple the integral (ps|ss)
!
!           Handoptimized and hopefully more efficient than the general code 
!
! Gunnar Schmitz
!-------------------------------------------------------------------------------------+

pure subroutine s1000(gout,&
                      eabtuv1,eabtuv2,eabtuv3,cnab,&
                      ecdtuv,cncd,&
                      Fn,gcdtuv,pfac,&
                      alpha,dx,dy,dz,&
                      nprima,nprimb,nprimc,nprimd,&
                      nprimij,nprimkl,nprimijkl) 
  implicit none


  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  ! dimensions
  integer, intent(in) :: nprima,nprimb,nprimc,nprimd
  integer, intent(in) :: nprimij,nprimkl,nprimijkl
 
  ! output:
  real(kdp), intent(out) :: gout(3)

  ! input:
  real(kdp), intent(in) :: eabtuv1(nprimij,2) , &
                           eabtuv2(nprimij,2) , &
                           eabtuv3(nprimij,2) 

  real(kdp), intent(in) :: ecdtuv(nprimkl)
 
  real(kdp), intent(in) :: cnab(nprimij,3),&
                           cncd(nprimkl)

  real(kdp), intent(in) :: Fn(0:1,nprimijkl)

  real(kdp), intent(inout) :: gcdtuv(nprimij*2,3)             

  real(kdp), intent(in) :: pfac(nprimijkl),alpha(nprimijkl),&
                           dx(nprimijkl),dy(nprimijkl),dz(nprimijkl)


  ! local
  integer   :: ijp,klp
  integer   :: ijkl
  integer   :: nprimijk
  integer   :: ind1
  real(kdp) :: dfac, dtemp, dp2, val1, val2, val3, ftuv 


  nprimijk = nprima*nprimb*nprimc
  
  ! Build:
  !   F_{tau,nu,phi}^{c_r d_s} = (-1)^(tau+nu+phi) * E_{tau,phi,nu}^{c_r d_s}
  !
  ! Transform:
  !         (tuv|c_r d_s] = \sum_{tau,phi,nu} (-1)^(tau+nu+phi) E_{tau,phi,nu}^{c_r d_s} R_{t+tau,u+nu,v+phi}
  !   a.k.a (tuv|c_r d_s] = \sum_{tau,phi,nu} F_{tau,phi,nu}^{c_r d_s} R_{t+tau,u+nu,v+phi}
  !call dcopy(nprimij*2*3,zero,0,gcdtuv,1)
  gcdtuv = zero
  do klp = 1, nprimkl
  
    ftuv = ecdtuv(klp) 

    dtemp = ftuv * cncd(klp)

    ind1 = nprimij
    ijkl = (klp-1) * nprimij
    do ijp = 1, nprimij
      ijkl = ijkl + 1
      ind1 = ind1 + 1

      dfac = pfac(ijkl) * dtemp
      dp2  = two * alpha(ijkl) * Fn(1,ijkl)

      ! Contract:
      !   (tuv|c d) =  \sum_{rs} C_r C_s (tuv|c_r d_s] 
      gcdtuv(ijp ,1) = gcdtuv(ijp ,1) + dfac * Fn(0,ijkl)  
      gcdtuv(ind1,1) = gcdtuv(ind1,1) - dfac * dx(ijkl) * dp2  

      gcdtuv(ijp ,2) = gcdtuv(ijp ,2) + dfac * Fn(0,ijkl)
      gcdtuv(ind1,2) = gcdtuv(ind1,2) - dfac * dy(ijkl) * dp2  

      gcdtuv(ijp ,3) = gcdtuv(ijp ,3) + dfac * Fn(0,ijkl)  
      gcdtuv(ind1,3) = gcdtuv(ind1,3) - dfac * dz(ijkl) * dp2  
    end do
  end do

  ! Transform:
  !   [a_m b_n|cd) = \sum_{tuv} E_{t,u,v}^{a_m b_n} (tuv|c d)
  !
  ! Contract:
  !   (ab|cd) = \sum_{mn} C_m C_n [a_m b_n|cd) 
  val1 = zero
  val2 = zero
  val3 = zero

  ind1 = nprimij 
  do ijp = 1, nprimij
     ind1 = ind1 + 1

     val1 = val1 + (gcdtuv(ijp, 1) * eabtuv1(ijp,1)  &
                 +  gcdtuv(ind1,1) * eabtuv1(ijp,2) ) * cnab(ijp,1)

     val2 = val2 + (gcdtuv(ijp, 2) * eabtuv2(ijp,1)  &
                 +  gcdtuv(ind1,2) * eabtuv2(ijp,2)  ) * cnab(ijp,2)

     val3 = val3 + (gcdtuv(ijp, 3) * eabtuv3(ijp,1)  &
                 +  gcdtuv(ind1,3) * eabtuv3(ijp,2)  ) * cnab(ijp,3)
  end do 

  gout(1) = val1
  gout(2) = val2
  gout(3) = val3

end subroutine

