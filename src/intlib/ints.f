!=================================================================================+
!  This file includes Fortran 90/95 routines for the evaluation of
!  molecular integrals based on the paper 
!
! "Gaussian-Expansion Methods for Molecular Integrals"
!   by H. Taketa, S. Huzinaga and K. O-Ohata, in the
!   Journal of the Physical Society of Japan, Vol 21, No 11, November 1966
!
!  The aim is not to provide a very efficient integral code, but a
!  working one, which is used by SNAIL. 
!=================================================================================+


function overlap(alpha,la,ma,na,xyza,beta,lb,mb,nb,xyzb) result(ov)
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the overlap of two gaussians
 !---------------------------------------------------------------------+

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'

  real(kdp) :: ov  ! output

  ! input
  real(kdp), intent(in) :: alpha,beta, xyza(3),xyzb(3)
  integer,   intent(in) :: la,ma,na,lb,mb,nb

  ! local
  real(kdp) :: rab2,zeta,pre,wx,wy,wz,xyzp(3)
 
  ! functions
  real(kdp) :: overlap_1d,dist2,product_center_1D 

  rab2 = dist2(xyza,xyzb)
  zeta = alpha + beta

  xyzp(1) = product_center_1D(alpha,xyza(1),beta,xyzb(1))
  xyzp(2) = product_center_1D(alpha,xyza(2),beta,xyzb(2))
  xyzp(3) = product_center_1D(alpha,xyza(3),beta,xyzb(3))


  pre = (PI/zeta)**1.5d0 * dexp(-alpha * beta * rab2 / zeta)
  
  wx = overlap_1d(la,xyzp(1)-xyza(1),lb,xyzp(1)-xyzb(1),zeta)  
  wy = overlap_1d(ma,xyzp(2)-xyza(2),mb,xyzp(2)-xyzb(2),zeta)  
  wz = overlap_1d(na,xyzp(3)-xyza(3),nb,xyzp(3)-xyzb(3),zeta)  

  ov = pre * wx * wy * wz

  return
end function


function overlap_1d(la,pax,lb,pbx,zeta) result(ov)
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the  overlap of two one dimensional gaussians
 !---------------------------------------------------------------------+

  implicit none

  include 'fortrankinds.h'

  real(kdp) :: ov  ! output

  real(kdp), intent(in) :: zeta,pax,pbx
  integer,   intent(in) :: la,lb

  ! local
  integer :: i

  ! functions
  real(kdp) :: fj,doublefact

  ov = 0.0d0

  do i = 0, int(floor(0.5d0*(la+lb))) 
    ov = ov + fj(2 * i, la, lb, pax, pbx) * &
               doublefact(2 * i - 1) / (2 * zeta)**i
  end do
  
  return
  
end function


function kinetic(alpha,la,ma,na,xyza,beta,lb,mb,nb,xyzb) result(dkin)
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the integrals over the kinetic energy operator 
 !---------------------------------------------------------------------+

  implicit none

  include 'fortrankinds.h'

  real(kdp) :: dkin

  real(kdp), intent(in) :: alpha,beta,xyza(3),xyzb(3)

  integer,   intent(in) :: la,ma,na,lb,mb,nb

  ! local
  real(kdp) :: kin0,kin1,kin2
  ! functions
  real(kdp) :: overlap

  kin0 = beta * (2.0d0 * (lb + mb + nb) + 3) * overlap(alpha, la, ma, na, xyza,&
                                                       beta,  lb, mb, nb, xyzb)

  kin1 =  -2.0d0 * beta**2.0d0 * & 
              (  overlap(alpha, la, ma, na, xyza, beta, lb + 2, mb, nb, xyzb) &
               + overlap(alpha, la, ma, na, xyza, beta, lb, mb + 2, nb, xyzb) &
               + overlap(alpha, la, ma, na, xyza, beta, lb, mb, nb + 2, xyzb))

  kin2 = 0.0d0

  If (lb .ge. 2) then
      kin2 = kin2 -0.5d0 * (lb * (lb - 1)) * overlap(alpha, la, ma, na, xyza, beta, lb - 2, mb, nb, xyzb)
  end if
  if (mb .ge. 2) then
      kin2 = kin2 -0.5d0 * (mb * (mb - 1)) * overlap(alpha, la, ma, na, xyza, beta, lb, mb - 2, nb, xyzb)
  end If
  if (nb .ge. 2) then
      kin2 = kin2 -0.5d0 * (nb * (nb - 1)) * overlap(alpha, la, ma, na, xyza, beta, lb, mb, nb - 2, xyzb)
  end If

  dkin = kin0 + kin1 + kin2

  return
end function


function A_term(i,r,u,la,lb,Pax,PBx,PCx,zeta) result(dret)
  implicit none
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the 'A-term'. An intermediate needed for
 !          the nuclear interaction integral 
 !---------------------------------------------------------------------+

  include 'fortrankinds.h'

  real(kdp) :: dret

  real(kdp), intent(in) :: Pax,PBx,PCx,zeta
  integer,   intent(in) :: i,r,u,la,lb

  ! functions
  real(kdp) :: fact,fj

  dret = (-1.0)**(i+u) * fj(i, la, lb, PAx, PBx) * &
         fact(i) * PCx**(i-2*r-2*u) * (4.0*zeta)**(-r-u) / &
         (fact(r) * fact(u) * fact(i - 2 * r - 2 * u)) 

  return
end function


function B_term(i1,i2,r1,r2,u,la,lb,lc,ld,&
                Px,Ax,Bx,Qx,Cx,Dx,zeta1,zeta2,delta) result(dret)

  implicit none
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the 'B-term'. An intermediate needed for
 !          the electron repulsion integral 
 !---------------------------------------------------------------------+

  include 'fortrankinds.h'

  real(kdp) :: dret

  integer,   intent(in) :: i1,i2,r1,r2,u,la,lb,lc,ld
  real(kdp), intent(in) :: Px,Ax,Bx,Qx,Cx,Dx,zeta1,zeta2,delta

  ! functions
  real(kdp) :: B_theta,fact_ratio2

  dret = B_theta(i1, la, lb, Px, Ax, Bx, r1, zeta1) & 
           * (-1.0d0)**i2 * B_theta(i2, lc, ld, Qx, Cx, Dx, r2, zeta2) &
           * (-1.0d0)**u * fact_ratio2(i1 + i2 - 2 * (r1 + r2), u) &
           * (Qx - Px)**(i1 + i2 - 2 * (r1 + r2) - 2 * u) &
           / delta**(i1 + i2 - 2 * (r1 + r2) - u) 

end function

function B_theta(i,la,lb,P,A,B,r,zeta) result(dret)
 !---------------------------------------------------------------------+
 ! Purpose: A function depending on positions and angular momenta,
 !          which is needes for ther eris
 !---------------------------------------------------------------------+
  implicit none

  include 'fortrankinds.h'

  real(kdp) :: dret

  real(kdp), intent(in) :: P,A,B,zeta
  integer,   intent(in) :: i,r,la,lb

  ! functions
  real(kdp) :: fj,fact_ratio2

  dret = fj(i, la, lb, P - A, P - B) * fact_ratio2(i, r) * (4.0d0*zeta)**(r-i)

  return
end function 

function fact_ratio2(a,b) result(dret)
  implicit none

  include 'fortrankinds.h'

  real(kdp) :: dret

  integer, intent(in) :: a,b

  ! functions
  real(kdp) :: fact

  dret =  fact(a) / fact(b) / fact(a - 2 * b)
end function

subroutine A_array(A,la,lb,PA,PB,PC,zeta,imax)
  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  integer, intent(in) :: imax

  real(kdp), intent(inout) :: A(0:imax)
  real(kdp), intent(in)    :: PA,PB,PC,zeta
  integer,   intent(in)    :: la, lb

  ! local
  integer :: ind,i,r,u
  ! functions
  real(kdp) :: A_term

  do ind = 0, imax
    A(ind) = zero
  end do

  do i = 0, imax
    do r = 0, int(floor(0.5d0*i))
      do u = 0, int(floor(0.5d0*(i-2*r)))
         ind = i - 2 * r - u
         A(ind) = A(ind) +  A_term(i, r, u, la, lb, PA, PB, PC, zeta) 
      end do
    end do
  end do
end subroutine


function nuclear(xyza,norma,la,ma,na,alpha, &
                 xyzb,normb,lb,mb,nb,beta,  &
                 xyz_nuc) result(dret)
  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  real(kdp) :: dret

  real(kdp), intent(in) :: xyza(3),xyzb(3),xyz_nuc(3),&
                           norma,normb,alpha,beta

  integer,   intent(in) :: la,ma,na,lb,mb,nb

  ! local
  real(kdp) :: zeta,rab2,rcp2,xyzp(3),boysarg,nai
  integer   :: Nx,Ny,Nz,N,ix,iy,iz

  real(kdp), allocatable :: bf(:),Ax(:),Ay(:),Az(:)

  ! dummies
  real(kdp) :: dummy
  integer   :: idummy

  real(kdp), allocatable :: fbdum(:,:) 

  ! functions
  real(kdp) :: dist2,product_center_1D

  ! for range checker
  allocate(fbdum(1,0:0))
 
  xyzp(1) = product_center_1D(alpha,xyza(1),beta,xyzb(1))
  xyzp(2) = product_center_1D(alpha,xyza(2),beta,xyzb(2))
  xyzp(3) = product_center_1D(alpha,xyza(3),beta,xyzb(3))


  rab2 = dist2(xyza, xyzb)
  rcp2 = dist2(xyz_nuc, xyzp)

  zeta = alpha + beta

  boysarg =  rcp2 * zeta
  nai = zero

  Nx = la + lb
  Ny = ma + mb
  Nz = na + nb

  N = Nx + Ny + Nz

  allocate(Ax(0:Nx),Ay(0:Ny),Az(0:Nz),bf(0:N))
  
  call A_array(Ax,la,lb,xyzp(1)-xyza(1),xyzp(1)-xyzb(1),xyzp(1)-xyz_nuc(1),zeta,Nx)
  call A_array(Ay,ma,mb,xyzp(2)-xyza(2),xyzp(2)-xyzb(2),xyzp(2)-xyz_nuc(2),zeta,Ny)
  call A_array(Az,na,nb,xyzp(3)-xyza(3),xyzp(3)-xyzb(3),xyzp(3)-xyz_nuc(3),zeta,Nz)

  ! generate BoysFunction
  call BoysFArr(N, boysarg, bf, fbdum, .false., idummy, idummy, idummy)

  do ix = 0, Nx
    do iy = 0, Ny
      do iz = 0, Nz
        nai = nai + Ax(ix) * Ay(iy) * Az(iz) * bf(ix + iy + iz)
      end do
    end do 
  end do

  deallocate(Ax,Ay,Az,bf)

  dret =  -norma * normb *  &
           two * PI / zeta * dexp(-alpha * beta * rab2 / zeta) * nai

  deallocate(fbdum)
  return
end function


function coulomb_repulsion(xyza,norma,la,ma,na,alphaa,&
                           xyzb,normb,lb,mb,nb,alphab,&
                           xyzc,normc,lc,mc,nc,alphac,&
                           xyzd,normd,ld,md,nd,alphad) result(dret)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  real(kdp) :: dret

  real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3)
  real(kdp), intent(in) :: norma,normb,normc,normd
  real(kdp), intent(in) :: alphaa,alphab,alphac,alphad
  integer,   intent(in) :: la,ma,na,lb,mb,nb,lc,mc,nc,ld,md,nd

  ! local
  real(kdp) :: delta,zeta1,zeta2,rab2,rcd2,rpq2,xyzp(3),xyzq(3)

  real(kdp), allocatable :: Bx(:),By(:),Bz(:),bf(:)
  integer   :: Nx,Ny,Nz,N,ix,iy,iz
  real(kdp) :: boysarg,eri,prefact

  ! dummies
  real(kdp) :: dummy
  integer   :: idummy

  real(kdp), allocatable :: fbdum(:,:)

  ! functions
  real(kdp) :: dist2,product_center_1D

  ! for range checker
  allocate(fbdum(1,0:0))

  rab2  = dist2(xyza, xyzb)
  rcd2  = dist2(xyzc, xyzd)

  xyzp(1) = product_center_1D(alphaa,xyza(1),alphab,xyzb(1))
  xyzp(2) = product_center_1D(alphaa,xyza(2),alphab,xyzb(2))
  xyzp(3) = product_center_1D(alphaa,xyza(3),alphab,xyzb(3))

  xyzq(1) = product_center_1D(alphac,xyzc(1),alphad,xyzd(1))
  xyzq(2) = product_center_1D(alphac,xyzc(2),alphad,xyzd(2))
  xyzq(3) = product_center_1D(alphac,xyzc(3),alphad,xyzd(3))

  rpq2 = dist2(xyzp, xyzq)

  zeta1 = alphaa + alphab
  zeta2 = alphac + alphad

  delta = 0.25d0 * (one / zeta1 + one / zeta2)

  Nx = la + lb + lc + ld
  Ny = ma + mb + mc + md
  Nz = na + nb + nc + nd

  N = Nx + Ny + Nz

  allocate(Bx(0:Nx),By(0:Ny),Bz(0:Nz),bf(0:N))

  boysarg = 0.25d0 * rpq2 / delta
  call BoysFArr(N,boysarg,bf,fbdum,.false.,idummy,idummy,idummy)

  call B_array(Bx,la,lb,lc,ld,xyzp(1),xyza(1),xyzb(1),xyzq(1),xyzc(1),xyzd(1),zeta1,zeta2,delta,Nx)
  call B_array(By,ma,mb,mc,md,xyzp(2),xyza(2),xyzb(2),xyzq(2),xyzc(2),xyzd(2),zeta1,zeta2,delta,Ny)
  call B_array(Bz,na,nb,nc,nd,xyzp(3),xyza(3),xyzb(3),xyzq(3),xyzc(3),xyzd(3),zeta1,zeta2,delta,Nz)

  eri = zero


  do ix = 0, Nx
    do iy = 0, Ny
      do iz = 0, Nz
        eri = eri + Bx(ix) * By(iy) * Bz(iz) * bf(ix + iy + iz)
      end do
    end do
  end do 


  prefact = two * PI**(2.5d0) / (zeta1 * zeta2 * dsqrt(zeta1 + zeta2)) &
             * dexp(-alphaa * alphab * rab2 / zeta1 - alphac * alphad * rcd2 / zeta2)

  dret = norma * normb * normc * normd * prefact * eri

  deallocate(Bx,By,Bz,bf)
  deallocate(fbdum)
  return
end function


subroutine B_array(Barr,la,lb,lc,ld,p,a,b,q,c,d,g1,g2,delta,imax)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h' 

  integer,   intent(in) :: imax
  integer,   intent(in) :: la,lb,lc,ld
  real(kdp), intent(in) :: p,a,b,q,c,d,g1,g2,delta

  real(kdp), intent(out) :: Barr(0:imax)

  integer ::  i1, i2, r1, r2, u, ind

  ! functions
  real(kdp) :: B_term

  do ind = 0, imax
    Barr(ind) = zero
  end do

  do i1 = 0, la + lb
    do i2 = 0, lc + ld
      do r1 = 0,  Int(Floor(0.5d0*i1))
        do r2 = 0, Int(Floor(0.5d0*i2))
          do u = 0, Int(Floor((0.5*(i1 + i2)))) - r1 - r2
            ind = i1 + i2 - 2 * (r1 + r2) - u 
            Barr(ind) = Barr(ind) + B_term(i1, i2, r1, r2, u, la, lb, lc, ld, p, a, b, q, c, d, g1, g2, delta)
          end do
        end do
      end do
    end do
  end do
end subroutine


function contr_coulomb(aexps,acoefs,anorms,xyza,la,ma,na,&
                       bexps,bcoefs,bnorms,xyzb,lb,mb,nb,&
                       cexps,ccoefs,cnorms,xyzc,lc,mc,nc,&
                       dexps,dcoefs,dnorms,xyzd,ld,md,nd,&
                       nexpa,nexpb,nexpc,nexpd) result(eri)
  implicit none
  
  include 'fortrankinds.h'
  include 'numbers.h'

  real(kdp) :: eri

  integer, intent(in) :: nexpa,nexpb,nexpc,nexpd

  real(kdp), intent(in) :: aexps(nexpa),acoefs(nexpa),anorms(nexpa),&
                           bexps(nexpb),bcoefs(nexpb),bnorms(nexpb),&
                           cexps(nexpc),ccoefs(nexpc),cnorms(nexpc),&
                           dexps(nexpd),dcoefs(nexpd),dnorms(nexpd)
  real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3)
  integer,   intent(in) :: la,ma,na,lb,mb,nb,lc,mc,nc,ld,md,nd

  real(kdp) :: incr
  integer   :: i,j,k,l

  ! functions
  real(kdp) :: coulomb_repulsion

  eri = zero

  do i = 1, nexpa
    do j = 1, nexpb
      do k = 1, nexpc
        do l = 1, nexpd
          incr = coulomb_repulsion(xyza, anorms(i), la, ma, na, aexps(i), &
                                   xyzb, bnorms(j), lb, mb, nb, bexps(j), &
                                   xyzc, cnorms(k), lc, mc, nc, cexps(k), &
                                   xyzd, dnorms(l), ld, md, nd, dexps(l))

          eri = eri + incr * acoefs(i) * bcoefs(j) * ccoefs(k) * dcoefs(l)

        end do
      end do
    end do
  end do

  return
end function
   
subroutine huz_eri(gout,                             &
                   aexps,acoefs,anorms,xyza,la,ma,na,&
                   bexps,bcoefs,bnorms,xyzb,lb,mb,nb,&
                   cexps,ccoefs,cnorms,xyzc,lc,mc,nc,&
                   dexps,dcoefs,dnorms,xyzd,ld,md,nd,&
                   nexpa,nexpb,nexpc,nexpd,          &
                   ncarta,ncartb,ncartc,ncartd) 
  implicit none
  
  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer, intent(in) :: nexpa,nexpb,nexpc,nexpd
  integer, intent(in) :: ncarta,ncartb,ncartc,ncartd

  ! output
  real(kdp), intent(out) :: gout(ncarta,ncartb,ncartc,ncartd)

  ! input 
  real(kdp), intent(in) :: aexps(nexpa,ncarta),acoefs(nexpa,ncarta),anorms(nexpa,ncarta),&
                           bexps(nexpb,ncartb),bcoefs(nexpb,ncartb),bnorms(nexpb,ncartb),&
                           cexps(nexpc,ncartc),ccoefs(nexpc,ncartc),cnorms(nexpc,ncartc),&
                           dexps(nexpd,ncartd),dcoefs(nexpd,ncartd),dnorms(nexpd,ncartd)
  real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3)
  integer,   intent(in) :: la(ncarta),ma(ncarta),na(ncarta),&
                           lb(ncartb),mb(ncartb),nb(ncartb),&
                           lc(ncartc),mc(ncartc),nc(ncartc),&
                           ld(ncartd),md(ncartd),nd(ncartd)

  ! local
  integer   :: icart,jcart,kcart,lcart

  ! functions
  real(kdp) :: contr_coulomb


  do icart = 1, ncarta
    do jcart = 1, ncartb
      do kcart = 1, ncartc
        do lcart = 1, ncartd
          gout(icart,jcart,kcart,lcart) &  
            = contr_coulomb(aexps(1,icart),acoefs(1,icart),anorms(1,icart),xyza,la(icart),ma(icart),na(icart),&
                            bexps(1,jcart),bcoefs(1,jcart),bnorms(1,jcart),xyzb,lb(jcart),mb(jcart),nb(jcart),&
                            cexps(1,kcart),ccoefs(1,kcart),cnorms(1,kcart),xyzc,lc(kcart),mc(kcart),nc(kcart),&
                            dexps(1,lcart),dcoefs(1,lcart),dnorms(1,lcart),xyzd,ld(lcart),md(lcart),nd(lcart),&
                            nexpa,nexpb,nexpc,nexpd)
        end do
      end do
    end do
  end do


end subroutine








