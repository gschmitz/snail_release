!-------------------------------------------------------------------------------------+
! Head-Gordon Pople schme for the evaluation of 2-electron repulsion integrals.
! The scheme is based on Obara-Saika.
!
! improvements in the future:
!  * reuse of intermediates
!  * usage of the shell structure of the integrals (we are wasting a lot of CPU time
!                                                   at the moment)
!
! The code is an adapted version of the similar PyQuante project:
!    http://pyquante.sourceforge.net/
!
!-------------------------------------------------------------------------------------+


subroutine vrr(dret,&
               xyza, norma, la, ma, na, alphaa, &
               xyzb, normb, alphab, &
               xyzc, normc, lc, mc, nc, alphac, &
               xyzd, normd, alphad, &
               rab2, rcd2, xyzp, xyzq, Kab, Kcd, &
               maux, vrr_terms, maxmtot, maxam, &
               fntab, nxvals, nrange, kmax)

   implicit none

   include 'fortrankinds.h'
   include 'constants.h'
   include 'numbers.h'

   ! output
   real(kdp), intent(out) :: dret

   ! parameter
   real(kdp), parameter  :: thrcoord = 1.d-16 

   ! dimensions
   integer,   intent(in) :: maxmtot, maxam
   integer,   intent(in) :: nxvals,nrange,kmax

   ! input
   real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3),&
                            rab2, rcd2, Kab, Kcd
   real(kdp), intent(in) :: xyzp(3), xyzq(3)
   real(kdp), intent(in) :: norma,normb,normc,normd
   real(kdp), intent(in) :: alphaa,alphab,alphac,alphad
   integer,   intent(in) :: la,ma,na, &
                            lc,nc,mc
   integer,   intent(in) :: maux

   real(kdp), intent(in) :: fntab(0:kmax,nxvals)

   ! scratch
   real(kdp), intent(inout) :: vrr_terms(0:maxmtot,&
                                         0:maxam, 0:maxam, 0:maxam, &
                                         0:maxam, 0:maxam, 0:maxam)

   ! local
   real(kdp) :: pxyz(3), qxyz(3), px, py, pz, qx, qy, qz
   real(kdp) :: zeta, eta
   real(kdp) :: etozetet,zetozetet,zetaeta
   real(kdp) :: halfoz,halfoe,halfoze
   real(kdp) :: wx, wy, wz
   real(kdp) :: rpq2
   real(kdp) :: boysarg
   !real(kdp) :: Kcd,Kab
   !real(kdp) :: abinv,cdinv
   integer   :: i,j,k,q,r,s,im,mtot,ltot

   real(kdp) :: dfac,dfac2,dpax,dwpx,dpay,dwpy,dpaz,dwpz,&
                dqcx,dwqx,dqcy,dwqy,dqcz,dwqz

   ! functions
   real(kdp) :: product_center_1D, dist2_3d

   !px = product_center_1D(alphaa, xyza(1), alphab, xyzb(1))
   !py = product_center_1D(alphaa, xyza(2), alphab, xyzb(2))
   !pz = product_center_1D(alphaa, xyza(3), alphab, xyzb(3))

   !qx = product_center_1D(alphac, xyzc(1), alphad, xyzd(1))
   !qy = product_center_1D(alphac, xyzc(2), alphad, xyzd(2))
   !qz = product_center_1D(alphac, xyzc(3), alphad, xyzd(3))

   px = xyzp(1) 
   py = xyzp(2) 
   pz = xyzp(3)

   qx = xyzq(1)
   qy = xyzq(2)
   qz = xyzq(3)

   zeta = alphaa + alphab
   eta  = alphac + alphad

   wx = product_center_1D(zeta, px, eta, qx)
   wy = product_center_1D(zeta, py, eta, qy)
   wz = product_center_1D(zeta, pz, eta, qz)

   rpq2 = dist2_3d(px, py, pz, qx, qy, qz)

   !abinv = one / zeta
   !cdinv = one / eta

   !Kab  = dsqrt(2.0d0) * PI**1.25d0 * abinv * dexp(-alphaa * alphab * abinv * rab2)

   !Kcd  = dsqrt(2.0d0) * PI**1.25d0 * cdinv * dexp(-alphac * alphad * cdinv * rcd2)

   if (Kab < 10.d-12 .or. Kcd < 10d-12) then
     dret = zero 
     return
   end if

   mtot = la + ma + na + lc + mc + nc + maux

   if (la * ma * na * lc * mc * nc * mtot > maxam**6 * maxmtot) then
     call quit("Buffer not large enough in vrr","vrr") 
   end if

   ! some shortcuts
   zetaeta   = zeta + eta
   etozetet  = eta / (zetaeta)
   zetozetet = zeta / (zetaeta)

   halfoz  = 0.50d0 / zeta
   halfoe  = 0.50d0 / eta
   halfoze = 0.50d0 / zetaeta

   boysarg = zeta * eta / (zetaeta) * rpq2
   call BoysFArr(mtot,boysarg,vrr_terms,fntab,.true.,nxvals,nrange,kmax)

   dfac = norma * normb * normc * normd * Kab * Kcd / dsqrt(zetaeta)
   do im = 0, mtot
     vrr_terms(im, 0, 0, 0, 0, 0, 0) = dfac * vrr_terms(im, 0, 0, 0, 0, 0, 0)
   end do


   !----------------------------------------------------------------------------------+
   ! generate integrals for first l -> [a0|00]
   !----------------------------------------------------------------------------------+

   ! Recurrence relation:
   !
   !      [a+1 0|0 0]{m} =   (P-A) * [a 0|0 0]{m}
   !                       + (W-P) * [a 0|0 0]{m+1}
   !                       + a/(2*zeta) * [a-1 0|0 0]{m}
   !                       - eta/(zeta+eta) * [a-1 0|0 0]{m+1}


   ! propbably the compiler already performs constant folding, but to be sure....
   dpax = px - xyza(1); dpay = py - xyza(2); dpaz = pz - xyza(3)
   dwpx = wx - px;      dwpy = wy - py;      dwpz = wz - pz

   ltot = la + ma + na
 
   if (ltot > 0) then
     do i = 0, la - 1
       dfac = real(i,kdp) * halfoz 
       do im = 0, mtot - i - 1

         vrr_terms(im, i + 1, 0, 0, 0, 0, 0) = dpax * vrr_terms(im, i, 0, 0, 0, 0, 0) & 
                                             + dwpx * vrr_terms(im + 1, i, 0, 0, 0, 0, 0)
         if (i > 0) then
           vrr_terms(im, i + 1, 0, 0, 0, 0, 0) = vrr_terms(im, i + 1, 0, 0, 0, 0, 0) + dfac * &
                                                (vrr_terms(im, i - 1, 0, 0, 0, 0, 0) &
                                           - etozetet * vrr_terms(im + 1, i - 1, 0, 0, 0, 0, 0))
         end if
       end do
     end do

     do j = 0, ma - 1
       dfac = real(j,kdp) * halfoz
       do i = 0, la
         do im = 0, mtot - i - j - 1
           vrr_terms(im, i, j + 1, 0, 0, 0, 0) = dpay * vrr_terms(im, i, j, 0, 0, 0, 0) &
                                               + dwpy * vrr_terms(im + 1, i, j, 0, 0, 0, 0)
           if (j > 0) then
             vrr_terms(im, i, j + 1, 0, 0, 0, 0) = vrr_terms(im, i, j + 1, 0, 0, 0, 0) + dfac * &
                                                  (vrr_terms(im, i, j - 1, 0, 0, 0, 0) &
                                               - etozetet * vrr_terms(im + 1, i, j - 1, 0, 0, 0, 0))
           end if
         end do
       end do
     end do

     do k = 0, na - 1
       dfac = real(k,kdp) * halfoz
       do j = 0, ma
         do i = 0, la
           do im = 0, mtot - i - j - k - 1
             vrr_terms(im, i, j, k + 1, 0, 0, 0) = dpaz * vrr_terms(im, i, j, k, 0, 0, 0) &
                                                 + dwpz * vrr_terms(im + 1, i, j, k, 0, 0, 0)

             if (k > 0) then
               vrr_terms(im, i, j, k + 1, 0, 0, 0) = vrr_terms(im, i, j, k + 1, 0, 0, 0) + dfac * &
                                                    (vrr_terms(im, i, j, k - 1, 0, 0, 0) &
                                               - etozetet * vrr_terms(im + 1, i, j, k - 1, 0, 0, 0))
             end if
           end do
         end do
       end do
     end do
   end if

   !----------------------------------------------------------------------------------+
   ! generate integrals for third l -> [a0|c0]
   !----------------------------------------------------------------------------------+
  
   !   The recurrence relation
   !
   !    [a 0|c+1 0]{m} =   (Q-C) * [a 0|c 0]{m}              
   !                     + (W-Q) * [a 0|c 0]{m+1}            
   !                     + c/(2*eta) * [a 0|c-1 0]{m}         
   !                     - zeta/(zeta+eta)  * [a 0|c-1 0]{m+1} 
   !                     + a/(2*(zeta+eta)) * [a-1 0|c 0]{m+1}  


   ! propbably the compiler already performs constant folding, but to be sure....
   dqcx = qx - xyzc(1); dqcy = qy - xyzc(2); dqcz = qz - xyzc(3)
   dwqx = wx - qx;      dwqy = wy - qy;      dwqz = wz - qz

   do q = 0, lc - 1
     dfac = real(q,kdp) * halfoe
     do k = 0, na
       do j = 0, ma
         do i = 0, la
           dfac2 = real(i,kdp)
           do im = 0, mtot - i - j - k - q - 1
             vrr_terms(im, i, j, k, q + 1, 0, 0) = dqcx * vrr_terms(im, i, j, k, q, 0, 0) &
                                                 + dwqx * vrr_terms(im + 1, i, j, k, q, 0, 0)
             if (q > 0) then
               vrr_terms(im, i, j, k, q + 1, 0, 0) = vrr_terms(im, i, j, k, q + 1, 0, 0) + dfac * &
                                                    (vrr_terms(im, i, j, k, q - 1, 0, 0) & 
                                                 - zetozetet * vrr_terms(im + 1, i, j, k, q - 1, 0, 0))
             end if
             if (i > 0) then
               vrr_terms(im, i, j, k, q + 1, 0, 0) = vrr_terms(im, i, j, k, q + 1, 0, 0) &
                                                     + dfac2 * halfoze             &
                                                     * vrr_terms(im + 1, i - 1, j, k, q, 0, 0)
             end if
           end do
         end do
       end do
     end do
   end do

   do r = 0, mc - 1
     dfac = real(r,kdp) * halfoe
     do q = 0, lc
       do k = 0, na
         do j = 0, ma
           dfac2 = real(j,kdp) 
           do i = 0, la
             do im = 0, mtot - i - j - k - q - r - 1
               vrr_terms(im, i, j, k, q, r + 1, 0) = dqcy * vrr_terms(im, i, j, k, q, r, 0) &
                                                   + dwqy * vrr_terms(im + 1, i, j, k, q, r, 0)
               if (r > 0) then
                 vrr_terms(im, i, j, k, q, r + 1, 0) = vrr_terms(im, i, j, k, q, r + 1, 0)    &
                                                       + dfac                                 &
                                                       * (vrr_terms(im, i, j, k, q, r - 1, 0) &
                                                  - zetozetet * vrr_terms(im + 1, i, j, k, q, r - 1, 0))
               end if
               if (j > 0) then
                 vrr_terms(im, i, j, k, q, r + 1, 0) = vrr_terms(im, i, j, k, q, r + 1, 0) &
                                                       + dfac2 * halfoze             &
                                                       * vrr_terms(im + 1, i, j - 1, k, q, r, 0)
               end if
             end do
           end do
         end do
       end do
     end do
   end do

   do s = 0, nc - 1
     dfac = real(s,kdp) * halfoe
     do r = 0, mc
       do q = 0, lc
         do k = 0, na
           dfac2 = real(k,kdp)
           do j = 0, ma
             do i = 0, la
               do im = 0, mtot - i - j - k - q - r - s - 1
                 vrr_terms(im, i, j, k, q, r, s + 1) = dqcz * vrr_terms(im, i, j, k, q, r, s) &
                                                     + dwqz * vrr_terms(im + 1, i, j, k, q, r, s)
                 if (s > 0) then
                   vrr_terms(im, i, j, k, q, r, s + 1) = vrr_terms(im, i, j, k, q, r, s + 1)   &
                                               + dfac * (vrr_terms(im, i, j, k, q, r, s - 1) &
                                               - zetozetet * vrr_terms(im + 1, i, j, k, q, r, s - 1))
                 end if
                 if (k > 0) then
                   vrr_terms(im, i, j, k, q, r, s + 1) = vrr_terms(im, i, j, k, q, r, s + 1) &
                                                         + dfac2 * halfoze             &
                                                         * vrr_terms(im + 1, i, j, k - 1, q, r, s)
                 end if
               end do
             end do
           end do
         end do
       end do
     end do
   end do


   dret = vrr_terms(maux, la, ma, na, lc, mc, nc)

   return
end subroutine


subroutine contr_vrr(dret,&
                     nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                     nprimb, xyzb, bnorms, bexps, bcoefs, &
                     nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                     nprimd, xyzd, dnorms, dexps, dcoefs, &
                     ibf, jbf, kbf, lbf, rab2, rcd2, xyzp, xyzq, Kab, Kcd, &
                     vrr_terms, maxmtot, maxam, &
                     fntab, nxvals, nrange, kmax)

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'
  include 'numbers.h'

  ! output
  real(kdp), intent(out) :: dret

  ! dimensions:
  integer, intent(in) :: nprima,nprimb,nprimc,nprimd
  integer, intent(in) :: maxam,maxmtot
  integer, intent(in) :: nxvals,nrange,kmax

  ! input
  real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3),&
                           rab2, rcd2
  real(kdp), intent(in) :: xyzp(3,nprima,nprimb),& 
                           xyzq(3,nprimc,nprimd),&
                           Kab(nprima,nprimb),&
                           Kcd(nprimc,nprimd)
  real(kdp), intent(in) :: anorms(nprima),bnorms(nprimb),cnorms(nprimc),dnorms(nprimd)
  real(kdp), intent(in) :: aexps(nprima),bexps(nprimb),cexps(nprimc),dexps(nprimd)
  real(kdp), intent(in) :: acoefs(nprima),bcoefs(nprimb),ccoefs(nprimc),dcoefs(nprimd)
  integer,   intent(in) :: la,ma,na, &
                           lc,nc,mc, &
                           ibf,jbf,kbf,lbf
  real(kdp), intent(in) :: fntab(0:kmax,nxvals)

  ! scratch
  real(kdp), intent(inout) :: vrr_terms(0:maxmtot, &
                                        0:maxam, 0:maxam, 0:maxam, &
                                        0:maxam, 0:maxam, 0:maxam)

  ! local:
  integer   ::  i, j, k, l
  real(kdp) ::  val, dtemp, dfacij, dfackl

  val = zero

  do i = 1, nprima
    do j = 1, nprimb
      dfacij = acoefs(i) * bcoefs(j)
      do k = 1, nprimc
        do l = 1, nprimd
          call vrr(dtemp,&
                   xyza,anorms(i),la,ma,na,aexps(i), &
                   xyzb,bnorms(j),bexps(j), &
                   xyzc,cnorms(k),lc,mc,nc,cexps(k), &
                   xyzd,dnorms(l),dexps(l), &
                   rab2, rcd2, xyzp(1,i,j), xyzq(1,k,l), Kab(i,j), Kcd(k,l), &
                   0,vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam, &
                   fntab, nxvals, nrange, kmax )


          val = val + dtemp * dfacij * ccoefs(k) * dcoefs(l) 
        end do
      end do
    end do
  end do

  dret = val
end subroutine

recursive subroutine contr_hrr(dret,&
                               nprima,xyza,anorms,la,ma,na,aexps,acoefs, &
                               nprimb,xyzb,bnorms,lb,mb,nb,bexps,bcoefs, &
                               nprimc,xyzc,cnorms,lc,mc,nc,cexps,ccoefs, &
                               nprimd,xyzd,dnorms,ld,md,nd,dexps,dcoefs, &
                               ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                               vrr_terms,maxmtot,maxam,nbuf, &
                               fntab, nxvals, nrange, kmax) 

  implicit none

  include 'fortrankinds.h'
  include 'constants.h'

  ! output
  real(kdp), intent(out) :: dret

  ! parameter
  real(kdp), parameter  :: thrcoord = 1.d-16 

  ! dimensions
  integer,   intent(in) :: nprima,nprimb,nprimc,nprimd
  integer,   intent(in) :: maxmtot, maxam, nbuf
  integer,   intent(in) :: nxvals, nrange, kmax

  ! input
  real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3),xyzd(3),&
                           rab2, rcd2
  real(kdp), intent(in) :: xyzp(3,nprima,nprimb),&
                           xyzq(3,nprimc,nprimd),&
                           Kab(nprima,nprimb),&
                           Kcd(nprimc,nprimd)
  real(kdp), intent(in) :: anorms(nprima),bnorms(nprimb),cnorms(nprimc),dnorms(nprimd)
  real(kdp), intent(in) :: acoefs(nprima),bcoefs(nprimb),ccoefs(nprimc),dcoefs(nprimd)
  real(kdp), intent(in) :: aexps(nprima),bexps(nprimb),cexps(nprimc),dexps(nprimd)
  integer,   intent(in) :: la,ma,na,lb,mb,nb, &
                           lc,nc,mc,ld,md,nd, &
                           ibf,jbf,kbf,lbf

  real(kdp), intent(in) :: fntab(0:kmax,nxvals)

  ! scratch
  real(kdp), intent(inout) :: vrr_terms(0:maxmtot, &
                                        0:maxam, 0:maxam, 0:maxam, &
                                        0:maxam, 0:maxam, 0:maxam)

  ! local
  real(kdp) :: dist,dtemp

  !------------------------------------------------------------------------------------------+
  ! use horizontal reccurence relation to transfer cartesian powers for 1. electron:
  !    (a(b+1)|cd) = ((a+1)b|cd) + X_ab (ab|cd)
  !------------------------------------------------------------------------------------------+
  if (lb > 0) then
    call contr_hrr(dret,&
                   nprima, xyza, anorms, la + 1, ma, na, aexps, acoefs, &
                   nprimb, xyzb, bnorms, lb - 1, mb, nb, bexps, bcoefs, &
                   nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                   nprimd, xyzd, dnorms, ld, md, nd, dexps, dcoefs, &
                   ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                   vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                   fntab, nxvals, nrange, kmax)

    dist = xyza(1) - xyzb(1) 
    if (dabs(dist) >= thrcoord) then
      call contr_hrr(dtemp,nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                     nprimb, xyzb, bnorms, lb - 1, mb, nb, bexps, bcoefs, &
                     nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                     nprimd, xyzd, dnorms, ld, md, nd, dexps, dcoefs, &
                     ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                     vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                     fntab, nxvals, nrange, kmax)

      dret = dret + dist * dtemp
    end if
    return 
  end if

  if (mb > 0) then
    call  contr_hrr(dret,nprima, xyza, anorms, la, ma + 1, na, aexps, acoefs, &
                    nprimb, xyzb, bnorms, lb, mb - 1, nb, bexps, bcoefs, &
                    nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                    nprimd, xyzd, dnorms, ld, md, nd, dexps, dcoefs, &
                    ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                    vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                    fntab, nxvals, nrange, kmax)

    dist = xyza(2) - xyzb(2)
    if (dabs(dist) >= thrcoord) then
      call contr_hrr(dtemp,nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                     nprimb, xyzb, bnorms, lb, mb - 1, nb, bexps, bcoefs, &
                     nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                     nprimd, xyzd, dnorms, ld, md, nd, dexps, dcoefs, &
                     ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                     vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                     fntab, nxvals, nrange, kmax)

      dret = dret + dist * dtemp 
    end if
    return 
  end if

  if (nb > 0) then
    call contr_hrr(dret,&
                   nprima, xyza, anorms, la, ma, na + 1, aexps, acoefs, &
                   nprimb, xyzb, bnorms, lb, mb, nb - 1, bexps, bcoefs, &
                   nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                   nprimd, xyzd, dnorms, ld, md, nd, dexps, dcoefs, &
                   ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                   vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, & 
                   fntab, nxvals, nrange, kmax)

    dist = xyza(3) - xyzb(3)
    if (dabs(dist) >= thrcoord) then
      call contr_hrr(dtemp,&
                     nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                     nprimb, xyzb, bnorms, lb, mb, nb - 1, bexps, bcoefs, &
                     nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                     nprimd, xyzd, dnorms, ld, md, nd, dexps, dcoefs, &
                     ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                     vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                     fntab, nxvals, nrange, kmax)

      dret = dret + dist * dtemp
    end if
    return 
  end if

  !------------------------------------------------------------------------------------------+
  ! use horizontal reccurence relation to transfer cartesian powers for 2. electron:
  !   (ab|c(d+1)) = (ab|(c+1)d) + X_cd (ab|cd)
  !------------------------------------------------------------------------------------------+
  if (ld > 0) then
    call contr_hrr(dret,&
                   nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                   nprimb, xyzb, bnorms, lb, mb, nb, bexps, bcoefs, &
                   nprimc, xyzc, cnorms, lc + 1, mc, nc, cexps, ccoefs, &
                   nprimd, xyzd, dnorms, ld - 1, md, nd, dexps, dcoefs, &
                   ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                   vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                   fntab, nxvals, nrange, kmax)

    dist = xyzc(1) - xyzd(1)
    if (dabs(dist) >= thrcoord) then
      call contr_hrr(dtemp,&
                     nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                     nprimb, xyzb, bnorms, lb, mb, nb, bexps, bcoefs, &
                     nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                     nprimd, xyzd, dnorms, ld - 1, md, nd, dexps, dcoefs,&
                     ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                     vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                     fntab, nxvals, nrange, kmax)

      dret = dret + dist * dtemp
    end if
    return 
  end if

  if (md > 0) then
    call contr_hrr(dret,&
                   nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                   nprimb, xyzb, bnorms, lb, mb, nb, bexps, bcoefs, &
                   nprimc, xyzc, cnorms, lc, mc + 1, nc, cexps, ccoefs, &
                   nprimd, xyzd, dnorms, ld, md - 1, nd, dexps, dcoefs, &
                   ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                   vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                   fntab, nxvals, nrange, kmax)

    dist = xyzc(2) - xyzd(2)
    if (dabs(dist) >= thrcoord) then 
          call contr_hrr(dtemp,&
                         nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                         nprimb, xyzb, bnorms, lb, mb, nb, bexps, bcoefs, &
                         nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                         nprimd, xyzd, dnorms, ld, md - 1, nd, dexps, dcoefs, &
                         ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                         vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                         fntab, nxvals, nrange, kmax)

          dret = dret + dist * dtemp
    end if
    return 
  end if

  if (nd > 0) then
    call contr_hrr(dret,&
                   nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                   nprimb, xyzb, bnorms, lb, mb, nb, bexps, bcoefs, &
                   nprimc, xyzc, cnorms, lc, mc, nc + 1, cexps, ccoefs, &
                   nprimd, xyzd, dnorms, ld, md, nd - 1, dexps, dcoefs, &
                   ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                   vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, & 
                   fntab, nxvals, nrange, kmax)

    dist = xyzc(3) - xyzd(3)
    if (dabs(dist) >= thrcoord) then
      call contr_hrr(dtemp,&
                     nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                     nprimb, xyzb, bnorms, lb, mb, nb, bexps, bcoefs, &
                     nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                     nprimd, xyzd, dnorms, ld, md, nd - 1, dexps, dcoefs, &
                     ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                     vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam,nbuf, &
                     fntab, nxvals, nrange, kmax)

      dret = dret + dist * dtemp
    end if
    return 
  end if


  !------------------------------------------------------------------------------------------+
  ! return contracted integral (a0|c0) = sum [a0|c0]
  !------------------------------------------------------------------------------------------+
  call contr_vrr(dret,&
                 nprima, xyza, anorms, la, ma, na, aexps, acoefs, &
                 nprimb, xyzb, bnorms, bexps, bcoefs, &
                 nprimc, xyzc, cnorms, lc, mc, nc, cexps, ccoefs, &
                 nprimd, xyzd, dnorms, dexps, dcoefs, &
                 ibf,jbf,kbf,lbf,rab2,rcd2,xyzp,xyzq,Kab,Kcd,&
                 vrr_terms(0,0,0,0,0,0,0),maxmtot,maxam, &
                 fntab, nxvals, nrange, kmax)

end subroutine
