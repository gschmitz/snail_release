

subroutine transferl(gout,gin,ama,xyza,amb,xyzb,ncartc,ncartd)

  implicit none

  include 'fortrankinds.h'

  ! input
  integer,   intent(in) :: ama,amb
  integer,   intent(in) :: ncartc,ncartd
  real(kdp), intent(in) :: xyza(3), xyzb(3)

  real(kdp), intent(in) :: gin(  (ama+amb+1)*(ama+amb+2)/2 , & 
                                 ncartc,ncartd             ) 

  ! output
  real(kdp), intent(out) :: gout( (ama+amb+1)*(ama+amb+2)/2 , & 
                                  (amb+1)*(amb+2)/2         , &
                                  ncartc                    , &
                                  ncartd                    ) 
  ! local
  integer   :: icarta,icartb,icartc,icartd,ia,ib,&
               iax,iay,iaz,ibx,iby,ibz,ncarta,ncartb
  integer   :: mxcarta,mxcartb
  integer   :: icartaxm,icartaym,icartazm,&
               icartbxp,icartbyp,icartbzp

  real(kdp) :: dij(3) 

  integer, allocatable :: abfinfo(:,:),bbfinfo(:,:)

  ! functions
  integer :: getind


  !
  ! Define dimensions
  !
  mxcarta = (ama+amb+1)*(ama+amb+2)/2
  mxcartb = (amb+1)*(amb+2)/2 


  !
  ! allocate memory
  !
  allocate(abfinfo(3,mxcarta),bbfinfo(3,mxcartb))


  !
  ! Copy integrals (a+b,0|cd) to scratch of size (a+b,b|cd)
  !
  call getbfinfo(abfinfo,ncarta,ama+amb)
  do icartd = 1, ncartd
    do icartc = 1, ncartc
      do icarta = 1, ncarta
        gout(icarta,1,icartc,icartd) = gin(icarta,icartc,icartd)
      end do
    end do
  end do 


  !  Electron transfer relation:
  ! ----------------------------
  !
  !   (a-1 b+1 | c d) = (a b | c d) + X_ab (a-1 b | c d) 
  !

  ! apply transfer relation
  ib = 0
  do ia = ama + amb, ama, -1

    ! loop over basis functions in shell ia
    call getbfinfo(abfinfo,ncarta,ia)
    do icarta = 1, ncarta

      ! Get cartesian exponents of basis function at center A
      iax = abfinfo(1,icarta)
      iay = abfinfo(2,icarta)
      iaz = abfinfo(3,icarta)

      ! loop over basis functions in shell ib 
      call getbfinfo(bbfinfo,ncartb,ib)
      do icartb = 1, ncartb

        ! Get cartesian exponents of basis function at center B
        ibx = bbfinfo(1,icartb)
        iby = bbfinfo(2,icartb)
        ibz = bbfinfo(3,icartb)

        ! Get indices for cartesian bf for decremented ia (we can do three at once)
        icartaxm = getind(iax - 1, iay, iaz)
        icartaym = getind(iax, iay - 1, iaz)
        icartazm = getind(iax, iay, iaz - 1)

        ! Get indices for cartesian bf for incremented ib (we can do three at once)
        icartbxp = getind(ibx + 1, iby, ibz)
        icartbyp = getind(ibx, iby + 1, ibz)
        icartbzp = getind(ibx, iby, ibz + 1)

        dij(1) = xyza(1) - xyzb(1)
        dij(2) = xyza(2) - xyzb(2)
        dij(3) = xyza(3) - xyzb(3)

        do icartc = 1, ncartc
          do icartd = 1, ncartd
            gout(icartaxm,icartbxp,icartc,icartd) = gout(icarta,icartb,icartc,icartd) + dij(1) * gout(icartaxm,icartb,icartc,icartd)
            gout(icartaym,icartbyp,icartc,icartd) = gout(icarta,icartb,icartc,icartd) + dij(2) * gout(icartaym,icartb,icartc,icartd)
            gout(icartazm,icartbzp,icartc,icartd) = gout(icarta,icartb,icartc,icartd) + dij(3) * gout(icartazm,icartb,icartc,icartd)
          end do
        end do

      end do ! end loop over basis functions in shell ib + 1 
    end do ! end loop over basis functions in shell ia - 1 

    ib = ib + 1
  end do
  
  deallocate(abfinfo,bbfinfo)

end subroutine

subroutine getbfinfo(bfinfo,icart,angm)

  implicit none

  integer, intent(in)  :: angm
  integer, intent(out) :: bfinfo(3, (angm + 1) * (angm + 2) / 2), icart

  ! local
  integer :: i, j, nx, ny, nz

  icart = 0
  do i = 0, angm  
    nx = angm - i
    do j = 0, i    
      ny = i - j
      nz = j

      icart = icart + 1

      bfinfo(1,icart) = nx
      bfinfo(2,icart) = ny
      bfinfo(3,icart) = nz

    end do
  end do

end subroutine
  
function getind(nx,ny,nz) result(ind)
  implicit none

  integer :: ind

  integer, intent(in) :: nx,ny,nz
  integer :: ii,jj

  ii = ny + nz
  jj = nz

  !  return index in one dimensional array
  ind = ii * (ii + 1) / 2 + jj + 1
  return
end function
