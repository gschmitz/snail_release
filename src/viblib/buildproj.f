subroutine buildproj(proj,coord,amass,natoms,ncoord)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: natoms,ncoord

  ! output
  real(kdp), intent(out) :: proj(ncoord,ncoord)

  ! input
  real(kdp), intent(in)  :: amass(natoms),coord(3,natoms)

  ! local
  real(kdp), allocatable :: xm(:),v(:,:),tmp(:,:,:),xi(:,:),w(:),work(:),xyz(:,:)
  real(kdp) :: totm, dlen, dd
  integer   :: iat,ix,lwork,info,j,k

  ! functions
  real(kdp) :: ddot

  ! init projector as identity operator
  proj = zero
  do ix = 1, ncoord
    proj(ix,ix) = one
  end do

  allocate(xm(natoms),v(3,ncoord),xyz(3,natoms))

  ! copy coordinates to xyz and mass weight them
  do iat = 1, natoms
    xyz(1,iat) = coord(1,iat) * sqrt(amass(iat))
    xyz(2,iat) = coord(2,iat) * sqrt(amass(iat))
    xyz(3,iat) = coord(3,iat) * sqrt(amass(iat))
  end do

  !--------------------------------------------------+
  ! Project out translation 
  !--------------------------------------------------+

  totm = zero

  do iat = 1, natoms
    totm = totm + amass(iat)
    xm(iat) = sqrt(amass(iat))
  end do
  totm = sqrt(totm)
  xm(:) = xm(:) / totm

  do ix = 1, 3
    v(:,:) = zero
    call dcopy(natoms, xm, 1, v(ix,1), 3)
    call dsyr('l', ncoord, -one, v, 1, proj, ncoord)
  end do

  !--------------------------------------------------+
  ! Project out rotation
  !--------------------------------------------------+
  allocate(tmp(3,3,natoms),w(3))

  ! build inverse sqrt moment of inertia
  dd = ddot(ncoord, xyz, 1, xyz, 1)

  allocate(xi(3,3))
  xi(:,:) = zero
  xi(1,1) = dd
  xi(2,2) = dd
  xi(3,3) = dd
  call dgemm('N','T', 3, 3, natoms, -one, xyz, 3, xyz, 3, one, xi, 3)
 
  ! first run to get dimension for work
  lwork = -1
  call dsyev('V','L',3,xi,3,w,dlen,lwork,info)

  ! allocate memory for work
  lwork = int(dlen)
  allocate(work(lwork))

  ! do eigendecomposition
  call dsyev('V','L',3,xi,3,w,work,lwork,info)
  call dcopy(3*3,xi,1,tmp,1)

  do ix = 1, 3
    w(ix) = one / sqrt(w(ix))
    w(ix) = sqrt(w(ix))
    call dscal(3,w(ix),tmp(1,ix,1),1)
  end do
  call dgemm('N','T',3,3,3,one,tmp,3,tmp,3,zero,xi,3)

  ! Now I^{-1/2} is on xi
   
  ! build set of rotational vector l for each R_alpha
  tmp(:,:,:) = zero 

  do ix = 1, 3
    j = mod(ix,3)+1
    k = mod(ix+1,3)+1
    call dcopy(natoms,xyz(ix,1),3,tmp(k,j,1),9)
    call dcopy(natoms,xyz(ix,1),3,tmp(j,k,1),9)
    call dscal(natoms,-one,tmp(j,k,1),9)
  end do

  ! l_(R_a,ci) = I^-1/2(a,d) tmp(d,ci) and save it on v
  call dsymm('l', 'l', 3, ncoord, one, xi, 3, tmp, 3, zero, v, 3)

  ! Update the projection matrix  
  call dsyrk('l', 't', ncoord, 3, -one, v, 3, one, proj, ncoord)

end subroutine
