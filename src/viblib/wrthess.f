subroutine wrthess(fc,ncoord)
  
  use mod_fileutil

  implicit none

  include 'fortrankinds.h'

  ! dimensions
  integer, intent(in) :: ncoord


  ! input
  real(kdp), intent(in) :: fc(ncoord*(ncoord+1)/2)


  ! local
  integer :: i,j,ic,maxcol,mincol
  integer :: iscrfl

  character(len=80) :: filhess

  filhess = 'hessian'
  iscrfl = openfile(filhess,'replace')

  write(iscrfl,'(a)') '$hessian (projected)'
  do i = 1, ncoord
    ic = 0
    maxcol = 0
    do while (maxcol < ncoord)
      mincol = maxcol+1
      maxcol = min(maxcol+5,ncoord)
      ic = ic + 1
      write(iscrfl,'(i3,i2,5f15.10)') i,ic,(fc((max(i,j)-3)*(max(i,j))/2+i+j),j=mincol,maxcol)
    end do   !if(maxcol.lt.ncoord) goto 200
  end do

  write(iscrfl,'(a)') '$end'

  call closefile(iscrfl)


end subroutine
