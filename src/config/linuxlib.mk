
##############################################################
# Set compiler flags
##############################################################

LD := ar

ifeq ($(profile),yes) 
  iprof = -pg
else
  iprof = 
endif

ifeq ($(mode),debug)
  ############################################################
  # for developmend:
  ############################################################

  #
  # ifort 
  # 
  ifeq ($(FC),ifort) 
    FCFLAGS = -fpp -assume byterecl -zero -free -std03 -nogen-interface -g -check all -CB -warn all -module $(OBJDIR) -I$(INCDIR) $(MODDIR) $(LIBS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -qopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif 

  #
  # gfortran 
  # 
  ifeq ($(FC),gfortran) 
    FCFLAGS = -cpp -ffree-form -std=gnu  -fcheck=all -fbacktrace  -g -c -O -J $(OBJDIR)  -I$(INCDIR) $(MODDIR) $(LIBS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -fopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

  #
  # pgfortran 
  #
  ifeq ($(FC),pgfortran)
    FCFLAGS = -Mpreprocess -Mfree -C -g -c -O -module $(OBJDIR) -I$(INCDIR) $(MODDIR) $(LIBS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -mp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

  #
  # flang 
  #
  ifeq ($(FC),flang)
    FCFLAGS = -cpp -ffree-form -g -c -O -module $(OBJDIR) -I$(INCDIR) $(MODDIR) $(LIBS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -fopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif
else
  ############################################################
  # for producing optimized code:
  ############################################################
  mode = release

  #
  # ifort
  #
  ifeq ($(FC),ifort) 
    FCFLAGS =  -fpp -assume byterecl  $(iprof) -traceback -free -std03 -nogen-interface -g -O3 -module $(OBJDIR) -I$(INCDIR) $(MODDIR) $(LIBS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -qopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif 

  #
  # gfortran 
  # 
  ifeq ($(FC),gfortran) 
    FCFLAGS = -cpp -ffree-form -std=gnu -g -c -O -J $(OBJDIR) -I$(INCDIR) $(MODDIR) $(LIBS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -fopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

  #
  # pgfortran 
  #
  ifeq ($(FC),pgfortran)
    FCFLAGS = -Mfree -Mpreprocess -g -c -O -module $(OBJDIR) -I$(INCDIR) $(MODDIR) $(LIBS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -mp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

  #
  # flang 
  #
  ifeq ($(FC),flang)
    FCFLAGS = -cpp -ffree-form -module $(OBJDIR) -I$(INCDIR) $(MODDIR) $(LIBS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -fopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

endif

ifeq ($(use_mkl),yes)
  FCFLAGS += -D MKL
endif

LDFLAGS = rc

