
##############################################################
# Set compiler flags
#############################################################

ifeq ($(profile),yes) 
  iprof = -pg
else
  iprof = 
endif

ifeq ($(mode),debug)
  ############################################################
  # for developmend:
  ############################################################

  #
  # ifort 
  # 
  ifeq ($(FC),ifort) 
    FCFLAGS = -fpp -assume byterecl -zero -free -std03 -nogen-interface -g -check all -CB -warn all -module $(OBJDIR) -I$(INCDIR) $(MODS)
    LDFLAGS = -g 
    ifeq ($(use_omp),yes)
      FCFLAGS += -qopenmp
      LDFLAGS += -qopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

  #
  # gfortran 
  # 
  ifeq ($(FC),gfortran) 
    FCFLAGS = -cpp -ffree-form -std=gnu -fcheck=all -fbacktrace  -g -c -O -J $(OBJDIR) -I$(INCDIR) $(MODS)
    LDFLAGS = -g  
    ifeq ($(use_omp),yes)
      FCFLAGS += -fopenmp
      LDFLAGS += -fopenmp
    else
      FCFLAGS += -D NOOMP 
      LDFLAGS += -static
    endif
  endif

  #
  # pgfortran 
  #
  ifeq ($(FC),pgfortran)
    FCFLAGS = -Mpreprocess -Mfree -C -g -c -O -module $(OBJDIR) -I$(INCDIR) $(MODS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -mp
      LDFLAGS += -mp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

  #
  # flang 
  #
  ifeq ($(FC),flang)
    FCFLAGS = -cpp -ffree-form -module $(OBJDIR) -I$(INCDIR) $(MODS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -fopenmp
      FDFLAGS += -fopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

else
  ############################################################
  # for producing optimized code:
  ############################################################
  mode = release

  #
  # ifort 
  # 
  ifeq ($(FC),ifort) 
    FCFLAGS = -fpp -assume byterecl $(iprof) -free -std03 -nogen-interface -g -O -module $(OBJDIR) -I$(INCDIR) $(MODS) 
    LDFLAGS = $(iprof) -g -O  
    ifeq ($(use_omp),yes)
      FCFLAGS += -qopenmp
      LDFLAGS += -qopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

  #
  # gfortran 
  # 
  ifeq ($(FC),gfortran) 
    FCFLAGS = -cpp -ffree-form -std=gnu -g -c -O -J $(OBJDIR) -I$(INCDIR) $(MODS)
    LDFLAGS = -g -O  
    ifeq ($(use_omp),yes)
      FCFLAGS += -fopenmp
      LDFLAGS += -fopenmp
    else
      FCFLAGS += -D NOOMP 
      LDFLAGS += -static
    endif
  endif

  #
  # pgfortran 
  #
  ifeq ($(FC),pgfortran)
    FCFLAGS = -Mpreprocess -Mfree -g -c -O -module $(OBJDIR) -I$(INCDIR) $(MODS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -mp
      LDFLAGS += -mp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

  #
  # flang 
  #
  ifeq ($(FC),flang)
    FCFLAGS = -cpp -ffree-form -module $(OBJDIR) -I$(INCDIR) $(MODS)
    ifeq ($(use_omp),yes)
      FCFLAGS += -fopenmp
      FDFLAGS += -fopenmp
    else
      FCFLAGS += -D NOOMP 
    endif
  endif

endif

ifeq ($(use_mkl),yes)
  FCFLAGS += -D MKL
endif

