subroutine getmpa(orb,shells,atoms,nmo,nbf,nshell,nocc,natoms)
!--------------------------------------------------------+
! Purpose: Do a Mulliken Population Analysis 
!
!--------------------------------------------------------+

  use type_shell
  use type_atom

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'
 
  ! constants
  logical, parameter :: isuhf = .false.

  ! dimensions
  integer, intent(in) :: nmo,nbf,nshell,nocc,natoms

  ! input
  real(kdp),        intent(in) :: orb(nmo,nbf)
  type(atom_info),  intent(in) :: atoms(natoms)
  type(shell_info), intent(in) :: shells(nshell)

  ! local
  real(kdp), allocatable :: ovlp(:,:), pop(:,:), dens(:,:), gop(:)
  integer,   allocatable :: imapatom(:),imapbf(:)
  real(kdp) :: qelec, gap, qcharge
  integer   :: ibf, jbf, ishl, iatom, iatold, ibfst, ibfnd

  allocate(ovlp(nbf,nbf),pop(nbf,nbf),dens(nbf,nbf),gop(nbf))

  allocate(imapatom(nbf),imapbf(natoms + 1))

  !-----------------------------------------------------------------------+
  ! print header information
  !-----------------------------------------------------------------------+
  call PrintHeader('Starting Mulliken population analysis')

  !-----------------------------------------------------------------------+
  ! Actual subroutine
  !-----------------------------------------------------------------------+

  ! get maping of basis functions to atoms
  iatold = -1
  imapbf(natoms + 1) = nbf + 1
  do ishl = 1, nshell
    ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
    iatom = shells(ishl)%iatom

    if (iatold .ne. iatom) then
      iatold = iatom
      imapbf(iatom) = shells(ishl)%ibfstart
    end if 

    do ibf = ibfst, ibfnd
      imapatom(ibf) = iatom
    end do    
  end do

  ! Build density
  call builddens(isuhf, dens, orb, 1, nocc, nbf)

  ! Get overlap matrix
  call getovlp(ovlp, shells, nbf, nshell)

  ! construct population matrix
  qelec = zero
  do jbf = 1, nbf
    do ibf = 1, nbf
      pop(ibf,jbf) = ovlp(ibf,jbf) * dens(ibf,jbf)
      qelec = qelec + pop(ibf,jbf)
    end do
  end do 

  ! create gross orbital population GOP
  gop = zero
  do jbf = 1, nbf
    do ibf = 1, nbf
      gop(ibf) = gop(ibf) + pop(ibf,jbf)
    end do
  end do

  write(istdout,'(2x,a,f12.8,/)') "number of electrons MPA:", qelec

  ! get gross atom population on specific atoms GAP
  do iatom = 1, natoms
    ibfst = imapbf(iatom); ibfnd = imapbf(iatom + 1) - 1
    gap = zero
    do ibf = ibfst, ibfnd
      gap = gap + gop(ibf) 
    end do
    ! calculate mulliken charge Q = Z - GAP
    qcharge = atoms(iatom)%charge - gap 
    write(istdout,'(1x,i4,2x,a3,f12.8)') iatom, atoms(iatom)%elem, qcharge
  end do

  deallocate(ovlp,pop,dens,gop,imapatom)
end subroutine
