#!/bin/bash 

#######################################################################
#
#  This is a handwritten script to setup the makefile system for
#  Snail. Maybe in the future one can switch to something more
#  elaborate using also autoconv and co. but for the moment this
#  is sufficient. Some parts of this script are inspired by the
#  configure script of DALTON.
#
#######################################################################


#######################################################################
# Helper functions
#######################################################################

get_yes_or_no() {
  answer=""
  while [ "$answer" != "y" -a "$answer" != "n" ]
  do
      if [ "$2" = "y" ]; then echo "$1 [Y/n] "
    elif [ "$2" = "n" ]; then echo "$1 [y/N] "
    else  echo "$1 [y/n] "; fi
    
    read answer
    if [ "$answer" = "" ]; then answer="$2"; fi
    # standardize answer
    case $answer in
      y* | Y*) answer="y" ;;
      n* | N*) answer="n" ;;
    esac
  done
}

#######################################################################
# main script
#######################################################################

# get arguments
argcheck=y
optstr=$@
while [ "$argcheck" = "y" ]; do
   if [ -n "$1" ]; then
      case $1 in
         "-h" | "-?" | "-help" | "--help") help="true"; break ;;
         "-lapack")   havelapack="true";   shift; lapackpath=$1 ;;
         "-compiler") havecompiler="true"; shift; compilerpath=$1 ;;
         "-enable-OMP") haveopenmp="true"; shift; openmp=$1 ;;
      esac
      shift
   else
      argcheck=n
   fi
done

# help
if [ "$help" == "true" ]; then
   #helpout
   echo "Help is comming soon..."
   exit 0
fi


# get CPU architecture
arch=$(uname -p)

# check which OS (linux, mac, cygwin)
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=linux;;
    Darwin*)    machine=mac;;
    CYGWIN*)    machine=cygwin;;
    MINGW*)     machine=minGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

echo "   We have a $arch architecture and run on $machine"

# generate unique name for OS and CPU architecture
archname=${machine}-${arch}

#
# Set compilers and libraries we are searching for depending on the OS
#
case "${machine}" in
  linux)   liblist='liblapack.a libmkl_intel_lp64.a'
           compf90list='ifort gfortran pgfortran flang';;
  mac)     liblist='liblapack.a libmkl_intel_lp64.a'
           compf90list='gfortran';;
  cygwin)  liblist='liblapack.a'
           compf90list='gfortran';;
  minGw)   liblist='liblapack.a'
           compf90list='gfortran';;
esac


#
# Test for the existence of F90 compiler
#
if [ "$havecompiler" == "true" ]; then
  echo "   Checking Fortran compiler you specified"
  
  comptype=`basename $compilerpath`

  accept='n'
  for comp in $compf90list; do
    if [ "$comp" == "$comptype" ]; then
      accept='y'
      break
    fi
  done 
  
  if [ "$accept" == "y" ]; then
    echo "   Compiler accepted!"
    F90=$comptype
    F90PATH=`dirname $compilerpath`
  else
    echo "   Sorry, I don't support $comptype" 
    exit 1
  fi
else

  echo "   Checking for Fortran compiler ..."
  echo "   from this list: $compf90list "
  for comp in $compf90list; do
  
    IFS="${IFS=   }"; save_ifs="$IFS"; IFS="${IFS}:"
    for compdir in $PATH; do
  
      test -z "$compdir" && compdir=.
  
      if [ -f $compdir/$comp ]; then
        get_yes_or_no "   $comp Compiler $compdir/$comp found, use this compiler?" y
        if [ $answer = 'y' ]; then
          prog_F90=$comp
          F90PATH=$compdir
          break
        fi
      fi
    done
    IFS="$save_ifs"
    test -z "$prog_F90" && prog_F90=""
    F90="$prog_F90"
    if test -n "$F90"; then
      echo "   Compiler $F90 found and accepted."
      break
    fi
  done
  
  if test -z "$F90"; then
    echo "   No Fortran compiler found on this computer"
    exit 1
  fi

fi

#
# Test for LAPACK and BLAS
#
if [ "$havelapack" == "true" ]; then
  echo "   Checking LAPACK libraries you specified"
  
  lapacklib=`basename $lapackpath`

  accept='n'
  for testlib in $liblist; do
    if [ "$testlib" == "$lapacklib" ]; then
      accept='y'
      break
    fi
  done 
  
  testdir=`dirname $lapackpath`

  use_mkl='no'
  if [ "$accept" == "y" ]; then
    echo "   Library accepted!"
    
    case $lapacklib in
      libmkl_intel_lp64.a) LIB="-L$testdir -lmkl_intel_lp64 -lmkl_sequential -lmkl_core";use_mkl='yes';;
      liblapack.a)         LIB="-L$testdir/$lapacklib -llapack -lblas";use_mkl='no';;
    esac
  else
    echo "   Sorry, I don't support $lapacklib" 
    exit 1
  fi
else

  dirlist="/lib /usr/local/lib /usr/lib64 /usr/lib"
  
  # might MKL be available?
  if [ $machine = "linux" ]; then
    if  which ifort 1>/dev/null 2>&1; then
      mkllib=`locate libmkl_intel_lp64.a` 
      if [ "$mkllib" != "" ]; then
        mkllib=`dirname $mkllib`
      fi
      dirlist="$mkllib $dirlist"
      liblist="libmkl_intel_lp64.a $liblist"
    fi
  fi
  
  LIB=''
  use_mkl='no'
  for testdir in $dirlist; do
    if [ -n "$liblist" ]; then
      for testlib in $liblist; do
        echo "   testing existence of $testdir/$testlib"
        if test -f $testdir/$testlib; then
          case $testlib in
            libmkl_intel_lp64.a) LIB="-L$testdir -lmkl_intel_lp64 -lmkl_sequential -lmkl_core";use_mkl='yes';;
            liblapack.a)         LIB="-L$testdir/$testlib -llapack -lblas";use_mkl='no';;
          esac
          
          get_yes_or_no "   $testlib in $testdir found, use it?" y
          if [ $answer = 'y' ]; then
            break
          else
            LIB=''
          fi
        fi
      done
      test -n "$LIB" && break
    fi
  done
  
  if test -z "$LIB"; then
    echo "   No LAPACK and BLAS libraries found on this computer"
    exit 1
  fi
fi


#
# Should we compile with OpenMP support?
#
if [ "$haveopenmp" == "true" ]; then
  omp=$openmp
else
  get_yes_or_no "   Enable OpenMP?" y
  omp="no"
  if [ $answer = 'y' ]; then
    omp="yes"
  fi
fi

echo "   -> adapt make file and directories"
echo ""

# generate config for Makefiles
echo "#"                                                    > config/arch.mk 
echo "#  Note this file is autogenerated by configure.sh"  >> config/arch.mk
echo "#"                                                   >> config/arch.mk 
echo ""

echo "##########################################################" >> config/arch.mk 
echo "# give specific name for object file directories          " >> config/arch.mk
echo "##########################################################" >> config/arch.mk
echo "                                                          " >> config/arch.mk                                            
echo 'ifeq ($(mode),debug)'                                       >> config/arch.mk
echo "  OBJDIR=$archname/debug"                                   >> config/arch.mk
echo "else"                                                       >> config/arch.mk
echo "  OBJDIR=$archname"                                         >> config/arch.mk
echo "endif"                                                      >> config/arch.mk
echo "                                                          " >> config/arch.mk
echo "##########################################################" >> config/arch.mk
echo "# specify here the compiler and linker we want to use     " >> config/arch.mk
echo "##########################################################" >> config/arch.mk 
echo "                                                          " >> config/arch.mk
echo "FC := $F90"                                                 >> config/arch.mk
echo "LD := $F90"                                                 >> config/arch.mk
echo ""                                                           >> config/arch.mk
echo "FPATH := $F90PATH"                                          >> config/arch.mk
echo ""                                                           >> config/arch.mk
echo "use_omp := $omp"                                            >> config/arch.mk
echo "use_mkl := $use_mkl"                                        >> config/arch.mk
echo ""                                                           >> config/arch.mk
echo "##########################################################" >> config/arch.mk
echo "# specify info on math libraries                          " >> config/arch.mk
echo "##########################################################" >> config/arch.mk 
echo "                                                          " >> config/arch.mk
echo "LAPACK = $LIB"                                              >> config/arch.mk
echo 'LDLIBS = $(LAPACK)'                                         >> config/arch.mk

#
# go through every source directory and generate directory for object files
#

directories=("viblib" "cilib"  "coordlib"  "intlib" "iolib" "main"  "proplib"  "rilib"  "scflib"  "stringlib"  "utilib"  "utilmod")

for((i=0;i<${#directories[*]};i++)); do
  dir=${directories[$i]}

  echo "     --> I create $dir/$archname"

  cd $dir

  if [ -d $archname ]; then
    rm -rf $archname/*
  else
    mkdir $archname
  fi

  if [ ! -d $archname/debug ]; then
    mkdir $archname/debug
  fi

  cd ..

done

echo " configure : done "
exit 0
