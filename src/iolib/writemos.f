subroutine writemos(isuhf,ispin,dens,eigval,nbf,nmo)
!-----------------------------------------------------------------+
!  Purpose:
!  --------
!
!  Writes MO coefficients in a human and machine readable form
!
!-----------------------------------------------------------------+

  use mod_fileutil

  implicit none

  include 'fortrankinds.h'

  ! constants
  character(len=80), parameter :: falpha = 'alpha'
  character(len=80), parameter :: fbeta  = 'beta'
  character(len=80), parameter :: fmos   = 'mos'

  ! dimensions
  integer, intent(in) :: nbf,nmo

  ! input
  real(kdp), intent(in) :: eigval(nmo), dens(nmo,nmo)
  integer,   intent(in) :: ispin
  logical,   intent(in) :: isuhf

  ! local
  integer :: imo,iumos,iao

  if (isuhf) then
    if (ispin == 1) then
      iumos = openfile(falpha,'unknown')
    elseif (ispin == 2) then
      iumos = openfile(fbeta,'unknown')
    else
      call quit('strange ispin','writemos')
    end if
  else
    iumos = openfile(fmos,'unknown')
  end if

  write(iumos,*) '$mos', nmo
  do imo = 1, nmo
    write(iumos,'(i6,a3,6x,a11,f20.14,3x,a6,i6)') &
          imo, 'a', 'eigenvalue=',  eigval(imo),'nbf=',nbf
    write(iumos,'(4f20.14)') (dens(iao,imo),iao=1,nbf)
  end do
  write(iumos,'(a4)') '$end'

  call closefile(iumos)

end subroutine
