module type_ciscalcdef


 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  type cis_calcdef

    logical   :: use_fulldiag = .false.
    logical   :: use_TDHF     = .false.
 
    integer   :: nroots = 5
 
    real(kdp) :: tolres  = 1.d-5 ! like Turbomole default
    real(kdp) :: toleig  = 1.d-5
    integer   :: maxiter = 100

  end type

  public :: cis_calcdef

!------------------------------------------------------------------------------!
!  contains
!------------------------------------------------------------------------------!

end module
