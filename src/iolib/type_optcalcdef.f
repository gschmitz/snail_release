module type_optcalcdef


 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  type opt_calcdef

    real(kdp) :: alpha    = .5d0  

    integer :: maxiter = 30
   
    integer :: iopt = 1 ! 1 = steepest descent
                        ! 2 = Adam optimizer    
 
    ! Adam optimizer
    real(kdp) :: beta1 = 0.9d0
    real(kdp) :: beta2 = 0.999d0
    real(kdp) :: eta = 0.001d0
 
  end type

  public :: opt_calcdef

!------------------------------------------------------------------------------!
!  contains
!------------------------------------------------------------------------------!

end module
