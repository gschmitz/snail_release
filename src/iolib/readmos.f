subroutine readmos(dens,eigval,fil,nbf,nmo)
!-----------------------------------------------------------------+
!  Purpose:
!  --------
!
!  read MO coefficients, which were written by writemos 
!
!-----------------------------------------------------------------+

  use mod_fileutil

  implicit none

  include 'fortrankinds.h'

  ! dimensions
  integer, intent(in) :: nbf,nmo

  ! output
  real(kdp), intent(out) :: eigval(nmo), dens(nmo,nmo)

  ! input
  character(len=80), intent(in) :: fil

  ! local
  integer :: imo,iumos,iao,jmo,nbas

  character(len=80) :: cline
  character(len=3)  :: c3dum
  character(len=6)  :: c6dum
  character(len=11) :: c11dum

  iumos = openfile(fil,'old')

  read(iumos,'(a)') cline

  do imo = 1, nmo
    ! read Fock matrix eigenvalues
    read(iumos,'(i6,a3,6x,a11,f20.14,3x,a6,i6)') &
          jmo, c3dum, c11dum,  eigval(imo),c6dum, nbas

    ! sanity check
    if (imo /= imo) then
      write(6,*) 'MO indeces do not match'
      stop
    end if

    if (nbas /= nbf) then
      write(6,*) 'No. of basis functions does not match'
      stop
    end if

    ! read MO coefficients
    read(iumos,'(4f20.14)') (dens(iao,imo),iao=1,nbf)
  end do

  call closefile(iumos)

end subroutine
