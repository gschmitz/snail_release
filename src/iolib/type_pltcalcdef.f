module type_pltcalcdef


 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  type plt_calcdef

    logical   :: plot_density = .false.
    logical   :: plot_mos     = .false.
 
    character(len=80) :: filcube = "density.cube"

    integer :: imos(256)  ! Will make this later dynamically growing
    integer :: nmos = 0  

  end type

  public :: plt_calcdef

!------------------------------------------------------------------------------!
!  contains
!------------------------------------------------------------------------------!

end module
