module type_string


 implicit none

  private

  include 'fortrankinds.h'

  type string 
    integer :: nlen = 0
    integer :: nmem = 0 
    character, pointer :: str(:)
  end type


  public :: string

!------------------------------------------------------------------------------!
  contains
!------------------------------------------------------------------------------!


end module
