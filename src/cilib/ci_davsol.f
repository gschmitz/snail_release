!==================================================================================================================================!
subroutine ci_davsol(iexc,tolres,toleig,nroots,maxiter, &
                     nonHermitian,lRPA,                 &
                     escf, tolint, lmax, twoeint,       &
                     basis, memmax, filmo,              &
                     nocc, nvir, nbf, mxprim,           &
                     mxcart, maxred, nfdim, nexc)
!----------------------------------------------------------------------------------------------------------------------------------!
!
!  Purpose: Diagonalization with the davidson method
!
!----------------------------------------------------------------------------------------------------------------------------------!

  use type_cgto
  use type_shell
  use type_int4idx
  use type_basis

  implicit none
  
  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'
  
  ! constants
  logical, parameter     :: locdbg  = .false., locprecond = .true.
  
  real(kdp), parameter   :: thrldp  = 1.0d-4, threps  = 1.0d-11
  
  ! dimensions
  integer, intent(in) :: nocc, nvir, nbf, mxprim, mxcart
  integer, intent(in) :: nfdim, maxred, nexc 

  ! input/output
  type(int4idx) ,    intent(inout) :: twoeint
  
  ! input
  type(basis_info),  intent(in) :: basis
  integer,           intent(in) :: nroots, maxiter
  integer,           intent(in) :: iexc(nexc,2), lmax
  real(kdp),         intent(in) :: tolres, toleig, tolint, escf
  integer(kli),      intent(in) :: memmax
  character(len=80), intent(in) :: filmo
  logical,           intent(in) :: nonHermitian, lRPA    ! Used in a redundant way, but maybe later usefull to distinguish 

  ! local
  integer   :: nstartv, ndvec, iter, ncycles, ncurvec, ivec, jvec, imin, iadr, ierr, lwork, &
               ilow, iadrlow, iadrhgh, jstart, ilen, mxiter
  logical   :: converged,lskipped
  real(kdp) :: dtemp,dmin,dlen,dummy
  
  real(kdp), allocatable :: resnrm(:), oldeig(:), diffeig(:), eig(:), diag(:), minvals(:), work(:), civecr(:), imag(:)
  real(kdp), allocatable :: redij(:,:),obasis(:,:),sigma(:,:),civecf(:,:),res(:,:),tempmat(:)
  real(kdp), allocatable :: orb(:,:),orben(:),kmnrs(:,:,:,:), scratch1(:), scratch2(:)
  logical,   allocatable :: lconv(:)
  integer,   allocatable :: minidx(:)
  
  ! functions
  real(kdp) ddot, dnrm2
  
  !-------------------------------------------------------------------------------------------------------------------------------!
  !   allocate memory for important arrays
  !-------------------------------------------------------------------------------------------------------------------------------!
  
  allocate(redij(nfdim,maxred),  &
           obasis(nfdim,maxred), &
           sigma(nfdim,maxred),  &
           civecf(nfdim,maxred), &
           res(nfdim,maxred))
  
  allocate(diag(nexc))
  
  allocate(civecr(maxred*maxred))
  
  allocate(resnrm(maxred),eig(nfdim),oldeig(nfdim),diffeig(nfdim),lconv(nfdim))
  
  oldeig(:) = zero
  eig(:)    = zero
  lconv(:)  = .false.
  
  res(:,:)   = zero
  redij(:,:) = zero
  sigma(:,:) = zero

  allocate(kmnrs(nbf,nbf,nbf,nbf))

  allocate(scratch1(nbf*nbf*nbf*nbf),&
           scratch2(nbf*nbf*nbf*nbf))

  !-----------------------------------------------------------------------+
  !  read orbitals and their energies
  !-----------------------------------------------------------------------+
  allocate(orben(nbf),orb(nbf,nbf))
  call readmos(orb,orben,filmo,nbf,nbf)
  
  !-------------------------------------------------------------------------------------------------------------------------------!
  !    generate startvectors 
  !-------------------------------------------------------------------------------------------------------------------------------!
  
  nstartv = min(nroots * 2, maxred)
  
  !-----------------------------------------------------------*
  ! Get the diagonal elements for preconditioning 
  !-----------------------------------------------------------*
  obasis(:,1) = one
  call ci_ampvec(diag, obasis, iexc, .true., lRPA, escf,      &
                 basis%shells, tolint, lmax, orb, orben,      &
                 kmnrs, scratch1, scratch2,                   &
                 .false., twoeint, basis,                     &
                 memmax, filmo, nocc, nvir, nexc, nbf,        &
                 basis%nshell, 1, mxprim, mxcart)
  
  !-----------------------------------------------------------*
  ! Find nstartv lowest elements of diagonal
  ! -> O(Nex * nroots) step 
  !-----------------------------------------------------------*
  allocate(minvals(nstartv),minidx(nstartv))
  dmin = 99d6
  do ivec = 1, nexc
    if (diag(ivec) < dmin) then
      dmin = diag(ivec)
      imin = ivec
    end if
  end do
  
  minvals(1) = dmin
  minidx(1)  = imin
  do ivec = 2, nstartv
    dmin = 99d6
    do jvec = 1, nexc
      if (diag(jvec) < dmin .and. diag(jvec) > minvals(ivec-1) ) then
        dmin = diag(jvec)
        imin = jvec
      end if
    end do
    minvals(ivec) = dmin
    minidx(ivec)  = imin
  end do
  
  ! create nstartv start vectors as unit vectors with elements 
  obasis(:,:) = zero
  do ivec = 1, nstartv
    imin = minidx(ivec)
    obasis(imin,ivec) = one
  end do
  
  if (locdbg) then
    write(istdout,'(/5x,a)') "Start vectors:"
    !call matprt(obasis,nfdim,nstartv)
  end if
  
  ! get memory for work
  lwork = -1
  if (nonHermitian) then
    allocate(imag(maxred))            ! For imaginary part of eigenvalues
    allocate(tempmat(maxred*maxred))  ! We need an extra copy of the matrix :-(

    call dgeev('n','v',maxred,tempmat,maxred,eig,imag,dummy,maxred,civecr,maxred,dlen,lwork,ierr) 
  else
    call dsyev('v','u',maxred,civecr,maxred,eig,dlen,lwork,ierr)
  end if

  lwork = int(dlen)
  allocate(work(lwork))
  
  !-------------------------------------------------------------------------------------------------------------------------------!
  !     davidson algorithm
  !-------------------------------------------------------------------------------------------------------------------------------!
  mxiter = maxiter 
  
  ncurvec = 0       ! Which is the current vector we finished working on?
  ndvec   = nstartv ! How many vectors do we have at the moment?
  
  ncycles = 0
  
  do iter = 1, mxiter
    if (locdbg) write(istdout,'(/5x,a,i4)') "Iteration:", iter
  
    ncycles = ncycles + 1
  
    !----------------------------------------------------------------------+
    ! calculate sigma vectors: sigma_j = H * obasis_j
    !----------------------------------------------------------------------+
    ivec = ncurvec + 1
    if (ivec.gt.maxred .or. ndvec.gt.maxred) then 
      write(istdout,'(/5x,a,3i10)') 'ivec, ndvec, maxred', ivec,ndvec, maxred
      call quit('reduced space too small!','ci_davsol')
    end if
  
    ivec = ncurvec + 1
    ilen = ndvec - ivec + 1
    !do ivec = ncurvec + 1, ndvec
      call ci_ampvec(sigma(1,ivec), obasis(1,ivec), iexc, .false., lRPA,  &
                     escf, basis%shells, tolint, lmax, orb, orben,        &
                     kmnrs, scratch1, scratch2,                           &
                     .true., twoeint, basis, memmax, filmo,               &
                     nocc, nvir, nexc, nbf, basis%nshell, ilen, mxprim, mxcart)
    !end do
    
    !---------------------------------------------------------------------+
    !  construct matrix in reduced sub-space
    !   => ~H_ij = obasis^(i) * sigma^(j)
    !---------------------------------------------------------------------+
    jstart = ncurvec + 1            ! For a symmetric matrix we don't need the full matrix 
    if (nonHermitian) jstart = 1    ! since we only need to reference on triangle

    do jvec = jstart, ndvec
      do ivec = 1, ndvec
        redij(ivec,jvec) = ddot(nfdim, obasis(1,ivec), 1, sigma(1,jvec), 1)
      enddo
    enddo
  
    !---------------------------------------------------------------------+
    ! Solve eigenvalue problem
    !---------------------------------------------------------------------+
 
    ierr = 0
    if (nonHermitian) then

      ! Copy matrix to be destroyed in degeev 
      do ivec = 1, ndvec
        iadr = (ivec - 1) * ndvec + 1
        call dcopy(ndvec,redij(1,ivec),1,tempmat(iadr),1)
      end do

      call dgeev('N','V',ndvec,tempmat,ndvec,eig,imag,civecr,ndvec,civecr,ndvec,work,lwork,ierr)

      ! Sort eigenvalues like it is done in dsyev:
      do ivec = 1, ndvec - 1
        ilow = ivec
        do jvec = ivec + 1, ndvec
          if (eig(jvec) < eig(ilow)) ilow = jvec
        end do
        if (ilow.ne.ivec) then
          ! swap real eigenvalues
          call dswap(1,eig(ilow),1,eig(ivec),1) 
      
          ! swap imaginary eigenvalues
          call dswap(1,imag(ilow),1,imag(ivec),1) 
           
          ! swap eigenvectors
          iadrhgh = (ivec - 1) * ndvec + 1
          iadrlow = (ilow - 1) * ndvec + 1
          call dswap(ndvec,civecr(iadrlow),1,civecr(iadrhgh),1)
        end if
      end do
 
    else
      ! Copy matrix in reduced space to civecr, which will be diagonalized
      do ivec = 1, ndvec
        iadr = (ivec - 1) * ndvec + 1
        call dcopy(ndvec,redij(1,ivec),1,civecr(iadr),1)
      end do

      call dsyev('v','u',ndvec,civecr,ndvec,eig,work,lwork,ierr)
    end if

    ! eigenvectors are now in civecr
    if (ierr.ne.0) then
      write(istdout,*) 'Error in dsyev: ierr=',ierr
      call quit('Error in dsyev!', 'ci_davsol')
    end if
  
    !---------------------------------------------------------------------+
    !  construct approx eigenvectors in full space and residual vectors 
    !---------------------------------------------------------------------+
  
    ! approximate vector in full space  
    call dgemm('n','n',nfdim,ndvec,ndvec,one,obasis,nfdim,civecr,ndvec,zero,civecf,nfdim)
   
    ! construct residual vectors:  r^k = (H - \lambda^k) c = H c - \lambda^k c
    !
    ! 1) r^k = H c
    call dgemm('n','n',nfdim,ndvec,ndvec,one,sigma,nfdim,civecr,ndvec,zero,res,nfdim)
    !
    ! 2) r^k += - \lambda^k c 
    do ivec = 1, ndvec
      call daxpy(nfdim,-eig(ivec),civecf(1,ivec),1,res(1,ivec),1)
      resnrm(ivec) = dnrm2(nfdim, res(1,ivec), 1)
    enddo
  
    ncurvec   = ndvec
    converged = .true.
  
    !---------------------------------------------------------------------+
    ! Test if converged
    !---------------------------------------------------------------------+
    do ivec = 1, nroots 
  
      ! calculate eigenvalue differences to last iter.
      diffeig(ivec) = abs(oldeig(ivec) - eig(ivec))
  
      if (resnrm(ivec).lt.tiny(res(1,1))) then
        ! if residual is smaller than the smallest representable
        ! number the vector is converged irrespectable of the
        ! difference of eigenvalues to the last iteration. 
        ! (Needed to avoid NaNs for very tight thresholds, when
        !  one divides below through resnrm(ivec) )
        lconv(ivec) = .true.
      else
        lconv(ivec) = (resnrm(ivec).lt.tolres) .and. (diffeig(ivec).lt.toleig)
      end if
  
      if (.not.lconv(ivec)) then
        converged = .false.
      end if
  
      if (locdbg) write(6,*) "||R||", resnrm(ivec), diffeig(ivec), ivec, lconv(ivec)
  
    end do
  
    ! if full dimension is reached algorithm is converged
    ! (without saving anything)
    if (ndvec == nfdim) converged = .true.
    if (ndvec > nfdim) call quit('ndvec > nfdim', 'ci_davsol')
    
    if (converged) then
      write(istdout,'(5x,a,i4)') 'cycles for convergence:', ncycles
      exit
    end if
  
    !---------------------------------------------------------------------+
    ! Add new trial vectors
    !---------------------------------------------------------------------+
    do ivec = 1, nroots 
     if (.not.lconv(ivec)) then
  
       if (ndvec.ge.maxred) exit
  
       ndvec = ndvec + 1
       mxiter = mxiter - 1
  
       ! create new vector from res(1,ivec)
       call dcopy(nfdim, res(1,ivec), 1, obasis(1,ndvec), 1)
  
       if (locprecond) then
         ! use preconditioner
         do jvec = 1, nfdim
           res(jvec,ivec) = res(jvec,ivec) / ( eig(ivec) - diag(ivec) ) 
         enddo
         dtemp = dnrm2(nfdim, res(1,ivec), 1)
       else
         dtemp = resnrm(ivec)
       end if
  
       ! normalize vector
       call dscal(nfdim, one / dtemp, obasis(1,ndvec), 1)
  
       ! use Gram Schmidt orthogonalization
       call gsortho(obasis, ndvec, thrldp, lskipped, nfdim, maxred)
  
       ! linear dependent vector skipped? 
       if (lskipped) then
         ndvec = ndvec - 1
         mxiter = mxiter + 1
       end if
  
     end if
    end do
  
    if (locdbg) write(istdout,'(/5x,a,i4)') 'dim of new basis:', ndvec
  
    !---------------------------------------------------------------------+
    ! Preparations for next cycle
    !---------------------------------------------------------------------+
  
    ! save eigenvalues for comparison with next iteration
    do ivec = 1, nroots 
      oldeig(ivec) = eig(ivec)
    end do
  
  enddo
  !-------------------------------------------------------------------------------------------------------------------------------!
  ! end davidson main loop
  !-------------------------------------------------------------------------------------------------------------------------------!
  
  if (converged) then 
    write(istdout,'(/5x,a)') 'Success: davidson method converged'
    write(istdout,'(5x,a,i4)')  'Size of reduced space:', ndvec
  else
    write(istdout,'(/5x,a)') 'Error: davidson method not converged'
  end if
  
  do ivec = 1, nroots
    if (lRPA) then
      write(istdout,'(/,3x,a,i5,3x,a,f16.12)') 'State:', ivec, 'energy',  dsqrt(eig(ivec))
    else
      write(istdout,'(/,3x,a,i5,3x,a,f16.12)') 'State:', ivec, 'energy',  eig(ivec)
    end if
    write(istdout,'(3x,a)') '----------------------------------------------'
    ! assign excitations to this state
    do jvec = 1, nexc
      if (abs(civecf(jvec,ivec)) .gt. 0.05) then
        write(istdout,'(5x,i4,2x,a,i4,3x,a,f16.12))') iexc(jvec,1), "->", iexc(jvec,2), 'c =', civecf(jvec,ivec)
      end if
    end do 
  end do

  deallocate(orb,orben)

end subroutine ci_davsol
!==================================================================================================================================!
