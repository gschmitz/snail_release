subroutine ci_getcismatrix(amat, doTDHF, escf, shells, tolint, lmax, &
                           have_eri, twoeint, basis, memmax, filmo,  &
                           nocc, nvir, nbf, nshell, mxprim, mxcart)

  use type_cgto
  use type_shell
  use type_int4idx
  use type_basis
  use type_integ
  use IntegralBuffer

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! constants
  logical, parameter :: use_contractor = .true.
  integer, parameter :: ndim = 4
  integer, parameter :: iprint = 1
  logical, parameter :: silent = .false. 

  ! dimensions
  integer, intent(in) :: nocc, nvir, nbf, nshell, mxprim, mxcart

  ! output
  real(kdp), intent(out) :: amat(nocc*nvir,nocc*nvir)
  
  ! input/output
  type(int4idx) ,    intent(inout) :: twoeint

  ! input
  logical,   intent(in) :: doTDHF
  real(kdp), intent(in) :: escf 
  type(shell_info),  intent(in) :: shells(nshell)
  type(basis_info),  intent(in) :: basis
  character(len=80), intent(in) :: filmo
  real(kdp),         intent(in) :: tolint
  logical,           intent(in) :: have_eri
  integer(kli),      intent(in) :: memmax
  integer,           intent(in) :: lmax

  ! local
  integer :: nexc, a, b, i, j, ia, jb, idm, imo, jmo, moa, mob, &
             ibf, jbf, lbf, kbf, nmat, ndata, ind, ncbf
  integer      :: nscreen
  integer(kli) :: memused,memavail
  integer, allocatable :: iexc(:,:)

  real(kdp)    :: cpu2ein,cpu0,cpux

  integer, allocatable :: idims_in(:),idims_out(:)

  real(kdp), allocatable :: kmnrs(:,:,:,:)
  real(kdp), allocatable :: scratch1(:),scratch2(:)
  real(kdp), allocatable :: orb(:,:),orben(:)
  real(kdp), allocatable :: bmat(:,:),abadd(:,:),absub(:,:)
  real(kdp), allocatable :: gab(:,:)

  type(TIntBuffer) :: intbuf
  type(integ_info) :: intopt

 
  real(kdp) :: dummy


  ! init
  ncbf = basis%ncbf

  nexc = nocc*nvir

  ! Get list of Single Exciations
  allocate(iexc(nexc,2))
  call ci_getsingexcspace(iexc, nocc, nvir, nexc)

  !-----------------------------------------------------------------------+
  ! allocate memory
  !-----------------------------------------------------------------------+
  allocate(kmnrs(nbf,nbf,nbf,nbf))

  allocate(scratch1(nbf*nbf*nbf*nbf),&
           scratch2(nbf*nbf*nbf*nbf))

  allocate(orb(nbf,nbf),orben(nbf))

  !-----------------------------------------------------------------------+
  !  read orbitals and their energies
  !-----------------------------------------------------------------------+
  call readmos(orb,orben,filmo,nbf,nbf)

  !-----------------------------------------------------------------------+
  !  calculate ERIs or reuse them
  !-----------------------------------------------------------------------+

  if (.not.have_eri) then

    ! init Buffer to calculate two electron integrals
    call allocate_intbuf(intbuf,intopt,lmax,mxcart,mxprim,silent)

    write(istdout,'(5x,a)')  'Start calculation of AO-Integrals...'
    call cpu_time(cpu0)
    memavail = memmax - memused
    allocate(gab(nshell,nshell))
    call gettwoeints(twoeint,intbuf,dummy,.false.,1,.true.,.false.,gab,.false.,dummy,tolint,&
                     nscreen,memavail,lmax,shells,iprint,silent,ncbf,nbf,nshell,mxprim,mxcart)
    deallocate(gab)
    call cpu_time(cpux)
    cpu2ein = cpux - cpu0

    write(istdout,'(5x,a,f10.2,/)')  'CPU-time for 2-el Ints (sec):', cpu2ein
  end if

  ! unpack AO-Integrals 
  do lbf = 1, nbf
    do jbf = 1, nbf
      do kbf = 1, nbf
        do ibf = 1, nbf
          kmnrs(ibf,jbf,kbf,lbf) = int4idx_getvalue(twoeint,ibf,jbf,kbf,lbf)
        end do 
      end do
    end do 
  end do

  !-----------------------------------------------------------------------+
  ! use inefficient way to transform AO-integrals to MO-integrals....
  !-----------------------------------------------------------------------+

  if (use_contractor) then
   
    allocate(idims_in(ndim),idims_out(ndim))

    do idm = 1, ndim
      idims_in(idm) = nbf
    end do

    ndata = nbf*nbf*nbf*nbf

    ! 1.) Transform first index
    call ContractForward(scratch1, idims_out, kmnrs,    orb, 4, idims_in, ndim, nbf, nbf, ndata, ndata)

    ! 2.) Transform second index
    call ContractForward(scratch2, idims_out, scratch1, orb, 3 ,idims_in, ndim, nbf, nbf, ndata, ndata)

    ! 3.) Transform third index
    call ContractForward(scratch1, idims_out, scratch2, orb, 2, idims_in, ndim, nbf, nbf, ndata, ndata)

    ! 4.) Transform fourth index
    call ContractForward(kmnrs,    idims_out, scratch1, orb, 1, idims_in, ndim, nbf, nbf, ndata, ndata)

    deallocate(idims_in, idims_out)
  else

    call quit("Not implemented","CI-Mat")
  end if

  !----------------------------------------------------------------------+
  ! Build CIS-Matrix
  !----------------------------------------------------------------------+
  amat = zero
  do jb = 1, nexc
    j = iexc(jb, 1)
    b = iexc(jb, 2)
    do ia = 1, nexc
      i = iexc(ia, 1)
      a = iexc(ia, 2)

      amat(ia,jb) = two * kmnrs(a,i,j,b) - kmnrs(a,b,j,i)

      if ((a == b) .and. (i == j)) then
        amat(ia,jb) = amat(ia,jb)  + orben(a) - orben(i)
      end if

    end do
  end do

  ! Do TDHF/RPA if requested (although it does not really fit in this lib...)
  if (doTDHF) then
    allocate(bmat(nexc,nexc))
    allocate(absub(nexc,nexc))
    allocate(abadd(nexc,nexc))

    bmat(:,:) = zero
    do jb = 1, nexc
      j = iexc(jb, 1)
      b = iexc(jb, 2)
      do ia = 1, nexc
        i = iexc(ia, 1)
        a = iexc(ia, 2)

        bmat(ia,jb) = two * kmnrs(a,i,b,j) - kmnrs(a,j,b,i) 
        !bmat(ia,jb) = two * kmnrs(b,j,a,i) - kmnrs(b,i,a,j) 

      end do
    end do

    ! MAT = (A + B) x (A - B)
    call dcopy(nexc*nexc,amat,1,abadd,1)
    call daxpy(nexc*nexc,one,bmat,1,abadd,1)    

    call dcopy(nexc*nexc,amat,1,absub,1)
    call daxpy(nexc*nexc,-one,bmat,1,absub,1)    

    call dgemm('n','n',nexc,nexc,nexc,one,abadd,nexc,absub,nexc,zero,amat,nexc)

    deallocate(bmat,abadd,absub)
  end if

  deallocate(iexc)
  deallocate(kmnrs,scratch1,scratch2)
  deallocate(orb,orben)
  if (.not.have_eri) call deallocate_intbuf(intbuf)
end subroutine
