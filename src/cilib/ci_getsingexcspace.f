
!-----------------------------------------------------------------------------+
! Purpose: Generate list with Singles excitation space
!-----------------------------------------------------------------------------+
subroutine ci_getsingexcspace(iexc,nocc,nvirt,nexc)

  implicit none

  include 'fortrankinds.h'

  ! dimensions
  integer, intent(in) :: nocc, nvirt, nexc

  ! output
  integer, intent(out) :: iexc(nexc, 2)

  ! local
  integer :: iocc, moa, idx

  idx = 1
  do iocc = 1, nocc
    do moa = 1, nvirt
      iexc(idx, 1) = iocc
      iexc(idx, 2) = moa + nocc
      idx = idx + 1
    end do
  end do

end subroutine
